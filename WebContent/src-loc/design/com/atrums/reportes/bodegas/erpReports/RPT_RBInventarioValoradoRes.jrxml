<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="RPT_RBInventarioValorado" pageWidth="820" pageHeight="28842" columnWidth="780" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.2100000000000066"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="BASE_WEB" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["http://localhost:8008/openbravoaf/web"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_ORG" class="java.lang.String"/>
	<parameter name="DATE_FROM" class="java.util.Date"/>
	<parameter name="DATE_TO" class="java.util.Date"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="PRODUCTO" class="java.lang.String"/>
	<parameter name="AUX_PRODUCTO" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{PRODUCTO}==null || $P{PRODUCTO}.equals("")) ? "" : "and t.m_product_id = '" + $P{PRODUCTO} + "'"]]></defaultValueExpression>
	</parameter>
	<parameter name="CATEGORIA" class="java.lang.String"/>
	<parameter name="AUX_CATEGORIA" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{CATEGORIA}==null || $P{CATEGORIA}.equals( "" )) ? "" : "and t.m_product_id IN (select m_product_id from m_product where m_product_category_id = '" + $P{CATEGORIA}+ "')"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[select
a.m_transaction_id,
coalesce(a.Sucursal,'') as Sucursal,
coalesce(a.categoria,'') as categoria,
a.movementdate,
a.movementtype,
coalesce(a.producto,'') as producto,
coalesce(a.entrada,0) as entrada,
coalesce(a.salida,0) as salida,
coalesce(a.unitcost,0) as unitcost,
coalesce(a.cost,0) as cost,
coalesce((sum(amtsourcedr)-sum(amtsourcecr)),0) as saldo,
'C32139D1C76941788E92B8D13F5174C2' AS org_id
from (select o.name as Sucursal,
(select name from m_product_category where m_product_category_id IN (select m_product_category_id from m_product where m_product_id = t.m_product_id)) as categoria,
t.movementdate,
case when t.movementtype='C-' then 'Despacho' when t.movementtype='V+' then 'Ingreso' when t.movementtype='I+' then 'Inventario +' when t.movementtype='I-' then 'Inventario -' end as movementtype,
p.name as producto,
case when t.movementqty > 0 then t.movementqty else 0 end as entrada,
case when t.movementqty < 0 then t.movementqty else 0 end as salida,
(select coalesce(cost,0) from m_costing where m_product_id = t.m_product_id and datefrom <= t.movementdate and dateto >= t.movementdate order by dateto desc limit 1) as unitcost,
(select coalesce(cost,0) from m_costing where m_product_id = t.m_product_id and datefrom <= t.movementdate and dateto >= t.movementdate order by dateto desc limit 1) * t.movementqty as cost,
p.m_product_id,
t.m_transaction_id
from m_transaction t
     join ad_org o on (t.ad_org_id = o.ad_org_id)
     left join m_product p on (t.m_product_id = p.m_product_id)
where t.ad_client_id IN ($P!{USER_CLIENT})
and
t.movementtype in ('V+','C-', 'I+', 'I-')
and date(t.movementdate) >= $P{DATE_FROM}
and date(t.movementdate) <= $P{DATE_TO}
$P!{AUX_PRODUCTO}
$P!{AUX_CATEGORIA}
order by 3, 6, 4) a
left join fact_acct fa on (fa.m_product_id = a.m_product_id and acctvalue =
        (select ev.value from m_product_acct pa
                              inner join c_validcombination v on (pa.p_asset_acct = v.c_validcombination_id)
                              inner join c_elementvalue ev on (v.account_id = ev.c_elementvalue_id)
                         where pa.m_product_id = a.m_product_id
                         and pa.c_acctschema_id IN (select c_acctschema_id from c_acctschema where isactive = 'Y'))
        and date(fa.dateacct) < $P{DATE_FROM})
group by 1,2,3,4,5,6,7,8,9,10]]>
	</queryString>
	<field name="sucursal" class="java.lang.String"/>
	<field name="categoria" class="java.lang.String"/>
	<field name="movementdate" class="java.sql.Timestamp"/>
	<field name="movementtype" class="java.lang.String"/>
	<field name="producto" class="java.lang.String"/>
	<field name="entrada" class="java.math.BigDecimal"/>
	<field name="salida" class="java.math.BigDecimal"/>
	<field name="unitcost" class="java.math.BigDecimal"/>
	<field name="cost" class="java.math.BigDecimal"/>
	<field name="saldo" class="java.math.BigDecimal"/>
	<field name="org_id" class="java.lang.String"/>
	<variable name="cost_sum" class="java.math.BigDecimal" resetType="Group" resetGroup="producto" calculation="Sum">
		<variableExpression><![CDATA[$F{cost}]]></variableExpression>
	</variable>
	<variable name="Total_Prod" class="java.math.BigDecimal" resetType="Group" resetGroup="producto">
		<variableExpression><![CDATA[$F{saldo}.add( $V{cost_sum} )]]></variableExpression>
		<initialValueExpression><![CDATA[]]></initialValueExpression>
	</variable>
	<variable name="Total_Cat" class="java.math.BigDecimal" resetType="Group" resetGroup="categoria" incrementType="Group" incrementGroup="producto" calculation="Sum">
		<variableExpression><![CDATA[$V{Total_Prod}]]></variableExpression>
	</variable>
	<variable name="Total_Gen" class="java.math.BigDecimal" incrementType="Group" incrementGroup="categoria" calculation="Sum">
		<variableExpression><![CDATA[$V{Total_Cat}]]></variableExpression>
	</variable>
	<group name="categoria">
		<groupExpression><![CDATA[$F{categoria}]]></groupExpression>
		<groupHeader>
			<band height="26">
				<textField>
					<reportElement style="SubTitle" x="0" y="1" width="396" height="25" forecolor="#006633"/>
					<textElement verticalAlignment="Middle">
						<font isBold="true" isUnderline="false"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{categoria}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="0" width="780" height="1"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="25">
				<staticText>
					<reportElement style="Column header" mode="Transparent" x="505" y="0" width="205" height="25" forecolor="#006633"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font size="13" isUnderline="true"/>
					</textElement>
					<text><![CDATA[Total Categoría $:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="710" y="0" width="70" height="25" forecolor="#006633"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="13" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{Total_Cat}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="producto">
		<groupExpression><![CDATA[$F{producto}]]></groupExpression>
		<groupHeader>
			<band/>
		</groupHeader>
		<groupFooter>
			<band height="20">
				<textField>
					<reportElement style="SubTitle" x="36" y="0" width="360" height="19" forecolor="#006600"/>
					<textElement verticalAlignment="Middle">
						<font size="16" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{producto}]]></textFieldExpression>
				</textField>
				<rectangle>
					<reportElement mode="Opaque" x="0" y="0" width="36" height="19" forecolor="#CCCCCC" backcolor="#CCCCCC"/>
				</rectangle>
				<textField pattern="#,##0.00">
					<reportElement style="Detail" x="396" y="0" width="109" height="19"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font isBold="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{saldo}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="614" y="0" width="96" height="19"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="12" isBold="false"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{Total_Prod}]]></textFieldExpression>
				</textField>
				<textField pattern="#,##0.00">
					<reportElement x="505" y="0" width="109" height="19"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="Arial" size="12"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{cost_sum}]]></textFieldExpression>
				</textField>
				<line>
					<reportElement positionType="FixRelativeToBottom" x="0" y="19" width="710" height="1"/>
					<graphicElement>
						<pen lineWidth="0.5" lineColor="#999999"/>
					</graphicElement>
				</line>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="49" splitType="Stretch">
			<staticText>
				<reportElement style="Title" x="0" y="0" width="614" height="49" forecolor="#006600"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font size="25"/>
				</textElement>
				<text><![CDATA[Inventario - Valoración Movimientos (Resumen)]]></text>
			</staticText>
			<image scaleImage="RetainShape" hAlign="Right" vAlign="Top" isUsingCache="true">
				<reportElement key="" x="681" y="2" width="98" height="44"/>
				<box>
					<topPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<leftPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
					<bottomPen lineWidth="0.0" lineColor="#000000"/>
					<rightPen lineWidth="0.0" lineStyle="Solid" lineColor="#000000"/>
				</box>
				<imageExpression class="java.awt.Image"><![CDATA[org.openbravo.erpCommon.utility.Utility.showImageLogo("yourcompanylegal", $F{org_id})]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band height="20" splitType="Stretch">
			<staticText>
				<reportElement style="Column header" x="396" y="4" width="109" height="16" forecolor="#000000"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Saldo Anterior $:]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="505" y="4" width="109" height="16" forecolor="#000000"/>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<text><![CDATA[Movimientos: $]]></text>
			</staticText>
			<staticText>
				<reportElement style="Column header" x="614" y="4" width="96" height="16" forecolor="#000000"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isItalic="false"/>
				</textElement>
				<text><![CDATA[Saldo Actual $:]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band splitType="Stretch"/>
	</detail>
	<columnFooter>
		<band height="25" splitType="Stretch">
			<staticText>
				<reportElement style="Column header" mode="Transparent" x="505" y="1" width="205" height="24" forecolor="#000000"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font size="13" isUnderline="false"/>
				</textElement>
				<text><![CDATA[TOTAL GENERAL:]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="710" y="1" width="70" height="24"/>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font fontName="Arial" size="13" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{Total_Gen}]]></textFieldExpression>
			</textField>
		</band>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement style="Column header" x="614" y="0" width="96" height="20"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Pág. "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="710" y="0" width="70" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="EEEEE dd MMMMM yyyy">
				<reportElement style="Column header" x="0" y="0" width="174" height="20"/>
				<textElement verticalAlignment="Middle">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
