<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_Factura" pageWidth="595" pageHeight="421" columnWidth="555" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="DOCUMENT_ID" class="java.lang.String"/>
	<parameter name="BASE_DESIGN" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["/opt/OpenbravoTablero/modules/com.atrums.felectronica/src"]]></defaultValueExpression>
	</parameter>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="BASE_WEB" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["https://165.227.84.8/openbravotablero/web"]]></defaultValueExpression>
	</parameter>
	<queryString>
		<![CDATA[SELECT coalesce(oi.taxid,'') AS cedruc,
       coalesce((CASE WHEN (dt.em_co_tp_comp_autorizador_sri = '1') THEN 'FACTURA'
            WHEN (dt.em_co_tp_comp_autorizador_sri = '4') THEN 'NOTA DE CREDITO'
            WHEN (dt.em_co_tp_comp_autorizador_sri = '5') THEN 'NOTA DE DEBITO'
       END),'') AS tipoDoc,
       'No. ' || lpad(coalesce(i.em_co_nro_estab,''),3,'0') || '-' ||
       lpad(coalesce(i.em_co_punto_emision,''),3,'0') || '-' ||
       lpad(coalesce(i.documentno,''),9,'0') AS numero,
       coalesce(i.em_co_nro_aut_sri,'Pendiente por el SRI...') as autoriza,
       coalesce(i.em_atecfe_fecha_autori,'Pendiente por el SRI...') as fecha,
       coalesce((CASE WHEN (cl.em_atecfe_tipoambiente = '1') THEN 'PRUEBA'
            WHEN (cl.em_atecfe_tipoambiente = '2') THEN 'PRODUCCION'
       END),'') AS ambiente,
       coalesce((CASE WHEN (cl.em_atecfe_tipoemisi = '1') THEN 'NORMAL'
            WHEN (cl.em_atecfe_tipoemisi = '2') THEN 'INDISPONIBILIDAD DEL SRI'
       END),'') AS emision,
       coalesce(i.em_atecfe_codigo_acc,'Pendiente por el SRI...') AS acceso,
       upper(cl.name) AS razonSocial,
       coalesce(l.address1,coalesce(l.address2,'')) AS direccion,
       coalesce(cl.em_atecfe_numresolsri,'') AS contriEspe,
       coalesce((CASE WHEN (cl.em_atecfe_obligcontabi = 'Y') THEN 'SI'
            WHEN (cl.em_atecfe_obligcontabi = 'N') THEN 'NO'
       END),'') AS obligCont,
       coalesce(bp.name,'') AS clieRazon,
       coalesce(bp.taxid,'') AS cliciruc,
       to_char(i.dateinvoiced,'dd/MM/YYYY') AS fechaFac,
	   --
		coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 12),0,00) AS subtotal_imp,
		coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 14),0,00) AS subtotal_imp14,
		--
       coalesce((SELECT sum(ilt.taxbaseamt)
       FROM c_invoicelinetax AS ilt
       INNER JOIN c_tax AS t ON (ilt.c_tax_id = t.c_tax_id)
       WHERE ilt.c_invoice_id = i.c_invoice_id AND t.em_atecfe_tartax = '0' AND ilt.taxamt=0), 0.00) AS subtotalSin_imp,
       coalesce((SELECT sum(ilt.taxbaseamt)
       FROM c_invoicelinetax AS ilt
       INNER JOIN c_tax AS t ON (ilt.c_tax_id = t.c_tax_id)
       WHERE ilt.c_invoice_id = i.c_invoice_id AND t.em_atecfe_tartax = '7' AND ilt.taxamt=0), 0.00) AS noSujeto,
       coalesce(i.totallines, 0) AS subtotal,
       coalesce(i.grandtotal, 0) AS total,
		(coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 12),0,00) * 12/100) AS iva12,
		(coalesce((SELECT sum(ilt.taxbaseamt)
			FROM c_invoicelinetax ilt, c_tax ct
			WHERE ilt.c_tax_id = ct.c_tax_id
			and ilt.c_invoice_id = i.c_invoice_id AND ct.rate = 14),0,00) * 14/100) AS iva14,
       0.00 AS descuento,
       0.00 AS ice,
       0.00 AS propina,
       coalesce((SELECT sum(ilt.taxamt)
       FROM c_invoicelinetax AS ilt
       WHERE ilt.c_invoice_id = i.c_invoice_id AND ilt.taxamt<>0),0.00) AS subtotaliva,
       coalesce(cs.direc_serv_consul,'') as consul,
       coalesce((SELECT array_to_string(array_agg(u.email), ';') FROM ad_user as u WHERE u.c_bpartner_id = i.c_bpartner_id AND u.em_atecfe_check_email = 'Y'),'') as emailcli,
       coalesce(bl.phone, coalesce(bl.phone2,'')) as telef,
       coalesce(i.description,'') as descrio,
       (select  coalesce(c.address1,coalesce(c.address2,''))
          from ad_orginfo ad, ad_org og, c_location c
         where ad.ad_org_id = og.ad_org_id
           and og.issummary = 'Y'
           and og.ad_orgtype_id = '1'
           and c.c_location_id = ad.c_location_id
         limit 1) as dir_matriz,
      (select o.ad_org_id
        from ad_client c, ad_org o
       where c.ad_client_id = o.ad_client_id
        and o.isperiodcontrolallowed = 'Y'
        and c.ad_client_id in ($P!{USER_CLIENT})
      limit 1) as organizationid
FROM c_invoice i
   INNER JOIN ad_org o ON (i.ad_org_id = o.ad_org_id)
   INNER JOIN ad_orginfo oi ON (i.ad_org_id = oi.ad_org_id)
   INNER JOIN ad_client cl ON(i.ad_client_id = cl.ad_client_id)
   INNER JOIN c_location l ON (oi.c_location_id = l.c_location_id)
   INNER JOIN c_country c ON (l.c_country_id = c.c_country_id)
   INNER JOIN c_bpartner bp ON (i.c_bpartner_id = bp.c_bpartner_id)
   INNER JOIN c_doctype dt ON (i.c_doctypetarget_id = dt.c_doctype_id)
   LEFT JOIN atecfe_conf_servidor cs on (i.ad_client_id = cs.ad_client_id)
   LEFT JOIN c_bpartner_location bl on (bp.c_bpartner_id = bl.c_bpartner_id and bl.c_bpartner_location_id = i.c_bpartner_location_id)
WHERE i.c_invoice_id = $P{DOCUMENT_ID}]]>
	</queryString>
	<field name="cedruc" class="java.lang.String"/>
	<field name="tipodoc" class="java.lang.String"/>
	<field name="numero" class="java.lang.String"/>
	<field name="autoriza" class="java.lang.String"/>
	<field name="fecha" class="java.lang.String"/>
	<field name="ambiente" class="java.lang.String"/>
	<field name="emision" class="java.lang.String"/>
	<field name="acceso" class="java.lang.String"/>
	<field name="razonsocial" class="java.lang.String"/>
	<field name="direccion" class="java.lang.String"/>
	<field name="contriespe" class="java.lang.String"/>
	<field name="obligcont" class="java.lang.String"/>
	<field name="clierazon" class="java.lang.String"/>
	<field name="cliciruc" class="java.lang.String"/>
	<field name="fechafac" class="java.lang.String"/>
	<field name="subtotal_imp" class="java.math.BigDecimal"/>
	<field name="subtotal_imp14" class="java.math.BigDecimal"/>
	<field name="subtotalsin_imp" class="java.math.BigDecimal"/>
	<field name="nosujeto" class="java.math.BigDecimal"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<field name="total" class="java.math.BigDecimal"/>
	<field name="iva12" class="java.math.BigDecimal"/>
	<field name="iva14" class="java.math.BigDecimal"/>
	<field name="descuento" class="java.math.BigDecimal"/>
	<field name="ice" class="java.math.BigDecimal"/>
	<field name="propina" class="java.math.BigDecimal"/>
	<field name="subtotaliva" class="java.math.BigDecimal"/>
	<field name="consul" class="java.lang.String"/>
	<field name="emailcli" class="java.lang.String"/>
	<field name="telef" class="java.lang.String"/>
	<field name="descrio" class="java.lang.String"/>
	<field name="dir_matriz" class="java.lang.String"/>
	<field name="organizationid" class="java.lang.String"/>
	<background>
		<band height="50"/>
	</background>
	<title>
		<band height="138">
			<rectangle radius="10">
				<reportElement x="0" y="92" width="276" height="42"/>
			</rectangle>
			<rectangle radius="10">
				<reportElement x="290" y="0" width="265" height="134"/>
			</rectangle>
			<staticText>
				<reportElement x="301" y="6" width="79" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[R.U.C.:]]></text>
			</staticText>
			<textField>
				<reportElement x="380" y="6" width="160" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{cedruc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="301" y="15" width="79" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{tipodoc}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="380" y="15" width="160" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{numero}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="301" y="27" width="239" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[NÚMERO DE AUTORIZACIÓN]]></text>
			</staticText>
			<textField>
				<reportElement x="301" y="36" width="239" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{autoriza}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="301" y="45" width="79" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[FECHA Y HORA DE AUTORIZACIÓN]]></text>
			</staticText>
			<textField>
				<reportElement x="380" y="45" width="160" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{fecha}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="301" y="54" width="79" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[AMBIENTE:]]></text>
			</staticText>
			<textField>
				<reportElement x="380" y="54" width="160" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{ambiente}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="301" y="63" width="79" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[EMISIÓN:]]></text>
			</staticText>
			<textField>
				<reportElement x="380" y="63" width="160" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{emision}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="301" y="72" width="239" height="15"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[CLAVE DE ACCESO]]></text>
			</staticText>
			<textField>
				<reportElement x="301" y="115" width="239" height="15"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{acceso}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="9" y="96" width="255" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{razonsocial}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="227" y="123" width="37" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{obligcont}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="69" y="114" width="195" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{direccion}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="9" y="105" width="60" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Dir. Matriz:]]></text>
			</staticText>
			<staticText>
				<reportElement x="9" y="114" width="60" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Dir. Sucursal:]]></text>
			</staticText>
			<textField>
				<reportElement x="69" y="105" width="195" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{dir_matriz}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="9" y="123" width="218" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[OBLIGADO A LLEVAR CONTABILIDAD]]></text>
			</staticText>
			<componentElement>
				<reportElement mode="Transparent" x="301" y="86" width="239" height="30"/>
				<jr:barbecue xmlns:jr="http://jasperreports.sourceforge.net/jasperreports/components" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports/components http://jasperreports.sourceforge.net/xsd/components.xsd" type="Code128" drawText="false" checksumRequired="false" barWidth="0" barHeight="0">
					<jr:codeExpression><![CDATA[$F{acceso}]]></jr:codeExpression>
				</jr:barbecue>
			</componentElement>
			<image scaleImage="RetainShape" hAlign="Right" vAlign="Top" isUsingCache="true">
				<reportElement key="image-1" x="0" y="0" width="176" height="65"/>
				<imageExpression class="java.awt.Image"><![CDATA[org.openbravo.erpCommon.utility.Utility.showImageLogo("yourcompanylegal", $F{organizationid})]]></imageExpression>
			</image>
		</band>
	</title>
	<columnHeader>
		<band height="30">
			<rectangle>
				<reportElement x="0" y="4" width="555" height="24"/>
			</rectangle>
			<textField>
				<reportElement x="437" y="7" width="105" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{cliciruc}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="18" width="82" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Fecha Emisión:]]></text>
			</staticText>
			<textField>
				<reportElement x="97" y="18" width="122" height="10"/>
				<textElement verticalAlignment="Middle">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{fechafac}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="390" y="7" width="47" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[RUC/Cl:]]></text>
			</staticText>
			<textField>
				<reportElement x="180" y="7" width="210" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{clierazon}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="15" y="7" width="165" height="10"/>
				<textElement verticalAlignment="Top">
					<font size="7" isBold="true"/>
				</textElement>
				<text><![CDATA[Razón Social/Nombres y Apellidos:]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="23">
			<subreport>
				<reportElement positionType="Float" stretchType="RelativeToTallestObject" x="0" y="0" width="555" height="23"/>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN} + "/com/atrums/felectronica/erpReport/Rpt_Factura_lineas.jasper"]]></subreportExpression>
			</subreport>
		</band>
		<band height="18">
			<subreport>
				<reportElement x="0" y="0" width="555" height="18"/>
				<subreportParameter name="DOCUMENT_ID">
					<subreportParameterExpression><![CDATA[$P{DOCUMENT_ID}]]></subreportParameterExpression>
				</subreportParameter>
				<connectionExpression><![CDATA[$P{REPORT_CONNECTION}]]></connectionExpression>
				<subreportExpression class="java.lang.String"><![CDATA[$P{BASE_DESIGN} + "/com/atrums/felectronica/erpReport/Rpt_Factura_Iva12.jasper"]]></subreportExpression>
			</subreport>
		</band>
	</detail>
</jasperReport>
