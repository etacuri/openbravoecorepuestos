<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rep_Metodo_Pago_Cobros" pageWidth="340" pageHeight="842" columnWidth="340" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="8bea83c8-b141-43c5-a9f0-82a4badafe0f">
	<property name="ireport.zoom" value="3.0"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="fechaIni" class="java.util.Date"/>
	<parameter name="fechaFin" class="java.util.Date"/>
	<parameter name="ad_org_id" class="java.lang.String"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<queryString>
		<![CDATA[(select (select sc.name from ad_org sc where sc.ad_org_id = $P{ad_org_id}) as sucursal,
        pm.name as metodo_pago,
        coalesce(sum(pd.amount),0) as total_cobrado
   from fin_paymentmethod pm
   left join fin_payment p on (p.fin_paymentmethod_id = pm.fin_paymentmethod_id
                               and p.generated_credit = 0
                               and p.ad_org_id = $P{ad_org_id})
   left join fin_payment_detail pd on (pd.fin_payment_id = p.fin_payment_id and pd.ad_org_id = $P{ad_org_id})
   left join fin_payment_scheduledetail sd on (sd.fin_payment_detail_id = pd.fin_payment_detail_id and sd.ad_org_id = $P{ad_org_id})
   left join fin_payment_schedule ps on (ps.fin_payment_schedule_id = sd.fin_payment_schedule_invoice and sd.ad_org_id = $P{ad_org_id})
   left join c_invoice c on (c.c_invoice_id = ps.c_invoice_id and c.issotrx = 'Y'
                                                              and c.docstatus = 'CO'
                                                              and date(c.dateinvoiced) between date($P{fechaIni}) and date($P{fechaFin})
															  and c.ad_org_id = $P{ad_org_id})
   left join ad_org s on (s.ad_org_id = c.ad_org_id and c.ad_org_id = $P{ad_org_id})
  where pm.name not in ('Retención', 'Mixto', 'Con Retención')
    and pm.ad_client_id in ($P!{USER_CLIENT})
  group by 1,2
  order by 1,2
 )
 union all
(
select (select sc.name from ad_org sc where sc.ad_org_id = $P{ad_org_id}) as sucursal,
       pm.name as metodo_pago,
       coalesce(sum(pd.amount),0) as total_cobrado
  from fin_paymentmethod pm
  left join fin_payment p on (p.fin_paymentmethod_id = pm.fin_paymentmethod_id
                              and (p.generated_credit - p.used_credit) = 0
                              and p.ad_org_id = $P{ad_org_id})
  left join fin_payment_detail pd on (pd.fin_payment_id = p.fin_payment_id and pd.ad_org_id = $P{ad_org_id})
  left join fin_payment_scheduledetail sd on (sd.fin_payment_detail_id = pd.fin_payment_detail_id and sd.ad_org_id = $P{ad_org_id})
  left join fin_payment_schedule ps on (ps.fin_payment_schedule_id = sd.fin_payment_schedule_invoice and sd.ad_org_id = $P{ad_org_id})
  left join c_invoice c on (c.c_invoice_id = ps.c_invoice_id and c.issotrx = 'Y'
                                                             and c.docstatus = 'CO'
                                                             and date(c.dateinvoiced) between date($P{fechaIni}) and date($P{fechaFin})
															 and c.ad_org_id = $P{ad_org_id})
  left join ad_org s on (s.ad_org_id = c.ad_org_id and c.ad_org_id = $P{ad_org_id})
 where pm.name in ('Retención')
   and pm.ad_client_id in ($P!{USER_CLIENT})
 group by 1,2
 order by 1,2
 )]]>
	</queryString>
	<field name="sucursal" class="java.lang.String"/>
	<field name="metodo_pago" class="java.lang.String"/>
	<field name="total_cobrado" class="java.math.BigDecimal"/>
	<variable name="sum_total_cobrado" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{total_cobrado}]]></variableExpression>
	</variable>
	<variable name="porcentaje" class="java.math.BigDecimal">
		<variableExpression><![CDATA[]]></variableExpression>
	</variable>
	<variable name="total_porcentaje" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$V{porcentaje}]]></variableExpression>
	</variable>
	<columnHeader>
		<band height="30" splitType="Stretch">
			<staticText>
				<reportElement mode="Opaque" x="0" y="0" width="200" height="30" backcolor="#CCCCCC" uuid="9192e457-7175-413e-8005-b840d0b0d062"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Método de Pago]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="200" y="0" width="70" height="30" backcolor="#CCCCCC" uuid="8b80b685-df3f-490b-a755-df55dd4eeada"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total Cobrado]]></text>
			</staticText>
			<staticText>
				<reportElement mode="Opaque" x="270" y="0" width="70" height="30" backcolor="#CCCCCC" uuid="8877addb-31bb-47ac-9403-9d357641a056"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Porcentaje]]></text>
			</staticText>
		</band>
	</columnHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<rectangle>
				<reportElement x="270" y="0" width="70" height="15" uuid="3821295f-23b5-4bca-8ab4-13f82b08b471"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement x="200" y="0" width="70" height="15" uuid="0d36ad31-a28d-47ab-b474-5b254c2dd06a"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<textField>
				<reportElement x="0" y="0" width="200" height="15" uuid="3f1841ba-ff45-45f8-8753-a716919fb020"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new String (" ") + $F{metodo_pago}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="0" width="55" height="15" uuid="203cf311-5f63-45aa-b360-ff0e5de527a6"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[$F{total_cobrado}]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Auto" pattern="#,##0.00">
				<reportElement x="270" y="0" width="55" height="15" uuid="0255d4a8-33b9-4a61-b0c1-c6a137c06ce5">
					<printWhenExpression><![CDATA[!$F{total_cobrado}.equals(BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[($F{total_cobrado}.multiply(new BigDecimal("100"))).divide($V{sum_total_cobrado},4)]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="255" y="0" width="15" height="15" uuid="e74f0fbd-b95f-4866-868f-6be74b2f8872"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[$]]></text>
			</staticText>
			<staticText>
				<reportElement x="325" y="0" width="15" height="15" uuid="65b94572-7e07-4b53-8c18-2bc42337e781"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle"/>
				<text><![CDATA[%]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="270" y="0" width="55" height="15" uuid="dc867fd7-4d6c-4b45-a8e3-33b8c86ae001">
					<printWhenExpression><![CDATA[$F{total_cobrado}.equals(BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle"/>
				<textFieldExpression><![CDATA[new BigDecimal("0")]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<summary>
		<band height="15" splitType="Stretch">
			<rectangle>
				<reportElement x="270" y="0" width="70" height="15" uuid="d0c69e78-ce0d-423c-8f49-af5f3c120b66"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement x="270" y="0" width="70" height="15" uuid="378c0143-f100-47a8-af94-97aa94bf8456"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<rectangle>
				<reportElement x="200" y="0" width="70" height="15" uuid="0f4330d2-13b5-4ad8-ba76-236273b24907"/>
				<graphicElement>
					<pen lineWidth="0.5"/>
				</graphicElement>
			</rectangle>
			<textField pattern="#,##0.00">
				<reportElement x="200" y="0" width="55" height="15" uuid="b17b626a-dc78-4256-9f5a-ee011e7bf820"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[$V{sum_total_cobrado}]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="0" y="0" width="200" height="15" uuid="470c2ec3-2da5-47ad-a91c-86c2b6285d09"/>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[Total Cobros  ]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="270" y="0" width="55" height="15" uuid="720482f3-ee76-49a6-9279-c78d87de5926">
					<printWhenExpression><![CDATA[!$V{sum_total_cobrado}.equals(BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.0"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new BigDecimal("100")]]></textFieldExpression>
			</textField>
			<staticText>
				<reportElement x="255" y="0" width="15" height="15" uuid="83878d78-b111-4ded-83eb-19fb2b8e8ac8"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[$]]></text>
			</staticText>
			<staticText>
				<reportElement x="325" y="0" width="15" height="15" uuid="dbb837ca-5980-454d-b49e-09a3feed10f2"/>
				<box>
					<topPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
					<rightPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<text><![CDATA[%]]></text>
			</staticText>
			<textField pattern="#,##0.00">
				<reportElement x="270" y="0" width="55" height="15" uuid="459c93b3-ad22-4257-8d8c-61d9f47f8a0b">
					<printWhenExpression><![CDATA[$V{sum_total_cobrado}.equals(BigDecimal.ZERO)]]></printWhenExpression>
				</reportElement>
				<box>
					<topPen lineWidth="0.5"/>
					<leftPen lineWidth="0.5"/>
					<bottomPen lineWidth="0.5"/>
				</box>
				<textElement textAlignment="Right" verticalAlignment="Middle">
					<font isBold="true"/>
				</textElement>
				<textFieldExpression><![CDATA[new BigDecimal("0")]]></textFieldExpression>
			</textField>
		</band>
	</summary>
</jasperReport>
