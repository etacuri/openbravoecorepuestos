<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="Rpt_ValoracionStock" pageWidth="850" pageHeight="820" columnWidth="810" leftMargin="20" rightMargin="20" topMargin="20" bottomMargin="20">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="74"/>
	<property name="ireport.y" value="0"/>
	<style name="Title" fontName="Arial" fontSize="26" isBold="true" pdfFontName="Helvetica-Bold"/>
	<style name="SubTitle" forecolor="#666666" fontName="Arial" fontSize="18"/>
	<style name="Column header" forecolor="#666666" fontName="Arial" fontSize="12" isBold="true"/>
	<style name="Detail" fontName="Arial" fontSize="12"/>
	<parameter name="USER_CLIENT" class="java.lang.String"/>
	<parameter name="DATE_TO" class="java.util.Date"/>
	<parameter name="AUX_BODEGA" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA[($P{BODEGA_ID} == null || $P{BODEGA_ID}.equals("")) ? "" : " AND w.m_warehouse_id ='" + $P{BODEGA_ID} + "'"]]></defaultValueExpression>
	</parameter>
	<parameter name="BASE_WEB" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["http://192.168.0.216/openbravo/web"]]></defaultValueExpression>
	</parameter>
	<parameter name="BODEGA_ID" class="java.lang.String"/>
	<parameter name="USER_ORG" class="java.lang.String"/>
	<queryString>
		<![CDATA[select a.organizationid, a.Nombre, a.categoria, a.Descripcion, a.Costo, a.Stock, a.Bodega, (a.Costo * a.Stock) as subtotal
from (
SELECT t.AD_ORG_ID AS organizationid,
       p.name AS Nombre,
       cat.name as Categoria,
       coalesce(p.description, p.description, '-') AS Descripcion,
       coalesce((select c.cost from m_costing c
              where p.m_product_id = c.m_product_id AND
                    c.datefrom < ($P{DATE_TO}) AND
                    c.dateto >= ($P{DATE_TO}) limit 1), '0'
                                             ) AS Costo,
       sum(t.movementqty) AS Stock,
       (select name from m_warehouse where m_warehouse_id = $P{BODEGA_ID}) as Bodega
FROM m_product p, m_transaction t, m_product_category cat
WHERE p.ad_client_id IN ($P!{USER_CLIENT}) AND p.isactive='Y' AND
      t.m_product_id = p.m_product_id AND
      cat.m_product_category_id = p.m_product_category_id AND
      t.m_locator_id IN (select m_locator_id from m_locator where m_warehouse_id = $P{BODEGA_ID}) AND
      t.movementdate <= ($P{DATE_TO})
GROUP BY 1, 2, 3, 4, 5, 7
ORDER BY 3,2
) a]]>
	</queryString>
	<field name="nombre" class="java.lang.String"/>
	<field name="descripcion" class="java.lang.String"/>
	<field name="costo" class="java.math.BigDecimal"/>
	<field name="stock" class="java.math.BigDecimal"/>
	<field name="bodega" class="java.lang.String"/>
	<field name="organizationid" class="java.lang.String"/>
	<field name="categoria" class="java.lang.String"/>
	<field name="subtotal" class="java.math.BigDecimal"/>
	<variable name="total" class="java.math.BigDecimal" resetType="Group" resetGroup="categoria" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal}]]></variableExpression>
	</variable>
	<variable name="total_general" class="java.math.BigDecimal" calculation="Sum">
		<variableExpression><![CDATA[$F{subtotal}]]></variableExpression>
	</variable>
	<group name="Bodega">
		<groupExpression><![CDATA[$F{bodega}]]></groupExpression>
		<groupHeader>
			<band height="47">
				<textField>
					<reportElement style="Detail" x="102" y="0" width="230" height="20"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14"/>
					</textElement>
					<textFieldExpression class="java.lang.String"><![CDATA[$F{bodega}]]></textFieldExpression>
				</textField>
				<staticText>
					<reportElement style="Column header" x="0" y="0" width="102" height="20"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="14" isBold="true"/>
					</textElement>
					<text><![CDATA[Bodega]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="0" y="30" width="102" height="17"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Código]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="332" y="30" width="262" height="17"/>
					<textElement verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Descripción]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="594" y="30" width="70" height="17"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Costo]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="664" y="30" width="70" height="17"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Stock]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" mode="Transparent" x="102" y="30" width="102" height="17" forecolor="#666666" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Categoría]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" x="734" y="30" width="76" height="17"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Subtotal]]></text>
				</staticText>
				<staticText>
					<reportElement style="Column header" mode="Transparent" x="204" y="30" width="128" height="17" forecolor="#666666" backcolor="#FFFFFF"/>
					<textElement textAlignment="Left" verticalAlignment="Middle" rotation="None" lineSpacing="Single" markup="none">
						<font fontName="SansSerif" size="12" isBold="true" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
					</textElement>
					<text><![CDATA[Nombre]]></text>
				</staticText>
			</band>
		</groupHeader>
		<groupFooter>
			<band height="17">
				<staticText>
					<reportElement style="Column header" x="594" y="0" width="140" height="17"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Bodega:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="734" y="0" width="76" height="17"/>
					<textElement textAlignment="Right">
						<font size="11" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{total_general}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<group name="categoria">
		<groupExpression><![CDATA[$F{categoria}]]></groupExpression>
		<groupFooter>
			<band height="17">
				<staticText>
					<reportElement style="Column header" x="594" y="0" width="140" height="17"/>
					<textElement textAlignment="Right" verticalAlignment="Middle">
						<font fontName="SansSerif" size="12" isBold="true"/>
					</textElement>
					<text><![CDATA[Total Categoría:]]></text>
				</staticText>
				<textField pattern="#,##0.00">
					<reportElement x="734" y="0" width="76" height="17"/>
					<textElement textAlignment="Right">
						<font size="10" isBold="true"/>
					</textElement>
					<textFieldExpression class="java.math.BigDecimal"><![CDATA[$V{total}]]></textFieldExpression>
				</textField>
			</band>
		</groupFooter>
	</group>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band height="40" splitType="Stretch">
			<staticText>
				<reportElement style="Title" x="0" y="0" width="594" height="40"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="SansSerif" size="24" isUnderline="false"/>
				</textElement>
				<text><![CDATA[Reporte Valoración de Inventario - Bodega]]></text>
			</staticText>
			<image scaleImage="RetainShape" hAlign="Right" vAlign="Top" isUsingCache="true">
				<reportElement key="image-1" x="687" y="0" width="122" height="40"/>
				<imageExpression class="java.net.URL"><![CDATA[new java.net.URL($P{BASE_WEB}+"/../utility/ShowImageLogo?logo=yourcompanydoc&orgId="+ $F{organizationid})]]></imageExpression>
			</image>
		</band>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<textField>
				<reportElement style="Detail" x="0" y="0" width="102" height="15"/>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{nombre}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement style="Detail" x="332" y="0" width="262" height="15"/>
				<textElement>
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{descripcion}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.0000">
				<reportElement style="Detail" x="594" y="0" width="70" height="15"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{costo}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement style="Detail" x="664" y="0" width="70" height="15"/>
				<textElement textAlignment="Right">
					<font fontName="SansSerif" size="10"/>
				</textElement>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{stock}]]></textFieldExpression>
			</textField>
			<textField pattern="#,##0.00">
				<reportElement x="734" y="0" width="76" height="15"/>
				<textElement textAlignment="Right"/>
				<textFieldExpression class="java.math.BigDecimal"><![CDATA[$F{subtotal}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="false">
				<reportElement mode="Transparent" x="102" y="0" width="102" height="15" forecolor="#000000" backcolor="#FFFFFF"/>
				<textElement textAlignment="Left" verticalAlignment="Top" rotation="None" lineSpacing="Single" markup="none">
					<font fontName="SansSerif" size="10" isBold="false" isItalic="false" isUnderline="false" isStrikeThrough="false" pdfFontName="Helvetica" pdfEncoding="Cp1252" isPdfEmbedded="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{categoria}]]></textFieldExpression>
			</textField>
			<textField>
				<reportElement x="204" y="0" width="128" height="15"/>
				<textElement/>
				<textFieldExpression class="java.lang.String"><![CDATA[$F{nombre}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band height="20" splitType="Stretch">
			<textField>
				<reportElement style="Column header" x="690" y="0" width="80" height="20"/>
				<textElement textAlignment="Right">
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA["Página "+$V{PAGE_NUMBER}+" of"]]></textFieldExpression>
			</textField>
			<textField evaluationTime="Report">
				<reportElement style="Column header" x="770" y="0" width="40" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.lang.String"><![CDATA[" " + $V{PAGE_NUMBER}]]></textFieldExpression>
			</textField>
			<textField pattern="dd-MMM-yyyy">
				<reportElement style="Column header" x="0" y="0" width="102" height="20"/>
				<textElement>
					<font size="10" isBold="false"/>
				</textElement>
				<textFieldExpression class="java.util.Date"><![CDATA[new java.util.Date()]]></textFieldExpression>
			</textField>
		</band>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
