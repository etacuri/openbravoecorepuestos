/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.ventas.pedido.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.geography.Location;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentMethod;
/**
 * Entity class for entity fpv_factura_cot (stored in table fpv_factura_cot).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class fpvFacturaCot extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "fpv_factura_cot";
    public static final String ENTITY_NAME = "fpv_factura_cot";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_EMPLEADO = "empleado";
    public static final String PROPERTY_PAYMENTMETHOD = "paymentMethod";
    public static final String PROPERTY_SALESORDER = "salesOrder";
    public static final String PROPERTY_PRODUCTOS = "productos";
    public static final String PROPERTY_FACTURAR = "facturar";
    public static final String PROPERTY_FECHACOTIZACION = "fechaCotizacion";
    public static final String PROPERTY_DOCSTATUS = "docstatus";
    public static final String PROPERTY_PARTNERADDRESS = "partnerAddress";
    public static final String PROPERTY_USERCONTACT = "userContact";
    public static final String PROPERTY__COMPUTEDCOLUMNS = "_computedColumns";
    public static final String PROPERTY_FPVFACTURACOTLINLIST = "fpvFacturaCotLinList";


    // Computed columns properties, these properties cannot be directly accessed, they need
    // to be read through _commputedColumns proxy. They cannot be directly used in HQL, OBQuery
    // nor OBCriteria. 
    public static final String COMPUTED_COLUMN_TOTALDOCUMENTO = "totalDocumento";
    public static final String COMPUTED_COLUMN_TOTALEFECTIVO = "totalEfectivo";
    public static final String COMPUTED_COLUMN_TOTALTARJETA = "totalTarjeta";

    public fpvFacturaCot() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PRODUCTOS, false);
        setDefaultValue(PROPERTY_FACTURAR, false);
        setDefaultValue(PROPERTY_DOCSTATUS, "BR");
        setDefaultValue(PROPERTY_FPVFACTURACOTLINLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public BusinessPartner getEmpleado() {
        return (BusinessPartner) get(PROPERTY_EMPLEADO);
    }

    public void setEmpleado(BusinessPartner empleado) {
        set(PROPERTY_EMPLEADO, empleado);
    }

    public FIN_PaymentMethod getPaymentMethod() {
        return (FIN_PaymentMethod) get(PROPERTY_PAYMENTMETHOD);
    }

    public void setPaymentMethod(FIN_PaymentMethod paymentMethod) {
        set(PROPERTY_PAYMENTMETHOD, paymentMethod);
    }

    public Order getSalesOrder() {
        return (Order) get(PROPERTY_SALESORDER);
    }

    public void setSalesOrder(Order salesOrder) {
        set(PROPERTY_SALESORDER, salesOrder);
    }

    public Boolean isProductos() {
        return (Boolean) get(PROPERTY_PRODUCTOS);
    }

    public void setProductos(Boolean productos) {
        set(PROPERTY_PRODUCTOS, productos);
    }

    public Boolean isFacturar() {
        return (Boolean) get(PROPERTY_FACTURAR);
    }

    public void setFacturar(Boolean facturar) {
        set(PROPERTY_FACTURAR, facturar);
    }

    public Date getFechaCotizacion() {
        return (Date) get(PROPERTY_FECHACOTIZACION);
    }

    public void setFechaCotizacion(Date fechaCotizacion) {
        set(PROPERTY_FECHACOTIZACION, fechaCotizacion);
    }

    public String getDocstatus() {
        return (String) get(PROPERTY_DOCSTATUS);
    }

    public void setDocstatus(String docstatus) {
        set(PROPERTY_DOCSTATUS, docstatus);
    }

    public Location getPartnerAddress() {
        return (Location) get(PROPERTY_PARTNERADDRESS);
    }

    public void setPartnerAddress(Location partnerAddress) {
        set(PROPERTY_PARTNERADDRESS, partnerAddress);
    }

    public User getUserContact() {
        return (User) get(PROPERTY_USERCONTACT);
    }

    public void setUserContact(User userContact) {
        set(PROPERTY_USERCONTACT, userContact);
    }

    public BigDecimal getTotalDocumento() {
        return (BigDecimal) get(COMPUTED_COLUMN_TOTALDOCUMENTO);
    }

    public void setTotalDocumento(BigDecimal totalDocumento) {
        set(COMPUTED_COLUMN_TOTALDOCUMENTO, totalDocumento);
    }

    public BigDecimal getTotalEfectivo() {
        return (BigDecimal) get(COMPUTED_COLUMN_TOTALEFECTIVO);
    }

    public void setTotalEfectivo(BigDecimal totalEfectivo) {
        set(COMPUTED_COLUMN_TOTALEFECTIVO, totalEfectivo);
    }

    public BigDecimal getTotalTarjeta() {
        return (BigDecimal) get(COMPUTED_COLUMN_TOTALTARJETA);
    }

    public void setTotalTarjeta(BigDecimal totalTarjeta) {
        set(COMPUTED_COLUMN_TOTALTARJETA, totalTarjeta);
    }

    public fpvFacturaCot_ComputedColumns get_computedColumns() {
        return (fpvFacturaCot_ComputedColumns) get(PROPERTY__COMPUTEDCOLUMNS);
    }

    public void set_computedColumns(fpvFacturaCot_ComputedColumns _computedColumns) {
        set(PROPERTY__COMPUTEDCOLUMNS, _computedColumns);
    }

    @SuppressWarnings("unchecked")
    public List<fpvFacturaCotLin> getFpvFacturaCotLinList() {
      return (List<fpvFacturaCotLin>) get(PROPERTY_FPVFACTURACOTLINLIST);
    }

    public void setFpvFacturaCotLinList(List<fpvFacturaCotLin> fpvFacturaCotLinList) {
        set(PROPERTY_FPVFACTURACOTLINLIST, fpvFacturaCotLinList);
    }


    @Override
    public Object get(String propName) {
      if (COMPUTED_COLUMN_TOTALDOCUMENTO.equals(propName)) {
        if (get_computedColumns() == null) {
          return null;
        }
        return get_computedColumns().getTotalDocumento();
      }
      if (COMPUTED_COLUMN_TOTALEFECTIVO.equals(propName)) {
        if (get_computedColumns() == null) {
          return null;
        }
        return get_computedColumns().getTotalEfectivo();
      }
      if (COMPUTED_COLUMN_TOTALTARJETA.equals(propName)) {
        if (get_computedColumns() == null) {
          return null;
        }
        return get_computedColumns().getTotalTarjeta();
      }
    
      return super.get(propName);
    }
}
