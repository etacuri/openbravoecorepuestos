/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.ecommerce.data;

import java.util.Date;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.plm.Product;
/**
 * Entity class for entity ecom_posicion_modelo (stored in table ecom_posicion_modelo).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class ecomPosicionModelo extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "ecom_posicion_modelo";
    public static final String ENTITY_NAME = "ecom_posicion_modelo";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_PROCESSED = "processed";
    public static final String PROPERTY_ECOMMARCAVEHICULO = "ecomMarcaVehiculo";
    public static final String PROPERTY_ECOMMODELOMARCAS = "ecomModeloMarcas";
    public static final String PROPERTY_PRODUCT = "product";
    public static final String PROPERTY_TIPO = "tipo";
    public static final String PROPERTY_FISCALYEAR = "fiscalYear";
    public static final String PROPERTY_ECOMPOSICIONES = "ecomPosiciones";
    public static final String PROPERTY_CILINDRAJE = "cilindraje";
    public static final String PROPERTY_TIPOTRANSMISION = "tipoTransmision";
    public static final String PROPERTY_TIPOCAJA = "tipoCaja";

    public ecomPosicionModelo() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESSED, false);
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Boolean isProcessed() {
        return (Boolean) get(PROPERTY_PROCESSED);
    }

    public void setProcessed(Boolean processed) {
        set(PROPERTY_PROCESSED, processed);
    }

    public ecomMarcaVehiculo getEcomMarcaVehiculo() {
        return (ecomMarcaVehiculo) get(PROPERTY_ECOMMARCAVEHICULO);
    }

    public void setEcomMarcaVehiculo(ecomMarcaVehiculo ecomMarcaVehiculo) {
        set(PROPERTY_ECOMMARCAVEHICULO, ecomMarcaVehiculo);
    }

    public ecomModeloMarcas getEcomModeloMarcas() {
        return (ecomModeloMarcas) get(PROPERTY_ECOMMODELOMARCAS);
    }

    public void setEcomModeloMarcas(ecomModeloMarcas ecomModeloMarcas) {
        set(PROPERTY_ECOMMODELOMARCAS, ecomModeloMarcas);
    }

    public Product getProduct() {
        return (Product) get(PROPERTY_PRODUCT);
    }

    public void setProduct(Product product) {
        set(PROPERTY_PRODUCT, product);
    }

    public String getTipo() {
        return (String) get(PROPERTY_TIPO);
    }

    public void setTipo(String tipo) {
        set(PROPERTY_TIPO, tipo);
    }

    public Long getFiscalYear() {
        return (Long) get(PROPERTY_FISCALYEAR);
    }

    public void setFiscalYear(Long fiscalYear) {
        set(PROPERTY_FISCALYEAR, fiscalYear);
    }

    public ecomPosiciones getEcomPosiciones() {
        return (ecomPosiciones) get(PROPERTY_ECOMPOSICIONES);
    }

    public void setEcomPosiciones(ecomPosiciones ecomPosiciones) {
        set(PROPERTY_ECOMPOSICIONES, ecomPosiciones);
    }

    public Long getCilindraje() {
        return (Long) get(PROPERTY_CILINDRAJE);
    }

    public void setCilindraje(Long cilindraje) {
        set(PROPERTY_CILINDRAJE, cilindraje);
    }

    public String getTipoTransmision() {
        return (String) get(PROPERTY_TIPOTRANSMISION);
    }

    public void setTipoTransmision(String tipoTransmision) {
        set(PROPERTY_TIPOTRANSMISION, tipoTransmision);
    }

    public String getTipoCaja() {
        return (String) get(PROPERTY_TIPOCAJA);
    }

    public void setTipoCaja(String tipoCaja) {
        set(PROPERTY_TIPOCAJA, tipoCaja);
    }

}
