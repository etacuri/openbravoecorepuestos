/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2008-2014 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package com.atrums.migracion.data;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.openbravo.base.structure.ActiveEnabled;
import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.base.structure.Traceable;
import org.openbravo.model.ad.access.User;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Entity class for entity SMA_Documentoxml (stored in table SMA_Documentoxml).
 *
 * NOTE: This class should not be instantiated directly. To instantiate this
 * class the {@link org.openbravo.base.provider.OBProvider} should be used.
 */
public class SMA_Documentoxml extends BaseOBObject implements Traceable, ClientEnabled, OrganizationEnabled, ActiveEnabled {
    private static final long serialVersionUID = 1L;
    public static final String TABLE_NAME = "SMA_Documentoxml";
    public static final String ENTITY_NAME = "SMA_Documentoxml";
    public static final String PROPERTY_ID = "id";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";
    public static final String PROPERTY_ACTIVE = "active";
    public static final String PROPERTY_CREATIONDATE = "creationDate";
    public static final String PROPERTY_CREATEDBY = "createdBy";
    public static final String PROPERTY_UPDATED = "updated";
    public static final String PROPERTY_UPDATEDBY = "updatedBy";
    public static final String PROPERTY_ACTUALIZACION = "actualizacion";
    public static final String PROPERTY_CLAVEACCESO = "claveacceso";
    public static final String PROPERTY_NOMBREDOCUMENTO = "nombredocumento";
    public static final String PROPERTY_MENSAJE = "mensaje";
    public static final String PROPERTY_ESTADO = "estado";
    public static final String PROPERTY_PROCESAMIENTO = "procesamiento";
    public static final String PROPERTY_TIPODOCUMENTO = "tipodocumento";
    public static final String PROPERTY_TOTALDOCUMENTO = "totaldocumento";
    public static final String PROPERTY_TOTALPAGO = "totalpago";
    public static final String PROPERTY_ORIGINALDOCUMENTO = "originaldocumento";
    public static final String PROPERTY_NOTACREDITO = "notacredito";
    public static final String PROPERTY_CHECKNUMBER = "checknumber";
    public static final String PROPERTY_TOTALESPAGOS = "totalespagos";
    public static final String PROPERTY_SMADOCUMENTOERRORLIST = "sMADocumentoerrorList";

    public SMA_Documentoxml() {
        setDefaultValue(PROPERTY_ACTIVE, true);
        setDefaultValue(PROPERTY_PROCESAMIENTO, "N");
        setDefaultValue(PROPERTY_TOTALDOCUMENTO, new BigDecimal(0));
        setDefaultValue(PROPERTY_TOTALPAGO, new BigDecimal(0));
        setDefaultValue(PROPERTY_SMADOCUMENTOERRORLIST, new ArrayList<Object>());
    }

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getId() {
        return (String) get(PROPERTY_ID);
    }

    public void setId(String id) {
        set(PROPERTY_ID, id);
    }

    public Client getClient() {
        return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
        set(PROPERTY_CLIENT, client);
    }

    public Organization getOrganization() {
        return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
        set(PROPERTY_ORGANIZATION, organization);
    }

    public Boolean isActive() {
        return (Boolean) get(PROPERTY_ACTIVE);
    }

    public void setActive(Boolean active) {
        set(PROPERTY_ACTIVE, active);
    }

    public Date getCreationDate() {
        return (Date) get(PROPERTY_CREATIONDATE);
    }

    public void setCreationDate(Date creationDate) {
        set(PROPERTY_CREATIONDATE, creationDate);
    }

    public User getCreatedBy() {
        return (User) get(PROPERTY_CREATEDBY);
    }

    public void setCreatedBy(User createdBy) {
        set(PROPERTY_CREATEDBY, createdBy);
    }

    public Date getUpdated() {
        return (Date) get(PROPERTY_UPDATED);
    }

    public void setUpdated(Date updated) {
        set(PROPERTY_UPDATED, updated);
    }

    public User getUpdatedBy() {
        return (User) get(PROPERTY_UPDATEDBY);
    }

    public void setUpdatedBy(User updatedBy) {
        set(PROPERTY_UPDATEDBY, updatedBy);
    }

    public Date getActualizacion() {
        return (Date) get(PROPERTY_ACTUALIZACION);
    }

    public void setActualizacion(Date actualizacion) {
        set(PROPERTY_ACTUALIZACION, actualizacion);
    }

    public String getClaveacceso() {
        return (String) get(PROPERTY_CLAVEACCESO);
    }

    public void setClaveacceso(String claveacceso) {
        set(PROPERTY_CLAVEACCESO, claveacceso);
    }

    public String getNombredocumento() {
        return (String) get(PROPERTY_NOMBREDOCUMENTO);
    }

    public void setNombredocumento(String nombredocumento) {
        set(PROPERTY_NOMBREDOCUMENTO, nombredocumento);
    }

    public String getMensaje() {
        return (String) get(PROPERTY_MENSAJE);
    }

    public void setMensaje(String mensaje) {
        set(PROPERTY_MENSAJE, mensaje);
    }

    public String getEstado() {
        return (String) get(PROPERTY_ESTADO);
    }

    public void setEstado(String estado) {
        set(PROPERTY_ESTADO, estado);
    }

    public String getProcesamiento() {
        return (String) get(PROPERTY_PROCESAMIENTO);
    }

    public void setProcesamiento(String procesamiento) {
        set(PROPERTY_PROCESAMIENTO, procesamiento);
    }

    public String getTipodocumento() {
        return (String) get(PROPERTY_TIPODOCUMENTO);
    }

    public void setTipodocumento(String tipodocumento) {
        set(PROPERTY_TIPODOCUMENTO, tipodocumento);
    }

    public BigDecimal getTotaldocumento() {
        return (BigDecimal) get(PROPERTY_TOTALDOCUMENTO);
    }

    public void setTotaldocumento(BigDecimal totaldocumento) {
        set(PROPERTY_TOTALDOCUMENTO, totaldocumento);
    }

    public BigDecimal getTotalpago() {
        return (BigDecimal) get(PROPERTY_TOTALPAGO);
    }

    public void setTotalpago(BigDecimal totalpago) {
        set(PROPERTY_TOTALPAGO, totalpago);
    }

    public String getOriginaldocumento() {
        return (String) get(PROPERTY_ORIGINALDOCUMENTO);
    }

    public void setOriginaldocumento(String originaldocumento) {
        set(PROPERTY_ORIGINALDOCUMENTO, originaldocumento);
    }

    public String getNotacredito() {
        return (String) get(PROPERTY_NOTACREDITO);
    }

    public void setNotacredito(String notacredito) {
        set(PROPERTY_NOTACREDITO, notacredito);
    }

    public String getChecknumber() {
        return (String) get(PROPERTY_CHECKNUMBER);
    }

    public void setChecknumber(String checknumber) {
        set(PROPERTY_CHECKNUMBER, checknumber);
    }

    public Long getTotalespagos() {
        return (Long) get(PROPERTY_TOTALESPAGOS);
    }

    public void setTotalespagos(Long totalespagos) {
        set(PROPERTY_TOTALESPAGOS, totalespagos);
    }

    @SuppressWarnings("unchecked")
    public List<SMA_Documentoerror> getSMADocumentoerrorList() {
      return (List<SMA_Documentoerror>) get(PROPERTY_SMADOCUMENTOERRORLIST);
    }

    public void setSMADocumentoerrorList(List<SMA_Documentoerror> sMADocumentoerrorList) {
        set(PROPERTY_SMADOCUMENTOERRORLIST, sMADocumentoerrorList);
    }

}
