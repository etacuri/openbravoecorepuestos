/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License.
 * The Original Code is Openbravo ERP.
 * The Initial Developer of the Original Code is Openbravo SLU
 * All portions are Copyright (C) 2013 Openbravo SLU
 * All Rights Reserved.
 * Contributor(s):  ______________________________________.
 ************************************************************************
*/
package org.openbravo.model.materialmgmt.transaction;

import java.math.BigDecimal;

import org.openbravo.base.structure.BaseOBObject;
import org.openbravo.base.structure.ClientEnabled;
import org.openbravo.base.structure.OrganizationEnabled;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.enterprise.Organization;
/**
 * Virtual entity class to hold computed columns for entity MaterialMgmtShipmentInOutLine.
 *
 * NOTE: This class should not be instantiated directly.
 */
public class ShipmentInOutLine_ComputedColumns extends BaseOBObject implements ClientEnabled, OrganizationEnabled {
    private static final long serialVersionUID = 1L;
    public static final String ENTITY_NAME = "ShipmentInOutLine_ComputedColumns";
    
    public static final String PROPERTY_ATECCOCODIGOPRODUCTO = "ateccoCodigoProducto";
    public static final String PROPERTY_ATECCOPENDIENTE = "aTECCOPendiente";
    public static final String PROPERTY_ATECCOPRECIOUNITARIO = "ateccoPrecioUnitario";
    public static final String PROPERTY_CLIENT = "client";
    public static final String PROPERTY_ORGANIZATION = "organization";

    @Override
    public String getEntityName() {
        return ENTITY_NAME;
    }

    public String getAteccoCodigoProducto() {
      return (String) get(PROPERTY_ATECCOCODIGOPRODUCTO);
    }

    public void setAteccoCodigoProducto(String ateccoCodigoProducto) {
      set(PROPERTY_ATECCOCODIGOPRODUCTO, ateccoCodigoProducto);
    }
    public BigDecimal getATECCOPendiente() {
      return (BigDecimal) get(PROPERTY_ATECCOPENDIENTE);
    }

    public void setATECCOPendiente(BigDecimal aTECCOPendiente) {
      set(PROPERTY_ATECCOPENDIENTE, aTECCOPendiente);
    }
    public BigDecimal getAteccoPrecioUnitario() {
      return (BigDecimal) get(PROPERTY_ATECCOPRECIOUNITARIO);
    }

    public void setAteccoPrecioUnitario(BigDecimal ateccoPrecioUnitario) {
      set(PROPERTY_ATECCOPRECIOUNITARIO, ateccoPrecioUnitario);
    }
    public Client getClient() {
      return (Client) get(PROPERTY_CLIENT);
    }

    public void setClient(Client client) {
      set(PROPERTY_CLIENT, client);
    }
    public Organization getOrganization() {
      return (Organization) get(PROPERTY_ORGANIZATION);
    }

    public void setOrganization(Organization organization) {
      set(PROPERTY_ORGANIZATION, organization);
    }
}
