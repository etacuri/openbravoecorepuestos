package com.atrums.offline.feletronica.process;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;

import org.apache.log4j.Logger;
import org.openbravo.base.exception.OBException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.common.invoice.Invoice;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

import com.atrums.contabilidad.data.CO_Retencion_Compra;

/**
 * @author ATRUMS-IT
 *
 */
public class ATECOFF_GenerarXml implements Process {

	static Logger log4j = Logger.getLogger(ATECOFF_GenerarXml.class);
	final OBError msg = new OBError();

	/**
	 * Expresión regular para verificar el correo
	 */
	private final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	@Override
	public void execute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub
		ConnectionProvider conn = bundle.getConnection();
		VariablesSecureApp varsAux = bundle.getContext().toVars();

		OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(), varsAux.getOrg());

		ATECOFFGenerarXmlData[] axmlLineas = null;
		ATECOFFGenerarXmlData[] axmlDat = null;
		ATECOFFGenerarXmlData[] validaConsumidor = null;
		Invoice invDato = null;

		/**
		 * Capturar datos de los ids para general el xml pertinenete al id
		 **/

		try {
			String strCinvoiceID = "";
			String strCoRetencionCompraID = "";
			String strMinoutID = "";

			if (bundle.getParams().get("C_Invoice_ID") != null) {
				strCinvoiceID = bundle.getParams().get("C_Invoice_ID").toString();
				invDato = OBDal.getInstance().get(Invoice.class, strCinvoiceID);
				validaConsumidor=ATECOFFGenerarXmlData.methodValidarConsumidor(conn, strCinvoiceID);
				if(validaConsumidor[0].dato2.equals("07") && Float.parseFloat(validaConsumidor[0].dato1)>50) {
					this.msg.setType("Error");
					this.msg.setMessage("No se puede procesar, consumidor final supera los 50$");
					this.msg.setTitle("@Error@");
					bundle.setResult(this.msg);
					return;
				}else {
					
				/**
				 * Consultando Cinvoice mediante hibernate del sistema
				 **/

				

				/**
				 * Consultando si el documento tiene lineas
				 */

				if (invDato.isSalesTransaction().equals(true)) {
					axmlLineas = ATECOFFGenerarXmlData.methodSeleccionarDetalles(conn, strCinvoiceID);

					if (axmlLineas.length == 0) {
						this.msg.setType("Error");
						this.msg.setMessage("No tiene lineas el documento");
						this.msg.setTitle("@Error@");
						bundle.setResult(this.msg);
						return;
					}
				} else { /* Liquidacion de Compra NPI */
					axmlLineas = ATECOFFGenerarXmlData.methodSelecDetallesLiqui(conn, strCinvoiceID);

					if (axmlLineas.length == 0) {
						this.msg.setType("Error");
						this.msg.setMessage("No tiene lineas el documento");
						this.msg.setTitle("@Error@");
						bundle.setResult(this.msg);
						return;
					}
				}

				if (invDato.getInvoiceDate() == null) {
					this.msg.setType("Error");
					this.msg.setMessage("No tiene fecha de facturación el documento");
					this.msg.setTitle("@Error@");
					bundle.setResult(this.msg);
					return;
				}

				if (!invDato.getDocumentNo().toString().matches("[0-9]*")) {
					this.msg.setType("Error");
					this.msg.setMessage("El N° del documento solo tienen que contener números");
					this.msg.setTitle("@Error@");
					bundle.setResult(this.msg);
					return;
				}

				/**
				 * Consultando informacion de la factura y servicios
				 */
				axmlDat = ATECOFFGenerarXmlData.methodFacturaElect(conn, invDato.getTransactionDocument().getId());

				if (axmlDat[0].dato1.equals("Y")) {
					if (verificarConfiguración(strCinvoiceID, "", "", conn)) {

						if (invDato.isSalesTransaction().equals(true)) {
							if (ATECOFF_GenerarFacturaXML.generarFacturaXML(invDato, conn, varsAux.getUser(), false,
									this.msg)) {
								if (!invDato.getDocumentStatus().equals("CO")) {
									ATECOFF_Operacion_Auxiliares.pintance_facInv(invDato.getId(),
											invDato.getCreatedBy().getId(), invDato.getClient().getId(),
											invDato.getOrganization().getId(), conn);
								}
								bundle.setResult(this.msg);
								return;
							} else {
								bundle.setResult(this.msg);
								return;
							}
						} else { /* Liquidacion de compra NPI */
							if (ATECOFF_GenerarLiquidacionXML.generarFacturaXMLLiq(invDato, conn, varsAux.getUser(),
									false, this.msg)) {
								if (!invDato.getDocumentStatus().equals("CO")) {
									ATECOFF_Operacion_Auxiliares.pintance_facInv(invDato.getId(),
											invDato.getCreatedBy().getId(), invDato.getClient().getId(),
											invDato.getOrganization().getId(), conn);
								}
								bundle.setResult(this.msg);
								return;
							} else {
								bundle.setResult(this.msg);
								return;
							}
						}
					} else {
						bundle.setResult(this.msg);
						return;
					}
				} else {
					if (!invDato.getDocumentStatus().equals("CO")) {
						Calendar cldFecha = new GregorianCalendar();
						SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
						String strFechaAut = "";

						if (invDato.getCoVencimientoAutSri() == null) {
							strFechaAut = sdfFormato.format(cldFecha.getTime());
						} else {
							strFechaAut = sdfFormato.format(invDato.getCoVencimientoAutSri());
						}

						ATECOFFGenerarXmlData.methodActualizarInvo(conn, null, null, null, "PD", "NE", "PD", "NE",
								invDato.getCoNroAutSri(), "", strFechaAut, strCinvoiceID);

						ATECOFF_Operacion_Auxiliares.pintance_facInv(invDato.getId(), invDato.getCreatedBy().getId(),
								invDato.getClient().getId(), invDato.getOrganization().getId(), conn);

						msg.setType("Success");
						msg.setTitle("Mensaje");
						msg.setMessage("Documento Procesado");
					} else {
						msg.setType("Error");
						this.msg.setTitle("@Error@");
						msg.setMessage("Documento Ya Procesado");
					}

					bundle.setResult(this.msg);
					return;
				}
			}

			} else if (bundle.getParams().get("CO_Retencion_Compra_ID") != null) {
				strCoRetencionCompraID = bundle.getParams().get("CO_Retencion_Compra_ID").toString();

				CO_Retencion_Compra crcDato = OBDal.getInstance().get(CO_Retencion_Compra.class,
						strCoRetencionCompraID);

				axmlDat = ATECOFFGenerarXmlData.methodFacturaElect(conn, crcDato.getDocumentType().getId());

				invDato = OBDal.getInstance().get(Invoice.class, crcDato.getInvoice().getId());

				if (!invDato.getDocumentNo().toString().matches("[0-9]*")) {
					msg.setType("Error");
					msg.setMessage("El N° de la factura solo tiene que contener números");
					msg.setTitle("@Error@");
					OBDal.getInstance().rollbackAndClose();
					bundle.setResult(msg);
					return;
				}

				if (!crcDato.getDocumentNo().toString().matches("[0-9]*")) {
					msg.setType("Error");
					msg.setMessage("El N° del documento solo tienen que contener números");
					msg.setTitle("@Error@");
					OBDal.getInstance().rollbackAndClose();
					bundle.setResult(msg);
					return;
				}

				if (invDato.getDocumentStatus().equals("CO")) {
					if (axmlDat[0].dato1.equals("Y")) {
						if (verificarConfiguración("", strCoRetencionCompraID, "", conn)) {
							if (ATECOFF_GenerarRetencionXML.generarFacturaXMLRet(crcDato, conn, varsAux.getUser(),
									false, this.msg)) {
								if (!crcDato.getDocstatus().equals("CO")) {
									ATECOFF_Operacion_Auxiliares.pintance_facRet(crcDato.getId(),
											crcDato.getCreatedBy().getId(), crcDato.getClient().getId(),
											crcDato.getOrganization().getId(), conn);
								}

								bundle.setResult(this.msg);
								return;
							} else {
								bundle.setResult(this.msg);
								return;
							}
						} else {
							bundle.setResult(this.msg);
							return;
						}
					} else {
						if (!crcDato.getDocstatus().equals("CO")) {
							Calendar cldFecha = new GregorianCalendar();
							SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
							String strFechaAut = "";

							if (invDato.getCoVencimientoAutSri() == null) {
								strFechaAut = sdfFormato.format(cldFecha.getTime());
							} else {
								strFechaAut = sdfFormato.format(invDato.getCoVencimientoAutSri());
							}

							ATECOFFGenerarXmlData.methodActualizarReten(conn, null, null, null, "PD", "NE", "PD", "NE",
									crcDato.getNoAutorizacin(), strFechaAut, strCoRetencionCompraID);

							ATECOFF_Operacion_Auxiliares.pintance_facRet(crcDato.getId(),
									crcDato.getCreatedBy().getId(), crcDato.getClient().getId(),
									crcDato.getOrganization().getId(), conn);

							msg.setType("Success");
							msg.setTitle("Mensaje");
							msg.setMessage("Documento Procesado");
						} else {
							msg.setType("Error");
							this.msg.setTitle("@Error@");
							msg.setMessage("Documento Ya Procesado");
						}

						bundle.setResult(this.msg);
						return;
					}
				} else {
					msg.setType("Error");
					msg.setMessage("Primero tiene que completar la factura para completar la retención");
					msg.setTitle("@Error@");
					OBDal.getInstance().rollbackAndClose();
					bundle.setResult(msg);
					return;
				}
			} else if (bundle.getParams().get("M_InOut_ID") != null) {
				strMinoutID = bundle.getParams().get("M_InOut_ID").toString();

				ShipmentInOut spiDato = OBDal.getInstance().get(ShipmentInOut.class, strMinoutID);

				axmlDat = ATECOFFGenerarXmlData.methodFacturaElect(conn, spiDato.getDocumentType().getId());

				if (axmlDat[0].dato1.equals("Y")) {
					if (verificarConfiguración("", "", strMinoutID, conn)) {
						if (ATECOFF_GenerarGuiaDespachoXML.generarFacturaXMLGre(spiDato, conn, varsAux.getUser(), false,
								this.msg)) {
							bundle.setResult(this.msg);
							return;
						} else {
							bundle.setResult(this.msg);
							return;
						}
					} else {
						bundle.setResult(this.msg);
						return;
					}
				}

				bundle.setResult(this.msg);
				return;
			}
		} catch (final OBException e) {
			// TODO: handle exception
			this.msg.setType("Error");
			this.msg.setMessage(e.getMessage());
			this.msg.setTitle("@Error@");
			bundle.setResult(this.msg);
		} finally {
			axmlLineas = null;
			axmlDat = null;
			invDato = null;
			OBDal.getInstance().rollbackAndClose();
		}

	}

	/**
	 * @param strCinvoice
	 * @param strCoRetencionCompra
	 * @param strMinout
	 * @param conn
	 * @return
	 * @throws ServletException
	 */

	private boolean verificarConfiguración(String strCinvoice, String strCoRetencionCompra, String strMinout,
			ConnectionProvider conn) throws ServletException {

		ATECOFFGenerarXmlData[] axmlConf = null;

		try {
			if (!strCinvoice.equals("")) {
				axmlConf = ATECOFFGenerarXmlData.methodSeleccionarConfiguracion(conn, strCinvoice);
			} else if (!strCoRetencionCompra.equals("")) {
				axmlConf = ATECOFFGenerarXmlData.methodSeleccionarConfiguracionReCompra(conn, strCoRetencionCompra);
			} else if (!strMinout.equals("")) {
				axmlConf = ATECOFFGenerarXmlData.methodSeleccionarConfiguracionMinout(conn, strMinout);
			}

			if (axmlConf != null && axmlConf.length == 1) {
				if (axmlConf[0].dato2 == null || axmlConf[0].dato2.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay dirección del servidor de consultas");
					msg.setTitle("@Error@");
					return false;
				} else if (axmlConf[0].dato3 == null || axmlConf[0].dato3.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay un nombre de la base de datos del servidor de consultas");
					msg.setTitle("@Error@");
					return false;
				} else if (axmlConf[0].dato4 == null || axmlConf[0].dato4.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay un puerto de la base de datos del servidor de consultas");
					msg.setTitle("@Error@");
					return false;
				} else if (axmlConf[0].dato5 == null || axmlConf[0].dato5.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay un usuario de la base de datos del servidor de consultas");
					msg.setTitle("@Error@");
					return false;
				} else if (axmlConf[0].dato6 == null || axmlConf[0].dato6.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay una contraseña de la base de daros del servidor de consultas");
					msg.setTitle("@Error@");
					return false;
				} else if (axmlConf[0].dato7 == null || axmlConf[0].dato7.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay un CI/RUC del cliente en el documento");
					msg.setTitle("@Error@");
					return false;
				} else if (axmlConf[0].dato8 == null || axmlConf[0].dato8.equals("")) {
					msg.setType("Error");
					msg.setMessage("No hay un email en el cliente para enviar el documento electronico");
					msg.setTitle("@Error@");
					return false;
				} else if (validarEmail(axmlConf[0].dato8)) {
					msg.setType("Error");
					msg.setMessage("El email del cliente no es valido");
					msg.setTitle("@Error@");
					return false;
				} else {
					return true;
				}
			} else {
				this.msg.setType("Error");
				this.msg.setMessage("No se ha encontrado una configuración del servidor de facturación electrónica");
				this.msg.setTitle("@Error@");
				return false;
			}
		} finally {
			axmlConf = null;
		}

	}

	/**
	 * @param strEmail
	 * @return
	 */
	private boolean validarEmail(String strEmail) {
		Pattern pattern = Pattern.compile(this.PATTERN_EMAIL);
		Matcher matcher = pattern.matcher(strEmail);

		return !matcher.matches();
	}
}
