package com.atrums.offline.feletronica.process;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.scheduling.Process;
import org.openbravo.scheduling.ProcessBundle;

public class ATECOFF_EnviarXML implements Process {

	private static final Logger log = Logger.getLogger(ATECOFF_EnviarXML.class);

	static Logger log4j = Logger.getLogger(ATECOFF_EnviarXML.class);
	final OBError msg = new OBError();

	ATECOFF_Funciones_Aux opeaux = new ATECOFF_Funciones_Aux();
	String type = null;
	boolean resultado;

	@Override
	public void execute(ProcessBundle bundle) throws Exception {
		// TODO Auto-generated method stub
		log.info("Proceso enviar documentos.");
		ConnectionProvider conn = bundle.getConnection();
		VariablesSecureApp varsAux = bundle.getContext().toVars();

		OBContext.setOBContext(varsAux.getUser(), varsAux.getRole(), varsAux.getClient(), varsAux.getOrg());

		String strCinvoiceID = "";
		String strCoRetencionCompraID = "";
		String strMinoutID = "";

		if (bundle.getParams().get("C_Invoice_ID") != null) {
			strCinvoiceID = bundle.getParams().get("C_Invoice_ID").toString();
		}

		if (bundle.getParams().get("CO_Retencion_Compra_ID") != null) {
			strCoRetencionCompraID = bundle.getParams().get("CO_Retencion_Compra_ID").toString();
		}

		if (bundle.getParams().get("M_InOut_ID") != null) {
			strMinoutID = bundle.getParams().get("M_InOut_ID").toString();
		}

		if (!strCinvoiceID.equals("") && strCinvoiceID != null && !strCinvoiceID.equals("null")) {
			resultado = imprXML(conn, strCinvoiceID);
		} else if (!strCoRetencionCompraID.equals("") && strCoRetencionCompraID != null
				&& !strCoRetencionCompraID.equals("null")) {
			resultado = imprXMLRet(conn, strCoRetencionCompraID);
		} else if (!strMinoutID.equals("") && strMinoutID != null && !strMinoutID.equals("null")) {
			resultado = imprXMLGuia(conn, strMinoutID);
		} else {
			resultado = false;
		}
		
	if (resultado == true) {
		msg.setType("Success");
		msg.setTitle("Mensaje");
		msg.setMessage("Documentos Enviados.");
	} else {
		msg.setType("Error");
		this.msg.setTitle("@Error@");
		msg.setMessage("No se logro enviar los documentos, por favor verificar.");
	}

	bundle.setResult(this.msg);
	return;

	}

	public boolean imprXML(ConnectionProvider connectionProvider, String strInvoice) throws Exception {

		byte[] btyDoc = null;
		ATECOFFGenerarXmlData[] ateData;
		File flPdf = null;
		String strContenido = "";

		ateData = ATECOFFGenerarXmlData.methodSeleccionarFact(connectionProvider, strInvoice);
		
		type = "text/html; charset=utf-8";
		strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
        		"	<tr style=\"background-color: #003764\">\r\n" + 
        		"		<td style=\"width: 85%\">\r\n" + 
        		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
        		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
        		"			</a></td>\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">	\r\n" + 
        		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
        		"			</a></td>	\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
        		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
        		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
        		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
        		"				<p><b>"+ ateData[0].dato6 +"</b></p>\r\n" + 
        		"				<p>Usted tiene una "+ ateData[0].dato4 +", generada por la empresa <b>"+ ateData[0].dato7 +".</b></p>\r\n" + 
        		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
        		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
        		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
        		"				</div>	\r\n" + 
        		"				<div style=\"font-size: 12px\">\r\n" + 
        		"					<p style=\"margin-top:50px\">\r\n" + 
        		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
        		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Recuerde que el documento electrónico cumple con todas las disposiciones establecidas en el marco legal vigente y sustituye al documento en formato impreso con igual valor legal.<br> \r\n" + 
        		"					   Le recomendamos no imprimir este correo electrónico a menos que sea estrictamente necesario.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Por favor, no responda a este correo electrónico.\r\n" + 
        		"					<p>\r\n" + 
        		"				</div>\r\n" + 
        		"			</div>\r\n" + 
        		"		  </div>\r\n" + 
        		"		</td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"	  <td colspan=\"6\">\r\n" + 
        		"		<table style=\"width: 100%\">\r\n" + 
        		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
        		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
        		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
        		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
        		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div style=\"font-size: 12px\">\r\n" + 
        		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
        		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
        		"						</div>\r\n" + 
        		"					</div>\r\n" + 
        		"				</td>\r\n" + 
        		"			</tr>\r\n" + 
        		"		</table>\r\n" + 
        		"	  </td>\r\n" + 
        		"	</tr>\r\n" + 
        		"</table>";

		

		if (ateData[0].dato5.equals("1") || ateData[0].dato5.equals("01")) {
			if (ateData[0].dato8.equals("77ACE2A682094931A861E3CB12C881D7")){
				flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
						"@basedesign@/com/atrums/felectronica/erpReport/JuanPabloEspinosa/Rpt_Factura.jrxml", "Factura_Venta", strInvoice);
			}else {
				flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
						"@basedesign@/com/atrums/felectronica/erpReport/Rpt_Factura.jrxml", "Factura_Venta", strInvoice);
			}
		} else if (ateData[0].dato5.equals("4") || ateData[0].dato5.equals("04")) {
			flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
					"@basedesign@/com/atrums/felectronica/erpReport/Rpt_NotaCredito.jrxml", "Nota_Credito", strInvoice);
		} else if (ateData[0].dato5.equals("5") || ateData[0].dato5.equals("05")) {
			flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
					"@basedesign@/com/atrums/felectronica/erpReport/Rpt_NotaDebito.jrxml", "Nota_Debito", strInvoice);
		}else if (ateData[0].dato5.equals("3") || ateData[0].dato5.equals("03")) {
			flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
					"@basedesign@/com/atrums/felectronica/erpReport/Rpt_Liquidacion_Compra.jrxml", "Liquidacion_Compra", strInvoice);
		}
		
		if (ateData.length > 0) {
			btyDoc = ateData[0].dato1.getBytes();
		}

		File flDocXML = null;
		List<File> lisdoc = new ArrayList<File>();

		if (btyDoc != null) {

			flDocXML = opeaux.bytetofile(btyDoc);
			if (flDocXML != null) {
			
				lisdoc.add(flDocXML);
				lisdoc.add(flPdf);
				
			}
		}
		
		if (ATECOFF_Operacion_Auxiliares.enviarCorreo(ateData[0].dato3.toString(), ateData[0].dato4.toString(),
				strContenido, type, lisdoc, false)) {
			flDocXML.delete();
			flPdf.delete();
		    return true;
		}else {
			return false;
		} 
	}

	public boolean imprXMLRet(ConnectionProvider connectionProvider, String strRetencio) throws Exception {

		byte[] btyDoc = null;
		ATECOFFGenerarXmlData[] ateData;
		File flPdf = null;
		String strContenido = "";
		
		ateData = ATECOFFGenerarXmlData.methodSeleccionarRete(connectionProvider, strRetencio);
		
		type = "text/html; charset=utf-8";
		strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
        		"	<tr style=\"background-color: #003764\">\r\n" + 
        		"		<td style=\"width: 85%\">\r\n" + 
        		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
        		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
        		"			</a></td>\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">	\r\n" + 
        		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
        		"			</a></td>	\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
        		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
        		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
        		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
        		"				<p><b>"+ ateData[0].dato6 +"</b></p>\r\n" + 
        		"				<p>Usted tiene una "+ ateData[0].dato4 +", generada por la empresa <b>"+ ateData[0].dato7 +".</b></p>\r\n" + 
        		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
        		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
        		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
        		"				</div>	\r\n" + 
        		"				<div style=\"font-size: 12px\">\r\n" + 
        		"					<p style=\"margin-top:50px\">\r\n" + 
        		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
        		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Recuerde que el documento electrónico cumple con todas las disposiciones establecidas en el marco legal vigente y sustituye al documento en formato impreso con igual valor legal.<br> \r\n" + 
        		"					   Le recomendamos no imprimir este correo electrónico a menos que sea estrictamente necesario.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Por favor, no responda a este correo electrónico.\r\n" + 
        		"					<p>\r\n" + 
        		"				</div>\r\n" + 
        		"			</div>\r\n" + 
        		"		  </div>\r\n" + 
        		"		</td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"	  <td colspan=\"6\">\r\n" + 
        		"		<table style=\"width: 100%\">\r\n" + 
        		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
        		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
        		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
        		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
        		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div style=\"font-size: 12px\">\r\n" + 
        		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
        		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
        		"						</div>\r\n" + 
        		"					</div>\r\n" + 
        		"				</td>\r\n" + 
        		"			</tr>\r\n" + 
        		"		</table>\r\n" + 
        		"	  </td>\r\n" + 
        		"	</tr>\r\n" + 
        		"</table>";

		
			flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
					"@basedesign@/com/atrums/felectronica/erpReport/Rpt_Retenciones.jrxml", "Retencion_Compra", strRetencio);

		
		if (ateData.length > 0) {
			btyDoc = ateData[0].dato1.getBytes();
		}

		File flDocXML = null;
		List<File> lisdoc = new ArrayList<File>();

		if (btyDoc != null) {

			flDocXML = opeaux.bytetofile(btyDoc);
			if (flDocXML != null) {
				lisdoc.add(flDocXML);
				lisdoc.add(flPdf);
			}
		}

		if(ATECOFF_Operacion_Auxiliares.enviarCorreo(ateData[0].dato3.toString(), ateData[0].dato4.toString(),
				strContenido, type, lisdoc, false)) {
        flDocXML.delete();
        flPdf.delete();
        	return true;
		}else {
			return false;
		}	

	}

	public boolean imprXMLGuia(ConnectionProvider connectionProvider, String strGuia) throws Exception {

		byte[] btyDoc = null;
		ATECOFFGenerarXmlData[] ateData;
		File flPdf = null;
		String strContenido = "";

		ateData = ATECOFFGenerarXmlData.methodSeleccionarGuia(connectionProvider, strGuia);
		
		
		type = "text/html; charset=utf-8";
		strContenido = "<table style=\"width: 85%; padding: 10px; margin:0 auto; border-collapse: collapse;font-family: sans-serif\">\r\n" + 
        		"	<tr style=\"background-color: #003764\">\r\n" + 
        		"		<td style=\"width: 85%\">\r\n" + 
        		"		    <a target=\"_blank\" href=\"http://atrums.com/\">\r\n" + 
        		"			   <img width=\"20%\" style=\"display:block; margin: 1.5% 3%\" src=\"http://ws.atrums.com/portal/images/publicidad/atrumsit-logo.png\">\r\n" + 
        		"			</a></td>\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.facebook.com/Atrumsit-393603567398708/\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/facebook.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.instagram.com/atrumsit/?hl=es-la\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/instagram.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">		\r\n" + 
        		"			<a target=\"_blank\" href=\"https://www.youtube.com/channel/UCQ6Vl9DHMH3NNa93HxsqAUw?view_as=subscriber\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/youtube.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"        <td style=\"width: 3%\">	\r\n" + 
        		"			<a target=\"_blank\" href=\"https://twitter.com/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/twitter.png\"/>\r\n" + 
        		"			</a></td>	\r\n" + 
        		"		<td style=\"width: 3%\">\r\n" + 
        		"			<a target=\"_blank\" href=\"https://au.linkedin.com/company/atrumsit\">\r\n" + 
        		"				<img style=\"width: 20px; margin: 5px\" src=\"http://ws.atrums.com/portal/images/publicidad/linkedin.png\"/>\r\n" + 
        		"			</a></td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"		<td style=\"background-color: #ECF0F1\" colspan=\"6\">\r\n" + 
        		"		  <div style=\"border: 6px solid #FFFFFF\">\r\n" + 
        		"			<div style=\"color: #34495e; margin: 2% 6% 2%; text-align: justify;font-size: 14px\">\r\n" + 
        		"				<h2 style=\"color: #8DC63F\">Estimad@:</h2>\r\n" + 
        		"				<p><b>"+ ateData[0].dato6 +"</b></p>\r\n" + 
        		"				<p>Usted tiene una "+ ateData[0].dato4 +", generada por la empresa <b>"+ ateData[0].dato7 +".</b></p>\r\n" + 
        		"				<div style=\"width: 100%;text-align: center;margin-top: 38px\">\r\n" + 
        		"					Si quiere conocer más de nuestros servicios, haga clic aquí<br><br>\r\n" + 
        		"					<a style=\"text-decoration: none; border-radius: 5px; padding: 8px 18px; color: white; background-color: #8DC63F\" target=\"_blank\" href=\"http://atrums.com/\">ATRUMS IT</a>	\r\n" + 
        		"				</div>	\r\n" + 
        		"				<div style=\"font-size: 12px\">\r\n" + 
        		"					<p style=\"margin-top:50px\">\r\n" + 
        		"					   La información y archivos adjuntos contenidos en este mensaje electrónico son confidenciales y reservados; por tanto no pueden ser usados, reproducidos o divulgados por otras personas distintas a su(s) destinatario(s). \r\n" + 
        		"					   Si Ud. no es el destinatario de este email, le solicitamos comedidamente eliminarlo.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Recuerde que el documento electrónico cumple con todas las disposiciones establecidas en el marco legal vigente y sustituye al documento en formato impreso con igual valor legal.<br> \r\n" + 
        		"					   Le recomendamos no imprimir este correo electrónico a menos que sea estrictamente necesario.\r\n" + 
        		"					</p>\r\n" + 
        		"					<p>\r\n" + 
        		"					   Por favor, no responda a este correo electrónico.\r\n" + 
        		"					<p>\r\n" + 
        		"				</div>\r\n" + 
        		"			</div>\r\n" + 
        		"		  </div>\r\n" + 
        		"		</td>\r\n" + 
        		"	</tr>\r\n" + 
        		"	<tr>\r\n" + 
        		"	  <td colspan=\"6\">\r\n" + 
        		"		<table style=\"width: 100%\">\r\n" + 
        		"		    <tr style=\"color: #FFFFFF;background-color: #003764;height: 40px\">\r\n" + 
        		"				<td style = \"width: 100%;font-size: 13px\">\r\n" + 
        		"				    <div style=\"width:98%;margin: 1% 2% 1%\">\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/ubicacion.png\"/>\r\n" + 
        		"							 <a target=\"_blank\" href=\"http://atrums.com/\" style=\"color: #FFFFFF\">www.atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div><img style=\"width: 14px;\" src=\"http://ws.atrums.com/portal/images/publicidad/email.png\"/>\r\n" + 
        		"							 <a style=\"text-decoration: none;color: #FFFFFF\">info@atrums.com</a>\r\n" + 
        		"						</div>\r\n" + 
        		"						<div style=\"font-size: 12px\">\r\n" + 
        		"							 <img style=\"width: 10px;\" src=\"http://ws.atrums.com/portal/images/publicidad/telefono.png\"/>\r\n" + 
        		"							 022247848 | 022241461 | <a target=\"_blank\" href=\"https://web.whatsapp.com/\" style=\"color: #FFFFFF\">0958743792</a>\r\n" + 
        		"						</div>\r\n" + 
        		"					</div>\r\n" + 
        		"				</td>\r\n" + 
        		"			</tr>\r\n" + 
        		"		</table>\r\n" + 
        		"	  </td>\r\n" + 
        		"	</tr>\r\n" + 
        		"</table>";

		
			flPdf = ATECOFF_Operacion_Auxiliares.generarPDF(connectionProvider,
					"@basedesign@/com/atrums/felectronica/erpReport/Rpt_Guia.jrxml", "Guia_Remision", strGuia);

		
		if (ateData.length > 0) {
			btyDoc = ateData[0].dato1.getBytes();
		}

		File flDocXML = null;
		List<File> lisdoc = new ArrayList<File>();

		if (btyDoc != null) {

			flDocXML = opeaux.bytetofile(btyDoc);
			if (flDocXML != null) {
				
				lisdoc.add(flDocXML);
				lisdoc.add(flPdf);
				
			}
		}

		if(ATECOFF_Operacion_Auxiliares.enviarCorreo(ateData[0].dato3.toString(), ateData[0].dato4.toString(),
				strContenido, type, lisdoc, false)) {
			flDocXML.delete();
			flPdf.delete();
			return true;
		}else {
			return false;
		}

	}

}
