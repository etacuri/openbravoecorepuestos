package com.atrums.offline.feletronica.process;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.ServletException;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.SAXReader;
import org.dom4j.io.XMLWriter;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.email.EmailUtils;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.poc.EmailManager;
import org.openbravo.exception.NoConnectionAvailableException;
import org.openbravo.model.common.enterprise.EmailServerConfiguration;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.utils.FormatUtilities;

import com.atrums.felectronica.data.ATECFE_conf_firma;
import com.atrums.felectronica.process.ATECFE_XAdESBESSignature;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

public class ATECOFF_Operacion_Auxiliares {
  private static Logger log = Logger.getLogger(ATECOFF_Operacion_Auxiliares.class);

  /**
   * @param elmNodo
   * @param strAmbiente
   * @param strCompAutoSRI
   * @param strRu
   * @param NumEst
   * @param strPunEmis
   * @param strSecu
   * @param strCodNum
   * @param strTipEm
   * @param strFecha
   * @param strDireccion
   * @param strRazonSoc
   * @param strNombrCom
   * @param msg
   * @param hstClave
   * @return
   */

  public static boolean generarCabecera(Element elmNodo, String strAmbiente, String strCompAutoSRI,
      String strRu, String NumEst, String strPunEmis, String strSecu, String strCodNum,
      String strTipEm, String strFecha, String strDireccion, String strRazonSoc, String strNombrCom,
      String strRegimen, String strAgente,
      OBError msg, Hashtable<String, String> hstClave) {

    if (strAmbiente != null) {
      if (strAmbiente.length() == 1) {
        elmNodo.addElement("ambiente").addText(strAmbiente);
      } else {
        msg.setType("Error");
        msg.setMessage(
            "El ambiente para el documento tiene que ser <1> de pruebas  o <2> de producción");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage(
          "El ambiente para el documento tiene que ser <1> de pruebas  o <2> de producción");
      msg.setTitle("@Error@");
      return false;
    }

    String strTipoComp = "";

    if (strCompAutoSRI != null) {
      if (strCompAutoSRI.length() <= 2) {
        strTipoComp = strCompAutoSRI.toString();
        if (strTipoComp.length() == 1) {
          strTipoComp = "0" + strTipoComp;
        }
      } else {
        msg.setType("Error");
        msg.setMessage("El tipo de comprobante es de 2 dígitos");
        msg.setTitle("@Error@");
        return false;
      }

    } else {
      msg.setType("Error");
      msg.setMessage("El tipo de comprobante es de 2 dígitos");
      msg.setTitle("@Error@");
      return false;
    }

    String strRuc = "";

    if (strRu != null) {
      if (strRu.length() == 13) {
        strRuc = strRu;
      } else {
        msg.setType("Error");
        msg.setMessage("El ruc es de 13 dígitos");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage("El ruc es de 13 dígitos");
      msg.setTitle("@Error@");
      return false;
    }

    String strSerie = "";

    if (NumEst != null && strPunEmis != null) {
      if (NumEst.length() <= 3 && strPunEmis.length() <= 3) {

        for (int i = 0; i < (3 - NumEst.length()); i++) {
          strSerie = strSerie + "0";
        }

        strSerie = strSerie + NumEst;

        for (int i = 0; i < (3 - strPunEmis.length()); i++) {
          strSerie = strSerie + "0";
        }

        strSerie = strSerie + strPunEmis;

      } else {
        msg.setType("Error");
        msg.setMessage("El punto de emisión y establecimiento son de 3 dígitos");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage("El punto de emisión y establecimiento son de 3 dígitos");
      msg.setTitle("@Error@");
      return false;
    }

    String strSecuencial = "";

    if (strSecu != null) {
      if (strSecu.length() <= 9) {
        for (int i = 0; i < (9 - strSecu.length()); i++) {
          strSecuencial = strSecuencial + "0";
        }
        strSecuencial = strSecuencial + strSecu;
      } else {
        msg.setType("Error");
        msg.setMessage("El secuencial es de 9 dígitos");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage("El secuencial es de 9 dígitos");
      msg.setTitle("@Error@");
      return false;
    }

    String strCodigoNum = "";

    if (strCodNum != null) {
      if (strCodNum.length() <= 8) {
        for (int i = 0; i < (8 - strCodNum.length()); i++) {
          strCodigoNum = strCodigoNum + "0";
        }
        strCodigoNum = strCodigoNum + strCodNum;
      } else {
        msg.setType("Error");
        msg.setMessage("El código numerico es de 8 dígitos");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage("El código numerico es de 8 dígitos");
      msg.setTitle("@Error@");
      return false;
    }

    if (strTipEm != null) {
      if (strTipEm.length() == 1) {
        elmNodo.addElement("tipoEmision").addText(strTipEm);
      } else {
        msg.setType("Error");
        msg.setMessage("El tipo de emisión es de 1 solo dígito");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage("El tipo de emisión es de 1 solo dígito");
      msg.setTitle("@Error@");
      return false;
    }

    if (strFecha != null) {
      if (strFecha.length() == 8) {
        hstClave.put("claveacc", generarclaveacceso(strFecha, strTipoComp, strRuc, strAmbiente,
            strSerie, strSecuencial, strCodigoNum, strTipEm));
      } else {
        msg.setType("Error");
        msg.setMessage("El documento tiene que tener una fecha de emisión");
        msg.setTitle("@Error@");
        return false;
      }
    } else {
      msg.setType("Error");
      msg.setMessage("El documento tiene que tener una fecha de emisión");
      msg.setTitle("@Error@");
      return false;
    }

    elmNodo.addElement("razonSocial").addText(strRazonSoc);

    if (!strNombrCom.equals("")) {
      elmNodo.addElement("nombreComercial").addText(strNombrCom);
    }

    elmNodo.addElement("ruc").addText(strRuc);
    elmNodo.addElement("claveAcceso").addText(hstClave.get("claveacc"));
    elmNodo.addElement("codDoc").addText(strTipoComp);
    elmNodo.addElement("estab").addText(NumEst);
    elmNodo.addElement("ptoEmi").addText(strPunEmis);
    elmNodo.addElement("secuencial").addText(strSecuencial);
    elmNodo.addElement("dirMatriz").addText(strDireccion);
    
    if (!strRegimen.equals("")) {
    	strRegimen = strRegimen.replace('\u00c9', 'É');
    	elmNodo.addElement("regimenMicroempresas").addText(strRegimen);
    }
    
    if (!strAgente.equals("")) {
        elmNodo.addElement("agenteRetencion").addText(strAgente);
    }

    return true;
  }

  /**
   * @param strFecha
   * @param strTipoComprobante
   * @param strRuc
   * @param strTipoAmbiente
   * @param strSerie
   * @param strSecuencial
   * @param strCodigoNum
   * @param strTipoEmision
   * @return
   */
  public static String generarclaveacceso(String strFecha, String strTipoComprobante, String strRuc,
      String strTipoAmbiente, String strSerie, String strSecuencial, String strCodigoNum,
      String strTipoEmision) {

    String strClave = strFecha + strTipoComprobante + strRuc + strTipoAmbiente + strSerie
        + strSecuencial + strCodigoNum + strTipoEmision;

    int[] intClaveAux = new int[48];

    for (int i = 0; i < strClave.length(); i++) {
      intClaveAux[47 - i] = Integer.parseInt(strClave.substring(i, i + 1));
    }

    int intAcumulado = 0;
    int j = 2;

    for (int i = 0; i < intClaveAux.length; i++) {
      intAcumulado = intAcumulado + (intClaveAux[i] * j);
      if (j == 7) {
        j = 1;
      }
      j++;
    }

    Integer strNumVer = 11 - (intAcumulado % 11);

    if (strNumVer >= 10)
      strNumVer = 11 - strNumVer;

    strClave = strClave + strNumVer.toString();

    return strClave;
  }

  /**
   * @param strPalabras
   * @return
   */
  public static String normalizacionPalabras(String strPalabras) {

    String strPalabraAux = "";

    strPalabraAux = strPalabras.replaceAll("á", "a");
    strPalabraAux = strPalabraAux.replaceAll("é", "e");
    strPalabraAux = strPalabraAux.replaceAll("í", "i");
    strPalabraAux = strPalabraAux.replaceAll("ó", "o");
    strPalabraAux = strPalabraAux.replaceAll("ú", "u");
    return strPalabraAux;
  }

  /**
   * @param elmNodo
   * @param conn
   * @param strAdClient
   * @throws ServletException
   * @throws UnsupportedEncodingException
   */
  public static void addCamposAdic(Element elmNodo, ConnectionProvider conn, String strAdClient, String strCInvoice)
      throws ServletException, UnsupportedEncodingException {
    String strDato = "dato";
    ATECOFFGenerarXmlData[] axmlInfAdici = null;

    try {
      axmlInfAdici = ATECOFFGenerarXmlData.methodSeleccionarAdicionalDesc(conn, strAdClient, strCInvoice);

      if (axmlInfAdici != null && axmlInfAdici.length > 0) {
        final Element elminfAdc = elmNodo.addElement("infoAdicional");

        for (int i = 1; i <= Integer.parseInt(axmlInfAdici[0].dato1); i++) {

          String utfCampoAdicional = new String(
              axmlInfAdici[0].getField(strDato + (i * 2)).getBytes("ISO-8859-1"), "utf-8");

          String utfInfo = new String(
              axmlInfAdici[0].getField(strDato + ((i * 2) + 1)).getBytes("ISO-8859-1"), "utf-8");


          if (!utfInfo.equals(null) && !utfInfo.equals("")) {
              elminfAdc.addElement("campoAdicional")
                  .addAttribute("nombre", normalizacionPalabras(utfCampoAdicional))
                  .addText(normalizacionPalabras(utfInfo));
              }
        }
      }
    } finally {
      axmlInfAdici = null;
    }
  }

  /**
   * @param flDoc
   * @param conn
   * @param strUse
   * @param msg
   * @return
   * @throws Exception
   */
  public static File firmarDocumento(File flDoc, ConnectionProvider conn, String strUse,String strCli,
      OBError msg, boolean enviarSRI) throws Exception {
    File flDocumentoFirmado = null;
    ATECOFFGenerarXmlData[] axmlConFir = null;

    try {
      axmlConFir = ATECOFFGenerarXmlData.methodSeleccionarFirma(conn, strUse, strCli);

      if (!enviarSRI && axmlConFir != null && axmlConFir.length > 0) {
        return flDoc;
      }

      if (axmlConFir != null && axmlConFir.length > 0) {
        ATECFE_conf_firma acfData = OBDal.getInstance().get(ATECFE_conf_firma.class,
            axmlConFir[0].dato1);

        String strPass = FormatUtilities.encryptDecrypt(acfData.getClaveContra(), false);
        String strDire = acfData.getDIRArchivoFirma();
        String strNArc = acfData.getNombreArchivo();

        flDocumentoFirmado = ATECFE_XAdESBESSignature.firmar(flDoc,
            strDire + File.separator + strNArc, strPass);

        return flDocumentoFirmado;
      } else {
    	  System.out.println("Usuario Offline: " + strUse+" Empresa: "+strCli);
        msg.setType("Error");
        msg.setMessage("El usuario no tiene permisos para firmar digitalmente el documento");
        msg.setTitle("@Error@");

        if (flDoc != null) {
          flDoc.delete();
        }

        return null;
      }
    } finally {
      axmlConFir = null;
    }
  }

  /**
   * @param flFile
   * @return
   * @throws IOException
   */
  public static byte[] filetobyte(File flFile) throws IOException {
    byte[] bytes = new byte[(int) flFile.length()];

    FileInputStream is = new FileInputStream(flFile);

    is.read(bytes);
    is.close();

    byte[] bytesen = Base64.encodeBase64(bytes);
    return bytesen;
  }

  /**
   * @param bytes
   * @return
   * @throws IOException
   */
  public static File bytetofile(byte[] bytes) throws IOException {
    File fldocumen = File.createTempFile("documento", ".xml", null);
    fldocumen.deleteOnExit();
    byte[] bytesde = Base64.decodeBase64(bytes);

    FileOutputStream os = new FileOutputStream(fldocumen);

    os.write(bytesde);
    os.close();

    return fldocumen;
  }

  /**
   * @param conn
   * @param strBaseDesin
   * @param strNombre
   * @param strEntiID
   * @return
   * @throws ServletException
   */
  public static File generarPDF(ConnectionProvider conn, String strBaseDesin, String strNombre,
      String strEntiID) throws ServletException {

    File flTemp = null;

    try {

      String strBaseDesign = strBaseDesin;

      if (!basedesign(strBaseDesign).equals("")) {

        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("DOCUMENT_ID", strEntiID);
        parameters.put("BASE_DESIGN", basedesign(strBaseDesign));

        strBaseDesign = strBaseDesign.replaceAll("@basedesign@", basedesign(strBaseDesign));

        JasperReport jasperRepo = JasperCompileManager.compileReport(strBaseDesign);

        Connection con = conn.getTransactionConnection();

        JasperPrint jasperPrint = JasperFillManager.fillReport(jasperRepo, parameters, con);

        Date date = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("dd.MM.yyyy");
        String strFecha = ft.format(date);
        String strNombreArch = strNombre + "-" + strFecha;
        OutputStream out = null;

        flTemp = File.createTempFile(strNombreArch, ".pdf", null);

        out = new FileOutputStream(flTemp);
        ByteArrayOutputStream byteout = new ByteArrayOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperPrint, out);
        out.write(byteout.toByteArray());
        out.flush();
        out.close();
      }

    } catch (JRException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (NoConnectionAvailableException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (Exception e) {
      // TODO: handle exception
      e.printStackTrace();
    }
    return flTemp;
  }

  /**
   * @param strReporte
   * @return
   */
  public static String basedesign(String strReporte) {

    String strDirectorio = ATECOFF_Operacion_Auxiliares.class.getResource("/").getPath();

    if (strDirectorio.indexOf("/C:") != -1 || strDirectorio.indexOf("/c:") != -1) {
      int intPos = strDirectorio.indexOf("/");

      if (intPos != -1) {
        strDirectorio = strDirectorio.substring(strDirectorio.indexOf("/") + 1);
      }
    }

    String strDirectorioInv = "";

    for (int i = 0; i < strDirectorio.length(); i++) {
      strDirectorioInv = strDirectorio.charAt(i) + strDirectorioInv;
    }

    int intPosicion = strDirectorioInv.indexOf("sessalc/dliub");

    if (intPosicion != -1) {
      strDirectorioInv = strDirectorioInv.substring(intPosicion + 13);
    } else {
      intPosicion = strDirectorioInv.indexOf("sessalc/FNI-BEW");
      if (intPosicion != -1) {
        strDirectorioInv = strDirectorioInv.substring(intPosicion + 15);
      } else {
        return "";
      }
    }

    strDirectorio = "";

    for (int i = 0; i < strDirectorioInv.length(); i++) {
      strDirectorio = strDirectorioInv.charAt(i) + strDirectorio;
    }

    String strBaseDesign = strReporte.replaceAll("@basedesign@",
        (strDirectorio + "WebContent/src-loc/design"));

    File flRep = new File(strBaseDesign);

    if (flRep.exists()) {
      return strDirectorio + "WebContent/src-loc/design";
    } else {
      strBaseDesign = strReporte.replaceAll("@basedesign@", (strDirectorio + "src-loc/design"));

      flRep = new File(strBaseDesign);

      if (flRep.exists()) {
        return strDirectorio + "src-loc/design";
      } else {
        return "";
      }
    }
  }

  /**
   * @param recipient
   * @param subject
   * @param body
   * @param type
   * @param attachments
   * @return
   */
  public static boolean enviarCorreo(final String recipient, final String subject,
      final String body, final String type, final List<File> attachments, boolean soporte) {
    try {
      Organization currenctOrg = OBContext.getOBContext().getCurrentOrganization();
      final EmailServerConfiguration mailConfig = EmailUtils.getEmailConfiguration(currenctOrg);

      if (mailConfig == null) {
        return false;
      }

      final String username = mailConfig.getSmtpServerAccount();
      final String password = FormatUtilities.encryptDecrypt(mailConfig.getSmtpServerPassword(),
          false);
      final String connSecurity = mailConfig.getSmtpConnectionSecurity();
      final int port = mailConfig.getSmtpPort().intValue();
      final String senderAddress = mailConfig.getSmtpServerSenderAddress();
      final String host = mailConfig.getSmtpServer();
      final boolean auth = mailConfig.isSMTPAuthentification();

      String auxRecipient = recipient;

      if (soporte) {
        auxRecipient = username; // username;
      }

      EmailManager.sendEmail(host, auth, username, password, connSecurity, port, senderAddress,
          auxRecipient, null, null, null, subject, body, type, attachments, null, null);
      return true;
    } catch (Exception e) {
      // TODO: handle exception
      log.error(e.getMessage());
      return false;
    }
  }

  /**
   * @param strCinvoice
   * @param strAdUser
   * @param strAdCli
   * @param strAdOrg
   * @param conn
   * @throws ServletException
   */
  public static void pintance_facInv(String strCinvoice, String strAdUser, String strAdCli,
      String strAdOrg, ConnectionProvider conn) throws ServletException {
    ATECOFFGenerarXmlData[] axmlPro = null;

    try {
      String strpInstaID = UUID.randomUUID().toString().replace("-", "").toUpperCase();

      axmlPro = ATECOFFGenerarXmlData.methodSeleccionarProcess(conn, "c_invoice_post0",
          "Process Invoice");

      ATECOFFGenerarXmlData.methodInsertarPInst(conn, strpInstaID, axmlPro[0].dato1, strCinvoice,
          strAdUser, strAdCli, strAdOrg);

      ATECOFFGenerarXmlData.methodEjecutarProcessInvo(conn, strpInstaID, strCinvoice, strCinvoice);
    } finally {
      axmlPro = null;
    }
  }

  /**
   * @param strCinvoice
   * @param strAdUser
   * @param strAdCli
   * @param strAdOrg
   * @param conn
   * @throws ServletException
   */
  public static void pintance_facMinout(String strCMinout, String strAdUser, String strAdCli,
      String strAdOrg, ConnectionProvider conn) throws ServletException {
    ATECOFFGenerarXmlData[] axmlPro = null;

    try {
      String strpInstaID = UUID.randomUUID().toString().replace("-", "").toUpperCase();

      axmlPro = ATECOFFGenerarXmlData.methodSeleccionarProcess(conn, "m_inout_post0",
          "Process Invoice");

      ATECOFFGenerarXmlData.methodInsertarPInst(conn, strpInstaID, axmlPro[0].dato1, strCMinout,
          strAdUser, strAdCli, strAdOrg);

      ATECOFFGenerarXmlData.methodEjecutarProcessGuia(conn, strpInstaID, strCMinout, strCMinout);
    } finally {
      axmlPro = null;
    }
  }

  /**
   * @param strCoRetenId
   * @param strAdUser
   * @param strAdCli
   * @param strAdOrg
   * @param conn
   * @throws ServletException
   */
  public static void pintance_facRet(String strCoRetenId, String strAdUser, String strAdCli,
      String strAdOrg, ConnectionProvider conn) throws ServletException {
    ATECOFFGenerarXmlData[] axmlPro = null;

    try {
      String strpInstaID = UUID.randomUUID().toString().replace("-", "").toUpperCase();

      axmlPro = ATECOFFGenerarXmlData.methodSeleccionarProcess(conn, "co_ejecuta_retencion_compra",
          "Completar Retención Compra");

      ATECOFFGenerarXmlData.methodInsertarPInst(conn, strpInstaID, axmlPro[0].dato1, strCoRetenId,
          strAdUser, strAdCli, strAdOrg);

      ATECOFFGenerarXmlData.methodEjecutarProcessInsepInpara(conn, strpInstaID, strpInstaID);

      ATECOFFGenerarXmlData.methodEjecutarProcessRet(conn, strpInstaID, strCoRetenId);
    } finally {
      axmlPro = null;
    }
  }

  /**
   * @param comprobante
   * @param estado
   * @param numeroAutorizacion
   * @param fechaAutorizacion
   * @param ambiente
   * @return
   */
  public static String stringtostring64Au(String comprobante, String estado,
      String numeroAutorizacion, String fechaAutorizacion, String ambiente) {
    String docXMLFinal = null;

    try {
      File flDoc = File.createTempFile("documento", ".xml", null);

      Document docXML = DocumentHelper.createDocument();
      OutputFormat ofFormat = OutputFormat.createPrettyPrint();
      ofFormat.setEncoding("UTF-8");
      Element elmact = docXML.addElement("autorizacion");
      elmact.addElement("estado").addText(estado);
      elmact.addElement("numeroAutorizacion").addText(numeroAutorizacion);
      elmact.addElement("fechaAutorizacion").addText(fechaAutorizacion);
      elmact.addElement("ambiente").addText(ambiente);

      SAXReader reader = new SAXReader();
      Document docAux = reader.read(new StringReader(comprobante));
      Element elmcom = elmact.addElement("comprobante");
      //elmcom.add(docAux.getRootElement());
      docAux.setXMLEncoding("UTF-8");
	  StringBuffer StrXML = new StringBuffer();
	  elmcom.addCDATA(StrXML.append(docAux.asXML()).toString());//21-04-2021

      XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flDoc), "UTF-8"),
          ofFormat);
      writer.write(docXML);
      writer.flush();
      writer.close();

      byte[] bytes = new byte[(int) flDoc.length()];

      FileInputStream is = new FileInputStream(flDoc);

      is.read(bytes);
      is.close();

      byte[] bytesen = Base64.encodeBase64(bytes);
      docXMLFinal = new String(bytesen, "UTF-8");
      flDoc.delete();
    } catch (DocumentException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
    } catch (UnsupportedEncodingException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
    } catch (FileNotFoundException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
    } catch (IOException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
    }

    return docXMLFinal;
  }

  /**
   * @param comprobante
   * @param estado
   * @param numeroAutorizacion
   * @param fechaAutorizacion
   * @param ambiente
   * @return
   */
  public static File stringtoDocumento(String comprobante, String estado, String numeroAutorizacion,
      String fechaAutorizacion, String ambiente) {
    File flDoc = null;

    try {
      flDoc = File.createTempFile("documento", ".xml", null);

      Document docXML = DocumentHelper.createDocument();
      OutputFormat ofFormat = OutputFormat.createPrettyPrint();
      ofFormat.setEncoding("UTF-8");
      Element elmact = docXML.addElement("autorizacion");
      elmact.addElement("estado").addText(estado);
      elmact.addElement("numeroAutorizacion").addText(numeroAutorizacion);
      elmact.addElement("fechaAutorizacion").addText(fechaAutorizacion);
      elmact.addElement("ambiente").addText(ambiente);

      SAXReader reader = new SAXReader();
      Document docAux = reader.read(new StringReader(comprobante));
      Element elmcom = elmact.addElement("comprobante");
      //elmcom.add(docAux.getRootElement());
      docAux.setXMLEncoding("UTF-8");
	  StringBuffer StrXML = new StringBuffer();
	  elmcom.addCDATA(StrXML.append(docAux.asXML()).toString()); //21-04-2021

      XMLWriter writer = new XMLWriter(new OutputStreamWriter(new FileOutputStream(flDoc), "UTF-8"),
          ofFormat);
      writer.write(docXML);
      writer.flush();
      writer.close();

    } catch (DocumentException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
      flDoc.deleteOnExit();
      flDoc = null;
    } catch (UnsupportedEncodingException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
      flDoc.deleteOnExit();
      flDoc = null;
    } catch (FileNotFoundException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
      flDoc.deleteOnExit();
      flDoc = null;
    } catch (IOException ex) {
      // TODO Auto-generated catch block
      StringWriter stack = new StringWriter();
      ex.printStackTrace(new PrintWriter(stack));
      log.warn(
          "Caught exception; decorating with appropriate status template : " + stack.toString());
      flDoc.deleteOnExit();
      flDoc = null;
    }

    return flDoc;
  }
}
