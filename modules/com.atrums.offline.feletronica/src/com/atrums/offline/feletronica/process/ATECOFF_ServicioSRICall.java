package com.atrums.offline.feletronica.process;

import java.io.IOException;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLStreamHandler;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

import org.apache.log4j.Logger;

public class ATECOFF_ServicioSRICall {
  private static Logger log = Logger.getLogger(ATECOFF_ServicioSRICall.class);
  private String keyCert = "/opt/certificadosri/cacerts";
  private String ambiente = null;
  private String fileString = null;
  private String clave = null;

  public ATECOFF_ServicioSRICall() {
    super();
  }

  public ATECOFF_ServicioSRICall(String ambiente, String fileString, String clave) {
    this.ambiente = ambiente;
    this.fileString = fileString;
    this.clave = clave;

    String strDirectorio = ATECOFF_ServicioSRICall.class.getResource("/").getPath();

    if (strDirectorio.indexOf(":") != -1) {
      int intPos = strDirectorio.indexOf(":");

      if (intPos != -1) {
        strDirectorio = strDirectorio.substring(0, intPos + 1);
      } else {
        strDirectorio = "";
      }
    } else {
      strDirectorio = "";
    }

    this.keyCert = strDirectorio + this.keyCert;

    if (this.keyCert.indexOf(":") != -1) {
      this.keyCert = keyCert.replaceFirst("/", "");
    } else if (this.keyCert.indexOf("//") != -1) {
      this.keyCert = keyCert.replaceFirst("//", "/");
    }

    log.info(this.keyCert);
  }

  public SOAPMessage RecibidoCall() {
    System.setProperty("javax.net.ssl.keyStore", this.keyCert);
    System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
    System.setProperty("javax.net.ssl.trustStore", this.keyCert);
    System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

    SOAPMessage soapMes = null;
    SOAPConnectionFactory soapConF;

    try {
      log.info("Iniciando Consulta");

      soapConF = SOAPConnectionFactory.newInstance();
      SOAPConnection soapCon = soapConF.createConnection();
      String wdsl = "";

      log.info("Creando Consulta");

      if (this.ambiente.equals("1")) {
        wdsl = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl";
      } else {
        wdsl = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/RecepcionComprobantesOffline?wsdl";
      }

      log.info("Consultando en Ambiente: " + wdsl);

      URL endPoint = new URL(null, wdsl, new URLStreamHandler() {

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
          URL clone_url = new URL(u.toString());
          HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();

          clone_urlconConnection.setConnectTimeout(10000);
          clone_urlconConnection.setReadTimeout(60000);
          return clone_urlconConnection;
        }
      });

      log.info("Realizando Consulta");

      soapMes = soapCon.call(createSoapReqRecXML("http://ec.gob.sri.ws.recepcion", this.fileString),
          endPoint);
      soapCon.close();
      soapCon = null;
      soapConF = null;

      log.info("Finalizando Consulta");
    } catch (SOAPException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage() + " - " + ex.getStackTrace());
      ex.printStackTrace();
    } catch (UnsupportedOperationException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage() + " - " + ex.getStackTrace());
      ex.printStackTrace();
    } catch (MalformedURLException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage() + " - " + ex.getStackTrace());
      ex.printStackTrace();
    } finally {

    }

    return soapMes;
  }

  public SOAPMessage AutorizadoCall() {
    System.setProperty("javax.net.ssl.keyStore", this.keyCert);
    System.setProperty("javax.net.ssl.keyStorePassword", "changeit");
    System.setProperty("javax.net.ssl.trustStore", this.keyCert);
    System.setProperty("javax.net.ssl.trustStorePassword", "changeit");

    SOAPMessage soapMes = null;
    SOAPConnectionFactory soapConF;
    try {
      log.info("Iniciando Consulta");

      soapConF = SOAPConnectionFactory.newInstance();
      SOAPConnection soapCon = soapConF.createConnection();
      String wdsl = "";

      log.info("Creando Consulta");

      if (this.ambiente.equals("1")) {
        wdsl = "https://celcer.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl";
      } else {
        wdsl = "https://cel.sri.gob.ec/comprobantes-electronicos-ws/AutorizacionComprobantesOffline?wsdl";
      }

      log.info("Consultando en Ambiente: " + wdsl);

      URL endPoint = new URL(null, wdsl, new URLStreamHandler() {

        @Override
        protected URLConnection openConnection(URL u) throws IOException {
          URL clone_url = new URL(u.toString());
          HttpURLConnection clone_urlconConnection = (HttpURLConnection) clone_url.openConnection();

          clone_urlconConnection.setConnectTimeout(10000);
          clone_urlconConnection.setReadTimeout(60000);
          return clone_urlconConnection;
        }
      });

      log.info("Realizando Consulta");

      soapMes = soapCon.call(createSoapReqAutXML("http://ec.gob.sri.ws.autorizacion", this.clave),
          endPoint);
      soapCon.close();
      soapCon = null;
      soapConF = null;

      log.info("Finalizando Consulta");
    } catch (UnsupportedOperationException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage() + " - " + ex.getStackTrace());
      ex.printStackTrace();
    } catch (SOAPException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage() + " - " + ex.getStackTrace());
      ex.printStackTrace();
    } catch (MalformedURLException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage() + " - " + ex.getStackTrace());
      ex.printStackTrace();
    }

    return soapMes;
  }

  private static SOAPMessage createSoapReqRecXML(String nameSpace, String fileString) {
    MessageFactory messageFac = null;
    SOAPMessage soapMes = null;

    try {
      log.info("Creando Soap: " + nameSpace);

      messageFac = MessageFactory.newInstance();
      soapMes = messageFac.createMessage();
      SOAPPart soapPart = soapMes.getSOAPPart();

      SOAPEnvelope soapEnv = soapPart.getEnvelope();
      soapEnv.addNamespaceDeclaration("ec", nameSpace);

      SOAPBody soapBod = soapEnv.getBody();

      SOAPElement soapEle = soapBod.addChildElement("validarComprobante", "ec");
      SOAPElement soapEle1 = soapEle.addChildElement("xml");
      soapEle1.addTextNode(fileString);

      soapMes.saveChanges();

      log.info("Creado Soap: " + nameSpace);
    } catch (SOAPException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage());
    }

    return soapMes;
  }

  private static SOAPMessage createSoapReqAutXML(String nameSpace, String claveAcceso) {
    MessageFactory messageFac = null;
    SOAPMessage soapMes = null;

    try {
      log.info("Creando Soap: " + nameSpace);

      messageFac = MessageFactory.newInstance();
      soapMes = messageFac.createMessage();
      SOAPPart soapPart = soapMes.getSOAPPart();

      SOAPEnvelope soapEnv = soapPart.getEnvelope();
      soapEnv.addNamespaceDeclaration("ec", nameSpace);

      SOAPBody soapBod = soapEnv.getBody();

      SOAPElement soapEle = soapBod.addChildElement("autorizacionComprobante", "ec");
      SOAPElement soapEle1 = soapEle.addChildElement("claveAccesoComprobante");
      soapEle1.addTextNode(claveAcceso);

      soapMes.saveChanges();

      log.info("Creado Soap: " + nameSpace);
    } catch (SOAPException ex) {
      // TODO Auto-generated catch block
      log.error(ex.getMessage());
    }

    return soapMes;
  }

  public void printSoapRes(SOAPMessage soapMes) throws SOAPException, TransformerException {
    TransformerFactory transformerFac = TransformerFactory.newInstance();
    Transformer transfor = transformerFac.newTransformer();

    StringWriter writer = new StringWriter();

    Source sourceCon = soapMes.getSOAPPart().getContent();
    log.info("\nResponse SOAP Message = ");
    StreamResult result = new StreamResult(writer);
    transfor.transform(sourceCon, result);

    log.info(writer.getBuffer());
  }
}
