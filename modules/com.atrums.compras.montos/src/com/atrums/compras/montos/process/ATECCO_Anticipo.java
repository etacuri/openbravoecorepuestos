package com.atrums.compras.montos.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.advpaymentmngt.dao.AdvPaymentMngtDao;
import org.openbravo.advpaymentmngt.process.FIN_AddPayment;
import org.openbravo.advpaymentmngt.process.FIN_PaymentProcess;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.client.kernel.RequestContext;
import org.openbravo.dal.core.DalUtil;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.dal.service.OBDao;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.OBMessageUtils;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.model.financialmgmt.payment.FIN_Payment;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentDetail;
import org.openbravo.model.financialmgmt.payment.FIN_PaymentScheduleDetail;
import org.openbravo.service.db.DalConnectionProvider;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.compras.montos.model.ATECCO_Temporal;

public class ATECCO_Anticipo extends HttpSecureAppServlet {

  /**
   *  
   */
  private static final long serialVersionUID = 1L;
  boolean continuar = true;
  String auxPagos = null;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");
      String strKey = vars.getGlobalVariable("inpcOrderId", strWindow + "|C_Order_ID");
      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("SAVE")) {
      this.continuar = true;
      this.auxPagos = null;

      String strWindow = vars.getStringParameter("inpWindowId");
      String strOrder = vars.getStringParameter("inpcOrderId");
      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|C_Order_ID");
      String strTab = vars.getStringParameter("inpTabId");

      String strMetodo = vars.getStringParameter("inpMetodo");
      boolean descuentoPago = true;

      List<ATECCO_Temporal> temp = new ArrayList<ATECCO_Temporal>();

      if (strMetodo.equals("Mixto")) {

        String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");

        if (strPagoEfectivo != null) {
          if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
            newTemp.setTipoPago("Efectivo".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

        if (strPagoElectronico != null) {
          if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoElectronico));
            newTemp.setTipoPago("Dinero Electrónico".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
        String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
        String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

        if (strPagoCheque != null) {
          if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoCheque));
            newTemp.setNro_cheque(strNroCheque);
            newTemp.setBanco_id(strBancoCheque);
            newTemp.setTipoPago("Cheque".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
        String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia");
        String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

        if (strPagoTransferencia != null) {
          if (Double
              .parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
            newTemp.setReferenceno(strNroTransferencia);
            newTemp.setBanco_id(strBancoTransferencia);
            newTemp.setTipoPago("Transferencia/Depósito".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
        String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
        String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");

        if (strPagoTarjeta != null) {
          if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
            descuentoPago = false;
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
            newTemp.setReferenceno(strNroTarjeta);
            newTemp.setTarjeta_id(strBancoTarjeta);
            newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
            temp.add(newTemp);
          }
        }

      } else if (strMetodo.equals("Efectivo")) {
        String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");

        if (strPagoEfectivo != null) {
          if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
            newTemp.setTipoPago("Efectivo".toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Electronico")) {
        String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

        if (strPagoElectronico != null) {
          if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoElectronico));
            newTemp.setTipoPago("Dinero Electrónico".toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Cheque")) {
        String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
        String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
        String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

        if (strPagoCheque != null) {
          if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoCheque));
            newTemp.setNro_cheque(strNroCheque);
            newTemp.setBanco_id(strBancoCheque);
            newTemp.setTipoPago("Cheque".toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Tarjeta")) {
        String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
        String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
        String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");

        if (strPagoTarjeta != null) {
          if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
            descuentoPago = false;
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
            newTemp.setReferenceno(strNroTarjeta);
            newTemp.setTarjeta_id(strBancoTarjeta);
            newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Transferencia")) {
        String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
        String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia");
        String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

        if (strPagoTransferencia != null) {
          if (Double
              .parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
            newTemp.setReferenceno(strNroTransferencia);
            newTemp.setBanco_id(strBancoTransferencia);
            newTemp.setTipoPago("Transferencia/Depósito".toUpperCase());
            temp.add(newTemp);
          }
        }
      }

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      OBError myError = processButton(vars, strKey, strOrder, strMetodo, temp, descuentoPago);
      log4j.debug(myError.getMessage());
      vars.setMessage(strTab, myError);
      printPageClosePopUp(response, vars, strWindowPath);
    }
  }

  @SuppressWarnings("resource")
  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

    Connection conn = null;

    try {
      Order order = OBDal.getInstance().get(Order.class, strKey);

      ComboTableData comboTableDataBanco = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_BANCO_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataTarjeta = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_TARJETA_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataCondicion = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_CONDICION_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Anticipo").createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("key", strKey);
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      String strMetodo = "";

      conn = OBDal.getInstance().getConnection();
      String sql = null;
      PreparedStatement ps = null;
      ResultSet rs = null;

      String auxValor = "";
      String auxValorNormal = "";

      String strAuxMetodoId = "Mixto";

      int intAuxTarjeta = 0;

      sql = "SELECT count(*) AS pagoTarjeta " + "FROM atecco_anticipo_pago aap "
          + "INNER JOIN fin_paymentmethod fpm ON (aap.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE aap.c_order_id = '" + strKey + "' " + "AND upper(fpm.name) LIKE '%TARJETA%';";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        intAuxTarjeta = rs.getInt("pagoTarjeta");
      }

      rs.close();

      if (strAuxMetodoId.toUpperCase().equals("Mixto".toUpperCase())) {
        strMetodo = "Mixto";

        /*
         * sql = "SELECT round(grandtotal, 2) AS grandtotal FROM c_order WHERE c_order_id = '" +
         * strKey + "'";
         */

        sql = "SELECT round(co.grandtotal - coalesce(sum(amount),0), 2) AS grandtotal "
            + "FROM c_order co "
            + "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
            + "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";

        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();

        while (rs.next()) {
          auxValorNormal = rs.getString("grandtotal");
        }

        rs.close();

        sql = "UPDATE c_orderline SET pricelist = round(pricelist * 1.05, 2), priceactual = round(priceactual * 1.05, 2) WHERE c_order_id = '"
            + strKey + "'";

        ps = conn.prepareStatement(sql);
        ps.executeUpdate();
      } else if (order.getPaymentMethod().getName().toUpperCase().equals("Cheque".toUpperCase())) {
        strMetodo = "Cheque";
      } else if (order.getPaymentMethod().getName().toUpperCase()
          .equals("Transferencia/Depósito".toUpperCase())) {
        strMetodo = "Transferencia";
      } else if (order.getPaymentMethod().getName().toUpperCase()
          .equals("Tarjeta de Crédito".toUpperCase())) {
        strMetodo = "Tarjeta";
        sql = "UPDATE c_orderline SET pricelist = round(pricelist * 1.05, 2), priceactual = round(priceactual * 1.05, 2) WHERE c_order_id = '"
            + strKey + "'";

        ps = conn.prepareStatement(sql);
        ps.executeUpdate();
      } else if (order.getPaymentMethod().getName().toUpperCase()
          .equals("Dinero Electrónico".toUpperCase())) {
        strMetodo = "Electronico";
      } else if (order.getPaymentMethod().getName().toUpperCase()
          .equals("Efectivo".toUpperCase())) {
        strMetodo = "Efectivo";
      } else if (order.getPaymentMethod().getName().toUpperCase()
          .equals("Con Retención".toUpperCase())) {
        strMetodo = "Retencion";
      }

      sql = "SELECT round(co.grandtotal - coalesce(sum(amount),0), 2) AS grandtotal "
          + "FROM c_order co "
          + "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
          + "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        auxValor = rs.getString("grandtotal");
      }

      rs.close();
      ps.close();
      conn.rollback();
      conn.close();

      if (strMetodo.toUpperCase().equals("Mixto".toUpperCase())) {
        if (intAuxTarjeta > 0) {
          xmlDocument.setParameter("tituloTotalTarjeta", "");
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("montoPagoDesc", auxValor);
        } else {
          xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Efectivo: " + auxValorNormal);
          xmlDocument.setParameter("montoPagoDesc", auxValorNormal);
        }
      } else if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {
        xmlDocument.setParameter("tituloTotalTarjeta", "");
        xmlDocument.setParameter("tituloTotalEfectivo", "Total Efectivo: " + auxValor);
      }

      xmlDocument.setParameter("metodo", strMetodo);

      xmlDocument.setParameter("montoPago", auxValor);

      xmlDocument.setParameter("valorPagoEfectivo", auxValor);

      if (strMetodo.toUpperCase().equals("Efectivo".toUpperCase())) {
        xmlDocument.setParameter("montoPagoEfectivo", auxValor);
      } else {
        xmlDocument.setParameter("montoPagoEfectivo", "0.00");
      }

      xmlDocument.setParameter("valorPagoElectronico", auxValor);

      if (strMetodo.toUpperCase().equals("Electronico".toUpperCase())) {
        xmlDocument.setParameter("montoPagoElectronico", auxValor);
      } else {
        xmlDocument.setParameter("montoPagoElectronico", "0.00");
      }

      xmlDocument.setParameter("valorPagoCheque", auxValor);

      if (strMetodo.toUpperCase().equals("Cheque".toUpperCase())) {
        xmlDocument.setParameter("montoPagoCheque", auxValor);
      } else {
        xmlDocument.setParameter("montoPagoCheque", "0.00");
      }

      xmlDocument.setParameter("nroPagoCheque", "0.00");

      xmlDocument.setParameter("valorPagoTransferencia", auxValor);

      if (strMetodo.toUpperCase().equals("Transferencia".toUpperCase())) {
        xmlDocument.setParameter("montoPagoTransferencia", auxValor);
      } else {
        xmlDocument.setParameter("montoPagoTransferencia", "0.00");
      }

      xmlDocument.setParameter("nroPagoTransferencia", "0.00");

      xmlDocument.setParameter("valorPagoTarjeta", auxValor);

      if (strMetodo.toUpperCase().equals("Tarjeta".toUpperCase())) {
        xmlDocument.setParameter("montoPagoTarjeta", auxValor);
      } else {
        xmlDocument.setParameter("montoPagoTarjeta", "0.00");
      }

      xmlDocument.setParameter("nroPagoTarjeta", "0.00");

      xmlDocument.setData("reportATECCOBANCOCHEQUE_ID", "liststructure",
          comboTableDataBanco.select(false));

      xmlDocument.setData("reportATECCOBANCOTRANSFERENCIA_ID", "liststructure",
          comboTableDataBanco.select(false));

      xmlDocument.setData("reportATECCOBANCOTARJETA_ID", "liststructure",
          comboTableDataTarjeta.select(false));

      xmlDocument.setData("reportATECCOBANCOCONDICION_ID", "liststructure",
          comboTableDataCondicion.select(false));

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strOrder,
      String strMetodo, List<ATECCO_Temporal> temp, boolean descuentoPago) {
    OBError myError = null;
    Connection conn = null;
    String sqlQuery = null;

    OBContext.setAdminMode(true);
    try {
      Order order = OBDal.getInstance().get(Order.class, strOrder);
      PreparedStatement ps = null;
      ResultSet rs = null;

      conn = OBDal.getInstance().getConnection();

      for (int i = 0; i < temp.size(); i++) {
        String finFinancialAccountId = null;
        String cDoctypeId = null;
        String sequence = null;
        String adSequenceId = null;

        /**
         * Cuenta Financiera
         **/
        if (temp.get(i).getTipoPago().equals("Efectivo".toUpperCase())
            || temp.get(i).getTipoPago().equals("Cheque".toUpperCase())) {
          sqlQuery = "SELECT ffa.fin_financial_account_id " + "FROM ad_org ao "
              + "INNER JOIN ad_treenode at ON (at.node_id = ao.ad_org_id) "
              + "INNER JOIN ad_treenode at2 ON (at2.node_id = at.parent_id) "
              + "INNER JOIN ad_org ao2 ON (ao2.ad_org_id = at2.node_id) "
              + "INNER JOIN fin_financial_account  ffa ON (ffa.ad_org_id = ao2.ad_org_id) "
              + "WHERE ao.ad_org_id = '" + order.getOrganization().getId() + "' "
              + "AND ao.ad_client_id = '" + order.getClient().getId() + "' " + "LIMIT 1;";
        } else if (temp.get(i).getTipoPago().equals("Dinero Electrónico".toUpperCase())
            || temp.get(i).getTipoPago().equals("Transferencia/Depósito".toUpperCase())) {
          sqlQuery = "SELECT fp.fin_financial_account_id " + "FROM fin_financial_account fp "
              + "WHERE upper(fp.name) LIKE 'BANCO%' AND fp.ad_client_id = '"
              + order.getClient().getId() + "' LIMIT 1;";
        } else if (temp.get(i).getTipoPago().equals("Tarjeta de Crédito".toUpperCase())) {
          sqlQuery = "SELECT fp.fin_financial_account_id " + "FROM fin_financial_account fp "
              + "WHERE upper(fp.name) LIKE 'RECAP' " + "AND fp.ad_client_id = '"
              + order.getClient().getId() + "' " + "LIMIT 1;";
        }

        ps = conn.prepareStatement(sqlQuery);
        rs = ps.executeQuery();

        while (rs.next()) {
          finFinancialAccountId = rs.getString("fin_financial_account_id");
        }

        rs.close();
        ps.close();

        int totalCajas = 0;

        sqlQuery = "SELECT count(acc.*) AS totalCajas " + "FROM atecco_cierrecaja acc "
            + "WHERE acc.docstatus IN ('AB', 'VA') " + "AND acc.ad_org_id = '"
            + order.getOrganization().getId() + "';";

        ps = conn.prepareStatement(sqlQuery);
        rs = ps.executeQuery();

        while (rs.next()) {
          totalCajas = rs.getInt("totalCajas");
        }

        rs.close();
        ps.close();

        if (totalCajas > 1) {
          OBDal.getInstance().rollbackAndClose();

          myError = new OBError();
          myError.setType("Error");
          myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
          myError.setMessage("El proceso no se ejecuto correctamente: "
              + "Hay más de un cierre de caja abierto, para poder guardar el anticipo");
          return myError;
        } else if (totalCajas == 0) {
          OBDal.getInstance().rollbackAndClose();

          myError = new OBError();
          myError.setType("Error");
          myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
          myError.setMessage("El proceso no se ejecuto correctamente: "
              + "No hay un cierre de caja abierto, para poder guardar el anticipo");
          return myError;
        }

        /**
         * Fin Cuenta Financiera
         **/

        sqlQuery = "SELECT aq.ad_sequence_id, aq.currentnext, cd.c_doctype_id "
            + "FROM c_doctype cd "
            + "INNER JOIN ad_sequence aq ON (cd.docnosequence_id = aq.ad_sequence_id) "
            + "WHERE cd.ad_client_id = '" + order.getClient().getId() + "' "
            + "AND cd.ad_org_id = '" + order.getOrganization().getId() + "' "
            + "AND upper(cd.name) LIKE 'COMPROBANTE DE INGRESO%';";

        ps = conn.prepareStatement(sqlQuery);
        rs = ps.executeQuery();

        while (rs.next()) {
          cDoctypeId = rs.getString("c_doctype_id");
          sequence = rs.getString("currentnext");
          adSequenceId = rs.getString("ad_sequence_id");
        }

        rs.close();
        ps.close();

        String auxFinMethoId = "";

        sqlQuery = "SELECT fin_paymentmethod_id " + "FROM fin_paymentmethod "
            + "WHERE upper(name) LIKE '" + temp.get(i).getTipoPago() + "' "
            + "AND isactive = 'Y' AND ad_org_id = '0';";

        ps = conn.prepareStatement(sqlQuery);
        rs = ps.executeQuery();

        while (rs.next()) {
          auxFinMethoId = rs.getString("fin_paymentmethod_id");
        }

        rs.close();
        ps.close();

        String finPaymentId = UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
        String auxReferencia = temp.get(i).getReferenceno() == null ? "null"
            : "'" + temp.get(i).getReferenceno() + "'";
        String auxNroCheque = temp.get(i).getNro_cheque() == null ? "null"
            : "'" + temp.get(i).getNro_cheque() + "'";

        if (temp.get(i).getTipoPago().equals("Cheque".toUpperCase())) {
          String ateccoCierrecajaId = "";

          sqlQuery = "SELECT acc.atecco_cierrecaja_id " + "FROM atecco_cierrecaja acc "
              + "WHERE acc.docstatus IN ('AB', 'VA') " + "AND acc.ad_org_id = '"
              + order.getOrganization().getId() + "';";

          ps = conn.prepareStatement(sqlQuery);
          rs = ps.executeQuery();

          while (rs.next()) {
            ateccoCierrecajaId = rs.getString("atecco_cierrecaja_id");
          }

          rs.close();
          ps.close();

          sqlQuery = "INSERT INTO public.atecco_retiro("
              + "atecco_retiro_id, atecco_cierrecaja_id, ad_client_id, ad_org_id, "
              + "isactive, created, createdby, updated, updatedby, hora, nro_deposito, "
              + "total_deposito, fin_paymentmethod_id, sales_id, nro_cheque) "
              + "VALUES (get_uuid(), '" + ateccoCierrecajaId + "', '" + order.getClient().getId()
              + "', '" + order.getOrganization().getId() + "',"
              + "'Y', now(), '100', now(), '100', now(), null, '" + temp.get(i).getAmount() + "', '"
              + auxFinMethoId + "', null, " + auxNroCheque + ");";

          ps = conn.prepareStatement(sqlQuery);
          ps.executeUpdate();

          ps.close();
        }

        sqlQuery = "INSERT INTO public.fin_payment("
            + "fin_payment_id, ad_client_id, ad_org_id, created, createdby, "
            + "updated, updatedby, isactive, isreceipt, c_bpartner_id, paymentdate, "
            + "c_currency_id, amount, writeoffamt, fin_paymentmethod_id, documentno, "
            + "referenceno, status, processed, processing, posted, description, "
            + "fin_financial_account_id, c_doctype_id, c_project_id, c_campaign_id, "
            + "c_activity_id, user1_id, user2_id, generated_credit, used_credit, "
            + "createdbyalgorithm, finacc_txn_convert_rate, finacc_txn_amount, "
            + "fin_rev_payment_id, c_costcenter_id, em_aprm_process_payment, "
            + "em_aprm_reconcile_payment, em_aprm_add_scheduledpayments, em_aprm_executepayment, "
            + "em_aprm_reversepayment, em_ct_cobro, em_atecdp_deposito, em_co_nro_cheque, "
            + "em_co_nombre_banco) " + "VALUES ('" + finPaymentId + "', '"
            + order.getClient().getId() + "', '" + order.getOrganization().getId()
            + "', now(), '100', " + "now(), '100', 'Y', 'Y', '" + order.getBusinessPartner().getId()
            + "', now(), " + "'100', '" + temp.get(i).getAmount() + "', '0', '" + auxFinMethoId
            + "', '" + sequence + "', " + auxReferencia + " , 'RPAP', 'N', 'N', 'N', null, '"
            + finFinancialAccountId + "', '" + cDoctypeId + "', null, null, "
            + "null, null, null, '0', '0','N', '1', '" + temp.get(i).getAmount()
            + "', null, null, 'P', " + "'N', 'N', 'N', " + "'N', 'N', null, " + auxNroCheque + ", "
            + "null);";

        ps = conn.prepareStatement(sqlQuery);
        this.continuar = ps.executeUpdate() == 1 ? true : false;

        ps.close();

        sqlQuery = "UPDATE ad_sequence " + "SET currentnext = currentnext + 1 "
            + "WHERE ad_sequence_id = '" + adSequenceId + "';";
        ps = conn.prepareStatement(sqlQuery);
        continuar = ps.executeUpdate() == 1 ? true : false;

        ps.close();

        if (this.continuar) {
          final String strActionId = "C0D9FAD1047343BAA53AF6F60D572DD0";
          final org.openbravo.model.ad.domain.List actionList = OBDal.getInstance()
              .get(org.openbravo.model.ad.domain.List.class, strActionId);
          final String strAction = actionList.getSearchKey();
          String strActualPayment = Double.toString(temp.get(i).getAmount());
          String strDifferenceAction = "";
          BigDecimal differenceAmount = BigDecimal.ZERO;

          differenceAmount = new BigDecimal(temp.get(i).getAmount());
          strDifferenceAction = "credit";

          BigDecimal exchangeRate = BigDecimal.ZERO;
          BigDecimal convertedAmount = BigDecimal.ZERO;

          exchangeRate = new BigDecimal("1");
          convertedAmount = new BigDecimal(temp.get(i).getAmount());

          List<String> pdToRemove = new ArrayList<String>();
          FIN_Payment payment = null;

          payment = OBDal.getInstance().get(FIN_Payment.class, finPaymentId);
          String strReferenceNo = "";

          payment.setReferenceNo(strReferenceNo);
          pdToRemove = OBDao.getIDListFromOBObject(payment.getFINPaymentDetailList());

          payment.setAmount(new BigDecimal(strActualPayment));
          FIN_AddPayment.setFinancialTransactionAmountAndRate(vars, payment, exchangeRate,
              convertedAmount);

          OBDal.getInstance().save(payment);

          removeNotSelectedPaymentDetails(payment, pdToRemove);

          String comingFrom = null;

          OBError message = processPayment(payment, strAction, strDifferenceAction,
              differenceAmount, exchangeRate, comingFrom, temp.get(i).getAmount());

          if (!this.continuar) {
            OBDal.getInstance().rollbackAndClose();

            myError = new OBError();
            myError.setType("Error");
            myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
            myError.setMessage("El proceso no se ejecuto correctamente: " + message.getMessage());
            return myError;
          } else {

            if (auxReferencia != null || auxNroCheque != null) {
              sqlQuery = "UPDATE fin_payment SET referenceno = " + auxReferencia
                  + ", em_co_nro_cheque = " + auxNroCheque + " WHERE fin_payment_id = '"
                  + finPaymentId + "'";
              ps = conn.prepareStatement(sqlQuery);
              ps.executeUpdate();
              ps.close();
            }

            this.auxPagos = (this.auxPagos == null ? "" : (this.auxPagos + ", "))
                + message.getMessage();
            temp.get(i).setFin_payment_id(finPaymentId);
          }
        } else {
          OBDal.getInstance().rollbackAndClose();

          myError = new OBError();
          myError.setType("Error");
          myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
          myError
              .setMessage("No se crearon los anticipos, porque no se pudieron guardar los datos");
        }
      }

      OBDal.getInstance().commitAndClose();

      for (int i = 0; i < temp.size(); i++) {
        ATECCOValidarXmlData.methodInsertarAnticipo(this, order.getClient().getId(),
            String.valueOf(temp.get(i).getAmount()), temp.get(i).getTarjeta_id(),
            temp.get(i).getBanco_id(), temp.get(i).getReferenceno(), temp.get(i).getNro_cheque(),
            temp.get(i).getTipoPago(), strOrder, temp.get(i).getFin_payment_id());
      }

      ATECCOValidarXmlData.methodUpdateStatusAnticipo(this, "CA", strOrder);

      myError = new OBError();
      myError.setType("Success");
      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
      myError.setMessage(
          "Anticipos Creados: " + this.auxPagos.replaceAll("Pago creado", "Anticipo Nro."));
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      try {
        ATECCOValidarXmlData.methodUpdateOrder(this, "SP", strOrder);
        ATECCOValidarXmlData.methodDeleteOrder(this, strOrder);
      } catch (ServletException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }

      e.printStackTrace();
      log4j.warn("Rollback in transaction");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }

  private void removeNotSelectedPaymentDetails(FIN_Payment payment, List<String> pdToRemove) {
    for (String pdId : pdToRemove) {
      FIN_PaymentDetail pd = OBDal.getInstance().get(FIN_PaymentDetail.class, pdId);

      List<String> pdsIds = OBDao.getIDListFromOBObject(pd.getFINPaymentScheduleDetailList());
      for (String strPDSId : pdsIds) {
        FIN_PaymentScheduleDetail psd = OBDal.getInstance().get(FIN_PaymentScheduleDetail.class,
            strPDSId);

        if (pd.getGLItem() == null) {
          List<FIN_PaymentScheduleDetail> outStandingPSDs = FIN_AddPayment.getOutstandingPSDs(psd);
          if (outStandingPSDs.size() == 0) {
            FIN_PaymentScheduleDetail newOutstanding = (FIN_PaymentScheduleDetail) DalUtil.copy(psd,
                false);
            newOutstanding.setPaymentDetails(null);
            newOutstanding.setWriteoffAmount(BigDecimal.ZERO);
            newOutstanding.setAmount(psd.getAmount().add(psd.getWriteoffAmount()));
            OBDal.getInstance().save(newOutstanding);
          } else {
            FIN_PaymentScheduleDetail outStandingPSD = outStandingPSDs.get(0);
            // First make sure outstanding amount is not equal zero
            if (outStandingPSD.getAmount().add(psd.getAmount()).add(psd.getWriteoffAmount())
                .signum() == 0) {
              OBDal.getInstance().remove(outStandingPSD);
            } else {
              // update existing PD with difference
              outStandingPSD.setAmount(
                  outStandingPSD.getAmount().add(psd.getAmount()).add(psd.getWriteoffAmount()));
              outStandingPSD.setDoubtfulDebtAmount(
                  outStandingPSD.getDoubtfulDebtAmount().add(psd.getDoubtfulDebtAmount()));
              OBDal.getInstance().save(outStandingPSD);
            }
          }
        }

        FIN_PaymentProcess.removePaymentProposalLines(psd);

        pd.getFINPaymentScheduleDetailList().remove(psd);
        OBDal.getInstance().save(pd);
        OBDal.getInstance().remove(psd);
      }
      payment.getFINPaymentDetailList().remove(pd);
      OBDal.getInstance().save(payment);
      OBDal.getInstance().remove(pd);
      OBDal.getInstance().flush();
      OBDal.getInstance().refresh(payment);
    }
  }

  private OBError processPayment(FIN_Payment payment, String strAction, String strDifferenceAction,
      BigDecimal refundAmount, BigDecimal exchangeRate, String comingFrom, double anticipo)
      throws Exception {
    ConnectionProvider conn = new DalConnectionProvider(true);
    VariablesSecureApp vars = RequestContext.get().getVariablesSecureApp();

    AdvPaymentMngtDao dao = new AdvPaymentMngtDao();
    BigDecimal assignedAmount = BigDecimal.ZERO;
    for (FIN_PaymentDetail paymentDetail : payment.getFINPaymentDetailList()) {
      assignedAmount = assignedAmount.add(paymentDetail.getAmount());
    }

    if (assignedAmount.compareTo(payment.getAmount()) == -1) {
      FIN_PaymentScheduleDetail refundScheduleDetail = dao.getNewPaymentScheduleDetail(
          payment.getOrganization(), payment.getAmount().subtract(assignedAmount));
      dao.getNewPaymentDetail(payment, refundScheduleDetail,
          payment.getAmount().subtract(assignedAmount), BigDecimal.ZERO, false, null);
    }

    OBError message = FIN_AddPayment.processPayment(vars, conn,
        (strAction.equals("PRP") || strAction.equals("PPP")) ? "P" : "D", payment, comingFrom);
    String strNewPaymentMessage = OBMessageUtils
        .parseTranslation("@PaymentCreated@" + " " + payment.getDocumentNo()) + ".";
    if (!"Error".equalsIgnoreCase(message.getType())) {
      message.setMessage(strNewPaymentMessage + " " + message.getMessage());
      message.setType(message.getType().toLowerCase());
    } else {
      this.continuar = false;
    }

    if (!strDifferenceAction.equals("refund")) {
      return message;
    }
    return message;
  }
}
