package com.atrums.compras.montos.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.compras.montos.data.ateccoCondicion;
import com.atrums.compras.montos.model.ATECCO_Temporal;

public class ATECCO_Validar extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");
      String strKey = vars.getGlobalVariable("inpcOrderId", strWindow + "|C_Order_ID");
      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("SAVE")) {
      String strWindow = vars.getStringParameter("inpWindowId");
      String strOrder = vars.getStringParameter("inpcOrderId");
      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|C_Order_ID");
      String strTab = vars.getStringParameter("inpTabId");

      String strMetodo = vars.getStringParameter("inpMetodo");
      boolean descuentoPago = true;

      OBContext.setAdminMode(true);
      Order order = OBDal.getInstance().get(Order.class, strOrder);

      List<ATECCO_Temporal> temp = new ArrayList<ATECCO_Temporal>();

      String strMetodoNota = vars.getStringParameter("inpAplicaNotaCredito").equals("Y") ? "Y"
          : "N";

      if (strMetodoNota.equals("Y")) {
        String StrPagoNota = vars.getStringParameter("inpMontoPagoNotaCredito");

        ATECCO_Temporal newTemp = new ATECCO_Temporal();
        newTemp.setAmount(Double.parseDouble(StrPagoNota));
        newTemp.setTipoPago("Nota de Crédito".toUpperCase());
        temp.add(newTemp);
      }

      if (strMetodo.equals("Mixto") || strMetodo.equals("Retencion")) {
        // String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");
        String strPagoEfectivo = vars.getStringParameter("inpMontoPagoPagado");

        if (strPagoEfectivo != null) {
          if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
            newTemp.setTipoPago("Efectivo%".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

        if (strPagoElectronico != null) {
          if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoElectronico));
            newTemp.setTipoPago("Dinero Electrónico%".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
        String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
        String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

        if (strPagoCheque != null) {
          if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoCheque));
            newTemp.setNro_cheque(strNroCheque);
            newTemp.setBanco_id(strBancoCheque);
            newTemp.setTipoPago("Cheque%".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
        String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia");
        String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

        if (strPagoTransferencia != null) {
          if (Double
              .parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
            newTemp.setReferenceno(strNroTransferencia);
            newTemp.setBanco_id(strBancoTransferencia);
            newTemp.setTipoPago("Transferencia/Depósito%".toUpperCase());
            temp.add(newTemp);
          }
        }

        String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
        String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
        String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");
        String strBancoCondicion = vars.getStringParameter("inpBancoPagoCondicion");

        if (strPagoTarjeta != null) {

          if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
            descuentoPago = false;
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
            newTemp.setReferenceno(strNroTarjeta);
            newTemp.setTarjeta_id(strBancoTarjeta);
            newTemp.setCondicion_id(strBancoCondicion);

            ateccoCondicion auxCon = OBDal.getInstance().get(ateccoCondicion.class,
                strBancoCondicion);

            if (auxCon.getCommercialName() != null) {
              if (auxCon.getCommercialName().toUpperCase().equals("Corriente".toUpperCase())) {
                newTemp
                    .setTipoPago("Tarjeta de Crédito ".toUpperCase() + "Corriente%".toUpperCase());
              } else {
                newTemp
                    .setTipoPago("Tarjeta de Crédito ".toUpperCase() + "Diferido%".toUpperCase());
              }
            } else {
              newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
            }

            temp.add(newTemp);
          }
        }

        String strPagoDebito = vars.getStringParameter("inpMontoPagoDebito");
        String strNroDebito = vars.getStringParameter("inpNroPagoDebito");
        String strBancoDebito = vars.getStringParameter("inpBancoPagoDebito");

        if (strPagoDebito != null) {

          if (Double.parseDouble(strPagoDebito.equals("") ? "0" : strPagoDebito) > 0) {
            descuentoPago = false;
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoDebito));
            newTemp.setReferenceno(strNroDebito);
            newTemp.setTarjeta_id(strBancoDebito);

            newTemp.setTipoPago("Tarjeta de Débito".toUpperCase());

            temp.add(newTemp);
          }
        }

      } else if (strMetodo.equals("Efectivo")) {
        // String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");
        String strPagoEfectivo = vars.getStringParameter("inpMontoPagoPagado");

        if (strPagoEfectivo != null) {
          if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
            newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Electronico")) {
        String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

        if (strPagoElectronico != null) {
          if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoElectronico));
            newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Cheque")) {
        String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
        String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
        String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

        if (strPagoCheque != null) {
          if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoCheque));
            newTemp.setNro_cheque(strNroCheque);
            newTemp.setBanco_id(strBancoCheque);
            newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Tarjeta")) {
        String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
        String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
        String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");
        String strBancoCondicion = vars.getStringParameter("inpBancoPagoCondicion");

        if (strPagoTarjeta != null) {
          if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
            descuentoPago = false;
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
            newTemp.setReferenceno(strNroTarjeta);
            newTemp.setTarjeta_id(strBancoTarjeta);
            newTemp.setCondicion_id(strBancoCondicion);
            newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Transferencia")) {
        String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
        String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia");
        String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

        if (strPagoTransferencia != null) {
          if (Double
              .parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
            newTemp.setReferenceno(strNroTransferencia);
            newTemp.setBanco_id(strBancoTransferencia);
            newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
            temp.add(newTemp);
          }
        }
      } else if (strMetodo.equals("Debito")) {
        String strPagoDebito = vars.getStringParameter("inpMontoPagoDebito");
        String strNroDebito = vars.getStringParameter("inpNroPagoDebito");
        String strBancoDebito = vars.getStringParameter("inpBancoPagoDebito");

        if (strPagoDebito != null) {
          if (Double.parseDouble(strPagoDebito.equals("") ? "0" : strPagoDebito) > 0) {
            descuentoPago = false;
            ATECCO_Temporal newTemp = new ATECCO_Temporal();
            newTemp.setAmount(Double.parseDouble(strPagoDebito));
            newTemp.setReferenceno(strNroDebito);
            newTemp.setTarjeta_id(strBancoDebito);
            newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
            temp.add(newTemp);
          }
        }
      }

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      OBError myError = processButton(vars, strKey, strOrder, strMetodo, temp, descuentoPago);
      log4j.debug(myError.getMessage());
      vars.setMessage(strTab, myError);
      printPageClosePopUp(response, vars, strWindowPath);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

    Connection conn = null;

    Order order = OBDal.getInstance().get(Order.class, strKey);

    try {
      OBContext.setAdminMode(true);

      ComboTableData comboTableDataBanco = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_BANCO_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataTarjeta = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_TARJETA_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataDebito = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_TARJETA_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataCondicion = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_CONDICION_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Validar").createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("key", strKey);
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      String strMetodo = "";

      conn = OBDal.getInstance().getConnection();
      String sql = null;
      PreparedStatement ps = null;
      ResultSet rs = null;

      /*NPI*/
      String mLocatorId = "";
       
      sql = "SELECT ml.m_locator_id " + "FROM ad_org_warehouse aow "
              + "INNER JOIN m_warehouse mw ON (aow.m_warehouse_id = mw.m_warehouse_id) "
              + "INNER JOIN m_locator ml ON (mw.m_warehouse_id = ml.m_warehouse_id) "
              + "WHERE aow.ad_org_id = '" + order.getOrganization().getId() + "';";
       
       ps = conn.prepareStatement(sql); 
       rs = ps.executeQuery();
       
       while (rs.next()) { 
    	  mLocatorId = rs.getString("m_locator_id"); 
       }
       
      boolean sinProducto = false;
      
      sql = "SELECT col.qtyordered, coalesce(mpsv.qtyonhand, 0) as qtyonhand "
 	         + "FROM c_orderline col " + "LEFT JOIN m_product_stock_v mpsv "
 	         + "ON (col.m_product_id = mpsv.m_product_id " + "AND mpsv.m_locator_id = '" + mLocatorId
 	         + "' " + "AND mpsv.c_uom_id = '100') " + "LEFT JOIN m_product p "
 	         + "ON (col.m_product_id = p.m_product_id) " + "WHERE col.c_order_id = '" + order.getId()
 	         + "' " + "AND p.producttype = 'I' " + ";";
      /**/
      
      /*sql = "SELECT col.qtyordered, sum(coalesce(msd.qtyonhand,0)) AS qtyonhand "
          + "FROM m_storage_detail msd "
          + "INNER JOIN m_locator ml ON (ml.m_locator_id=msd.m_locator_id) "
          + "INNER JOIN m_warehouse mw ON (mw.m_warehouse_id=ml.m_warehouse_id) "
          + "INNER JOIN ad_org_warehouse aow ON (aow.m_warehouse_id = mw.m_warehouse_id) "
          + "INNER JOIN c_orderline col ON (msd.m_product_id = col.m_product_id) "
          + "WHERE col.c_order_id = '" + order.getId() + "' " + "AND aow.ad_org_id = col.ad_org_id "
          + "GROUP BY 1;";*/

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        double movementQty = rs.getDouble("qtyordered");
        double auxQtyHand = rs.getDouble("qtyonhand");

        if (movementQty > auxQtyHand) {
          sinProducto = true;
          break;
        }
      }

      rs.close();
      ps.close();

      // ** Temporalmente false //
      // sinProducto = false;

      if (sinProducto) {
        OBDal.getInstance().rollbackAndClose();
        log4j.warn("Rollback in transaction");

        String strWindowPath = Utility.getTabURL(strTab, "R", true);
        if (strWindowPath.equals("")) {
          strWindowPath = strDefaultServlet;
        }

        OBError myError = new OBError();
        myError.setType("Error");
        myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
        myError.setMessage("El proceso no se ejecuto correctamente: "
            + "No hay producto necesario en la bodega para realizar la transacción");

        log4j.debug(myError.getMessage());
        vars.setMessage(strTab, myError);
        printPageClosePopUp(response, vars, strWindowPath);
        return;
      }

      if (order.getBusinessPartner() == null) {
        OBDal.getInstance().rollbackAndClose();
        log4j.warn("Rollback in transaction");

        String strWindowPath = Utility.getTabURL(strTab, "R", true);
        if (strWindowPath.equals("")) {
          strWindowPath = strDefaultServlet;
        }

        OBError myError = new OBError();
        myError.setType("Error");
        myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
        myError
            .setMessage("El proceso no se ejecuto correctamente: No hay tercero en el documento");

        log4j.debug(myError.getMessage());
        vars.setMessage(strTab, myError);
        printPageClosePopUp(response, vars, strWindowPath);
        return;
      }

      String auxValor = "";
      String auxValorNormal = "";

      int intAuxTarjeta = 0;

      /** Calculo con tarjeta **/
      /*
       * sql = "SELECT count(*) AS pagoTarjeta " + "FROM atecco_anticipo_pago aap " +
       * "INNER JOIN fin_paymentmethod fpm ON (aap.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
       * + "WHERE aap.c_order_id = '" + strKey + "' " + "AND upper(fpm.name) LIKE '%TARJETA%';";
       * 
       * ps = conn.prepareStatement(sql); rs = ps.executeQuery();
       * 
       * while (rs.next()) { intAuxTarjeta = rs.getInt("pagoTarjeta"); }
       * 
       * rs.close();
       */

      /**
       * Codigo Nota de Credito
       */

      String auxTotalNota = "0";

      sql = "SELECT sum(ci.grandtotal) AS totalNota " + "FROM c_invoice ci "
          + "INNER JOIN c_doctype cd ON (cd.c_doctype_id = ci.c_doctypetarget_id) "
          + "WHERE ci.c_bpartner_id = '" + order.getBusinessPartner().getId()
          + "' AND ci.ad_org_id = '" + order.getOrganization().getId() + "' "
          + "AND cd.docbasetype = 'ARC' " + "AND cd.issotrx = 'Y' " + "AND ispaid = 'N';";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        auxTotalNota = rs.getString("totalNota");
      }

      rs.close();

      /**
       * Fin Nota de Credito
       */

      /**
       * Codigo Anticipos
       **/

      String auxAnticipoEfectivo = "0";
      String auxAnticipoCheque = "0";
      String auxAnticipoElectronico = "0";
      String auxAnticipoTransferencia = "0";
      String auxAnticipoTarjeta = "0";

      sql = "SELECT upper(fp.name) AS name, sum(aap.amount) AS total "
          + "FROM atecco_anticipo_pago aap "
          + "INNER JOIN fin_paymentmethod fp ON (aap.fin_paymentmethod_id = fp.fin_paymentmethod_id) "
          + "WHERE aap.c_order_id = '" + strKey + "' " + "GROUP BY fp.name;";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        if (rs.getString("name").equals("Efectivo".toUpperCase())) {
          auxAnticipoEfectivo = rs.getString("total");
        } else if (rs.getString("name").equals("Cheque".toUpperCase())) {
          auxAnticipoCheque = rs.getString("total");
        } else if (rs.getString("name").equals("Dinero Electrónico".toUpperCase())) {
          auxAnticipoElectronico = rs.getString("total");
        } else if (rs.getString("name").equals("Transferencia/Depósito".toUpperCase())) {
          auxAnticipoTransferencia = rs.getString("total");
        } else if (rs.getString("name").equals("Tarjeta de Crédito".toUpperCase())) {
          auxAnticipoTarjeta = rs.getString("total");
        }
      }

      rs.close();

      /**
       * Fin Anticipos
       **/

      String auxMetodo = order.getPaymentMethod().getName().toUpperCase();

      // auxMetodo = "Mixto";

      if (auxMetodo.equals("Nota de Crédito".toUpperCase())) {
        auxMetodo = "Mixto".toUpperCase();
      }

      /** Calculo con tarjeta **/
      /*
       * if (!auxMetodo.equals("Mixto".toUpperCase()) && intAuxTarjeta > 0) { sql =
       * "SELECT round(co.grandtotal - coalesce(sum(amount),0), 2) AS grandtotal " +
       * "FROM c_order co " +
       * "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) " +
       * "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";
       * 
       * ps = conn.prepareStatement(sql); rs = ps.executeQuery();
       * 
       * while (rs.next()) { auxValorNormal = rs.getString("grandtotal"); }
       * 
       * rs.close();
       * 
       * sql =
       * "UPDATE c_orderline SET pricelist = round(pricelist * 1.05, 2), priceactual = round(priceactual * 1.05, 2) WHERE c_order_id = '"
       * + strKey + "'";
       * 
       * ps = conn.prepareStatement(sql); ps.executeUpdate(); }
       */

      sql = "UPDATE c_orderline col SET "
          + "pricelist = (SELECT (pricelist * ((fpm.em_atecco_porcentaje + 100)/100)) "
          + "FROM c_order co "
          + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE co.c_order_id = col.c_order_id), "
          + "priceactual = (SELECT (priceactual * ((fpm.em_atecco_porcentaje + 100)/100)) "
          + "FROM c_order co "
          + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE co.c_order_id = col.c_order_id) WHERE col.c_order_id = '" + strKey + "' "
          + "AND col.m_product_id NOT IN (SELECT mp.m_product_id " + "FROM m_product mp "
          + "WHERE upper(mp.name) LIKE '%DESCUENTO%' " + "AND mp.ad_client_id = '"
          + order.getClient().getId() + "');";
      ps = conn.prepareStatement(sql);
      ps.executeUpdate();

      strMetodo = auxMetodo;

      if (auxMetodo.equals("Mixto".toUpperCase())) {
        strMetodo = "Mixto";
      } else if (auxMetodo.equals("Cheque".toUpperCase())) {
        strMetodo = "Cheque";
      } else if (auxMetodo.equals("Transferencia/Depósito".toUpperCase())) {
        strMetodo = "Transferencia";
      } else if (auxMetodo.equals("Tarjeta de Crédito".toUpperCase())) {
        strMetodo = "Tarjeta";
      } else if (auxMetodo.equals("Dinero Electrónico".toUpperCase())) {
        strMetodo = "Electronico";
      } else if (auxMetodo.equals("Efectivo".toUpperCase())) {
        strMetodo = "Efectivo";
      } else if (auxMetodo.equals("Con Retención".toUpperCase())) {
        strMetodo = "Retencion";
      } else if (auxMetodo.indexOf("Tarjeta de Crédito".toUpperCase()) != -1) {
        strMetodo = "Tarjeta";
      } else if (auxMetodo.indexOf("Tarjeta de Credito".toUpperCase()) != -1) {
        strMetodo = "Tarjeta";
      } else if (auxMetodo.indexOf("Cheque".toUpperCase()) != -1) {
        strMetodo = "Cheque";
      } else if (auxMetodo.indexOf("Efectivo".toUpperCase()) != -1) {
        strMetodo = "Efectivo";
      } else if (auxMetodo.indexOf("Tarjeta de Débito".toUpperCase()) != -1) {
        strMetodo = "Debito";
      }

      /*
       * if (auxMetodo.equals("Mixto".toUpperCase())) { strMetodo = "Mixto";
       * 
       * sql = "SELECT round(co.grandtotal - coalesce(sum(aap.amount),0), 2) AS grandtotal " +
       * "FROM c_order co " +
       * "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) " +
       * "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";
       * 
       * ps = conn.prepareStatement(sql); rs = ps.executeQuery();
       * 
       * while (rs.next()) { auxValorNormal = rs.getString("grandtotal"); }
       * 
       * rs.close();
       * 
       * if (Double.valueOf(auxValorNormal) < 0) { auxValorNormal = "0"; }
       * 
       * sql =
       * "UPDATE c_orderline SET pricelist = round(pricelist * 1.05, 2), priceactual = round(priceactual * 1.05, 2) WHERE c_order_id = '"
       * + strKey + "'";
       * 
       * ps = conn.prepareStatement(sql); ps.executeUpdate(); } else if
       * (auxMetodo.equals("Cheque".toUpperCase())) { strMetodo = "Cheque"; auxTotalNota = "0"; }
       * else if (auxMetodo.equals("Transferencia/Depósito".toUpperCase())) { strMetodo =
       * "Transferencia"; auxTotalNota = "0"; } else if
       * (auxMetodo.equals("Tarjeta de Crédito".toUpperCase())) { strMetodo = "Tarjeta";
       * auxTotalNota = "0"; sql =
       * "UPDATE c_orderline SET pricelist = round(pricelist * 1.05, 2), priceactual = round(priceactual * 1.05, 2) WHERE c_order_id = '"
       * + strKey + "'";
       * 
       * ps = conn.prepareStatement(sql); ps.executeUpdate(); } else if
       * (auxMetodo.equals("Dinero Electrónico".toUpperCase())) { strMetodo = "Electronico";
       * auxTotalNota = "0"; } else if (auxMetodo.equals("Efectivo".toUpperCase())) { strMetodo =
       * "Efectivo"; auxTotalNota = "0"; } else if (auxMetodo.equals("Con Retención".toUpperCase()))
       * { strMetodo = "Retencion"; }
       */

      sql = "SELECT round(co.grandtotal - coalesce(sum(aap.amount),0), 2) AS grandtotal "
          + "FROM c_order co "
          + "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
          + "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        auxValor = rs.getString("grandtotal");
      }

      rs.close();
      ps.close();

      if (Double.valueOf(auxValor) < 0) {
        auxValor = "0";
      }

      /*
       * if (intAuxTarjeta > 0) { auxValorNormal = auxValor; }
       */

      auxValorNormal = auxValor;

      if (strMetodo.toUpperCase().equals("Mixto".toUpperCase())) {
        if (intAuxTarjeta > 0) {
          xmlDocument.setParameter("tituloTotalTarjeta", "");
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("montoPagoDesc", auxValor);
        } else {
          xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Documento: " + auxValorNormal);
          xmlDocument.setParameter("montoPagoDesc", auxValorNormal);
        }
      } else if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {

        if (intAuxTarjeta > 0) {
          xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("tituloTotalEfectivo", "");
        } else {
          xmlDocument.setParameter("tituloTotalTarjeta", "");
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Documento: " + auxValor);
        }
      } else {
        xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
        xmlDocument.setParameter("tituloTotalEfectivo", "Total Documento: " + auxValorNormal);
        xmlDocument.setParameter("montoPagoDesc", auxValorNormal);
      }

      xmlDocument.setParameter("metodo", strMetodo);
      xmlDocument.setParameter("montoPago", auxValor);

      xmlDocument.setParameter("valorPagoEfectivo", auxValor);

      /**
       * Nota de Credito
       */

      xmlDocument.setParameter("montoPagoNotaCredito", auxTotalNota);

      /**
       * Fin Nota de Credito
       */

      /**
       * Anticipos
       **/

      Double totalEfectivo = Double.valueOf(auxAnticipoEfectivo) + Double.valueOf(auxAnticipoCheque)
          + Double.valueOf(auxAnticipoElectronico) + Double.valueOf(auxAnticipoTransferencia)
          + Double.valueOf(auxAnticipoTarjeta);

      xmlDocument.setParameter("montoAnticipoEfectivo", auxAnticipoEfectivo);
      xmlDocument.setParameter("montoAnticipoCheque", auxAnticipoCheque);
      xmlDocument.setParameter("montoAnticipoElectronico", auxAnticipoElectronico);
      xmlDocument.setParameter("montoAnticipoTransferencia", auxAnticipoTransferencia);
      xmlDocument.setParameter("montoAnticipoTarjeta", auxAnticipoTarjeta);

      /**
       * Fin anicipos
       **/

      if (strMetodo.toUpperCase().equals("Efectivo".toUpperCase())) {
        /*
         * xmlDocument.setParameter("montoPagoEfectivo", auxValor);
         */
        xmlDocument.setParameter("montoAnticipoEfectivo", totalEfectivo.toString());
        xmlDocument.setParameter("montoPagoEfectivo", "0.00");
      } else {
        xmlDocument.setParameter("montoPagoEfectivo", "0.00");
      }

      xmlDocument.setParameter("valorPagoElectronico", auxValor);

      if (strMetodo.toUpperCase().equals("Electronico".toUpperCase())) {
        xmlDocument.setParameter("montoPagoElectronico", auxValor);
        xmlDocument.setParameter("montoAnticipoElectronico", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoElectronico", "0.00");
      }

      xmlDocument.setParameter("valorPagoCheque", auxValor);

      if (strMetodo.toUpperCase().equals("Cheque".toUpperCase())) {
        xmlDocument.setParameter("montoPagoCheque", auxValor);
        xmlDocument.setParameter("montoAnticipoCheque", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoCheque", "0.00");
      }

      xmlDocument.setParameter("nroPagoCheque", "0.00");

      xmlDocument.setParameter("valorPagoTransferencia", auxValor);

      if (strMetodo.toUpperCase().equals("Transferencia".toUpperCase())) {
        xmlDocument.setParameter("montoPagoTransferencia", auxValor);
        xmlDocument.setParameter("montoAnticipoTransferencia", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoTransferencia", "0.00");
      }

      xmlDocument.setParameter("nroPagoTransferencia", "0.00");

      xmlDocument.setParameter("valorPagoTarjeta", auxValor);

      if (strMetodo.toUpperCase().equals("Tarjeta".toUpperCase())) {
        xmlDocument.setParameter("montoPagoTarjeta", auxValor);
        xmlDocument.setParameter("montoAnticipoTarjeta", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoTarjeta", "0.00");
      }

      xmlDocument.setParameter("nroPagoTarjeta", "0.00");

      xmlDocument.setParameter("valorPagoRetencion", auxValor);

      xmlDocument.setParameter("montoPagoRetencion", "0.00");

      xmlDocument.setParameter("nroPagoRetencion", "0.00");

      xmlDocument.setParameter("montoPagoRetencionIva", "0.00");
      xmlDocument.setParameter("montoPagoRetencionRenta", "0.00");

      if (strMetodo.toUpperCase().equals("Debito".toUpperCase())) {
        xmlDocument.setParameter("montoPagoDebito", auxValor);
        xmlDocument.setParameter("montoAnticipoDebito", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoDebito", "0.00");
      }

      xmlDocument.setParameter("nroPagoDebito", "0.00");

      if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {

        String strRetencionIva = "0.00";

        sql = "SELECT round((ctr.porcentaje / 100) * colt.taxamt, 2) AS porcentaje "
            + "FROM c_order co "
            + "INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
            + "INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
            + "INNER JOiN c_ordertax colt ON (colt.c_order_id = co.c_order_id) "
            + "WHERE co.c_order_id = '" + strKey + "' " + "AND ctr.tipo = 'IVA' "
            + "AND ctr.isactive = 'Y' " + "LIMIT 1;";

        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();

        while (rs.next()) {
          strRetencionIva = rs.getString("porcentaje");
        }

        rs.close();
        ps.close();

        String strRetencionFuente = "0.00";

        sql = "SELECT round((ctr.porcentaje / 100) * colt.taxbaseamt, 2) AS porcentaje "
            + "FROM c_order co "
            + "INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) "
            + "INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) "
            + "INNER JOiN c_ordertax colt ON (colt.c_order_id = co.c_order_id) "
            + "WHERE co.c_order_id = '" + strKey + "' " + "AND ctr.tipo = 'FUENTE' "
            + "AND ctr.isactive = 'Y' " + "LIMIT 1;";

        ps = conn.prepareStatement(sql);
        rs = ps.executeQuery();

        while (rs.next()) {
          strRetencionFuente = rs.getString("porcentaje");
        }

        rs.close();
        ps.close();

        xmlDocument.setParameter("montoPagoRetencionIva", strRetencionIva);
        xmlDocument.setParameter("montoPagoRetencionRenta", strRetencionFuente);
      }

      xmlDocument.setData("reportATECCOBANCOCHEQUE_ID", "liststructure",
          comboTableDataBanco.select(false));

      xmlDocument.setData("reportATECCOBANCOTRANSFERENCIA_ID", "liststructure",
          comboTableDataBanco.select(false));

      xmlDocument.setData("reportATECCOBANCOTARJETA_ID", "liststructure",
          comboTableDataTarjeta.select(false));

      xmlDocument.setData("reportATECCOBANCODEBITO_ID", "liststructure",
          comboTableDataDebito.select(false));

      xmlDocument.setData("reportATECCOBANCOCONDICION_ID", "liststructure",
          comboTableDataCondicion.select(false));

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

      conn.rollback();
      conn.close();
    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strOrder,
      String strMetodo, List<ATECCO_Temporal> temp, boolean descuentoPago) {
    OBError myError = null;

    Connection conn = null;

    try {
      Order order = OBDal.getInstance().get(Order.class, strOrder);

      conn = OBDal.getInstance().getConnection();

      /*
       * String sql = null; PreparedStatement ps = null; ResultSet rs = null;
       */

      // int intAuxTarjeta = 0;

      /*
       * sql = "SELECT count(*) AS pagoTarjeta " + "FROM atecco_anticipo_pago aap " +
       * "INNER JOIN fin_paymentmethod fpm ON (aap.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
       * + "WHERE aap.c_order_id = '" + strOrder + "' " + "AND upper(fpm.name) LIKE '%TARJETA%';";
       * 
       * ps = conn.prepareStatement(sql); rs = ps.executeQuery();
       * 
       * while (rs.next()) { intAuxTarjeta = rs.getInt("pagoTarjeta"); }
       * 
       * rs.close();
       */

      /*
       * String auxstrMetodo = strMetodo; boolean auxdescuentoPago = descuentoPago;
       */

      /*
       * if (intAuxTarjeta > 0) { auxstrMetodo = "Mixto"; auxdescuentoPago = false; }
       */

      /*
       * auxstrMetodo = "Mixto"; auxdescuentoPago = false;
       */

      /*
       * if (auxstrMetodo.equals("Efectivo") || auxstrMetodo.equals("Cheque") ||
       * auxstrMetodo.equals("Transferencia") || auxstrMetodo.equals("Electronico") ||
       * auxstrMetodo.equals("Retencion") || auxdescuentoPago) { sql =
       * "UPDATE c_order SET em_atecco_pagodescuento = 'Y' WHERE c_order_id = '" + strOrder + "'";
       * 
       * ps = conn.prepareStatement(sql); ps.executeUpdate(); } else { sql =
       * "UPDATE c_orderline SET pricelist = round(pricelist * 1.05, 2), priceactual = round(priceactual *  1.05, 2) WHERE c_order_id = '"
       * + strOrder + "'";
       * 
       * ps = conn.prepareStatement(sql); ps.executeUpdate(); }
       */

      myError = new OBError();
      myError.setType("Success");
      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
      // myError.setMessage(Utility.messageBD(this, "RecordsCopied", vars.getLanguage()));

      for (int i = 0; i < temp.size(); i++) {
        ATECCOValidarXmlData.methodInsertarTemp(this, order.getClient().getId(),
            String.valueOf(temp.get(i).getAmount()), temp.get(i).getTarjeta_id(),
            temp.get(i).getCondicion_id(), temp.get(i).getBanco_id(), temp.get(i).getReferenceno(),
            temp.get(i).getNro_cheque(), temp.get(i).getTipoPago(), order.getClient().getId(),
            strOrder);
      }

      OBDal.getInstance().commitAndClose();

      ATECCOValidarXmlData.methodUpdateOrder(this, "CO", strOrder);
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      try {
        ATECCOValidarXmlData.methodUpdateOrder(this, "SP", strOrder);
        ATECCOValidarXmlData.methodDeleteOrder(this, strOrder);
      } catch (ServletException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }

      e.printStackTrace();
      log4j.warn("Rollback in transaction");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
      // myError = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
    } finally {
      OBContext.restorePreviousMode();

      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }

}
