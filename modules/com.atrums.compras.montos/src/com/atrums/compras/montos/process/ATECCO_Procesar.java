package com.atrums.compras.montos.process;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.felectronica.process.ATECCO_Operaciones_XML;

public class ATECCO_Procesar extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  final OBError msg = new OBError();

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strProcessId = vars.getStringParameter("inpProcessId");
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");
      String strKey = vars.getGlobalVariable("inpcOrderId", strWindow + "|C_Order_ID");
      printPage(response, vars, strKey, strWindow, strTab, strProcessId);
    } else if (vars.commandIn("SAVE")) {
      String strWindow = vars.getStringParameter("inpWindowId");
      String strOrder = vars.getStringParameter("inpcOrderId");
      String strKey = vars.getRequestGlobalVariable("inpKey", strWindow + "|C_Order_ID");
      String strTab = vars.getStringParameter("inpTabId");

      String strWindowPath = Utility.getTabURL(strTab, "R", true);
      if (strWindowPath.equals("")) {
        strWindowPath = strDefaultServlet;
      }

      OBError myError = processButton(vars, strKey, strOrder);
      log4j.debug(myError.getMessage());
      vars.setMessage(strTab, myError);
      printPageClosePopUp(response, vars, strWindowPath);
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strKey,
      String windowId, String strTab, String strProcessId) throws IOException, ServletException {

    Connection conn = null;

    try {
      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/compras/montos/process/ATECCO_Procesar").createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("key", strKey);
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strKey, String strOrder) {
    OBError myError = null;
    Connection conn = OBDal.getInstance().getConnection();

    OBContext.setAdminMode(true);
    try {
      Order order = OBDal.getInstance().get(Order.class, strOrder);

      // String auxMetodo = order.getPaymentMethod().getName().toUpperCase();
      // boolean auxDescuentoPago = order.isAteccoPagodescuento() ? false : true;

      String sqlQuery = "";
      int updateOrder = 0;
      int insertpinstance = 0;
      String auxAdPinstanceId = null;
      String strPInstance = null;

      sqlQuery = "UPDATE c_orderline col SET "
          + "pricelist = (SELECT (pricelist * ((fpm.em_atecco_porcentaje + 100)/100)) "
          + "FROM c_order co "
          + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE co.c_order_id = col.c_order_id), "
          + "priceactual = (SELECT (priceactual * ((fpm.em_atecco_porcentaje + 100)/100)) "
          + "FROM c_order co "
          + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE co.c_order_id = col.c_order_id) WHERE col.c_order_id = '" + strOrder + "' "
          + "AND col.m_product_id NOT IN (SELECT mp.m_product_id " + "FROM m_product mp "
          + "WHERE upper(mp.name) LIKE '%DESCUENTO%' " + "AND mp.ad_client_id = '"
          + order.getClient().getId() + "');";
      PreparedStatement ps = conn.prepareStatement(sqlQuery);
      ps.executeUpdate();
      ps.close();

      sqlQuery = "SELECT * FROM atecco_cuadrardescuento_linea('" + strOrder + "')";
      ps = conn.prepareStatement(sqlQuery);
      ps.execute();

      ps.close();

      sqlQuery = "SELECT ad_pinstance_id " + "FROM ad_pinstance " + "WHERE record_id = '" + strOrder
          + "' AND result = 1 " + "ORDER BY created DESC " + "LIMIT 1;";
      ps = conn.prepareStatement(sqlQuery);
      ResultSet rs = ps.executeQuery();

      while (rs.next()) {
        auxAdPinstanceId = rs.getString("ad_pinstance_id");
        insertpinstance = 1;
        updateOrder = 1;
      }

      ps.close();
      rs.close();

      if (auxAdPinstanceId == null) {
        sqlQuery = "UPDATE c_order SET em_atecco_docaction = 'CO', docaction = 'CO' WHERE c_order_id = '"
            + strOrder + "'";

        ps = conn.prepareStatement(sqlQuery);
        updateOrder = ps.executeUpdate();
        strPInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();
        ps.close();
      }

      if (updateOrder == 1) {

        if (auxAdPinstanceId == null) {
          sqlQuery = "INSERT INTO ad_pinstance("
              + "ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
              + "ad_user_id, updated, result, errormsg, ad_client_id, ad_org_id, "
              + "createdby, updatedby, isactive) " + "VALUES ('" + strPInstance + "', '104', '"
              + strOrder + "', 'N', now(), " + "'100', now(), null, null, '"
              + order.getClient().getId() + "', '0', " + "'100', '100', 'Y');";

          ps = conn.prepareStatement(sqlQuery);
          insertpinstance = ps.executeUpdate();
          ps.close();
        }

        if (insertpinstance == 1) {

          if (auxAdPinstanceId == null) {
            sqlQuery = "SELECT * FROM c_order_post('" + strPInstance + "')";

            ps = conn.prepareStatement(sqlQuery);
            ps.execute();

            ps.close();
          } else {
            strPInstance = auxAdPinstanceId;
          }

          sqlQuery = "SELECT result, errormsg FROM ad_pinstance WHERE ad_pinstance_id = '"
              + strPInstance + "'";

          ps = conn.prepareStatement(sqlQuery);
          rs = ps.executeQuery();
          int auxresult = 0;
          String auxerrormsg = null;

          while (rs.next()) {
            if (rs.getString("result") != null) {
              auxresult = rs.getInt("result");
            }

            auxerrormsg = rs.getString("errormsg") != null ? rs.getString("errormsg") : "";
          }

          ps.close();

          sqlQuery = "UPDATE c_order SET em_atecco_docaction = docaction WHERE c_order_id = '"
              + strOrder + "'";

          ps = conn.prepareStatement(sqlQuery);
          ps.executeUpdate();

          ps.close();
          rs.close();

          if (auxresult == 1) {
            sqlQuery = "SELECT ci.c_invoice_id, cd.em_atecfe_fac_elec AS doc1, cd2.em_atecfe_fac_elec AS doc2, cd2.name "
                + "FROM c_order co " + "INNER JOIN c_invoice ci ON (co.c_order_id = ci.c_order_id) "
                + "INNER JOIN c_doctype cd ON (co.c_doctype_id = cd.c_doctype_id) "
                + "INNER JOIN c_doctype cd2 ON (ci.c_doctype_id = cd2.c_doctype_id) "
                + "WHERE co.c_order_id = '" + strOrder + "' " + "AND ci.em_atecfe_docaction = 'PR' "
                + "LIMIT 1;";

            ps = conn.prepareStatement(sqlQuery);
            rs = ps.executeQuery();

            String strCinvoiceId = null;
            String auxdoc1 = null;
            String auxdoc2 = null;
            String auxname = null;

            while (rs.next()) {
              if (rs.getString("c_invoice_id") != null) {
                strCinvoiceId = rs.getString("c_invoice_id");
                auxdoc1 = rs.getString("doc1");
                auxdoc2 = rs.getString("doc2");
                auxname = rs.getString("name");
              }
            }

            ps.close();
            rs.close();

            if (strCinvoiceId != null) {
              sqlQuery = "SELECT * FROM atecco_gen_pago('" + strCinvoiceId + "', '" + strOrder
                  + "')";

              ps = conn.prepareStatement(sqlQuery);
              ps.execute();

              ps.close();

              sqlQuery = "UPDATE c_order SET em_atecco_docstatus = '--' WHERE c_order_id = '"
                  + strOrder + "';";
              ps = conn.prepareStatement(sqlQuery);
              ps.execute();

              ps.close();
            }

            if (strCinvoiceId != null && auxdoc1.equals("Y")) {
              if (auxdoc2.equals("Y")) {
                ATECCO_Operaciones_XML opeXML = new ATECCO_Operaciones_XML();

                OBDal.getInstance().commitAndClose();

                if (opeXML.declararFacturaSRI(strCinvoiceId, vars.getUser(), msg, this)) {
                  OBDal.getInstance().commitAndClose();
                  ATECCOValidarXmlData.methodUpdateOrder(this, "--", strOrder);
                  myError = new OBError();
                  myError.setType("Success");
                  myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
                  auxerrormsg = auxerrormsg + ", " + this.msg.getMessage();
                } else {
                  OBDal.getInstance().rollbackAndClose();
                  ATECCOValidarXmlData.methodUpdateOrder(this, "--", strOrder);
                  myError = new OBError();
                  myError.setType("Success");
                  myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
                  auxerrormsg = auxerrormsg + ", Error Factura Electrónica: "
                      + this.msg.getMessage()
                      + ", corrija el error y procese con el SRI desde la ventana documento de venta";
                }
              } else {
                auxerrormsg = "@El tipo de documento '" + auxname
                    + "' no esta configurado como factura electronica@";
                OBDal.getInstance().rollbackAndClose();
                myError = new OBError();
                myError.setType("Error");
                myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
                myError.setMessage(auxerrormsg);
                return myError;
              }
            } else {
              OBDal.getInstance().commitAndClose();
              ATECCOValidarXmlData.methodUpdateOrder(this, "--", strOrder);
              myError = new OBError();
              myError.setType("Success");
              myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
            }
          } else {
            OBDal.getInstance().rollbackAndClose();

            ATECCOValidarXmlData.methodUpdateOrder(this, "SP", strOrder);
            ATECCOValidarXmlData.methodDeleteOrder(this, strOrder);

            conn = OBDal.getInstance().getConnection();

            // int intAuxTarjeta = 0;

            /*
             * sqlQuery = "SELECT count(*) AS pagoTarjeta " + "FROM atecco_anticipo_pago aap " +
             * "INNER JOIN fin_paymentmethod fpm ON (aap.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
             * + "WHERE aap.c_order_id = '" + strOrder + "' " +
             * "AND upper(fpm.name) LIKE '%TARJETA%';";
             * 
             * ps = conn.prepareStatement(sqlQuery); rs = ps.executeQuery();
             * 
             * while (rs.next()) { intAuxTarjeta = rs.getInt("pagoTarjeta"); }
             * 
             * rs.close();
             */

            /*
             * if (intAuxTarjeta > 0) { auxMetodo = "Mixto"; auxDescuentoPago = false; }
             */

            /*
             * if (auxMetodo.equals("Transferencia/Depósito".toUpperCase()) ||
             * auxMetodo.equals("Cheque".toUpperCase()) ||
             * auxMetodo.equals("Efectivo".toUpperCase()) ||
             * auxMetodo.equals("Dinero Electrónico".toUpperCase()) ||
             * auxMetodo.equals("Con Retención".toUpperCase()) || auxDescuentoPago) { sqlQuery =
             * "UPDATE c_order SET em_atecco_pagodescuento = 'N' WHERE c_order_id = '" + strOrder +
             * "'";
             * 
             * ps = conn.prepareStatement(sqlQuery); ps.executeUpdate(); } else { sqlQuery =
             * "UPDATE c_orderline SET pricelist = pricelist / 1.05, priceactual = priceactual / 1.05 WHERE c_order_id = '"
             * + strOrder + "'";
             * 
             * ps = conn.prepareStatement(sqlQuery); ps.executeUpdate(); }
             */

            ps.close();
            OBDal.getInstance().commitAndClose();

            myError = new OBError();
            myError.setType("Error");
            myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

            conn.close();
            conn = null;
          }

          if (auxerrormsg != null) {
            auxerrormsg = auxerrormsg.replaceAll("@ERROR=", "");
          } else {
            auxerrormsg = "@Documento procesado@";
          }

          String auxerrormsgTrad = "";

          while (auxerrormsg.indexOf("@") != -1) {
            int auxIni = auxerrormsg.indexOf("@");
            int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

            String auxParteInicio = auxerrormsg.substring(0, auxIni);
            String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);

            auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());

            auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;

            auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
          }

          auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

          myError.setMessage(auxerrormsgTrad);
          return myError;
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
      log4j.warn("Rollback in transaction");
      // myError = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
    } finally {
      OBContext.restorePreviousMode();
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }

}
