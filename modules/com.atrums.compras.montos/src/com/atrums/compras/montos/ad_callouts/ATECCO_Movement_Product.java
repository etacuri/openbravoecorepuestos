package com.atrums.compras.montos.ad_callouts;

import javax.servlet.ServletException;

import org.apache.commons.lang.StringUtils;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.data.FieldProvider;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.plm.AttributeSet;
import org.openbravo.model.common.plm.Product;

public class ATECCO_Movement_Product extends SimpleCallout {
  private static final long serialVersionUID = 1L;

  @Override
  protected void execute(CalloutInfo info) throws ServletException {

    // Locator

    String strLocator = info.vars.getStringParameter("inpmProductId_LOC");

    if (strLocator.startsWith("\"")) {
      strLocator = strLocator.substring(1, strLocator.length() - 1);
    }
    // info.addResult("inpmLocatorId", strLocator);
    // info.addResult("inpmLocatorId_R",ATECCOInOutLineProductData.locator(this, strLocator,
    // info.vars.getLanguage()));

    // UOM

    String strUOM = info.vars.getStringParameter("inpmProductId_UOM");
    info.addResult("inpcUomId", strUOM);

    // Attributes

    String strAttribute = info.vars.getStringParameter("inpmProductId_ATR");

    if (strAttribute.startsWith("\"")) {
      strAttribute = strAttribute.substring(1, strAttribute.length() - 1);
    }
    info.addResult("inpmAttributesetinstanceId", strAttribute);
    info.addResult("inpmAttributesetinstanceId_R",
        ATECCOInOutLineProductData.attribute(this, strAttribute));

    // Attribute set

    String strMProductID = info.vars.getStringParameter("inpmProductId");
    String strAttrSet = "";
    String strAttrSetValueType = "";

    OBContext.setAdminMode();
    try {
      final Product product = OBDal.getInstance().get(Product.class, strMProductID);
      if (product != null) {
        AttributeSet attributeset = product.getAttributeSet();
        if (attributeset != null) {
          strAttrSet = product.getAttributeSet().toString();
        }
        strAttrSetValueType = product.getUseAttributeSetValueAs();
      }
    } finally {
      OBContext.restorePreviousMode();
    }
    info.addResult("inpattributeset", strAttrSet);
    info.addResult("inpattrsetvaluetype", strAttrSetValueType);

    String strMMovementId = info.vars.getStringParameter("inpmMovementId");

    // Product - Locator
    // Movement qty

    String strQtyOrder = info.vars.getNumericParameter("inpmProductId_PQTY");
    String strQty = info.vars.getNumericParameter("inpmProductId_QTY");

    ATECCOOrderProductData[] dataLocat = ATECCOOrderProductData.selectLocatorOrgDes(this,
        strMProductID, strMProductID, strMMovementId);

    if (dataLocat.length > 0) {
      if (!dataLocat[0].loc1.equals("")) {
        info.addResult("inpmLocatorId", dataLocat[0].loc1);
        strQty = dataLocat[0].qtyonhand;
      }

      if (!dataLocat[0].loc2.equals("")) {
        info.addResult("inpmLocatortoId", dataLocat[0].loc2);
      }
    }

    info.addResult("inpmovementqty", StringUtils.isEmpty(strQty) ? "\"\"" : (Object) strQty);
    info.addResult("inpquantityorder",
        StringUtils.isEmpty(strQtyOrder) ? "\"\"" : (Object) strQtyOrder);

    // Secondary UOM

    String strPUOM = info.vars.getStringParameter("inpmProductId_PUOM");

    String strHasSecondaryUOM = ATECCOOrderProductData.hasSecondaryUOM(this, strMProductID);
    info.addResult("inphasseconduom", (Object) strHasSecondaryUOM);

    if (strPUOM.startsWith("\"")) {
      strPUOM = strPUOM.substring(1, strPUOM.length() - 1);
    }

    FieldProvider[] tld = null;
    try {
      ComboTableData comboTableData = new ComboTableData(info.vars, this, "TABLE", "",
          "M_Product_UOM", "",
          Utility.getContext(this, info.vars, "#AccessibleOrgTree", "SLMovementProduct"),
          Utility.getContext(this, info.vars, "#User_Client", "SLMovementProduct"), 0);
      Utility.fillSQLParameters(this, info.vars, null, comboTableData, "SLOrderProduct", "");
      tld = comboTableData.select(false);
      comboTableData = null;
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

    if (tld != null && tld.length > 0) {
      info.addSelect("inpmProductUomId");
      for (int i = 0; i < tld.length; i++) {
        info.addSelectResult(tld[i].getField("id"), tld[i].getField("name"),
            tld[i].getField("id").equalsIgnoreCase(strPUOM));
      }
      info.endSelect();
    } else {
      info.addResult("inpmProductUomId", null);
    }

    // displayLogic

    info.addResult("EXECUTE", "displayLogic();");
  }
}
