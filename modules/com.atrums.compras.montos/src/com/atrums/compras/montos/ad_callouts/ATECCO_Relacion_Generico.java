package com.atrums.compras.montos.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECCO_Relacion_Generico extends HttpSecureAppServlet {

  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strAteccoFamiliaId = vars.getStringParameter("inpemAteccoFamiliaId");
      String strAteccoGenericoId = vars.getStringParameter("inpemAteccoGenericoId");
      String strAteccoCategoriaId = vars.getStringParameter("inpemAteccoCategoriaId");
      String strAteccoSubcategoriaId = vars.getStringParameter("inpemAteccoSubCategoriaId");

      try {
        printPage(response, vars, strAteccoFamiliaId, strAteccoGenericoId, strAteccoCategoriaId,
            strAteccoSubcategoriaId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strAteccoFamiliaId, String strAteccoGenericoId, String strAteccoCategoriaId,
      String strAteccoSubcategoriaId) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/compras/montos/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='CO_Datos_Retencion';\n\n");

    result.append("var respuesta = new Array(");

    if (strAteccoGenericoId != null) {
      result.append("new Array(\"inpemAteccoCategoriaId\", \"" + null + "\"),");
      result.append("new Array(\"inpemAteccoSubCategoriaId\", \"" + null + "\")");
      result.append(");");
    }

    // inject the generated code xmlDocument.setParameter("array",
    // result.toString());
    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();

  }

}
