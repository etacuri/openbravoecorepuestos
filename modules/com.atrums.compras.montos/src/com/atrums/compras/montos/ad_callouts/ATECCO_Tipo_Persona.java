package com.atrums.compras.montos.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.xmlEngine.XmlDocument;

public class ATECCO_Tipo_Persona extends HttpSecureAppServlet {

  /**
  * 
  */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strCoNaturalJuridico = vars.getStringParameter("inpemCoNaturalJuridico");
      try {
        printPage(response, vars, strCoNaturalJuridico);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strCoNaturalJuridico) throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/compras/montos/ad_callouts/CallOut").createXmlDocument();

    StringBuffer result = new StringBuffer();
    result.append("var calloutName='CO_Datos_Retencion';\n\n");

    result.append("var respuesta = new Array(");

    if (strCoNaturalJuridico.equals("PN")) {
      result.append("new Array(\"inpemCoTipoIdentificacion\", \"" + "02" + "\"),");
      result.append("new Array(\"inpemAteccoTipoIdentificacion\", \"" + "02" + "\"),");
      result.append("new Array(\"inptaxid\", \"" + null + "\")");
      result.append(");");
    } else if (strCoNaturalJuridico.equals("PJ")) {
      result.append("new Array(\"inpemCoTipoIdentificacion\", \"" + "01" + "\"),");
      result.append("new Array(\"inpemAteccoTipoIdentificacion\", \"" + "01" + "\"),");
      result.append("new Array(\"inptaxid\", \"" + null + "\")");
      result.append(");");
    }

    // inject the generated code xmlDocument.setParameter("array",
    // result.toString());
    xmlDocument.setParameter("array", result.toString());
    xmlDocument.setParameter("frameName", "appFrame");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();

  }

}
