package com.atrums.ecommerce.ad_callouts;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.businessUtility.BpartnerMiscData;
import org.openbravo.xmlEngine.XmlDocument;

public class ECOM_TipoAnio extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {

      // Obtencion de la marca seleccionada
      String strcMarcaVehiculoId = vars.getStringParameter("inpecomMarcaVehiculoId");
      
   // Obtencion del modelo seleccionada
      String strcModeloMarcaId = vars.getStringParameter("inpecomModeloMarcasId");

      try {
        printPage(response, vars, strcMarcaVehiculoId, strcModeloMarcaId);
      } catch (ServletException ex) {
        pageErrorCallOut(response);
      }
    } else
      pageError(response);
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strcMarcaVehiculoId, String strcModeloMarcaId) throws IOException, ServletException {
	    if (log4j.isDebugEnabled())
	      log4j.debug("Output: dataSheet");
	    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
	        "com/atrums/ecommerce/ad_callouts/CallOut").createXmlDocument();

	    ECOMTipoAnioData[] data = ECOMTipoAnioData.selectModelo(this, strcMarcaVehiculoId, strcModeloMarcaId);

	    StringBuffer resultado = new StringBuffer();
	    resultado.append("var calloutName='ECOMTipoAnio';\n\n");

	    if (data == null || data.length == 0) {
	      resultado.append("var respuesta = new Array(new Array(\"inptipo\", null),");
	      resultado.append("new Array(\"inpyear\",null),");
		  resultado.append("new Array(\"inpcilindraje\",null),");
		  resultado.append("new Array(\"inptipoTransmision\",null),");
		  resultado.append("new Array(\"inptipoCaja\",null));");
	    } else {
	      if (data != null && data.length > 0) {
	        resultado.append("var respuesta = new Array(");
	        resultado.append("new Array(\"inptipo\", \"" + data[0].getField("tipo") + "\"),");
	        resultado.append("new Array(\"inpyear\", \"" + data[0].getField("anio") + "\"),");
			resultado.append("new Array(\"inpcilindraje\", \"" + data[0].getField("cilindraje") + "\"),");
			resultado.append("new Array(\"inptipoTransmision\", \"" + data[0].getField("tipoTransmision") + "\"),");
			resultado.append("new Array(\"inptipoCaja\", \"" + data[0].getField("tipocaja") + "\")");
	      } else {
	        resultado.append("null");
	      }

	      resultado.append(");");
	    }

	    xmlDocument.setParameter("array", resultado.toString());
	    xmlDocument.setParameter("frameName", "appFrame");
	    response.setContentType("text/html; charset=UTF-8");
	    PrintWriter out = response.getWriter();
	    out.println(xmlDocument.print());
	    out.close();
	  }

}