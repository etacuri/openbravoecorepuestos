package com.atrums.nomina.ad_process;

import org.apache.log4j.Logger;
import org.codehaus.jettison.json.JSONArray;

public class CapaIntermediaLlamaRol extends Thread {
  private static final Logger log = Logger.getLogger(CapaIntermediaServicio.class);
  private JSONArray rolesIds = null;

  public CapaIntermediaLlamaRol(JSONArray rolesIds) {
    this.rolesIds = rolesIds;
  }

  public void run() {
    try {
      
      if (this.rolesIds.length() > 0) {
          for (int i = 0; i < rolesIds.length(); i++) {
            final String rolId = rolesIds.getString(i);
            CapaIntermedia capaIntermedia = new CapaIntermedia();
            capaIntermedia.setIdRolPago(rolId);
            capaIntermedia.start();
            capaIntermedia.join();
          }
        }
    } catch (Exception e) {
      log.error(e.getMessage());
    }
  }
}
