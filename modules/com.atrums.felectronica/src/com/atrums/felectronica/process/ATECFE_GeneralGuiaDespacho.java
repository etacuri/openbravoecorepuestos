package com.atrums.felectronica.process;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.io.OutputFormat;
import org.dom4j.io.XMLWriter;
import org.openbravo.dal.service.OBDal;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.model.ad.system.Client;
import org.openbravo.model.common.businesspartner.BusinessPartner;
import org.openbravo.model.common.enterprise.DocumentType;
import org.openbravo.model.common.enterprise.Organization;
import org.openbravo.model.common.enterprise.OrganizationInformation;
import org.openbravo.model.materialmgmt.transaction.ShipmentInOut;
import org.openbravo.model.shipping.ShippingCompany;

public class ATECFE_GeneralGuiaDespacho {

  ATECFE_Funciones_Aux ope = new ATECFE_Funciones_Aux();

  public boolean generarFacturaXMLGre(String strMInout, ConnectionProvider conn, String strUser,
      OBError msg) throws Exception {

    Hashtable<String, String> hstClave = new Hashtable<String, String>();

    ShipmentInOut spiDato = OBDal.getInstance().get(ShipmentInOut.class, strMInout);

    File flXml = File.createTempFile("documento.xml", null);
    flXml.deleteOnExit();

    /*
     * Crea documento XML
     */
    Document docXML = DocumentHelper.createDocument();

    /*
     * Formato del documento XML
     */
    final OutputFormat ofFormat = OutputFormat.createPrettyPrint();
    ofFormat.setEncoding("utf-8");
    ofFormat.setTrimText(true);

    /*
     * Creando nodo y agregando al documento XML
     */
    final Element elmgre = docXML.addElement("guiaRemision");
    elmgre.addAttribute("id", "comprobante");
    elmgre.addAttribute("version", "1.0.0");

    /*
     * Agregando la información tributaria al documento
     */
    final Element elminftri = elmgre.addElement("infoTributaria");

    if (spiDato.getClient() != null) {
      Client cltDato = OBDal.getInstance().get(Client.class, spiDato.getClient().getId());

      /*
       * Creando formatos para las fechas de la factura
       */
      Date cldFechaIn = spiDato.getMovementDate();
      SimpleDateFormat sdfFormato = new SimpleDateFormat("dd/MM/yyyy");
      SimpleDateFormat sdfFormatoClave = new SimpleDateFormat("ddMMyyyy");

      /*
       * Realizando una consulta del tipo de documento
       */
      DocumentType dctDato = OBDal.getInstance().get(DocumentType.class,
          spiDato.getDocumentType().getId());

      /*
       * Realizando una consulta de la organización
       */
      OrganizationInformation oriDato = OBDal.getInstance().get(OrganizationInformation.class,
          spiDato.getOrganization().getId());

      /*
       * Realizando una consulta del tercero de la factura
       */

      BusinessPartner bspDato = null;
      ShippingCompany shcDato = null;

      if (spiDato.getShippingCompany() == null) {
        bspDato = OBDal.getInstance().get(BusinessPartner.class,
            spiDato.getBusinessPartner().getId());
      } else {
        shcDato = OBDal.getInstance().get(ShippingCompany.class,
            spiDato.getShippingCompany().getId());

        if (shcDato.getBusinessPartner() != null) {
          bspDato = OBDal.getInstance().get(BusinessPartner.class,
              shcDato.getBusinessPartner().getId());
        } else {
          msg.setType("Error");
          msg.setMessage("Es necesario un tercero en el transportista de la guía de despacho nro: "
              + spiDato.getDocumentNo());
          msg.setTitle("@Error@");
          return false;
        }
      }

      BusinessPartner bspDatoDest = OBDal.getInstance().get(BusinessPartner.class,
          spiDato.getBusinessPartner().getId());

      /*
       * Realizando una consulta de la direccion de la Matriz
       */
      ATECFEGenerarXmlData[] axmlDirMatriz = ATECFEGenerarXmlData.methodSelDirMatriz(conn);

      String strDirMat = "";
      if (axmlDirMatriz != null && axmlDirMatriz.length == 1) {
        strDirMat = axmlDirMatriz[0].dato1;
      }

      /*
       * Realizando una consulta de la direccion de la organización
       */
      ATECFEGenerarXmlData[] axmlDirec = ATECFEGenerarXmlData.methodSeleccionarDirec(conn,
          spiDato.getOrganization().getId());

      String strDir = "";
      if (axmlDirec != null && axmlDirec.length == 1) {
        strDir = axmlDirec[0].dato1;
      }

      Organization orgDato = OBDal.getInstance().get(Organization.class,
          spiDato.getOrganization().getId());

      if (ope.generarCabecera(elminftri, cltDato.getAtecfeTipoambiente(),
          dctDato.getCoTipoComprobanteAutorizadorSRI().toString(), oriDato.getTaxID(),
          orgDato.getCoNroEstab(), orgDato.getCoPuntoEmision(), spiDato.getDocumentNo(),
          cltDato.getAtecfeCodinumerico(), cltDato.getAtecfeTipoemisi(),
          sdfFormatoClave.format(cldFechaIn), strDirMat, cltDato.getName(), cltDato.getName(), msg,
          hstClave) && dctDato != null && oriDato != null && bspDato != null
          && bspDatoDest != null) {

        /*
         * Agregando la información de la retencion al documento
         */
        final Element elmcomgre = elmgre.addElement("infoGuiaRemision");

        /*
         * Agregando la dirección del Establecimiento
         */
        if (!strDir.equals("")) {
          elmcomgre.addElement("dirEstablecimiento").addText(ope.normalizacionPalabras(strDir));
        } else {
          msg.setType("Error");
          msg.setMessage(
              "Es necesaria la dirección del establecimiento de la guía de despacho nro: "
                  + spiDato.getDocumentNo());
          msg.setTitle("@Error@");
          return false;
        }

        /*
         * Agregando la dirección del Establecimiento
         */
        if (!strDir.equals("")) {
          ATECFEGenerarXmlData[] strDirPartida = ATECFEGenerarXmlData
              .methodSeleccionarDirecBodega(conn, spiDato.getWarehouse().getId());

          if (strDirPartida != null) {
            if (strDirPartida.length > 0) {
              elmcomgre.addElement("dirPartida")
                  .addText(ope.normalizacionPalabras(strDirPartida[0].dato1));
            } else {
              msg.setType("Error");
              msg.setMessage(
                  "Es necesaria la dirección de la bodega de partida de la guía de despacho nro: "
                      + spiDato.getDocumentNo());
              msg.setTitle("@Error@");
              return false;
            }
          } else {
            msg.setType("Error");
            msg.setMessage(
                "Es necesaria la dirección de la bodega de partida de la guía de despacho nro: "
                    + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }
        }

        /*
         * Agregando la razon social
         */
        if (bspDato.getName() != null) {
          elmcomgre.addElement("razonSocialTransportista")
              .addText(ope.normalizacionPalabras(bspDato.getName()));
        } else if (bspDato.getName2() != null) {
          elmcomgre.addElement("razonSocialTransportista")
              .addText(ope.normalizacionPalabras(bspDato.getName2()));
        } else {
          msg.setType("Error");
          msg.setMessage("Es necesaria la razón social del comprador de la guía de despacho nro: "
              + spiDato.getDocumentNo());
          msg.setTitle("@Error@");
          return false;
        }

        if (bspDato.getCOTipoIdentificacion().toString().equals("01")
            || bspDato.getCOTipoIdentificacion().toString().equals("1")) {
          elmcomgre.addElement("tipoIdentificacionTransportista").addText("04");
        } else if (bspDato.getCOTipoIdentificacion().toString().equals("02")
            || bspDato.getCOTipoIdentificacion().toString().equals("2")) {
          elmcomgre.addElement("tipoIdentificacionTransportista").addText("05");
        } else if (bspDato.getCOTipoIdentificacion().toString().equals("03")
            || bspDato.getCOTipoIdentificacion().toString().equals("3")) {
          elmcomgre.addElement("tipoIdentificacionTransportista").addText("06");
        } else if (bspDato.getCOTipoIdentificacion().toString().equals("07")
            || bspDato.getCOTipoIdentificacion().toString().equals("7")) {
          elmcomgre.addElement("tipoIdentificacionTransportista").addText("07");
        } else {
          msg.setType("Error");
          msg.setMessage("El transportista debe tener un tipo de identificacion válido");
          msg.setTitle("@Error@");
          return false;
        }

        if (bspDato.getTaxID() != null) {
          elmcomgre.addElement("rucTransportista").addText(bspDato.getTaxID());
        } else {
          msg.setType("Error");
          msg.setMessage("El transportista debe tener el ruc válido");
          msg.setTitle("@Error@");
          return false;
        }

        if (cltDato.isAtecfeObligcontabi()) {
          elmcomgre.addElement("obligadoContabilidad").addText("SI");
        } else {
          elmcomgre.addElement("obligadoContabilidad").addText("NO");
        }

        elmcomgre.addElement("fechaIniTransporte")
            .addText(sdfFormato.format(spiDato.getMovementDate()));

        elmcomgre.addElement("fechaFinTransporte")
            .addText(sdfFormato.format(spiDato.getMovementDate()));

        if (shcDato != null) {
          if (shcDato.getDescription() != null) {
            elmcomgre.addElement("placa")
                .addText(ope.normalizacionPalabras(shcDato.getDescription()));
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la placa del transportista de la guía de despacho nro: "
                + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }
        } else {
          ATECFEGenerarXmlData[] transp = ATECFEGenerarXmlData.methodSelectTransport(conn);

          if (transp.length > 0) {
            elmcomgre.addElement("placa").addText(ope.normalizacionPalabras(transp[0].dato1));
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la placa del transportista de la guía de despacho nro: "
                + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }
        }

        final Element elmdestina = elmgre.addElement("destinatarios");
        final Element elmdestin = elmdestina.addElement("destinatario");

        elmdestin.addElement("identificacionDestinatario").addText(bspDatoDest.getTaxID());

        if (bspDatoDest.getName2() != null) {
          elmdestin.addElement("razonSocialDestinatario")
              .addText(ope.normalizacionPalabras(bspDatoDest.getName2()));
        } else if (bspDatoDest.getName() != null) {
          elmdestin.addElement("razonSocialDestinatario")
              .addText(ope.normalizacionPalabras(bspDatoDest.getName()));
        } else {
          msg.setType("Error");
          msg.setMessage(
              "Es necesaria la razón social del destinatario de la guía de despacho nro: "
                  + spiDato.getDocumentNo());
          msg.setTitle("@Error@");
          return false;
        }

        ATECFEGenerarXmlData[] strDirecDestino = ATECFEGenerarXmlData
            .methodSeleccionarDirecDestinatario(conn, bspDatoDest.getId());

        if (strDirecDestino != null) {
          if (strDirecDestino.length > 0) {
            elmdestin.addElement("dirDestinatario").addText(strDirecDestino[0].dato1);
          } else {
            msg.setType("Error");
            msg.setMessage("Es necesaria la dirección del destinatario de la guía de despacho nro: "
                + spiDato.getDocumentNo());
            msg.setTitle("@Error@");
            return false;
          }
        }

        if (spiDato.getDescription() != null) {
          elmdestin.addElement("motivoTraslado").addText(spiDato.getDescription());
        } else {
          elmdestin.addElement("motivoTraslado").addText("Transporte".toUpperCase());
        }

        elmdestin.addElement("codDocSustento").addText("01");

        ATECFEGenerarXmlData[] axmlDocSustento = ATECFEGenerarXmlData
            .methodSeleccionarDatoSusteno(conn, spiDato.getId());

        if (axmlDocSustento.length > 0) {
          elmdestin.addElement("numDocSustento").addText(axmlDocSustento[0].dato1);
          elmdestin.addElement("fechaEmisionDocSustento").addText(axmlDocSustento[0].dato2);
        }

        ATECFEGenerarXmlData[] axmlDetalles = ATECFEGenerarXmlData
            .methodSeleccionarDetallesGuia(conn, spiDato.getId());

        if (axmlDetalles != null && axmlDetalles.length > 0) {
          Element elmdetalles = elmdestin.addElement("detalles");
          for (int i = 0; i < axmlDetalles.length; i++) {
            Element elmdetalle = elmdetalles.addElement("detalle");
            elmdetalle.addElement("codigoInterno")
                .addText(ope.normalizacionPalabras(axmlDetalles[i].dato1));
            elmdetalle.addElement("codigoAdicional")
                .addText(ope.normalizacionPalabras(axmlDetalles[i].dato1));
            elmdetalle.addElement("descripcion")
                .addText(ope.normalizacionPalabras(axmlDetalles[i].dato3));
            elmdetalle.addElement("cantidad").addText(axmlDetalles[i].dato4);
          }
        }

        ope.addCamposAdic(elmgre, conn, spiDato.getClient().getId());

        final XMLWriter writer = new XMLWriter(
            new OutputStreamWriter(new FileOutputStream(flXml), "utf-8"), ofFormat);
        writer.write(docXML);
        writer.flush();
        writer.close();

        flXml = ope.firmarDocumento(flXml, conn, strUser, msg);

        if (flXml != null) {
          byte[] bytes = ope.filetobyte(flXml);
          String encodedString = new String(bytes, "UTF-8");

          final Hashtable<String, String> hstResult = new Hashtable<String, String>();

          Hashtable<String, String> hstInvDato = new Hashtable<String, String>();

          hstInvDato.put("id", spiDato.getId());
          hstInvDato.put("docid", spiDato.getDocumentType().getId());
          hstInvDato.put("atecdocsts", spiDato.getATECFEEstadoSRI());

          File flFirmado = new File(
              "/opt/OpenbravoERP/xmlsri/" + hstClave.get("claveacc") + ".xml");

          InputStream input = new FileInputStream(flXml);
          OutputStream ouput = new FileOutputStream(flFirmado);

          byte[] buf = new byte[2024];
          int len;

          while ((len = input.read(buf)) > 0) {
            ouput.write(buf, 0, len);
          }

          input.close();
          ouput.close();

          if (ope.enviarDocSRI(encodedString, cltDato, hstClave.get("claveacc"), hstResult,
              hstInvDato, conn)) {

            if (hstResult.get("dstd").toString().equals("RZ")) {
              msg.setType("Error");
              msg.setMessage("Su documento tiene el siguiente error: " + hstResult.get("mens")
                  + ", corrija el error e intentelo de nuevo");
              msg.setTitle("@Error@");
              return false;
            }

            if (ATECFEGenerarXmlData.methodActualizarMinout(conn, hstClave.get("claveacc"),
                hstResult.get("doc"), hstResult.get("mens"), hstResult.get("std"),
                hstResult.get("dstd"), hstResult.get("numaut"), hstResult.get("fecaut"),
                spiDato.getId()) == 1) {
              return true;
            }
          } else {
            msg.setType("Error");
            msg.setMessage(
                "Documento XML no autorizado por problemas de comunicación con el servidor de transacciones, revise la configuracion del servidor de facturación electronica"
                    + " e intentelo más tarde");
            msg.setTitle("@Error@");
            return false;
          }
        } else {
          return false;
        }

      } else {
        return false;
      }
    }
    msg.setType("Error");
    msg.setMessage("No hay un tercero en la guía de despacho nro: " + spiDato.getDocumentNo());
    msg.setTitle("@Error@");
    return false;
  }
}
