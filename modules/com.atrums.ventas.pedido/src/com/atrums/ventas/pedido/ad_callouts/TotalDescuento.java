package com.atrums.ventas.pedido.ad_callouts;

import java.math.BigDecimal;
import javax.servlet.ServletException;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.erpCommon.ad_callouts.SimpleCallout;

public class TotalDescuento extends SimpleCallout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void execute(CalloutInfo info) throws ServletException {
		VariablesSecureApp vars = info.vars;

		BigDecimal Cantidad = BigDecimal.ZERO;
		
		BigDecimal Descuento = BigDecimal.ZERO;
		BigDecimal PrecioUnitario = BigDecimal.ZERO;
		BigDecimal TotalLinea = BigDecimal.ZERO;
		BigDecimal ValorDescuento = BigDecimal.ZERO;
		BigDecimal TotalDescuento = BigDecimal.ZERO;

		//DecimalFormat numberFormat = Utility.getFormat(vars, "priceRelation");
		
		
		String strQtyOrdered = vars.getNumericParameter("inpqtyordered");
		Cantidad =  (new BigDecimal(strQtyOrdered)).setScale(2, BigDecimal.ROUND_HALF_UP);
		
		String strDescuento = vars.getNumericParameter("inpemFpvDescuento");
		Descuento =  (new BigDecimal(strDescuento)).setScale(2, BigDecimal.ROUND_HALF_UP);

		String strPrecioUnitario = vars.getNumericParameter("inpemFpvPrecioUnitario");
		PrecioUnitario =  (new BigDecimal(strPrecioUnitario)).setScale(2, BigDecimal.ROUND_HALF_UP);
		

		if (!Descuento.equals(BigDecimal.ZERO))

		{
			
			Descuento = Descuento.divide(new BigDecimal("100"));
			Descuento.setScale(2, BigDecimal.ROUND_HALF_UP);
			
			ValorDescuento = PrecioUnitario.multiply(Descuento);
			ValorDescuento.setScale(2, BigDecimal.ROUND_HALF_UP);

			TotalDescuento = PrecioUnitario.subtract(ValorDescuento);
			TotalDescuento.setScale(2, BigDecimal.ROUND_HALF_UP);
			
			TotalLinea = Cantidad.multiply(TotalDescuento);
			TotalLinea.setScale(2, BigDecimal.ROUND_HALF_UP);

			try {
				info.addResult("inpemFpvPrecioDescuento", ValorDescuento);
				info.addResult("inpemAteccoPrecioefectivo", TotalDescuento);
				info.addResult("inppriceactual", TotalDescuento);
				info.addResult("inpemAteccoTotalefectivo", TotalLinea);
				info.addResult("inplinenetamt", TotalLinea);
				info.addResult("inptaxbaseamt", TotalLinea);

			} catch (Exception e) {
				log4j.info("No default info for the selected payment method");
			}
		}
	}

}
