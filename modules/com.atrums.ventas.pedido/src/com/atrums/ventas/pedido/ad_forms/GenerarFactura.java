package com.atrums.ventas.pedido.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.util.Log;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.compras.montos.data.ateccoCondicion;
import com.atrums.compras.montos.model.ATECCO_Temporal;
import com.atrums.felectronica.process.ATECCO_Operaciones_XML;

public class GenerarFactura extends HttpSecureAppServlet {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;
  final OBError msg = new OBError();

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    if (vars.commandIn("DEFAULT")) {
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");  
      
      String strFpvFacturaCotId = vars.getStringParameter("inpNameValue");
      String strMetodoPagoId = vars.getStringParameter("inpMetodoPago"); 
             
      GenerarFacturaData.methodUpdatePedido(this, strMetodoPagoId, strFpvFacturaCotId);
      
      try {
          printPage(response, vars, strFpvFacturaCotId, strWindow, strTab);
      } catch (Exception e) {
          // TODO: handle exception
      }     
    } else if (vars.commandIn("SAVE")) {
      String strWindow = vars.getStringParameter("inpwindowId");
      String strTab = vars.getStringParameter("inpTabId");  
        
      String strFpvFacturaCotId = vars.getStringParameter("inpcOrderId");
      
      if ((!strFpvFacturaCotId.equals("")) || (!strFpvFacturaCotId.equals(null))) {
          FacturaPosData[] enviarData = null;

          String strmensaje = null;

          try {
            /*Primero creo el pedido de venta*/
            enviarData = FacturaPosData.methodSeleccionarTemporalPedido(this, strFpvFacturaCotId, vars.getWarehouse(), vars.getUser());
          } catch (Exception e) {
            // TODO: handle exception
            Log.error(e.getMessage());
            strmensaje = e.getMessage();
          }

          if (strmensaje != null) {
            OBError myMessage = new OBError();
            printPageClosePopUp(response, vars); 

            myMessage.setTitle("Error");
            myMessage.setType("ERROR");
            myMessage.setMessage(Utility.messageBD(this,
                "Documento No Creado, " + strmensaje.replaceAll("@CODE=0@ERROR: ", ""),
                vars.getLanguage()));

            vars.setMessage("FacturaPos", myMessage);
          } else if (enviarData.length > 0) {
        	
        	/*Crear factura, la guia y pago*/        	  
        	  GenerarFacturaData[] data1 = null; 
        	  String cOrderId = null;
        	  
        	  data1 = GenerarFacturaData.methodSeleccionarPedido(this, vars.getClient(), vars.getOrg(), ("%" + enviarData[0].fpvdocumentno));
        	  cOrderId = data1[0].dato1;
        	  
              String strMetodo = vars.getStringParameter("inpMetodo");
              boolean descuentoPago = true;

              OBContext.setAdminMode(true);
              Order order = OBDal.getInstance().get(Order.class, cOrderId); 
              
              List<ATECCO_Temporal> temp = new ArrayList<ATECCO_Temporal>();

              String strMetodoNota = vars.getStringParameter("inpAplicaNotaCredito").equals("Y") ? "Y"
                  : "N";

              if (strMetodoNota.equals("Y")) {
                String StrPagoNota = vars.getStringParameter("inpMontoPagoNotaCredito");

                ATECCO_Temporal newTemp = new ATECCO_Temporal();
                newTemp.setAmount(Double.parseDouble(StrPagoNota));
                newTemp.setTipoPago("Nota de Crédito".toUpperCase());
                temp.add(newTemp);
              }              
       
              if (strMetodo.equals("Mixto") || strMetodo.equals("Retencion")) {
                  //String strPagoEfectivo = vars.getStringParameter("inpMontoPagoPagado");
                  String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");

                  if (strPagoEfectivo != null) {
                    if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
                      newTemp.setTipoPago("Efectivo".toUpperCase());
                      temp.add(newTemp);
                    }
                  }

                  String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

                  if (strPagoElectronico != null) {
                    if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoElectronico));
                      newTemp.setTipoPago("Dinero Electrónico%".toUpperCase());
                      temp.add(newTemp);
                    }
                  }

                  String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
                  String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
                  String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

                  if (strPagoCheque != null) {
                    if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoCheque));
                      newTemp.setNro_cheque(strNroCheque);
                      newTemp.setBanco_id(strBancoCheque);
                      newTemp.setTipoPago("Cheque%".toUpperCase());
                      temp.add(newTemp);
                    }
                  }

                  String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
                  String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia");
                  String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

                  if (strPagoTransferencia != null) {
                    if (Double
                        .parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
                      newTemp.setReferenceno(strNroTransferencia);
                      newTemp.setBanco_id(strBancoTransferencia);
                      newTemp.setTipoPago("Transferencia/Depósito%".toUpperCase());
                      temp.add(newTemp);
                    }
                  }

                  String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
                  String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
                  String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");
                  String strBancoCondicion = vars.getStringParameter("inpBancoPagoCondicion");

                  if (strPagoTarjeta != null) {

                    if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
                      descuentoPago = false;
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
                      newTemp.setReferenceno(strNroTarjeta);
                      newTemp.setTarjeta_id(strBancoTarjeta);
                      newTemp.setCondicion_id(strBancoCondicion);

                      ateccoCondicion auxCon = OBDal.getInstance().get(ateccoCondicion.class,
                          strBancoCondicion);

                      /*if (auxCon.getCommercialName() != null) {
                        if (auxCon.getCommercialName().toUpperCase().equals("Corriente".toUpperCase())) {
                          newTemp
                              .setTipoPago("Tarjeta de Crédito ".toUpperCase() + "Corriente%".toUpperCase());
                        } else {
                          newTemp
                              .setTipoPago("Tarjeta de Crédito ".toUpperCase() + "Diferido%".toUpperCase());
                        }
                      } else { */
                        newTemp.setTipoPago("Tarjeta de Crédito".toUpperCase());
                      //}

                      temp.add(newTemp);
                    }
                  }

                  String strPagoDebito = vars.getStringParameter("inpMontoPagoDebito");
                  String strNroDebito = vars.getStringParameter("inpNroPagoDebito");
                  String strBancoDebito = vars.getStringParameter("inpBancoPagoDebito");

                  if (strPagoDebito != null) {

                    if (Double.parseDouble(strPagoDebito.equals("") ? "0" : strPagoDebito) > 0) {
                      descuentoPago = false;
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoDebito));
                      newTemp.setReferenceno(strNroDebito);
                      newTemp.setTarjeta_id(strBancoDebito);

                      newTemp.setTipoPago("Tarjeta de Débito".toUpperCase());

                      temp.add(newTemp);
                    }
                  }

                } else if (strMetodo.equals("Efectivo")) {
                  // String strPagoEfectivo = vars.getStringParameter("inpMontoPagoEfectivo");
                  String strPagoEfectivo = vars.getStringParameter("inpMontoPagoPagado");

                  if (strPagoEfectivo != null) {
                    if (Double.parseDouble(strPagoEfectivo.equals("") ? "0" : strPagoEfectivo) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoEfectivo));
                      newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                      temp.add(newTemp);
                    }
                  }
                } else if (strMetodo.equals("Electronico")) {
                  String strPagoElectronico = vars.getStringParameter("inpMontoPagoElectronico");

                  if (strPagoElectronico != null) {
                    if (Double.parseDouble(strPagoElectronico.equals("") ? "0" : strPagoElectronico) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoElectronico));
                      newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                      temp.add(newTemp);
                    }
                  }
                } else if (strMetodo.equals("Cheque")) {
                  String strPagoCheque = vars.getStringParameter("inpMontoPagoCheque");
                  String strNroCheque = vars.getStringParameter("inpNroPagoCheque");
                  String strBancoCheque = vars.getStringParameter("inpBancoPagoCheque");

                  if (strPagoCheque != null) {
                    if (Double.parseDouble(strPagoCheque.equals("") ? "0" : strPagoCheque) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoCheque));
                      newTemp.setNro_cheque(strNroCheque);
                      newTemp.setBanco_id(strBancoCheque);
                      newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                      temp.add(newTemp);
                    }
                  }
                }  else if (strMetodo.equals("Giftcard")) {
                    String strPagoGiftcard = vars.getStringParameter("inpMontoPagoGiftcard");

                    if (strPagoGiftcard != null) {
                      if (Double.parseDouble(strPagoGiftcard.equals("") ? "0" : strPagoGiftcard) > 0) {
                        ATECCO_Temporal newTemp = new ATECCO_Temporal();
                        newTemp.setAmount(Double.parseDouble(strPagoGiftcard));
                        newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                        temp.add(newTemp);
                      }
                    }                    
                } else if (strMetodo.equals("Tarjeta")) {
                  String strPagoTarjeta = vars.getStringParameter("inpMontoPagoTarjeta");
                  String strNroTarjeta = vars.getStringParameter("inpNroPagoTarjeta");
                  String strBancoTarjeta = vars.getStringParameter("inpBancoPagoTarjeta");
                  String strBancoCondicion = vars.getStringParameter("inpBancoPagoCondicion");

                  if (strPagoTarjeta != null) {
                    if (Double.parseDouble(strPagoTarjeta.equals("") ? "0" : strPagoTarjeta) > 0) {
                      descuentoPago = false;
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoTarjeta));
                      newTemp.setReferenceno(strNroTarjeta);
                      newTemp.setTarjeta_id(strBancoTarjeta);
                      newTemp.setCondicion_id(strBancoCondicion);
                      newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                      temp.add(newTemp);
                    }
                  }
                } else if (strMetodo.equals("Transferencia")) {
                  String strPagoTransferencia = vars.getStringParameter("inpMontoPagoTransferencia");
                  String strNroTransferencia = vars.getStringParameter("inpNroPagoTransferencia");
                  String strBancoTransferencia = vars.getStringParameter("inpBancoPagoTransferencia");

                  if (strPagoTransferencia != null) {
                    if (Double
                        .parseDouble(strPagoTransferencia.equals("") ? "0" : strPagoTransferencia) > 0) {
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoTransferencia));
                      newTemp.setReferenceno(strNroTransferencia);
                      newTemp.setBanco_id(strBancoTransferencia);
                      newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                      temp.add(newTemp);
                    }
                  }
                } else if (strMetodo.equals("Debito")) {
                  String strPagoDebito = vars.getStringParameter("inpMontoPagoDebito");
                  String strNroDebito = vars.getStringParameter("inpNroPagoDebito");
                  String strBancoDebito = vars.getStringParameter("inpBancoPagoDebito");

                  if (strPagoDebito != null) {
                    if (Double.parseDouble(strPagoDebito.equals("") ? "0" : strPagoDebito) > 0) {
                      descuentoPago = false;
                      ATECCO_Temporal newTemp = new ATECCO_Temporal();
                      newTemp.setAmount(Double.parseDouble(strPagoDebito));
                      newTemp.setReferenceno(strNroDebito);
                      newTemp.setTarjeta_id(strBancoDebito);
                      newTemp.setTipoPago(order.getPaymentMethod().getName().toUpperCase());
                      temp.add(newTemp);
                    }
                  }
                }

                String strWindowPath = Utility.getTabURL(strTab, "R", true);
                if (strWindowPath.equals("")) {
                  strWindowPath = strDefaultServlet;
                }

                OBError myError = processButton(vars, cOrderId, strMetodo, temp, descuentoPago);
                log4j.debug(myError.getMessage());
                vars.setMessage(strTab, myError);
                printPageClosePopUp(response, vars); 
  
	        	/*Mensaje*/
	            OBError myMessage = myError;
	
	            myMessage.setTitle("Success");
	            myMessage.setType("SUCCESS");
	            myMessage.setMessage(Utility.messageBD(this,
	                "Información: Pedido de venta Nº "+ enviarData[0].fpvdocumentno + " creado, " + myError.getMessage(), vars.getLanguage()));
	
	            vars.setMessage("FacturaPos", myMessage);
	      
          } else {
            OBError myMessage = new OBError();

            myMessage.setTitle("Error");
            myMessage.setType("ERROR");
            myMessage.setMessage(Utility.messageBD(this, "Documento No Creado", vars.getLanguage()));

            vars.setMessage("FacturaPos", myMessage);
          }
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this, "No hay productos para generar la factura",
              vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        }
    }
  }

  private void printPage(HttpServletResponse response, VariablesSecureApp vars, String strFpvFacturaCotId,
      String windowId, String strTab) throws IOException, ServletException {

    Connection conn = null;

    //Order order = OBDal.getInstance().get(Order.class, strFpvFacturaCotId);

    try {
      OBContext.setAdminMode(true);

      ComboTableData comboTableDataBanco = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_BANCO_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataTarjeta = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_TARJETA_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataDebito = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_TARJETA_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      ComboTableData comboTableDataCondicion = new ComboTableData(vars, this, "TABLEDIR",
          "ATECCO_CONDICION_ID", "", null,
          Utility.getContext(this, vars, "#User_Org", "MaterialReceiptPending"),
          Utility.getContext(this, vars, "#User_Client", "MaterialReceiptPending"), 0);

      XmlDocument xmlDocument = xmlEngine
          .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/GenerarFactura").createXmlDocument();
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("key", strFpvFacturaCotId);
      xmlDocument.setParameter("theme", vars.getTheme());
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("tab", strTab);

      String strMetodo = "";

      conn = OBDal.getInstance().getConnection();
      String sql = null;
      PreparedStatement ps = null;
      ResultSet rs = null;   
      
      /*Datos del pedido*/
      String sucursal = null;
      String tercero = null;
      String cliente = null;
      String metodo_pago = null;
      
      sql = "select ct.ad_org_id, ct.c_bpartner_id, ct.ad_client_id, fp.name as metodo_pago\r\n" + 
      		"       from fpv_factura_cot as ct\r\n" + 
      		"       inner join fin_paymentmethod fp on (ct.fin_paymentmethod_id = fp.fin_paymentmethod_id)\r\n" + 
      		"       where ct.fpv_factura_cot_id = '"+ strFpvFacturaCotId +"'";
             
       ps = conn.prepareStatement(sql); 
       rs = ps.executeQuery();
       
       while (rs.next()) { 
    	   sucursal = rs.getString("ad_org_id");
    	   tercero = rs.getString("c_bpartner_id");
    	   metodo_pago = rs.getString("metodo_pago");
       }
       
      
      String mLocatorId = "";
       
      sql = "SELECT ml.m_locator_id " + "FROM ad_org_warehouse aow "
              + "INNER JOIN m_warehouse mw ON (aow.m_warehouse_id = mw.m_warehouse_id) "
              + "INNER JOIN m_locator ml ON (mw.m_warehouse_id = ml.m_warehouse_id) "
              + "WHERE aow.ad_org_id = '"+ sucursal +"'";
       
       ps = conn.prepareStatement(sql); 
       rs = ps.executeQuery();
       
       while (rs.next()) { 
    	  mLocatorId = rs.getString("m_locator_id"); 
       }
       
      boolean sinProducto = false;
      
      sql = "SELECT col.cantidad as qtyordered, coalesce(mpsv.qtyonhand, 0) as qtyonhand \r\n" + 
      		"FROM fpv_factura_cot_lin col \r\n" + 
      		"LEFT JOIN m_product_stock_v mpsv ON (col.m_product_id = mpsv.m_product_id AND mpsv.m_locator_id = '"+ mLocatorId +"' AND mpsv.c_uom_id = '100') \r\n" + 
      		"LEFT JOIN m_product p ON (col.m_product_id = p.m_product_id) \r\n" + 
      		"WHERE col.fpv_factura_cot_id = '"+ strFpvFacturaCotId +"' AND p.producttype = 'I';";    

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        double movementQty = rs.getDouble("qtyordered");
        double auxQtyHand = rs.getDouble("qtyonhand");

        if (movementQty > auxQtyHand) {
          sinProducto = true;
          break;
        }
      }

      rs.close();
      ps.close();

      // ** Temporalmente false //
      // sinProducto = false;

      if (sinProducto) {
        OBDal.getInstance().rollbackAndClose();
        log4j.warn("Rollback in transaction");

        String strWindowPath = Utility.getTabURL(strTab, "R", true);
        if (strWindowPath.equals("")) {
          strWindowPath = strDefaultServlet;
        }
        
        OBError myError = new OBError();
        log4j.debug(myError.getMessage());
        vars.setMessage(strTab, myError);
        printPageClosePopUp(response, vars); 

        myError.setTitle("Error");
        myError.setType("ERROR");
        myError.setMessage(Utility.messageBD(this, "El proceso no se ejecuto correctamente: "
                + "No hay producto necesario en la bodega para realizar la transacción",
            vars.getLanguage()));

        vars.setMessage("FacturaPos", myError);
      }

      if (tercero == null) {
        OBDal.getInstance().rollbackAndClose();
        log4j.warn("Rollback in transaction");

        String strWindowPath = Utility.getTabURL(strTab, "R", true);
        if (strWindowPath.equals("")) {
          strWindowPath = strDefaultServlet;
        }
        
        OBError myError = new OBError();
        log4j.debug(myError.getMessage());
        vars.setMessage(strTab, myError);
        printPageClosePopUp(response, vars); 

        myError.setTitle("Error");
        myError.setType("ERROR");
        myError.setMessage(Utility.messageBD(this, "El proceso no se ejecuto correctamente: No hay tercero en el documento",
            vars.getLanguage()));

        vars.setMessage("FacturaPos", myError);        
      }

      String auxValor = "0";
      String auxValorNormal = "";

      int intAuxTarjeta = 0;

      /**
       * Codigo Nota de Credito
       */

      String auxTotalNota = "0";

      /*sql = "SELECT sum(ci.grandtotal) AS totalNota " + "FROM c_invoice ci "
          + "INNER JOIN c_doctype cd ON (cd.c_doctype_id = ci.c_doctypetarget_id) "
          + "WHERE ci.c_bpartner_id = '" + tercero
          + "' AND ci.ad_org_id = '" + sucursal + "' "
          + "AND cd.docbasetype = 'ARC' " + "AND cd.issotrx = 'Y' " + "AND ispaid = 'N';";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        auxTotalNota = rs.getString("totalNota");
      }

      rs.close();*/

      /**
       * Fin Nota de Credito
       */

      /**
       * Codigo Anticipos
       **/

      String auxAnticipoEfectivo = "0";
      String auxAnticipoCheque = "0";
      String auxAnticipoElectronico = "0";
      String auxAnticipoTransferencia = "0";
      String auxAnticipoTarjeta = "0"; 

      /*sql = "SELECT upper(fp.name) AS name, sum(aap.amount) AS total "
          + "FROM atecco_anticipo_pago aap "
          + "INNER JOIN fin_paymentmethod fp ON (aap.fin_paymentmethod_id = fp.fin_paymentmethod_id) "
          + "WHERE aap.c_order_id = '" + strKey + "' " + "GROUP BY fp.name;";

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        if (rs.getString("name").equals("Efectivo".toUpperCase())) {
          auxAnticipoEfectivo = rs.getString("total");
        } else if (rs.getString("name").equals("Cheque".toUpperCase())) {
          auxAnticipoCheque = rs.getString("total");
        } else if (rs.getString("name").equals("Dinero Electrónico".toUpperCase())) {
          auxAnticipoElectronico = rs.getString("total");
        } else if (rs.getString("name").equals("Transferencia/Depósito".toUpperCase())) {
          auxAnticipoTransferencia = rs.getString("total");
        } else if (rs.getString("name").equals("Tarjeta de Crédito".toUpperCase())) {
          auxAnticipoTarjeta = rs.getString("total");
        }
      }

      rs.close();*/

      /**
       * Fin Anticipos
       **/

      String auxMetodo = metodo_pago.toUpperCase();

      // auxMetodo = "Mixto";

      if (auxMetodo.equals("Nota de Crédito".toUpperCase())) {
        auxMetodo = "Mixto".toUpperCase();
      }
      /*no aplica*/
      /*sql = "UPDATE c_orderline col SET "
          + "pricelist = (SELECT (pricelist * ((fpm.em_atecco_porcentaje + 100)/100)) "
          + "FROM c_order co "
          + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE co.c_order_id = col.c_order_id), "
          + "priceactual = (SELECT (priceactual * ((fpm.em_atecco_porcentaje + 100)/100)) "
          + "FROM c_order co "
          + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
          + "WHERE co.c_order_id = col.c_order_id) WHERE col.c_order_id = '" + strKey + "' "
          + "AND col.m_product_id NOT IN (SELECT mp.m_product_id " + "FROM m_product mp "
          + "WHERE upper(mp.name) LIKE '%DESCUENTO%' " + "AND mp.ad_client_id = '"
          + cliente + "');";
      ps = conn.prepareStatement(sql);
      ps.executeUpdate();*/

      strMetodo = auxMetodo;

      if (auxMetodo.equals("Mixto".toUpperCase())) {
        strMetodo = "Mixto";
      } else if (auxMetodo.equals("Cheque".toUpperCase())) {
        strMetodo = "Cheque";
      } else if (auxMetodo.equals("Transferencia/Depósito".toUpperCase())) {
        strMetodo = "Transferencia";
      } else if (auxMetodo.equals("Tarjeta de Crédito".toUpperCase())) {
        strMetodo = "Tarjeta";
      } else if (auxMetodo.equals("Dinero Electrónico".toUpperCase())) {
        strMetodo = "Electronico";
      } else if (auxMetodo.equals("Efectivo".toUpperCase())) {
        strMetodo = "Efectivo";
      } else if (auxMetodo.equals("Gift Card".toUpperCase())) {
          strMetodo = "Giftcard";        
      } else if (auxMetodo.equals("Con Retención".toUpperCase())) {
        strMetodo = "Retencion";
      } else if (auxMetodo.indexOf("Tarjeta de Crédito".toUpperCase()) != -1) {
        strMetodo = "Tarjeta";
      } else if (auxMetodo.indexOf("Tarjeta de Credito".toUpperCase()) != -1) {
        strMetodo = "Tarjeta";
      } else if (auxMetodo.indexOf("Cheque".toUpperCase()) != -1) {
        strMetodo = "Cheque";
      } else if (auxMetodo.indexOf("Efectivo".toUpperCase()) != -1) {
        strMetodo = "Efectivo";
      } else if (auxMetodo.indexOf("Tarjeta de Débito".toUpperCase()) != -1) {
        strMetodo = "Debito";
      }

      sql = "select round((r.subtotal + r.precio_iva),2) as grandtotal \r\n" + 
      		"      	from (select sum(round(subtotal,2)) as subtotal, sum(precio_iva) as precio_iva\r\n" + 
      		"      	from fpv_factura_cot_lin where fpv_factura_cot_id = '"+ strFpvFacturaCotId +"') as r";
    		      
      /*sql = "SELECT round(co.grandtotal - coalesce(sum(aap.amount),0), 2) AS grandtotal "
          + "FROM c_order co "
          + "LEFT JOIN atecco_anticipo_pago aap ON (co.c_order_id = aap.c_order_id) "
          + "WHERE co.c_order_id = '" + strKey + "' " + "GROUP BY co.grandtotal";*/

      ps = conn.prepareStatement(sql);
      rs = ps.executeQuery();

      while (rs.next()) {
        auxValor = rs.getString("grandtotal");
      }

      rs.close();
      ps.close();

      /*if (Double.valueOf(auxValor) < 0) {
        auxValor = "0";
      }*/

      auxValorNormal = auxValor;

      if (strMetodo.toUpperCase().equals("Mixto".toUpperCase())) {
        if (intAuxTarjeta > 0) {
          xmlDocument.setParameter("tituloTotalTarjeta", "");
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("montoPagoDesc", auxValor);
        } else {
          xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Documento: " + auxValorNormal);
          xmlDocument.setParameter("montoPagoDesc", auxValorNormal);
        }
      } else if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {

        if (intAuxTarjeta > 0) {
          xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
          xmlDocument.setParameter("tituloTotalEfectivo", "");
        } else {
          xmlDocument.setParameter("tituloTotalTarjeta", "");
          xmlDocument.setParameter("tituloTotalEfectivo", "Total Documento: " + auxValor);
        }
      } else {
        xmlDocument.setParameter("tituloTotalTarjeta", "Total Tarjeta: " + auxValor);
        xmlDocument.setParameter("tituloTotalEfectivo", "Total Documento: " + auxValorNormal);
        xmlDocument.setParameter("montoPagoDesc", auxValorNormal);
      }

      xmlDocument.setParameter("metodo", strMetodo);
      xmlDocument.setParameter("montoPago", auxValor);

      xmlDocument.setParameter("valorPagoEfectivo", auxValor);

      /**
       * Nota de Credito
       */

      xmlDocument.setParameter("montoPagoNotaCredito", auxTotalNota);

      /**
       * Fin Nota de Credito
       */

      /**
       * Anticipos
       **/

      Double totalEfectivo = Double.valueOf(auxAnticipoEfectivo) + Double.valueOf(auxAnticipoCheque)
          + Double.valueOf(auxAnticipoElectronico) + Double.valueOf(auxAnticipoTransferencia)
          + Double.valueOf(auxAnticipoTarjeta);

      xmlDocument.setParameter("montoAnticipoEfectivo", auxAnticipoEfectivo);
      xmlDocument.setParameter("montoAnticipoCheque", auxAnticipoCheque);
      xmlDocument.setParameter("montoAnticipoElectronico", auxAnticipoElectronico);
      xmlDocument.setParameter("montoAnticipoTransferencia", auxAnticipoTransferencia);
      xmlDocument.setParameter("montoAnticipoTarjeta", auxAnticipoTarjeta);

      /**
       * Fin anicipos
       **/

      if (strMetodo.toUpperCase().equals("Efectivo".toUpperCase())) {
        /*
         * xmlDocument.setParameter("montoPagoEfectivo", auxValor);
         */
        xmlDocument.setParameter("montoAnticipoEfectivo", totalEfectivo.toString());
        xmlDocument.setParameter("montoPagoEfectivo", "0.00");
      } else {
        xmlDocument.setParameter("montoPagoEfectivo", "0.00");
      }

      xmlDocument.setParameter("valorPagoElectronico", auxValor);

      if (strMetodo.toUpperCase().equals("Electronico".toUpperCase())) {
        xmlDocument.setParameter("montoPagoElectronico", auxValor);
        xmlDocument.setParameter("montoAnticipoElectronico", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoElectronico", "0.00");
      }

      xmlDocument.setParameter("valorPagoCheque", auxValor);

      if (strMetodo.toUpperCase().equals("Cheque".toUpperCase())) {
        xmlDocument.setParameter("montoPagoCheque", auxValor);
        xmlDocument.setParameter("montoAnticipoCheque", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoCheque", "0.00");
      }

      xmlDocument.setParameter("nroPagoCheque", "0.00");
      
      //Gif Card
      xmlDocument.setParameter("valorPagoGiftcard", auxValor);

      if (strMetodo.toUpperCase().equals("Giftcard".toUpperCase())) {
        xmlDocument.setParameter("montoPagoGiftcard", auxValor);
        //xmlDocument.setParameter("montoAnticipoCheque", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoGiftcard", "0.00");
      }
      //xmlDocument.setParameter("nroPagoCheque", "0.00");
      
      //Fin Gif Card

      xmlDocument.setParameter("valorPagoTransferencia", auxValor);

      if (strMetodo.toUpperCase().equals("Transferencia".toUpperCase())) {
        xmlDocument.setParameter("montoPagoTransferencia", auxValor);
        xmlDocument.setParameter("montoAnticipoTransferencia", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoTransferencia", "0.00");
      }

      xmlDocument.setParameter("nroPagoTransferencia", "0.00");

      xmlDocument.setParameter("valorPagoTarjeta", auxValor);

      if (strMetodo.toUpperCase().equals("Tarjeta".toUpperCase())) {
        xmlDocument.setParameter("montoPagoTarjeta", auxValor);
        xmlDocument.setParameter("montoAnticipoTarjeta", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoTarjeta", "0.00");
      }

      xmlDocument.setParameter("nroPagoTarjeta", "0.00");

      xmlDocument.setParameter("valorPagoRetencion", auxValor);

      xmlDocument.setParameter("montoPagoRetencion", "0.00");

      xmlDocument.setParameter("nroPagoRetencion", "0.00");

      xmlDocument.setParameter("montoPagoRetencionIva", "0.00");
      xmlDocument.setParameter("montoPagoRetencionRenta", "0.00");

      if (strMetodo.toUpperCase().equals("Debito".toUpperCase())) {
        xmlDocument.setParameter("montoPagoDebito", auxValor);
        xmlDocument.setParameter("montoAnticipoDebito", totalEfectivo.toString());
      } else {
        xmlDocument.setParameter("montoPagoDebito", "0.00");
      }

      xmlDocument.setParameter("nroPagoDebito", "0.00");

      if (strMetodo.toUpperCase().equals("Retencion".toUpperCase())) {      
    	  
    	  String strKey = null;
    	  BigDecimal base_imponible = BigDecimal.ZERO;
    	  BigDecimal impuesto = BigDecimal.ZERO;
    	  
    	  /*Trae el subtotal y y el total de los impuestos de la venta*/
    	  FacturaPosData[] data3 = FacturaPosData.methodTotales(this, strFpvFacturaCotId);
    	  base_imponible = new BigDecimal(data3[0].mproductpricetotal);
    	  impuesto = new BigDecimal(data3[0].mproducttotalimp);
    	  
      	/*
      	 * 
      	 * MODIFICADO ..::ETA::.. PARA CALCULO RETENCIÓN DE RETENCION ARTICULOS Y SERVICIOS 
      	 * 
      	 * */  
      	  
      	/*
      	 *IVA
      	 */
      	  
      	BigDecimal retTotal = BigDecimal.ZERO;
      	  
      	String strRetIvaServicios="0.00";
      	String strRetIvaProductos="0.00";
        String strRetencionIva = "0.00";
        BigDecimal retIvaTotal = BigDecimal.ZERO;
          
          /*
           * OBTIENE EL VALOR DE RETENCIÓN TIPO IVA DE TODOS LOS PRODUCTOS TIPO ARTICULOS 
           */          
          
          sql = "SELECT (coalesce((ctr.porcentaje/100),0) * '"+ impuesto +"') as valor FROM fpv_factura_cot co \r\n" + 
          		"          		 INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) \r\n" + 
          		"          		 INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id)   \r\n" + 
          		"          		 WHERE co.fpv_factura_cot_id = '"+ strFpvFacturaCotId +"' \r\n" + 
          		"          		 AND ctr.tipo = 'IVA' \r\n" + 
          		"          		 AND ctr.isactive = 'Y' \r\n" + 
          		"          		 AND cbr.em_atecco_producto = 'Y' limit 1";

          ps = conn.prepareStatement(sql);
          rs = ps.executeQuery();

          while (rs.next()) {
          	strRetIvaProductos = rs.getString("valor");
          }

          rs.close();
          ps.close();
          
          /*
           * OBTIENE EL VALOR DE RETENCIÓN TIPO IVA DE TODOS LOS PRODUCTOS TIPO SERVICIOS 
           */
                 
          /*sql = "SELECT coalesce(sum(a.porcentajeRet),0) as valor" + 
          		" FROM ( " + 
          		" SELECT (ctr.porcentaje / 100) * colt.taxamt AS porcentajeRet, ctr.porcentaje, colt.taxbaseamt, colt.taxamt, ol.m_product_id" + 
          		" FROM c_order co " + 
          		" INNER JOIN c_orderline ol on (ol.c_order_id = co.c_order_id) " + 
          		" INNER JOIN m_product p on (p.m_product_id = ol.m_product_id) " + 
          		" INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) " + 
          		" INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) " + 
          		" INNER JOIN c_orderlinetax colt ON (ol.c_orderline_id = colt.c_orderline_id and colt.c_order_id = co.c_order_id and colt.c_orderline_id in (select ol.c_orderline_id" + 
          		"      	                                                                                            	from c_orderline ol" + 
          		"      	                                                                                            	inner join m_product p on (p.m_product_id = ol.m_product_id)" + 
          		"      	                                                                                            	where c_order_id = '" + strKey + "'"+ 
          		"      	                                                                                            	and p.producttype = 'S'))" + 
          		" WHERE co.c_order_id = '" + strKey + "'" + 
          		" AND ctr.tipo = 'IVA'" + 
          		" AND ctr.isactive = 'Y'" + 
          		" AND cbr.em_atecco_servicio = 'Y'" + 
          		" AND p.producttype = 'S' " + 
          		" GROUP BY 2,3,4,5) AS a;";

          ps = conn.prepareStatement(sql);
          rs = ps.executeQuery();

          while (rs.next()) {
          	strRetIvaServicios = rs.getString("valor");
          }

          rs.close();
          ps.close();*/
          
          /*
           *  SUMO EL TOTAL DE RETENCIÓN EN IVA Y ASIGNO A VARIABLE PARA MOSTRAR
           */

          retIvaTotal = (new BigDecimal(strRetIvaProductos).add(new BigDecimal(strRetIvaServicios)));   
          retIvaTotal = retIvaTotal.setScale(2,RoundingMode.HALF_UP);         
          retTotal=retTotal.add(retIvaTotal);       
          strRetencionIva = retIvaTotal.toString(); 
          
          /*
      	 * FUENTE
      	 */ 	  
      	  
      	  String strRetFuenteServicios="0.00";
      	  String strRetFuenteProductos="0.00";
          String strRetencionFuente = "0.00";
          String strRetencionTotal = "0.00";
          
          BigDecimal retFuenteTotal = BigDecimal.ZERO;
          
          /*
           * OBTIENE EL VALOR DE RETENCIÓN TIPO FUENTE DE TODOS LOS PRODUCTOS TIPO ARTICULOS 
           */
          
          sql = "SELECT (coalesce((ctr.porcentaje/100),0) * '"+ base_imponible +"') AS valor FROM fpv_factura_cot co \r\n" + 
          		"     INNER JOIN fpv_factura_cot_lin ol on (ol.fpv_factura_cot_id = co.fpv_factura_cot_id) \r\n" + 
          		"     INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id) \r\n" + 
          		"     INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id) \r\n" + 
          		"        		WHERE co.fpv_factura_cot_id = '"+ strFpvFacturaCotId +"' \r\n" + 
          		"        		AND ctr.tipo = 'FUENTE' \r\n" + 
          		"        		AND ctr.isactive = 'Y' \r\n" + 
          		"        		AND cbr.em_atecco_producto = 'Y' ";

          ps = conn.prepareStatement(sql);
          rs = ps.executeQuery();

          while (rs.next()) {
          	strRetFuenteProductos = rs.getString("valor");
          }

          rs.close();
          ps.close();          
          
          /*
           * OBTIENE EL VALOR DE RETENCIÓN TIPO FUENTE DE TODOS LOS PRODUCTOS TIPO SERVICIOS 
           */
                    
          /*sql = "SELECT coalesce(sum(a.porcentajeRet),0) as valor" + 
          		"    FROM (" + 
          		"            SELECT (ctr.porcentaje / 100) * ol.linenetamt AS porcentajeRet, ctr.porcentaje, ol.linenetamt, ol.m_product_id" + 
          		"            FROM c_order co" + 
          		"            INNER JOIN c_orderline ol on (ol.c_order_id = co.c_order_id)" + 
          		"            INNER JOIN m_product p on (p.m_product_id = ol.m_product_id)" + 
          		"            INNER JOIN co_bp_retencion_venta cbr ON (cbr.c_bpartner_id = co.c_bpartner_id)" + 
          		"            INNER JOIN co_tipo_retencion ctr ON (cbr.co_tipo_retencion_id = ctr.co_tipo_retencion_id)" + 
          		" 	 WHERE co.c_order_id = '" + strKey + "'" + 
          		"    AND ctr.tipo = 'FUENTE'" + 
          		"    AND ctr.isactive = 'Y'" + 
          		"    AND cbr.em_atecco_servicio = 'Y'" + 
          		"    AND p.producttype = 'S' " +
          		"	 GROUP BY 2,3,4) AS a;";


          ps = conn.prepareStatement(sql);
          rs = ps.executeQuery();

          while (rs.next()) {
          	strRetFuenteServicios = rs.getString("valor");
          }

          rs.close();
          ps.close(); */          
          
          /*
           *  SUMO EL TOTAL DE RETENCIÓN EN FUENTE Y ASIGNO A VARIABLE PARA MOSTRAR
           */
          
          retFuenteTotal = new BigDecimal(strRetFuenteProductos).add(new BigDecimal(strRetFuenteServicios));          
          retFuenteTotal = retFuenteTotal.setScale(2,RoundingMode.HALF_UP);         
          retTotal=retTotal.add(retFuenteTotal);          
          strRetencionFuente = retFuenteTotal.toString();       
          strRetencionTotal = retTotal.toString();
          
          xmlDocument.setParameter("montoPagoRetencionIva", strRetencionIva);
          xmlDocument.setParameter("montoPagoRetencionRenta", strRetencionFuente);
          xmlDocument.setParameter("montoPagoRetencionTotal", strRetencionTotal);
      }

      xmlDocument.setData("reportATECCOBANCOCHEQUE_ID", "liststructure",
          comboTableDataBanco.select(false));

      xmlDocument.setData("reportATECCOBANCOTRANSFERENCIA_ID", "liststructure",
          comboTableDataBanco.select(false));

      xmlDocument.setData("reportATECCOBANCOTARJETA_ID", "liststructure",
          comboTableDataTarjeta.select(false));

      xmlDocument.setData("reportATECCOBANCODEBITO_ID", "liststructure",
          comboTableDataDebito.select(false));

      xmlDocument.setData("reportATECCOBANCOCONDICION_ID", "liststructure",
          comboTableDataCondicion.select(false));

      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      out.println(xmlDocument.print());
      out.close();

      conn.rollback();
      conn.close();
    } catch (Exception ex) {
      // TODO: handle exception
      throw new ServletException(ex);
    } finally {
      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }
  }

  private OBError processButton(VariablesSecureApp vars, String strOrder,
      String strMetodo, List<ATECCO_Temporal> temp, boolean descuentoPago) {
    OBError myError = null;
    Connection conn = OBDal.getInstance().getConnection();
    
    OBContext.setAdminMode(true);
    try {

      myError = new OBError();
      myError.setType("Success");
      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
      // myError.setMessage(Utility.messageBD(this, "RecordsCopied", vars.getLanguage()));

      for (int i = 0; i < temp.size(); i++) {
        GenerarFacturaData.methodInsertarTemp(this, vars.getClient(),
            String.valueOf(temp.get(i).getAmount()), temp.get(i).getTarjeta_id(),
            temp.get(i).getCondicion_id(), temp.get(i).getBanco_id(), temp.get(i).getReferenceno(),
            temp.get(i).getNro_cheque(), temp.get(i).getTipoPago(), vars.getClient(),
            strOrder);
      }

      //OBDal.getInstance().commitAndClose();

      GenerarFacturaData.methodUpdateOrder(this, "CO", strOrder);
      
      /*Procesar factura*/
      String sqlQuery = "";
      int updateOrder = 0;
      int insertpinstance = 0;
      String auxAdPinstanceId = null;
      String strPInstance = null;
      
      sqlQuery = "UPDATE c_orderline col SET "
              + "pricelist = (SELECT (pricelist * ((fpm.em_atecco_porcentaje + 100)/100)) "
              + "FROM c_order co "
              + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
              + "WHERE co.c_order_id = col.c_order_id), "
              + "priceactual = (SELECT (priceactual * ((fpm.em_atecco_porcentaje + 100)/100)) "
              + "FROM c_order co "
              + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
              + "WHERE co.c_order_id = col.c_order_id) WHERE col.c_order_id = '" + strOrder + "' "
              + "AND col.m_product_id NOT IN (SELECT mp.m_product_id " + "FROM m_product mp "
              + "WHERE upper(mp.name) LIKE '%DESCUENTO%' " + "AND mp.ad_client_id = '"
              + vars.getClient() + "');";
          PreparedStatement ps = conn.prepareStatement(sqlQuery);
          ps.executeUpdate();
          ps.close();

          sqlQuery = "SELECT * FROM atecco_cuadrardescuento_linea('" + strOrder + "')";
          ps = conn.prepareStatement(sqlQuery);
          ps.execute();

          ps.close();

          sqlQuery = "SELECT ad_pinstance_id " + "FROM ad_pinstance " + "WHERE record_id = '" + strOrder
              + "' AND result = 1 " + "ORDER BY created DESC " + "LIMIT 1;";
          ps = conn.prepareStatement(sqlQuery);
          ResultSet rs = ps.executeQuery();

          while (rs.next()) {
            auxAdPinstanceId = rs.getString("ad_pinstance_id");
            insertpinstance = 1;
            updateOrder = 1;
          }

          ps.close();
          rs.close();

          if (auxAdPinstanceId == null) {
            sqlQuery = "UPDATE c_order SET em_atecco_docaction = 'CO', docaction = 'CO' WHERE c_order_id = '"
                + strOrder + "'";

            ps = conn.prepareStatement(sqlQuery);
            updateOrder = ps.executeUpdate();
            strPInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();
            ps.close();
          }

          if (updateOrder == 1) {

            if (auxAdPinstanceId == null) {
              sqlQuery = "INSERT INTO ad_pinstance("
                  + "ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
                  + "ad_user_id, updated, result, errormsg, ad_client_id, ad_org_id, "
                  + "createdby, updatedby, isactive) " + "VALUES ('" + strPInstance + "', '104', '"
                  + strOrder + "', 'N', now(), " + "'100', now(), null, null, '"
                  + vars.getClient() + "', '0', " + "'100', '100', 'Y');";

              ps = conn.prepareStatement(sqlQuery);
              insertpinstance = ps.executeUpdate();
              ps.close();
            }

            if (insertpinstance == 1) {

              if (auxAdPinstanceId == null) {
            	            	  
                sqlQuery = "SELECT * FROM c_order_post('" + strPInstance + "')";

                ps = conn.prepareStatement(sqlQuery);
                ps.execute();

                ps.close();
                
              } else {
                strPInstance = auxAdPinstanceId;
              }

              sqlQuery = "SELECT result, errormsg FROM ad_pinstance WHERE ad_pinstance_id = '"
                  + strPInstance + "'";

              ps = conn.prepareStatement(sqlQuery);
              rs = ps.executeQuery();
              int auxresult = 0;
              String auxerrormsg = null;

              while (rs.next()) {
                if (rs.getString("result") != null) {
                  auxresult = rs.getInt("result");
                }

                auxerrormsg = rs.getString("errormsg") != null ? rs.getString("errormsg") : "";
              }

              ps.close();

              sqlQuery = "UPDATE c_order SET em_atecco_docaction = docaction WHERE c_order_id = '"
                  + strOrder + "'";

              ps = conn.prepareStatement(sqlQuery);
              ps.executeUpdate();

              ps.close();
              rs.close();

              if (auxresult == 1) {
                sqlQuery = "SELECT ci.c_invoice_id, cd.em_atecfe_fac_elec AS doc1, cd2.em_atecfe_fac_elec AS doc2, cd2.name "
                    + "FROM c_order co " + "INNER JOIN c_invoice ci ON (co.c_order_id = ci.c_order_id) "
                    + "INNER JOIN c_doctype cd ON (co.c_doctype_id = cd.c_doctype_id) "
                    + "INNER JOIN c_doctype cd2 ON (ci.c_doctype_id = cd2.c_doctype_id) "
                    + "WHERE co.c_order_id = '" + strOrder + "' " + "AND ci.em_atecfe_docaction = 'PR' "
                    + "LIMIT 1;";

                ps = conn.prepareStatement(sqlQuery);
                rs = ps.executeQuery();

                String strCinvoiceId = null;
                String auxdoc1 = null;
                String auxdoc2 = null;
                String auxname = null;
                String procesado = null;

                while (rs.next()) {
                  if (rs.getString("c_invoice_id") != null) {
                    strCinvoiceId = rs.getString("c_invoice_id");
                    auxdoc1 = rs.getString("doc1");
                    auxdoc2 = rs.getString("doc2");
                    auxname = rs.getString("name");
                  }
                }

                ps.close();
                rs.close();
               
                if (strCinvoiceId != null) {
                  sqlQuery = "SELECT * FROM fpv_gen_pago('" + strCinvoiceId + "', '" + strOrder
                      + "')";

                  ps = conn.prepareStatement(sqlQuery);
                  ps.execute();

                  ps.close();

                  sqlQuery = "UPDATE c_order SET em_atecco_docstatus = '--' WHERE c_order_id = '"
                      + strOrder + "';";
                  ps = conn.prepareStatement(sqlQuery);
                  ps.execute();

                  ps.close();
                  
                  sqlQuery = "SELECT * FROM fpv_actua_invoiceline('" + strCinvoiceId + "', '" + strOrder
                          + "')";
                  ps = conn.prepareStatement(sqlQuery);
                  ps.execute();

                  ps.close();
                }

                if (strCinvoiceId != null && auxdoc1.equals("Y")) {
                  if (auxdoc2.equals("Y")) {
                    ATECCO_Operaciones_XML opeXML = new ATECCO_Operaciones_XML();

                    OBDal.getInstance().commitAndClose();                

                    if (opeXML.declararFacturaSRI(strCinvoiceId, vars.getUser(), msg, this)) {
                      OBDal.getInstance().commitAndClose();
                      GenerarFacturaData.methodUpdateOrder(this, "--", strOrder);
                      myError = new OBError();
                      myError.setType("Success");
                      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
                      auxerrormsg = auxerrormsg + ", " + this.msg.getMessage();
                    } else {
                      OBDal.getInstance().rollbackAndClose();
                      GenerarFacturaData.methodUpdateOrder(this, "--", strOrder);
                      myError = new OBError();
                      myError.setType("Success");
                      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
                      auxerrormsg = auxerrormsg + ", Error Factura Electrónica: "
                          + this.msg.getMessage()
                          + ", corrija el error y procese con el SRI desde la ventana documento de venta";
                    }
                  } else {
                    auxerrormsg = "@El tipo de documento '" + auxname
                        + "' no esta configurado como factura electronica@";
                    OBDal.getInstance().rollbackAndClose();
                    myError = new OBError();
                    myError.setType("Error");
                    myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
                    myError.setMessage(auxerrormsg);
                    return myError;
                  }
                } else {
                  OBDal.getInstance().commitAndClose();
                  GenerarFacturaData.methodUpdateOrder(this, "--", strOrder);
                  myError = new OBError();
                  myError.setType("Success");
                  myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
                }
              } else {
                OBDal.getInstance().rollbackAndClose();

                GenerarFacturaData.methodUpdateOrder(this, "SP", strOrder);
                GenerarFacturaData.methodDeleteOrder(this, strOrder);

                conn = OBDal.getInstance().getConnection();          

                ps.close();
                OBDal.getInstance().commitAndClose();

                myError = new OBError();
                myError.setType("Error");
                myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

                conn.close();
                conn = null;
              }

              if (auxerrormsg != null) {
                auxerrormsg = auxerrormsg.replaceAll("@ERROR=", "");
              } else {
                auxerrormsg = "@Documento procesado@";
              }

              String auxerrormsgTrad = "";

              while (auxerrormsg.indexOf("@") != -1) {
                int auxIni = auxerrormsg.indexOf("@");
                int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

                String auxParteInicio = auxerrormsg.substring(0, auxIni);
                String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);

                auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());

                auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;

                auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
              }

              auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

              myError.setMessage(auxerrormsgTrad);
              return myError;
            }
          }
          
          OBDal.getInstance().commitAndClose();
      /*Fin procesar factura*/
      
      
    } catch (Exception e) {
      OBDal.getInstance().rollbackAndClose();

      try {
    	  GenerarFacturaData.methodUpdateOrder(this, "SP", strOrder);
    	  GenerarFacturaData.methodDeleteOrder(this, strOrder);
      } catch (ServletException e1) {
        // TODO Auto-generated catch block
        e1.printStackTrace();
      }

      e.printStackTrace();
      log4j.warn("Rollback in transaction");
      myError = new OBError();
      myError.setType("Error");
      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
      // myError = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
    } finally {
      OBContext.restorePreviousMode();

      if (conn != null) {
        try {
          conn.close();
        } catch (Exception e) {
        }
      }
    }

    return myError;
  }

}
