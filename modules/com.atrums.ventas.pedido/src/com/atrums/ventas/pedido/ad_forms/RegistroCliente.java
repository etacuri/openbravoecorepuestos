/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.ventas.pedido.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.xmlEngine.XmlDocument;

public class RegistroCliente extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    final VariablesSecureApp vars = new VariablesSecureApp(request);

    OBContext.setAdminMode(true);
 
    String strWindow = vars.getStringParameter("inpwindowId");
    String strTab = vars.getStringParameter("inpTabId");  
    
    if (vars.commandIn("DEFAULT")) {
        
        try {
            printPage(response, vars, "DEFAULT", strWindow, strTab);
        } catch (Exception e) {
            // TODO: handle exception
        }
    } else if (vars.commandIn("BUSCARTERCERO")) {
        String strfpvFacturaCotId = vars.getStringParameter("inpfpvFacturaCotId");

        String strIdentificacion = vars.getStringParameter("inpIdentificacion");
        String strTipoPersona = vars.getStringParameter("inpTipoPersona");
        String strTipoIdentificacion = vars.getStringParameter("inpTipoIdentificacion");

        String strNombre = vars.getStringParameter("inpNombre");
        String strApellido = vars.getStringParameter("inpApellido");
        String strNombreComercial = vars.getStringParameter("inpNombreComercial");

        String strRazonSocial = vars.getStringParameter("inpRazonSocial");
        String strEmail = vars.getStringParameter("inpEmail");
        String strTelefono = vars.getStringParameter("inpTelefono");
        String strDireccionClie = vars.getStringParameter("inpDireccion");

        String strMetodoPago = vars.getStringParameter("inpMetodoPago");
        String strMontoPago = vars.getStringParameter("inpMontoPago");

        String strcheck = vars.getStringParameter("inpTipoConsumido");
        if (strcheck.equals("")) { strcheck = "N"; }
        
        printPageTercero(response, vars, strfpvFacturaCotId, strWindow, strTab,
            vars.getClient(), strIdentificacion, strTipoPersona, strTipoIdentificacion, strNombre,
            strApellido, strNombreComercial, strRazonSocial, strEmail, strTelefono, strDireccionClie,
            strMetodoPago, strMontoPago, strcheck);
      } /*else if (vars.commandIn("BUSCARMETODO")) {
        String strfpvFacturaCotId = vars.getStringParameter("inpfpvFacturaCotId");

        String strIdentificacion = vars.getStringParameter("inpIdentificacion");
        String strTipoPersona = vars.getStringParameter("inpTipoPersona");
        String strTipoIdentificacion = vars.getStringParameter("inpTipoIdentificacion");

        String strNombre = vars.getStringParameter("inpNombre");
        String strApellido = vars.getStringParameter("inpApellido");
        String strNombreComercial = vars.getStringParameter("inpNombreComercial");

        String strRazonSocial = vars.getStringParameter("inpRazonSocial");
        String strEmail = vars.getStringParameter("inpEmail");
        String strTelefono = vars.getStringParameter("inpTelefono");
        String strDireccionClie = vars.getStringParameter("inpDireccion");

        String strMetodoPago = vars.getStringParameter("inpMetodoPago");
        String strMontoPago = vars.getStringParameter("inpMontoPago");
        
        String strcheck = vars.getStringParameter("inpTipoConsumido");
        if (strcheck.equals("")) { strcheck = "N"; }

        printPageMetodo(response, vars, strfpvFacturaCotId, strWindow, strTab, vars.getClient(),
            strIdentificacion, strTipoPersona, strTipoIdentificacion, strNombre, strApellido,
            strNombreComercial, strRazonSocial, strEmail, strTelefono, strDireccionClie,
            strMetodoPago, strMontoPago, strcheck);
      }*/ else if (vars.commandIn("SAVE")) {   
    	  
    	String strfpvFacturaCotId = vars.getStringParameter("inpfpvFacturaCotId");
             
        String strTipoPersona = vars.getStringParameter("inpTipoPersona");
        String strTipoIdentificacion = vars.getStringParameter("inpTipoIdentificacion");
        String strIdentificacion = vars.getStringParameter("inpIdentificacion");

        String strNombre = vars.getStringParameter("inpNombre");
        String strApellido = vars.getStringParameter("inpApellido");
        String strNombreComercial = vars.getStringParameter("inpNombreComercial");

        String strRazonSocial = vars.getStringParameter("inpRazonSocial");
        String strEmail = vars.getStringParameter("inpEmail");
        String strTelefono = vars.getStringParameter("inpTelefono");
        String strDireccionClie = vars.getStringParameter("inpDireccion");
        /*String strMetodoPago = vars.getStringParameter("inpMetodoPago");*/

        String strWindowPath = Utility.getTabURL(strTab, "R", true);
        if (strWindowPath.equals("")) {
          strWindowPath = strDefaultServlet;
        }
        
        OBError myError = processButton(vars, strfpvFacturaCotId, strTipoPersona,
            strTipoIdentificacion, strIdentificacion, strNombre, strApellido, strNombreComercial,
            strRazonSocial, strEmail, strTelefono, strDireccionClie);
        log4j.debug(myError.getMessage());
        vars.setMessage(strTab, myError);

        printPageClosePopUp(response, vars);   
      }
  }
  
  private void printPageMetodo(HttpServletResponse response, VariablesSecureApp vars,
	      String strfpvFacturaCotId, String windowId, String strTab, String strClient,
	      String strIdentificacion, String strTipoPersona, String strTipoIdentificacion,
	      String strNombre, String strApellido, String strNombreComercial, String strRazonSocial,
	      String strEmail, String strTelefono, String strDireccionClie, String auxFinPaymentmethod,
	      String auxMontoPago, String strcheck) throws IOException, ServletException {

	    String montoPago = auxMontoPago;
	  
	    try {
	      ComboTableData comboTableDataTipoPersona = new ComboTableData(vars, this, "TABLEDIR",
	          "FPV_TIPOPERSONA_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      ComboTableData comboTableDataTipoIdentificacion = new ComboTableData(vars, this, "TABLEDIR",
	          "FPV_TIPOIDENTIFICACION_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      ComboTableData comboTableDataMetodoPago = new ComboTableData(vars, this, "TABLEDIR",
	          "FPV_METODOPAGO_V_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      XmlDocument xmlDocument = xmlEngine
	          .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/RegistroCliente")
	          .createXmlDocument();
	      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	      xmlDocument.setParameter("theme", vars.getTheme());
	      xmlDocument.setParameter("fpvfacturacotid", strfpvFacturaCotId);
	      xmlDocument.setParameter("controltipoconsumo", strcheck);
	      xmlDocument.setParameter("window", windowId);
	      xmlDocument.setParameter("tab", strTab);

	      /**
	       * Carga de datos de tercero
	       **/
	      xmlDocument.setParameter("identificacion", strIdentificacion);

	      Utility.fillSQLParameters(this, vars, null, comboTableDataTipoPersona, windowId, null);
	      xmlDocument.setData("reportFPVTipoPersona_ID", "liststructure",
	          comboTableDataTipoPersona.select(false));
	      Utility.fillSQLParameters(this, vars, null, comboTableDataTipoIdentificacion, windowId, null);
	      xmlDocument.setData("reportFPVTipoIdentificacion_ID", "liststructure",
	          comboTableDataTipoIdentificacion.select(false));
	      Utility.fillSQLParameters(this, vars, null, comboTableDataMetodoPago, windowId, null);
	      xmlDocument.setData("reportFPVMetodoPago_ID", "liststructure",
	          comboTableDataMetodoPago.select(false));

	      xmlDocument.setParameter("nombre", strNombre);
	      xmlDocument.setParameter("apellido", strApellido);
	      xmlDocument.setParameter("nombreComercial", strNombreComercial);
	      xmlDocument.setParameter("razonSocial", strRazonSocial);
	      xmlDocument.setParameter("email", strEmail);
	      xmlDocument.setParameter("telefono", strTelefono);
	      xmlDocument.setParameter("direccionClie", strDireccionClie);

	      RegistroClienteData[] auxMontoPagoData = RegistroClienteData.methodTotalMetodo(this,
	          auxFinPaymentmethod, strfpvFacturaCotId);

	      if (auxMontoPagoData.length > 0) {
	        montoPago = auxMontoPagoData[0].dato1;
	      }

	      xmlDocument.setParameter("montopago", montoPago);

	      xmlDocument.setParameter("tipopersonaId", strTipoPersona);
	      xmlDocument.setParameter("tipotdentificacionid", strTipoIdentificacion);
	      xmlDocument.setParameter("metodopagoid", auxFinPaymentmethod);

	      response.setContentType("text/html; charset=UTF-8");
	      PrintWriter out = response.getWriter();
	      out.println(xmlDocument.print());
	      out.close();

	    } catch (Exception ex) {
	      // TODO: handle exception
	      throw new ServletException(ex);
	    }
	  }

	  private void printPageTercero(HttpServletResponse response, VariablesSecureApp vars,
	      String strfpvFacturaCotId, String windowId, String strTab, String strClient,
	      String strIdentificacion, String strTipoPersona, String strTipoIdentificacion,
	      String strNombre, String strApellido, String strNombreComercial, String strRazonSocial,
	      String strEmail, String strTelefono, String strDireccionClie, String auxFinPaymentmethod,
	      String auxMontoPago, String strcheck) throws IOException, ServletException {

	    String tipoPersona = strTipoPersona;
	    String tipoIdentificacion = strTipoIdentificacion;
	    String nombre = strNombre;
	    String apellido = strApellido;
	    String nombreComercial = strNombreComercial;
	    String razonSocial = strRazonSocial;
	    String email = strEmail;
	    String telefono = strTelefono;
	    String direccionClie = strDireccionClie;

	    try {
	      ComboTableData comboTableDataTipoPersona = new ComboTableData(vars, this, "TABLEDIR",
	          "FPV_TIPOPERSONA_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      ComboTableData comboTableDataTipoIdentificacion = new ComboTableData(vars, this, "TABLEDIR",
	          "FPV_TIPOIDENTIFICACION_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      ComboTableData comboTableDataMetodoPago = new ComboTableData(vars, this, "TABLEDIR",
	          "FPV_METODOPAGO_V_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
	          Utility.getContext(this, vars, "#User_Client", windowId), 0);

	      XmlDocument xmlDocument = xmlEngine
	          .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/RegistroCliente")
	          .createXmlDocument();
	      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
	      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
	      xmlDocument.setParameter("theme", vars.getTheme());
	      xmlDocument.setParameter("fpvfacturacotid", strfpvFacturaCotId);
	      xmlDocument.setParameter("controltipoconsumo", strcheck);
	      xmlDocument.setParameter("window", windowId);
	      xmlDocument.setParameter("tab", strTab);

	      /**
	       * Carga de datos de tercero
	       **/
	      xmlDocument.setParameter("identificacion", strIdentificacion);
	      xmlDocument.setParameter("montopago", auxMontoPago);

	      Utility.fillSQLParameters(this, vars, null, comboTableDataTipoPersona, windowId, null);
	      xmlDocument.setData("reportFPVTipoPersona_ID", "liststructure",
	          comboTableDataTipoPersona.select(false));
	      Utility.fillSQLParameters(this, vars, null, comboTableDataTipoIdentificacion, windowId, null);
	      xmlDocument.setData("reportFPVTipoIdentificacion_ID", "liststructure",
	          comboTableDataTipoIdentificacion.select(false));
	      Utility.fillSQLParameters(this, vars, null, comboTableDataMetodoPago, windowId, null);
	      xmlDocument.setData("reportFPVMetodoPago_ID", "liststructure",
	          comboTableDataMetodoPago.select(false));

	      RegistroClienteData[] auxMetodoData = RegistroClienteData.methodBuscarTercero(this,
	          strIdentificacion, strClient);

	      if (auxMetodoData.length > 0) {
	        tipoPersona = auxMetodoData[0].dato1;
	        tipoIdentificacion = auxMetodoData[0].dato2;
	        nombre = auxMetodoData[0].dato3;
	        apellido = auxMetodoData[0].dato4;
	        nombreComercial = auxMetodoData[0].dato5;
	        razonSocial = auxMetodoData[0].dato6;
	        email = auxMetodoData[0].dato7;
	        telefono = auxMetodoData[0].dato8;
	        direccionClie = auxMetodoData[0].dato9;
	      }

	      xmlDocument.setParameter("nombre", nombre);
	      xmlDocument.setParameter("apellido", apellido);
	      xmlDocument.setParameter("nombreComercial", nombreComercial);
	      xmlDocument.setParameter("razonSocial", razonSocial);
	      xmlDocument.setParameter("email", email);
	      xmlDocument.setParameter("telefono", telefono);
	      xmlDocument.setParameter("direccionClie", direccionClie);

	      xmlDocument.setParameter("tipopersonaId", tipoPersona);
	      xmlDocument.setParameter("tipotdentificacionid", tipoIdentificacion);
	      xmlDocument.setParameter("metodopagoid", auxFinPaymentmethod);

	      response.setContentType("text/html; charset=UTF-8");
	      PrintWriter out = response.getWriter();
	      out.println(xmlDocument.print());
	      out.close();

	    } catch (Exception ex) {
	      // TODO: handle exception
	      throw new ServletException(ex);
	    }
	  }  

  private void printPage(HttpServletResponse response, VariablesSecureApp vars,
      String strKeyValue, String windowId, String strTab)
      throws IOException, ServletException {

    String strfpvFacturaCotId = vars.getStringParameter("inpNameValue"); 
    String strcheck = vars.getStringParameter("inpTipoConsumido");
    if (strcheck.equals("")) { strcheck = "N"; }

    try {
        ComboTableData comboTableDataTipoPersona = new ComboTableData(vars, this, "TABLEDIR",
            "FPV_TIPOPERSONA_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", windowId), 0);

        ComboTableData comboTableDataTipoIdentificacion = new ComboTableData(vars, this, "TABLEDIR",
            "FPV_TIPOIDENTIFICACION_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", windowId), 0);

        /* ComboTableData comboTableDataMetodoPago = new ComboTableData(vars, this, "TABLEDIR",
            "FPV_METODOPAGO_V_ID", "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", windowId), 0);

        RegistroClienteData[] auxMetodoData = RegistroClienteData.methodMetodoPagoAlto(this,
        		vars.getClient());

        String auxMontoPago = "0";
        String auxFinPaymentmethod = "";

        if (auxMetodoData.length > 0) {
          auxFinPaymentmethod = auxMetodoData[0].dato1;
        }

        RegistroClienteData[] auxMontoPagoData = RegistroClienteData.methodTotalMetodo(this,
            auxFinPaymentmethod, strfpvFacturaCotId);

        if (auxMontoPagoData.length > 0) {
          auxMontoPago = auxMontoPagoData[0].dato1;
        } */

        XmlDocument xmlDocument = xmlEngine
            .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/RegistroCliente")
            .createXmlDocument();
        xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
        xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
        xmlDocument.setParameter("theme", vars.getTheme());
        xmlDocument.setParameter("fpvfacturacotid", strfpvFacturaCotId);
        /*xmlDocument.setParameter("montopago", auxMontoPago);*/
        xmlDocument.setParameter("controltipoconsumo", strcheck);
        xmlDocument.setParameter("window", windowId);
        xmlDocument.setParameter("tab", strTab);

        Utility.fillSQLParameters(this, vars, null, comboTableDataTipoPersona, windowId, null);
        xmlDocument.setData("reportFPVTipoPersona_ID", "liststructure",
            comboTableDataTipoPersona.select(false));
        Utility.fillSQLParameters(this, vars, null, comboTableDataTipoIdentificacion, windowId, null);
        xmlDocument.setData("reportFPVTipoIdentificacion_ID", "liststructure",
            comboTableDataTipoIdentificacion.select(false));
        /*Utility.fillSQLParameters(this, vars, null, comboTableDataMetodoPago, windowId, null);
        xmlDocument.setData("reportFPVMetodoPago_ID", "liststructure",
            comboTableDataMetodoPago.select(false));*/

        xmlDocument.setParameter("tipopersonaId", "803BF35D3A814EBDAA09809D4F4171F9");
        xmlDocument.setParameter("tipotdentificacionid", "FC6B133DF4534B90A42D83E78DC795E6");
        /*xmlDocument.setParameter("metodopagoid", auxFinPaymentmethod);*/

        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out = response.getWriter();
        out.println(xmlDocument.print());
        out.close();

      } catch (Exception ex) {
        // TODO: handle exception
        throw new ServletException(ex);
      }
  }
  
  private OBError processButton(VariablesSecureApp vars, String strfpvFacturaCotId,
	      String strTipoPersona, String strTipoIdentificacion, String strIdentificacion,
	      String strNombre, String strApellido, String strNombreComercial, String strRazonSocial,
	      String strEmail, String strTelefono, String strDireccionClien) {
	    OBError myError = null;

	    OBContext.setAdminMode(true);
	    try {

	      RegistroClienteData.methodCrearFactura(this, strfpvFacturaCotId, strTipoPersona,
	          strTipoIdentificacion, strIdentificacion, strNombre, strApellido, strNombreComercial,
	          strRazonSocial, strEmail, strTelefono, strDireccionClien, null, "0");

	      myError = new OBError();
	      myError.setType("Success");
	      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
	    } catch (Exception e) {
	      OBDal.getInstance().rollbackAndClose();

	      e.printStackTrace();
	      log4j.warn("Rollback in transaction");
	      myError = new OBError();
	      myError.setType("Error");
	      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
	      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
	    } finally {
	      OBContext.restorePreviousMode();
	    }

	    return myError;
	  }  

  @Override
  public String getServletInfo() {
    return "Servlet that presents the business partners seeker";
  } // end of getServletInfo() method
}
