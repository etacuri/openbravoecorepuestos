/*
 *************************************************************************
 * The contents of this file are subject to the Openbravo  Public  License
 * Version  1.1  (the  "License"),  being   the  Mozilla   Public  License
 * Version 1.1  with a permitted attribution clause; you may not  use this
 * file except in compliance with the License. You  may  obtain  a copy of
 * the License at http://www.openbravo.com/legal/license.html 
 * Software distributed under the License  is  distributed  on  an "AS IS"
 * basis, WITHOUT WARRANTY OF ANY KIND, either express or implied. See the
 * License for the specific  language  governing  rights  and  limitations
 * under the License. 
 * The Original Code is Openbravo ERP. 
 * The Initial Developer of the Original Code is Openbravo SLU 
 * All portions are Copyright (C) 2001-2010 Openbravo SLU 
 * All Rights Reserved. 
 * Contributor(s):  ______________________________________.
 ************************************************************************
 */
package com.atrums.ventas.pedido.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.xmlEngine.XmlDocument;

public class InformacionProducto extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    final VariablesSecureApp vars = new VariablesSecureApp(request);

    OBContext.setAdminMode(true);

    String strWindow = vars.getStringParameter("inpwindowId");
    String strTab = vars.getStringParameter("inpTabId");

    try {
      if (vars.commandIn("DEFAULT")) {
        printPageDataSheet(response, vars, "DEFAULT", strWindow, strTab, null);
      } else {
        pageError(response);
      }
    } finally {
      // TODO: handle finally clause
      OBContext.restorePreviousMode();
    }
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strKeyValue, String windowId, String strTab, String strKey)
      throws IOException, ServletException {
    log4j.debug("Output: dataSheet");

    String strMProductId = vars.getStringParameter("inpNameValue");

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    /**
     * Contruyendo el base de XML para el HTMLss
     **/
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/InformacionProducto")
        .createXmlDocument();

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    xmlDocument.setParameter("key", strKey);
    xmlDocument.setParameter("window", windowId);
    xmlDocument.setParameter("tab", strTab);

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.ventas.pedido.ad_forms.InformacionProducto");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());

      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "InformacionProducto.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "InformacionProducto.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    } finally {
      OBContext.restorePreviousMode();
    }
	
	String[] strDatos = strMProductId.split("-"); 
	
	String strAuxMProductId = "";
	String strAuxMarca = "%";
	String strAuxModule = "%";
	
	if (strDatos.length > 0) {
		strAuxMProductId = strDatos[0];
	}
	
	if (strDatos.length > 1) {
		strAuxMarca = strDatos[1];
	}

	if (strDatos.length > 2) {
		strAuxModule = strDatos[2];
	}
	
    InformacionProductoData[] data1 = InformacionProductoData.selectInfoProductos(this, strAuxMProductId);

    if (data1.length > 0) {
      String strDatosProducto = "";       

      for (int i = 0; i < data1.length; i++) {
    	  
    	  if (data1[0].mproductimageid.equals("")) {
             strDatosProducto = strDatosProducto + "<p>No existe imagen</p>";
    	  } else {
             strDatosProducto = strDatosProducto + "<img src=\"../utility/ShowImage?id="+ data1[0].mproductimageid +"&amp;nocache="+ data1[0].date +"\" width=\"600\" height=\"550\">";
    	  }
      }

      xmlDocument.setParameter("productname", "Nombre del producto: " + data1[0].mproductname);
      xmlDocument.setParameter("productvalue", "Código del producto: " + data1[0].mproductvalue);
      xmlDocument.setParameter("productdescripcion", "Descripción del producto: " + data1[0].mproductdescripcion);
      xmlDocument.setParameter("products", strDatosProducto);
    } else {
      xmlDocument.setParameter("products",
          "<p>No hay información</p>");
    }

    out.println(xmlDocument.print());
    out.close();
  }

  @Override
  public String getServletInfo() {
    return "Servlet that presents the business partners seeker";
  } // end of getServletInfo() method
}
