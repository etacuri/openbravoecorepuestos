package com.atrums.ventas.pedido.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.util.Log;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.dal.service.OBDal;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.model.common.order.Order;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;

import com.atrums.compras.montos.model.ATECCO_Temporal;
import com.atrums.felectronica.process.ATECCO_Operaciones_XML;

public class FacturaPos extends HttpSecureAppServlet {

  /**
   *  
   */
  private static final long serialVersionUID = 1L;
  final OBError msg = new OBError();

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    OBContext.setAdminMode(true);

    try {
      if (vars.commandIn("DEFAULT")) {
        printPageDataSheet(response, vars, "DEFAULT");
      } else if (vars.commandIn("REGISTRAR")) {
        printPageDataSheet(response, vars, "REGISTRAR");
      } else if (vars.commandIn("BUSCAR")) {
        printPageDataSheet(response, vars, "BUSCAR");
      } else if (vars.commandIn("LIMPIAR")) {
        printPageDataSheet(response, vars, "LIMPIAR");
      } else if (vars.commandIn("ENVIAR")) {
        printPageDataSheet(response, vars, "ENVIAR");
      } else if (vars.commandIn("CREDITO")) {
        printPageDataSheet(response, vars, "CREDITO");
      } else if (vars.commandIn("ELIMINAR")) {
        printPageDataSheet(response, vars, "ELIMINAR");
      } else if (vars.commandIn("ACTUALIZAR")) {
        printPageDataSheet(response, vars, "ACTUALIZAR");
      } else if (vars.commandIn("METODOPAGO")) {
        printPageDataSheet(response, vars, "METODOPAGO");
      } else if (vars.commandIn("FILTRAR")) {
        printPageDataSheet(response, vars, "FILTRAR");
      } else if (vars.commandIn("FILTRARLIMPIAR")) {
        printPageDataSheet(response, vars, "FILTRARLIMPIAR");
      } else if (vars.commandIn("NOPRODUCTOS")) {
        printPageDataSheet(response, vars, "NOPRODUCTOS");
      } else if (vars.commandIn("NOCLIENTE")) {
          printPageDataSheet(response, vars, "NOCLIENTE");
      } else if (vars.commandIn("OBTENER_FAMILIA") || vars.commandIn("OBTENER_CATEGORIA")
          || vars.commandIn("OBTENER_MARCA") || vars.commandIn("OBTENER_MODELO")) {
        printPageDataSheet(response, vars, "OBTENER_CATEGORIA");
      } else {
        pageError(response);
      }
    } finally {
      // TODO: handle finally clause
      OBContext.restorePreviousMode();
    }

  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strCommand) throws IOException, ServletException {
    // TODO Auto-generated method stub
    log4j.debug("Output: dataSheet");
    
    String strMProductWarehouseName = vars.getStringParameter("inpmProductWarehouseName");
    String strFpvFacturaCotId = vars.getStringParameter("fpvFacturaCotId");
    String auxFinPaymentmethod = vars.getStringParameter("inpMetodoPago");

    if (strCommand.equals("LIMPIAR")) {
      strFpvFacturaCotId = "";
    }

    if (strCommand.equals("ACTUALIZAR")) {
      String strfpvFacturaCotLinId = "";    

      try {
        strfpvFacturaCotLinId = vars.getRequiredInStringParameter("inpfpvFacturaCotLinId",
            IsIDFilter.instance);
      } catch (Exception ex) {
        // TODO: handle exception
        Log.error(ex.getMessage(), ex);
      }
      
      if (strfpvFacturaCotLinId.startsWith("("))
        strfpvFacturaCotLinId = strfpvFacturaCotLinId.substring(1,
            strfpvFacturaCotLinId.length() - 1);

      if (!strfpvFacturaCotLinId.equals("")) {
        strfpvFacturaCotLinId = Replace.replace(strfpvFacturaCotLinId, "'", "");
        StringTokenizer st = new StringTokenizer(strfpvFacturaCotLinId, ",", false);

        boolean ingresoExito = false;

        while (st.hasMoreTokens()) {
          String strpFpvFacturaCotLinId = st.nextToken().trim();

          if (!strpFpvFacturaCotLinId.equals("")) {
            String strCantidadOrdenada = vars
                .getStringParameter("inpfpvCantidadOrdenada" + strpFpvFacturaCotLinId);
            String strProductPrice = vars
                .getStringParameter("inpfpvProductPrice" + strpFpvFacturaCotLinId);
            String strTotalCantidad = vars
                .getStringParameter("inpfpvTotalLinea" + strpFpvFacturaCotLinId);

            BigDecimal bdmCantidad = new BigDecimal(strCantidadOrdenada);

            if (bdmCantidad.doubleValue() > 0) {
              FacturaPosData.methodActualizarTemporalFacuraCot(this, strpFpvFacturaCotLinId,
                  strProductPrice, strCantidadOrdenada, strTotalCantidad);

              ingresoExito = true;
            }
          }
        }

        if (ingresoExito) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s actualizado/s", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage.setMessage(
              Utility.messageBD(this, "Producto/s no actualizado/s", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        }
      }
    }

    if (strCommand.equals("ELIMINAR")) {
      String strfpvFacturaCotLinId = "";

      try {
        strfpvFacturaCotLinId = vars.getRequiredInStringParameter("inpfpvFacturaCotLinId",
            IsIDFilter.instance);
      } catch (Exception ex) {
        // TODO: handle exception
        Log.error(ex.getMessage(), ex);
      }
      
      if (strfpvFacturaCotLinId.startsWith("("))
        strfpvFacturaCotLinId = strfpvFacturaCotLinId.substring(1,
            strfpvFacturaCotLinId.length() - 1);

      if (!strfpvFacturaCotLinId.equals("")) {
        strfpvFacturaCotLinId = Replace.replace(strfpvFacturaCotLinId, "'", "");
        StringTokenizer st = new StringTokenizer(strfpvFacturaCotLinId, ",", false);

        boolean ingresoExito = false;

        while (st.hasMoreTokens()) {
          String strpFpvFacturaCotLinId = st.nextToken().trim();

          if (!strpFpvFacturaCotLinId.equals("")) {
            FacturaPosData.methodBorrarTemporalFacuraCot(this, strpFpvFacturaCotLinId);

            ingresoExito = true;
          }
        }

        if (ingresoExito) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s eliminado/s", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s no eliminado/s", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        }
      }
    }

    if (strCommand.equals("REGISTRAR")) {
      String strmProductStockId = "";

      try {
        strmProductStockId = vars.getRequiredInStringParameter("inpProductStockId",
            IsIDFilter.instance);
      } catch (Exception ex) {
        // TODO: handle exception
        Log.error(ex.getMessage(), ex);
      }

      if (strmProductStockId.startsWith("("))
        strmProductStockId = strmProductStockId.substring(1, strmProductStockId.length() - 1);

      if (!strmProductStockId.equals("")) {
        strmProductStockId = Replace.replace(strmProductStockId, "'", "");
        StringTokenizer st = new StringTokenizer(strmProductStockId, ",", false);

        boolean ingresoExito = false;
        boolean ingresoStock = false;
        boolean ingresoValorZero = false;
        boolean ingresoDescuento = false;
        boolean camposnulos = false;
        boolean cantidadnegativa = false;

        while (st.hasMoreTokens()) {
          String strMProductStockId = st.nextToken().trim();

          if (!strMProductStockId.equals("")) {
            String strCantidadOrdenada = vars
                .getStringParameter("inpCantidadOrdenada" + strMProductStockId);  //cantidad de productos a vender
            String strTotalCantidad = vars
                .getStringParameter("inpTotalCantidad" + strMProductStockId);  //precio del producto - el descuento
            String strProductPrice = vars
                .getStringParameter("inpmProductPrice" + strMProductStockId);  //precio unitario del producto
            String strTotalStock = vars.getStringParameter("inpTotalStock" + strMProductStockId);  //stock del producto
            String strMProductId = vars.getStringParameter("inpMProductId" + strMProductStockId); //id del producto
            String strDescuento = vars.getStringParameter("inpDescuento" + strMProductStockId);  //% de descuento

            if (!strCantidadOrdenada.equals("") && !strDescuento.equals("")) {
            	
            BigDecimal bdmCantidad = new BigDecimal(strCantidadOrdenada);
            BigDecimal bdmTotalCantidad = new BigDecimal(strTotalCantidad);
            BigDecimal bdmTotalStock = new BigDecimal(strTotalStock);
            BigDecimal bdmDescuento = new BigDecimal(strDescuento);
            
            if (bdmCantidad.doubleValue() > 0) {
            
            if (bdmCantidad.doubleValue() <= bdmTotalStock.doubleValue()) {
            	
            	if (!vars.getClient().equals("77ACE2A682094931A861E3CB12C881D7")) {
		              if (bdmDescuento.doubleValue() >= 0 && bdmDescuento.doubleValue() <= 100) {
		                if (strFpvFacturaCotId.equals("") && bdmCantidad.doubleValue() > 0
		                    && bdmTotalCantidad.doubleValue() > 0) {
		                  FacturaPosData[] fpvFacturaCotIdData = FacturaPosData
		                      .methodSeleccionarTemporalFacuraCot(this, vars.getClient(), vars.getOrg(), vars.getUser());
		
		                  if (fpvFacturaCotIdData.length > 0) {
		                    strFpvFacturaCotId = fpvFacturaCotIdData[0].fpvfacturacotid;
		
		                    FacturaPosData.methodSeleccionarTemporalFacuraCot(this, vars.getClient(),
		                        vars.getOrg(), strFpvFacturaCotId, strMProductId, vars.getWarehouse(),
		                        strProductPrice, strCantidadOrdenada, strTotalCantidad, strDescuento);
		
		                    ingresoExito = true;
		                  }
		                } else if (!strFpvFacturaCotId.equals("") && bdmCantidad.doubleValue() > 0
		                    && bdmTotalCantidad.doubleValue() > 0) {
		                    FacturaPosData.methodSeleccionarTemporalFacuraCot(this, vars.getClient(),
		                        vars.getOrg(), strFpvFacturaCotId, strMProductId, vars.getWarehouse(),
		                        strProductPrice, strCantidadOrdenada, strTotalCantidad, strDescuento);
		
		                  ingresoExito = true;
		                } else if (bdmTotalCantidad.doubleValue() <= 0) {
		                  ingresoValorZero = true;
		                }
		              } else {
		                ingresoDescuento = true;
		              }
            	}else {
            		if (bdmDescuento.doubleValue() >= 0 && bdmDescuento.doubleValue() <= 10) {
		                if (strFpvFacturaCotId.equals("") && bdmCantidad.doubleValue() > 0
		                    && bdmTotalCantidad.doubleValue() > 0) {
		                  FacturaPosData[] fpvFacturaCotIdData = FacturaPosData
		                      .methodSeleccionarTemporalFacuraCot(this, vars.getClient(), vars.getOrg(), vars.getUser());
		
		                  if (fpvFacturaCotIdData.length > 0) {
		                    strFpvFacturaCotId = fpvFacturaCotIdData[0].fpvfacturacotid;
		
		                    FacturaPosData.methodSeleccionarTemporalFacuraCot(this, vars.getClient(),
		                        vars.getOrg(), strFpvFacturaCotId, strMProductId, vars.getWarehouse(),
		                        strProductPrice, strCantidadOrdenada, strTotalCantidad, strDescuento);
		
		                    ingresoExito = true;
		                  }
		                } else if (!strFpvFacturaCotId.equals("") && bdmCantidad.doubleValue() > 0
		                    && bdmTotalCantidad.doubleValue() > 0) {
		                    FacturaPosData.methodSeleccionarTemporalFacuraCot(this, vars.getClient(),
		                        vars.getOrg(), strFpvFacturaCotId, strMProductId, vars.getWarehouse(),
		                        strProductPrice, strCantidadOrdenada, strTotalCantidad, strDescuento);
		
		                  ingresoExito = true;
		                } else if (bdmTotalCantidad.doubleValue() <= 0) {
		                  ingresoValorZero = true;
		                }
		              } else {
		                ingresoDescuento = true;
		              }
            	}
		              
		              
             } else {
              ingresoStock = true;
             }
            
            
            
            } else {
            	cantidadnegativa = true;
            }            
            
           } else {
        	   camposnulos = true;
           } 
            
          }
        }

        if (ingresoStock) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this,
              "Producto/s no registrado/s, porque la cantidad supera el stock",
              vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        } else if (ingresoExito) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s registrado/s", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        } else if (ingresoValorZero) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this,
              "Producto/s no registrado/s, porque el total es 0", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        } else if (ingresoDescuento) {
          OBError myMessage = new OBError();
          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          
          if (!vars.getClient().equals("77ACE2A682094931A861E3CB12C881D7")) {
          myMessage.setMessage(Utility.messageBD(this,
              "Producto/s no registrado/s, El descuento por producto no puede ser mayor al 100%",
              vars.getLanguage()));
          }else {
        	  myMessage.setMessage(Utility.messageBD(this,
                      "Producto/s no registrado/s, El descuento por producto no puede ser mayor al 10%",
                      vars.getLanguage()));
          }
          

          vars.setMessage("FacturaPos", myMessage);
        } else if (camposnulos) {
            OBError myMessage = new OBError();
            myMessage.setTitle("Error");
            myMessage.setType("ERROR");
            myMessage.setMessage(Utility.messageBD(this,
                "El campo cantidad y descuento deben tener un valor",
                vars.getLanguage()));

            vars.setMessage("FacturaPos", myMessage);
        } else if (cantidadnegativa) {
            OBError myMessage = new OBError();
            myMessage.setTitle("Error");
            myMessage.setType("ERROR");
            myMessage.setMessage(Utility.messageBD(this,
                "El campo cantidad debe ser mayor a 0",
                vars.getLanguage()));

            vars.setMessage("FacturaPos", myMessage);
       } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage.setMessage(
              Utility.messageBD(this, "Producto/s no registrado/s", vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        }
      }
    }

    if (strCommand.equals("METODOPAGO")) {
      FacturaPosData[] DataMetodo = null;

      String strmensaje = null;

      try {
        DataMetodo = FacturaPosData.methodActualizarMetodoFacuraCot(this, strFpvFacturaCotId,
            auxFinPaymentmethod);
      } catch (Exception e) {
        // TODO: handle exception
        Log.error(e.getMessage());
        strmensaje = e.getMessage();
      }

      if (strmensaje != null) {
        OBError myMessage = new OBError();

        myMessage.setTitle("Error");
        myMessage.setType("ERROR");
        myMessage.setMessage(Utility.messageBD(this,
            "Precio no actualizado, " + strmensaje.replaceAll("@CODE=0@ERROR: ", ""),
            vars.getLanguage()));

        vars.setMessage("FacturaPos", myMessage);
      } else if (DataMetodo.length > 0) {
        OBError myMessage = new OBError();

        myMessage.setTitle("Success");
        myMessage.setType("SUCCESS");
        myMessage.setMessage(Utility.messageBD(this, "Precio actualizado", vars.getLanguage()));

        vars.setMessage("FacturaPos", myMessage);

      } else {
        OBError myMessage = new OBError();

        myMessage.setTitle("Error");
        myMessage.setType("ERROR");
        myMessage.setMessage(Utility.messageBD(this, "Precio no actualizado", vars.getLanguage()));

        vars.setMessage("FacturaPos", myMessage);
      }
    }

    if (strCommand.equals("ENVIAR")) {

      if (!strFpvFacturaCotId.equals("")) {
    	  FacturaPosData[] enviarData = null;    	  
	      String strmensaje = null;
	      
	      
	      FacturaPosData[] validarFactura = FacturaPosData.methodValidarConsumidor(this, strFpvFacturaCotId);
	      //float total_fact = Float.parseFloat(validarFactura[0].total);// && total_fact >50
	      //String tipo_iden =validarFactura[0].tipoIden.toString();
	    		  
	      if(validarFactura[0].tipoIden.equals("07") && Float.parseFloat(validarFactura[0].total)>50) {
    		  OBError myMessage = new OBError();

	          myMessage.setTitle("Error");
	          myMessage.setType("ERROR");
	          myMessage.setMessage(Utility.messageBD(this, "Consumidor final excede de 50$", vars.getLanguage()));

	          vars.setMessage("FacturaPos", myMessage);
    	  }else {
    		  /**/
    	        
    	        
    	        /*Al dar clic en enviar pedido se envia con el metodo efectivo*/
    	        FacturaPosData.methodUpdateMetodoPago(this, vars.getClient(), strFpvFacturaCotId);

    	        try {
    	          enviarData = FacturaPosData.methodSeleccionarTemporalPedido(this, strFpvFacturaCotId, vars.getWarehouse(), vars.getUser());
    	        } catch (Exception e) {
    	          // TODO: handle exception
    	          Log.error(e.getMessage());
    	          strmensaje = e.getMessage();
    	        }

    	        if (strmensaje != null) {
    	          OBError myMessage = new OBError();

    	          myMessage.setTitle("Error");
    	          myMessage.setType("ERROR");
    	          myMessage.setMessage(Utility.messageBD(this,
    	              "Documento No Creado, " + strmensaje.replaceAll("@CODE=0@ERROR: ", ""),
    	              vars.getLanguage()));

    	          vars.setMessage("FacturaPos", myMessage);
    	        } else if (enviarData.length > 0) {
    	          OBError myMessage = new OBError();

    	          myMessage.setTitle("Success");
    	          myMessage.setType("SUCCESS");
    	          myMessage.setMessage(Utility.messageBD(this,
    	              "Documento Creado # " + enviarData[0].fpvdocumentno, vars.getLanguage()));

    	          vars.setMessage("FacturaPos", myMessage);

    	        } else {
    	          OBError myMessage = new OBError();

    	          myMessage.setTitle("Error");
    	          myMessage.setType("ERROR");
    	          myMessage.setMessage(Utility.messageBD(this, "Documento No Creado", vars.getLanguage()));

    	          vars.setMessage("FacturaPos", myMessage);
    	        }
    	        /**/
    	  }
    	
      } else {
        OBError myMessage = new OBError();

        myMessage.setTitle("Error");
        myMessage.setType("ERROR");
        myMessage.setMessage(Utility.messageBD(this, "No hay productos para generar la factura",
            vars.getLanguage()));

        vars.setMessage("FacturaPos", myMessage);
      }
    }
    
    /*VENTAS A CREDITO*/
    if (strCommand.equals("CREDITO")) {

        if (!strFpvFacturaCotId.equals("")) {
          FacturaPosData[] enviarData = null;

          String strmensaje = null;
          
          /*Al dar clic en ventas a credito se envia con el metodo efectivo*/
          FacturaPosData.methodUpdateMetodoPago(this, vars.getClient(), strFpvFacturaCotId);

          try {
            enviarData = FacturaPosData.methodSeleccionarTemporalPedido(this, strFpvFacturaCotId, vars.getWarehouse(),vars.getUser());
          } catch (Exception e) {
            // TODO: handle exception
            Log.error(e.getMessage());
            strmensaje = e.getMessage();
          }

          if (strmensaje != null) {
            OBError myMessage = new OBError();

            myMessage.setTitle("Error");
            myMessage.setType("ERROR");
            myMessage.setMessage(Utility.messageBD(this,
                "Documento No Creado, " + strmensaje.replaceAll("@CODE=0@ERROR: ", ""),
                vars.getLanguage()));

            vars.setMessage("FacturaPos", myMessage);
          } else if (enviarData.length > 0) {
        	  
        	  /*Crear factura y la guia*/   	  
        	  GenerarFacturaData[] data1 = null; 
        	  String cOrderId = null;
        	  
        	  data1 = GenerarFacturaData.methodSeleccionarPedido(this, vars.getClient(), vars.getOrg(), ("%" + enviarData[0].fpvdocumentno));
        	  cOrderId = data1[0].dato1;
                     
              List<ATECCO_Temporal> temp = new ArrayList<ATECCO_Temporal>();
              
              OBError myError = processButton(vars, cOrderId, temp);
	          OBError myMessage = myError;
	
	          myMessage.setTitle("Success");
	          myMessage.setType("SUCCESS");
	          myMessage.setMessage(Utility.messageBD(this,
	              "Información: Pedido de venta Nº "+ enviarData[0].fpvdocumentno + " creado, " + myError.getMessage(), vars.getLanguage()));
	
	            vars.setMessage("FacturaPos", myMessage);        	  
        	  
          } else {
            OBError myMessage = new OBError();

            myMessage.setTitle("Error");
            myMessage.setType("ERROR");
            myMessage.setMessage(Utility.messageBD(this, "Documento No Creado", vars.getLanguage()));

            vars.setMessage("FacturaPos", myMessage);
          }
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this, "No hay productos para generar la factura",
              vars.getLanguage()));

          vars.setMessage("FacturaPos", myMessage);
        }
    }
    
   /*Mensajes*/
    if (strCommand.equals("NOPRODUCTOS")) {
	    OBError myMessage = new OBError();
	
	    myMessage.setTitle("Error");
	    myMessage.setType("ERROR");
	    myMessage.setMessage(Utility.messageBD(this, "No hay productos para generar la factura",
	        vars.getLanguage()));
	
	    vars.setMessage("FacturaPos", myMessage);
    }  
    
    if (strCommand.equals("NOCLIENTE")) {
	    OBError myMessage = new OBError();
	
	    myMessage.setTitle("Error");
	    myMessage.setType("ERROR");
	    myMessage.setMessage(Utility.messageBD(this, "La factura debe tener un consumidor final",
	        vars.getLanguage()));
	
	    vars.setMessage("FacturaPos", myMessage);
    }     
    //

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();

    /**
     * Contruyendo el base de XML para el HTMLss
     **/
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/FacturaPos").createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "FacturaPos", false, "", "", "",
        false, "ad_forms", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    OBContext.setAdminMode(true);

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.ventas.pedido.ad_forms.FacturaPos");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "FacturaPos.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "FacturaPos.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    } finally {
      OBContext.restorePreviousMode();
    }

    if (strMProductWarehouseName.equals("")) {
      FacturaPosData[] mWarehouseNameData = FacturaPosData.methodSeleccionarProductosOrg(this,
          vars.getWarehouse());

      if (mWarehouseNameData.length > 0) {
        strMProductWarehouseName = mWarehouseNameData[0].mproductwarehousename;
      } else {
        strMProductWarehouseName = "%";
      }
    }

    OBError myMessage = vars.getMessage("FacturaPos");
    vars.removeMessage("FacturaPos");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }

    ComboTableData comboTableDataMetodoPago;

    try {
      if (auxFinPaymentmethod.equals("")) {
        FacturaPosData[] auxMetodoData = FacturaPosData.methodMetodoPagoAlto(this,
            vars.getClient());

        if (auxMetodoData.length > 0) {
          auxFinPaymentmethod = auxMetodoData[0].fpvmetodopagovid;
        }
      }

      // Filtros
      ComboTableData comboTableDataFamilia;
      ComboTableData comboTableDataCategoria;
      ComboTableData comboTableDataMarca;
      ComboTableData comboTableDataModelo;

      if (vars.getStringParameter("inpsearchAteccoFamilia") != ""
          && !(strCommand.equals("FILTRARLIMPIAR") || strCommand.equals("LIMPIAR"))) {
        comboTableDataCategoria = new ComboTableData(vars, this, "TABLEDIR", "FPV_CATEGORIA_ID", "",
            "ATECCO_CATEGORIA FAMILIA", Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      } else {
        comboTableDataCategoria = new ComboTableData(vars, this, "TABLEDIR", "FPV_CATEGORIA_ID", "",
            null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      }

      if (vars.getStringParameter("inpsearchAteccoCategoria") != ""
          && !(strCommand.equals("FILTRARLIMPIAR") || strCommand.equals("LIMPIAR"))) {
        comboTableDataFamilia = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_FAMILIA_ID", "",
            "ATECCO_FAMILIA CATEGORIA", Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      } else {
        comboTableDataFamilia = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_FAMILIA_ID", "",
            null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      }

      if (vars.getStringParameter("inpsearchProductMarca") != ""
          && !(strCommand.equals("FILTRARLIMPIAR") || strCommand.equals("LIMPIAR"))) {
        comboTableDataModelo = new ComboTableData(vars, this, "TABLEDIR", "FPV_MODELO_ID", "",
            "ATECCO_MODELO MARCA", Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      } else {
        comboTableDataModelo = new ComboTableData(vars, this, "TABLEDIR", "FPV_MODELO_ID", "", null,
            Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      }

      comboTableDataMarca = new ComboTableData(vars, this, "TABLEDIR", "M_BRAND_ID", "", null,
          Utility.getReferenceableOrg(vars, vars.getOrg()),
          Utility.getContext(this, vars, "#User_Client", null), 0);

      Utility.fillSQLParameters(this, vars, null, comboTableDataFamilia, null, null);

      xmlDocument.setData("reportAteccoFamilia_ID", "liststructure",
          comboTableDataFamilia.select(false));

      Utility.fillSQLParameters(this, vars, null, comboTableDataCategoria, null, null);

      xmlDocument.setData("reportAteccoCategoria_ID", "liststructure",
          comboTableDataCategoria.select(false));

      Utility.fillSQLParameters(this, vars, null, comboTableDataMarca, null, null);

      xmlDocument.setData("reportProductMarca_ID", "liststructure",
          comboTableDataMarca.select(false));

      Utility.fillSQLParameters(this, vars, null, comboTableDataModelo, null, null);

      xmlDocument.setData("reportProductModelo_ID", "liststructure",
          comboTableDataModelo.select(false));

      // Fin Flltros

      comboTableDataMetodoPago = new ComboTableData(vars, this, "TABLEDIR", "FPV_METODOPAGO_V_ID",
          "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
          Utility.getContext(this, vars, "#User_Client", null), 0);

      Utility.fillSQLParameters(this, vars, null, comboTableDataMetodoPago, null, null);
      xmlDocument.setData("reportFPVMetodoPago_ID", "liststructure",
          comboTableDataMetodoPago.select(false));

      xmlDocument.setParameter("metodopagoid", auxFinPaymentmethod);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    String strMProductValue = "%";
    String strMProductUpc = "%";
    String strMProductName = "%";
    String strMProductReferenceCode = "%";
    String strMProductFamiliaName = "%";
    String strMProductGenericoName = "%";

    String strMProductCategoriaName = "%";
    String strMProductSubCategoriaName = "%";
    String strMProductMarcaName = "%";
    String strMProductModeloName = "%";
    String strMProductYear = "0";
    String strMProductCilindraje = "0";

    String strMProductFamiliaId = "%";
    String strMProductCategoria = "%";
    String strMProductMarcaId = "%";
    String strMProductModeloId = "%";

    String strSearchMProductMarcaName = vars.getStringParameter("inpsearchMProductMarcaName");
    String strSearchMProductModeloName = vars.getStringParameter("inpsearchMProductModeloName");
    String strSearchMProductName = vars.getStringParameter("inpsearchMProductName");
    String strSearchMProductValue = vars.getStringParameter("inpsearchMProductValue");
    String strSearchMProductYear = vars.getStringParameter("inpsearchMProductYear");
    String strSearchMProductCilindraje = vars.getStringParameter("inpsearchMProductCilindraje");

    String strSearchMProductFamiliaId = vars.getStringParameter("inpsearchAteccoFamilia");
    String strSearchMProductCategoriaId = vars.getStringParameter("inpsearchAteccoCategoria");
    String strSearchMProductMarcaId = vars.getStringParameter("inpsearchProductMarca");
    String strSearchMProductModeloId = vars.getStringParameter("inpsearchProductModelo");

    if (strCommand.equals("DEFAULT") || strCommand.equals("LIMPIAR")
        || strCommand.equals("FILTRARLIMPIAR")) {
      xmlDocument.setParameter("searchProductMarcaName", "");
      xmlDocument.setParameter("searchProductModeloName", "");
      xmlDocument.setParameter("searchProductName", "");
      xmlDocument.setParameter("searchProductValue", "");
      xmlDocument.setParameter("searchProductYear", "");
      xmlDocument.setParameter("searchProductCilindraje", "");

      xmlDocument.setParameter("ateccofamiliaid", "");
      xmlDocument.setParameter("ateccocategoriaid", "");
      xmlDocument.setParameter("productmarcaid", "");
      xmlDocument.setParameter("productmodeloid", "");

      strSearchMProductMarcaName = "";
      strSearchMProductModeloName = "";
      strSearchMProductName = "";
      strSearchMProductValue = "";
      strSearchMProductYear = "";
      strSearchMProductCilindraje = "";

      strSearchMProductFamiliaId = "";
      strSearchMProductCategoriaId = "";
      strSearchMProductMarcaId = "";
      strSearchMProductModeloId = "";

      strMProductMarcaName = "";
      strMProductModeloName = "";
      strMProductName = "";
      strMProductValue = "";
      strMProductYear = "";
      strMProductCilindraje = "";
    } else {
      strMProductMarcaName = vars.getStringParameter("inpmProductMarcaName");
      strMProductModeloName = vars.getStringParameter("inpmProductModeloName");
      strMProductName = vars.getStringParameter("inpmProductName");
      strMProductValue = vars.getStringParameter("inpmProductValue");
      strMProductYear = vars.getStringParameter("inpmProductYear");
      strMProductCilindraje = vars.getStringParameter("inpmProductCilindraje");

      strMProductFamiliaId = vars.getStringParameter("inpsearchAteccoFamilia");
      strMProductCategoria = vars.getStringParameter("inpsearchAteccoCategoria");
      strMProductMarcaId = vars.getStringParameter("inpsearchProductMarca");
      strMProductModeloId = vars.getStringParameter("inpsearchProductModelo");

      if (!strMProductFamiliaId.equals("") && !strMProductFamiliaId.equals("%")) {
        xmlDocument.setParameter("ateccofamiliaid", strMProductFamiliaId);
        strMProductFamiliaId = "%" + strMProductFamiliaId + "%";
      } else {
        strMProductFamiliaId = "%";
      }

      if (!strMProductCategoria.equals("") && !strMProductCategoria.equals("%")) {
        xmlDocument.setParameter("ateccocategoriaid", strMProductCategoria);
        strMProductCategoria = "%" + strMProductCategoria + "%";
      } else {
        strMProductCategoria = "%";
      }

      if (!strMProductMarcaId.equals("") && !strMProductMarcaId.equals("%")) {
        xmlDocument.setParameter("productmarcaid", strMProductMarcaId);
        strMProductMarcaId = "%" + strMProductMarcaId + "%";
      } else {
        strMProductMarcaId = "%";
      }

      if (!strMProductModeloId.equals("") && !strMProductModeloId.equals("%")) {
        xmlDocument.setParameter("productmodeloid", strMProductModeloId);
        strMProductModeloId = strMProductModeloId;
      } else {
        strMProductModeloId = "%";
      }

      if (!strMProductMarcaName.equals("") && !strMProductMarcaName.equals("%")) {
        xmlDocument.setParameter("productMarcaName", strMProductMarcaName.toUpperCase());
        strMProductMarcaName = "%" + strMProductMarcaName.toUpperCase() + "%";
      } else {
        strMProductMarcaName = "%";
      }

      if (!strMProductModeloName.equals("") && !strMProductModeloName.equals("%")) {
        xmlDocument.setParameter("productModeloName", strMProductModeloName.toUpperCase());
        strMProductModeloName = "%" + strMProductModeloName.toUpperCase() + "%";
      } else {
        strMProductModeloName = "%";
      }

      if (!strMProductReferenceCode.equals("") && !strMProductReferenceCode.equals("%")) {
        xmlDocument.setParameter("productProductReferenceCode",
            strMProductReferenceCode.toUpperCase());
        strMProductReferenceCode = "%" + strMProductReferenceCode.toUpperCase() + "%";
      } else {
        strMProductReferenceCode = "%";
      }

      if (!strMProductName.equals("") && !strMProductName.equals("%")) {
        xmlDocument.setParameter("productName", strMProductName.toUpperCase());
        strMProductName = "%" + strMProductName.toUpperCase() + "%";
      } else {
        strMProductName = "%";
      }

      if (!strMProductValue.equals("") && !strMProductValue.equals("%")) {
        xmlDocument.setParameter("productValueName", strMProductValue.toUpperCase());
        strMProductValue = "%" + strMProductValue.toUpperCase() + "%";
      } else {
        strMProductValue = "%";
      }

      if (!strMProductYear.equals("") && !strMProductYear.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductYear);
      } else {
        strMProductYear = "0";
      }

      if (!strMProductCilindraje.equals("") && !strMProductCilindraje.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductCilindraje);
      } else {
        strMProductCilindraje = "0";
      }
    }

    if (strCommand.equals("FILTRAR")) {
      strMProductMarcaName = strSearchMProductMarcaName;
      strMProductModeloName = strSearchMProductModeloName;
      strMProductName = strSearchMProductName;
      strMProductValue = strSearchMProductValue;
      strMProductYear = strSearchMProductYear;
      strMProductCilindraje = strSearchMProductCilindraje;

      strMProductFamiliaId = strSearchMProductFamiliaId;
      strMProductCategoria = strSearchMProductCategoriaId;
      strMProductMarcaId = strSearchMProductMarcaId;
      strMProductModeloId = strSearchMProductModeloId;

      if (!strMProductFamiliaId.equals("") && !strMProductFamiliaId.equals("%")) {
        xmlDocument.setParameter("ateccofamiliaid", strMProductFamiliaId);
        strMProductFamiliaId = "%" + strMProductFamiliaId + "%";
      } else {
        strMProductFamiliaId = "%";
      }

      if (!strMProductCategoria.equals("") && !strMProductCategoria.equals("%")) {
        xmlDocument.setParameter("ateccocategoriaid", strMProductCategoria);
        strMProductCategoria = "%" + strMProductCategoria + "%";
      } else {
        strMProductCategoria = "%";
      }

      if (!strMProductMarcaId.equals("") && !strMProductMarcaId.equals("%")) {
        xmlDocument.setParameter("productmarcaid", strMProductMarcaId);
        strMProductMarcaId = "%" + strMProductMarcaId + "%";
      } else {
        strMProductMarcaId = "%";
      }

      if (!strMProductModeloId.equals("") && !strMProductModeloId.equals("%")) {
        xmlDocument.setParameter("productmodeloid", strMProductModeloId);
        strMProductModeloId = strMProductModeloId;
      } else {
        strMProductModeloId = "%";
      }

      if (!strMProductMarcaName.equals("") && !strMProductMarcaName.equals("%")) {
        xmlDocument.setParameter("productMarcaName", strMProductMarcaName.toUpperCase());
        strMProductMarcaName = "%" + strMProductMarcaName.toUpperCase() + "%";
      } else {
        strMProductMarcaName = "%";
      }

      if (!strMProductModeloName.equals("") && !strMProductModeloName.equals("%")) {
        xmlDocument.setParameter("productModeloName", strMProductModeloName.toUpperCase());
        strMProductModeloName = "%" + strMProductModeloName.toUpperCase() + "%";
      } else {
        strMProductModeloName = "%";
      }

      if (!strMProductName.equals("") && !strMProductName.equals("%")) {
        xmlDocument.setParameter("productName", strMProductName.toUpperCase());
        strMProductName = "%" + strMProductName.toUpperCase() + "%";
      } else {
        strMProductName = "%";
      }

      if (!strMProductValue.equals("") && !strMProductValue.equals("%")) {
        xmlDocument.setParameter("productValue", strMProductValue.toUpperCase());
        strMProductValue = "%" + strMProductValue.toUpperCase() + "%";
      } else {
        strMProductValue = "%";
      }

      if (!strMProductYear.equals("") && !strMProductYear.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductYear);
      } else {
        strMProductYear = "0";
      }

      if (!strMProductCilindraje.equals("") && !strMProductCilindraje.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductCilindraje);
      } else {
        strMProductCilindraje = "0";
      }
    }

    xmlDocument.setParameter("searchProductMarcaName", strSearchMProductMarcaName.toUpperCase());
    xmlDocument.setParameter("searchProductModeloName", strSearchMProductModeloName.toUpperCase());
    xmlDocument.setParameter("searchProductName", strSearchMProductName.toUpperCase());
    xmlDocument.setParameter("searchProductValue", strSearchMProductValue.toUpperCase());
    xmlDocument.setParameter("searchProductYear", strSearchMProductYear);
    xmlDocument.setParameter("searchProductCilindraje", strSearchMProductCilindraje);

    xmlDocument.setParameter("productYear", "");
    xmlDocument.setParameter("productCilindraje", "");
    xmlDocument.setParameter("productModeloName", "");
    xmlDocument.setParameter("productMarcaName", "");
    xmlDocument.setParameter("productSubCategoriaName", "");
    xmlDocument.setParameter("productCategoriaName", "");
    xmlDocument.setParameter("productValue", "");
    xmlDocument.setParameter("productUpc", "");
    xmlDocument.setParameter("productName", "");
    xmlDocument.setParameter("productFamiliaName", "");
    xmlDocument.setParameter("productGenericoName", "");

    if (strCommand.equals("BUSCAR")) {
      strMProductValue = vars.getStringParameter("inpmProductValue");
      strMProductUpc = vars.getStringParameter("inpmProductUpc");
      strMProductName = vars.getStringParameter("inpmProductName");
      strMProductFamiliaName = vars.getStringParameter("inpmProductFamiliaName");
      strMProductGenericoName = vars.getStringParameter("inpmProductGenericoName");

      strMProductCategoriaName = vars.getStringParameter("inpmProductCategoriaName");
      strMProductSubCategoriaName = vars.getStringParameter("inpmProductSubCategoriaName");
      strMProductReferenceCode = vars.getStringParameter("inpmProductReferenceCode");
      strMProductMarcaName = vars.getStringParameter("inpmProductMarcaName");
      strMProductModeloName = vars.getStringParameter("inpmProductModeloName");
      strMProductYear = vars.getStringParameter("inpmProductYear");
      strMProductCilindraje = vars.getStringParameter("inpmProductCilindraje");

      strMProductFamiliaId = vars.getStringParameter("inpsearchAteccoFamilia");
      strMProductCategoria = vars.getStringParameter("inpsearchAteccoCategoria");
      strMProductMarcaId = vars.getStringParameter("inpsearchProductMarca");
      strMProductModeloId = vars.getStringParameter("inpsearchProductModelo");

      if (!strMProductFamiliaId.equals("") && !strMProductFamiliaId.equals("%")) {
        xmlDocument.setParameter("ateccofamiliaid", strMProductFamiliaId);
        strMProductFamiliaId = "%" + strMProductFamiliaId + "%";
      } else {
        strMProductFamiliaId = "%";
      }

      if (!strMProductCategoria.equals("") && !strMProductCategoria.equals("%")) {
        xmlDocument.setParameter("ateccocategoriaid", strMProductCategoria);
        strMProductCategoria = "%" + strMProductCategoria + "%";
      } else {
        strMProductCategoria = "%";
      }

      if (!strMProductMarcaId.equals("") && !strMProductMarcaId.equals("%")) {
        xmlDocument.setParameter("productmarcaid", strMProductMarcaId);
        strMProductMarcaId = "%" + strMProductMarcaId + "%";
      } else {
        strMProductMarcaId = "%";
      }

      if (!strMProductModeloId.equals("") && !strMProductModeloId.equals("%")) {
        xmlDocument.setParameter("productmodeloid", strMProductModeloId);
        strMProductModeloId = strMProductModeloId;
      } else {
        strMProductModeloId = "%";
      }

      if (!strMProductYear.equals("") && !strMProductYear.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductYear);
      } else {
        strMProductYear = "0";
      }

      if (!strMProductCilindraje.equals("") && !strMProductCilindraje.equals("0")) {
        xmlDocument.setParameter("productCilindraje", strMProductCilindraje);
      } else {
        strMProductCilindraje = "0";
      }

      if (!strMProductModeloName.equals("") && !strMProductModeloName.equals("%")) {
        xmlDocument.setParameter("productModeloName", strMProductModeloName.toUpperCase());
        strMProductModeloName = "%" + strMProductModeloName.toUpperCase() + "%";
      } else {
        strMProductModeloName = "%";
      }

      if (!strMProductMarcaName.equals("") && !strMProductMarcaName.equals("%")) {
        xmlDocument.setParameter("productMarcaName", strMProductMarcaName.toUpperCase());
        strMProductMarcaName = "%" + strMProductMarcaName.toUpperCase() + "%";
      } else {
        strMProductMarcaName = "%";
      }

      if (!strMProductSubCategoriaName.equals("") && !strMProductSubCategoriaName.equals("%")) {
        xmlDocument.setParameter("productSubCategoriaName",
            strMProductSubCategoriaName.toUpperCase());
        strMProductSubCategoriaName = "%" + strMProductSubCategoriaName.toUpperCase() + "%";
      } else {
        strMProductSubCategoriaName = "%";
      }

      if (!strMProductCategoriaName.equals("") && !strMProductCategoriaName.equals("%")) {
        xmlDocument.setParameter("productCategoriaName", strMProductCategoriaName.toUpperCase());
        strMProductCategoriaName = "%" + strMProductCategoriaName.toUpperCase() + "%";
      } else {
        strMProductCategoriaName = "%";
      }

      if (!strMProductValue.equals("") && !strMProductValue.equals("%")) {
        xmlDocument.setParameter("productValue", strMProductValue.toUpperCase());
        strMProductValue = "%" + strMProductValue.toUpperCase() + "%";
      } else {
        strMProductValue = "%";
      }

      if (!strMProductUpc.equals("") && !strMProductUpc.equals("%")) {
        xmlDocument.setParameter("productUpc", strMProductUpc.toUpperCase());
        strMProductUpc = "%" + strMProductUpc.toUpperCase() + "%";
      } else {
        strMProductUpc = "%";
      }

      if (!strMProductName.equals("") && !strMProductName.equals("%")) {
        xmlDocument.setParameter("productName", strMProductName.toUpperCase());
        strMProductName = "%" + strMProductName.toUpperCase() + "%";
      } else {
        strMProductName = "%";
      }

      if (!strMProductFamiliaName.equals("") && !strMProductFamiliaName.equals("%")) {
        xmlDocument.setParameter("productFamiliaName", strMProductFamiliaName.toUpperCase());
        strMProductFamiliaName = "%" + strMProductFamiliaName.toUpperCase() + "%";
      } else {
        strMProductFamiliaName = "%";
      }

      if (!strMProductGenericoName.equals("") && !strMProductGenericoName.equals("%")) {
        xmlDocument.setParameter("productGenericoName", strMProductGenericoName.toUpperCase());
        strMProductGenericoName = "%" + strMProductGenericoName.toUpperCase() + "%";
      } else {
        strMProductGenericoName = "%";
      }

      if (!strMProductReferenceCode.equals("") && !strMProductReferenceCode.equals("%")) {
        xmlDocument.setParameter("productReferenceCode", strMProductReferenceCode.toUpperCase());
        strMProductReferenceCode = "%" + strMProductReferenceCode.toUpperCase() + "%";
      } else {
        strMProductReferenceCode = "%";
      }
    }

    FacturaPosData[] data1 = null;
    
    /*Busqueda por coincidencias*/
    String strDato = ""; //en este campo se envia el parametro de busqueda
    strDato = strMProductName;

    if (strCommand.equals("BUSCAR")) {
    	if (strDato.length() >= 4) {  /*Busqueda por coincidencias*/	
	        data1 = FacturaPosData.methodSeleccionarProductosOrg(this, 
	                vars.getClient(), strMProductWarehouseName, "%%", strMProductName, 
	                strDato, strDato, strDato); //cod. producto, marca producto, cod. barras 
    	}
    } else {  /*busca en todas las categorias*/       
        data1 = FacturaPosData.methodSeleccionarProductosOrg(this, 
                vars.getClient(), strMProductWarehouseName, strMProductCategoria, strMProductName, 
                strDato, strDato, strDato); //cod. producto, marca producto, cod. barras        
    }
    
    //Lista de productos seleccionados
    FacturaPosData[] data2 = FacturaPosData.methodSeleccionarProductosTemporales(this, strFpvFacturaCotId);

    /*Totales*/
    FacturaPosData[] data3 = FacturaPosData.methodTotales(this, strFpvFacturaCotId);

    if (data3.length > 0) {
      xmlDocument.setParameter("facturaCotSubtotal", data3[0].mproductpricetotal);  //base imponible
      xmlDocument.setParameter("facturaCotIVA", data3[0].mproducttotalimp);   //impuestos
      xmlDocument.setParameter("facturaCotTotal", data3[0].mproducttotal);    //total
      xmlDocument.setParameter("facturaCantTotal", data3[0].mproducttotalcant); //total de productos a vender
      xmlDocument.setParameter("facturaDescuentoTotal", data3[0].mproducttotaldescuento); //descuento total de la 
    
      xmlDocument.setParameter("facturaCotTotalEfec", data3[0].mproducttotal);
    } else {
      xmlDocument.setParameter("facturaCotSubtotal", "0");
      xmlDocument.setParameter("facturaCotIVA", "0");
      xmlDocument.setParameter("facturaCotTotal", "0");
      xmlDocument.setParameter("facturaCantTotal", "0");
      xmlDocument.setParameter("facturaDescuentoTotal", "0");
      
      xmlDocument.setParameter("facturaCotTotalEfec", "0");
    }

    /*FacturaPosData[] data4 = FacturaPosData.methodTotalEfecFacuraCot(this,
        strFpvFacturaCotId);

    if (data4.length > 0) {
      xmlDocument.setParameter("facturaCotTotalEfec", data4[0].mproductpricetotal);
    } else {
      xmlDocument.setParameter("facturaCotTotalEfec", "0");
    }*/
    
    /*Datos del cliente*/
    FacturaPosData[] data6 = FacturaPosData.methodDatosCliente(this, strFpvFacturaCotId);

    if (data6.length > 0) {
      xmlDocument.setParameter("idenCliente", data6[0].idenclient);
      xmlDocument.setParameter("nombreCliente", data6[0].nameclient);
      xmlDocument.setParameter("telefonoCliente", data6[0].telefonoclient);
      xmlDocument.setParameter("emailCliente", data6[0].emailclient);
      xmlDocument.setParameter("direccionCliente", data6[0].direccionclient);
    } else {
      xmlDocument.setParameter("idenCliente", "");
      xmlDocument.setParameter("nombreCliente", "");
      xmlDocument.setParameter("telefonoCliente", "");
      xmlDocument.setParameter("emailCliente", "");
      xmlDocument.setParameter("direccionCliente", "");
    }
    
    //metodo de pago
    FacturaPosData[] datam = null;
    datam = FacturaPosData.buscarMetodoPago(this, vars.getClient());

    xmlDocument.setParameter("productWarehouseName", strMProductWarehouseName);
    xmlDocument.setParameter("fpvFacturaCotId", strFpvFacturaCotId);

    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");

    xmlDocument.setData("structure1", data1);
    xmlDocument.setData("structure2", data2);
    xmlDocument.setData("structure3", datam);

    out.println(xmlDocument.print());
    out.close();
  }
  
  private OBError processButton(VariablesSecureApp vars, String strOrder,
	      List<ATECCO_Temporal> temp) {
	    OBError myError = null;
	    Connection conn = OBDal.getInstance().getConnection();
	    
	    OBContext.setAdminMode(true);
	    try {

	      myError = new OBError();
	      myError.setType("Success");
	      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));

	      for (int i = 0; i < temp.size(); i++) {
	        GenerarFacturaData.methodInsertarTemp(this, vars.getClient(),
	            String.valueOf(temp.get(i).getAmount()), temp.get(i).getTarjeta_id(),
	            temp.get(i).getCondicion_id(), temp.get(i).getBanco_id(), temp.get(i).getReferenceno(),
	            temp.get(i).getNro_cheque(), temp.get(i).getTipoPago(), vars.getClient(),
	            strOrder);
	      }

	      GenerarFacturaData.methodUpdateOrder(this, "CO", strOrder);
	      
	      /*Procesar factura*/
	      String sqlQuery = "";
	      int updateOrder = 0;
	      int insertpinstance = 0;
	      String auxAdPinstanceId = null;
	      String strPInstance = null;
	      
	      sqlQuery = "UPDATE c_orderline col SET "
	              + "pricelist = (SELECT (pricelist * ((fpm.em_atecco_porcentaje + 100)/100)) "
	              + "FROM c_order co "
	              + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
	              + "WHERE co.c_order_id = col.c_order_id), "
	              + "priceactual = (SELECT (priceactual * ((fpm.em_atecco_porcentaje + 100)/100)) "
	              + "FROM c_order co "
	              + "INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) "
	              + "WHERE co.c_order_id = col.c_order_id) WHERE col.c_order_id = '" + strOrder + "' "
	              + "AND col.m_product_id NOT IN (SELECT mp.m_product_id " + "FROM m_product mp "
	              + "WHERE upper(mp.name) LIKE '%DESCUENTO%' " + "AND mp.ad_client_id = '"
	              + vars.getClient() + "');";
	          PreparedStatement ps = conn.prepareStatement(sqlQuery);
	          ps.executeUpdate();
	          ps.close();

	          sqlQuery = "SELECT * FROM atecco_cuadrardescuento_linea('" + strOrder + "')";
	          ps = conn.prepareStatement(sqlQuery);
	          ps.execute();

	          ps.close();

	          sqlQuery = "SELECT ad_pinstance_id " + "FROM ad_pinstance " + "WHERE record_id = '" + strOrder
	              + "' AND result = 1 " + "ORDER BY created DESC " + "LIMIT 1;";
	          ps = conn.prepareStatement(sqlQuery);
	          ResultSet rs = ps.executeQuery();

	          while (rs.next()) {
	            auxAdPinstanceId = rs.getString("ad_pinstance_id");
	            insertpinstance = 1;
	            updateOrder = 1;
	          }

	          ps.close();
	          rs.close();

	          if (auxAdPinstanceId == null) {
	            sqlQuery = "UPDATE c_order SET em_atecco_docaction = 'CO', docaction = 'CO' WHERE c_order_id = '"
	                + strOrder + "'";

	            ps = conn.prepareStatement(sqlQuery);
	            updateOrder = ps.executeUpdate();
	            strPInstance = UUID.randomUUID().toString().replace("-", "").toUpperCase();
	            ps.close();
	          }

	          if (updateOrder == 1) {

	            if (auxAdPinstanceId == null) {
	              sqlQuery = "INSERT INTO ad_pinstance("
	                  + "ad_pinstance_id, ad_process_id, record_id, isprocessing, created, "
	                  + "ad_user_id, updated, result, errormsg, ad_client_id, ad_org_id, "
	                  + "createdby, updatedby, isactive) " + "VALUES ('" + strPInstance + "', '104', '"
	                  + strOrder + "', 'N', now(), " + "'100', now(), null, null, '"
	                  + vars.getClient() + "', '0', " + "'100', '100', 'Y');";

	              ps = conn.prepareStatement(sqlQuery);
	              insertpinstance = ps.executeUpdate();
	              ps.close();
	            }

	            if (insertpinstance == 1) {

	              if (auxAdPinstanceId == null) {
	                sqlQuery = "SELECT * FROM c_order_post('" + strPInstance + "')";

	                ps = conn.prepareStatement(sqlQuery);
	                ps.execute();

	                ps.close();
	              } else {
	                strPInstance = auxAdPinstanceId;
	              }

	              sqlQuery = "SELECT result, errormsg FROM ad_pinstance WHERE ad_pinstance_id = '"
	                  + strPInstance + "'";

	              ps = conn.prepareStatement(sqlQuery);
	              rs = ps.executeQuery();
	              int auxresult = 0;
	              String auxerrormsg = null;

	              while (rs.next()) {
	                if (rs.getString("result") != null) {
	                  auxresult = rs.getInt("result");
	                }

	                auxerrormsg = rs.getString("errormsg") != null ? rs.getString("errormsg") : "";
	              }

	              ps.close();

	              sqlQuery = "UPDATE c_order SET em_atecco_docaction = docaction WHERE c_order_id = '"
	                  + strOrder + "'";

	              ps = conn.prepareStatement(sqlQuery);
	              ps.executeUpdate();

	              ps.close();
	              rs.close();

	              if (auxresult == 1) {
	                sqlQuery = "SELECT ci.c_invoice_id, cd.em_atecfe_fac_elec AS doc1, cd2.em_atecfe_fac_elec AS doc2, cd2.name "
	                    + "FROM c_order co " + "INNER JOIN c_invoice ci ON (co.c_order_id = ci.c_order_id) "
	                    + "INNER JOIN c_doctype cd ON (co.c_doctype_id = cd.c_doctype_id) "
	                    + "INNER JOIN c_doctype cd2 ON (ci.c_doctype_id = cd2.c_doctype_id) "
	                    + "WHERE co.c_order_id = '" + strOrder + "' " + "AND ci.em_atecfe_docaction = 'PR' "
	                    + "LIMIT 1;";

	                ps = conn.prepareStatement(sqlQuery);
	                rs = ps.executeQuery();

	                String strCinvoiceId = null;
	                String auxdoc1 = null;
	                String auxdoc2 = null;
	                String auxname = null;

	                while (rs.next()) {
	                  if (rs.getString("c_invoice_id") != null) {
	                    strCinvoiceId = rs.getString("c_invoice_id");
	                    auxdoc1 = rs.getString("doc1");
	                    auxdoc2 = rs.getString("doc2");
	                    auxname = rs.getString("name");
	                  }
	                }

	                ps.close();
	                rs.close();

	                if (strCinvoiceId != null) {
	                  sqlQuery = "UPDATE c_order SET em_atecco_docstatus = '--' WHERE c_order_id = '"
	                      + strOrder + "';";
	                  ps = conn.prepareStatement(sqlQuery);
	                  ps.execute();

	                  ps.close();
	                  
	                  sqlQuery = "SELECT * FROM fpv_actua_invoiceline('" + strCinvoiceId + "', '" + strOrder
	                          + "')";
	                  ps = conn.prepareStatement(sqlQuery);
	                  ps.execute();

	                  ps.close();
	                }

	                if (strCinvoiceId != null && auxdoc1.equals("Y")) {
	                  if (auxdoc2.equals("Y")) {
	                    ATECCO_Operaciones_XML opeXML = new ATECCO_Operaciones_XML();

	                    OBDal.getInstance().commitAndClose();

	                    if (opeXML.declararFacturaSRI(strCinvoiceId, vars.getUser(), msg, this)) {
	                      OBDal.getInstance().commitAndClose();
	                      GenerarFacturaData.methodUpdateOrder(this, "--", strOrder);
	                      myError = new OBError();
	                      myError.setType("Success");
	                      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
	                      auxerrormsg = auxerrormsg + ", " + this.msg.getMessage();
	                    } else {
	                      OBDal.getInstance().rollbackAndClose();
	                      GenerarFacturaData.methodUpdateOrder(this, "--", strOrder);
	                      myError = new OBError();
	                      myError.setType("Success");
	                      myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
	                      auxerrormsg = auxerrormsg + ", Error Factura Electrónica: "
	                          + this.msg.getMessage()
	                          + ", corrija el error y procese con el SRI desde la ventana documento de venta";
	                    }
	                  } else {
	                    auxerrormsg = "@El tipo de documento '" + auxname
	                        + "' no esta configurado como factura electronica@";
	                    OBDal.getInstance().rollbackAndClose();
	                    myError = new OBError();
	                    myError.setType("Error");
	                    myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
	                    myError.setMessage(auxerrormsg);
	                    return myError;
	                  }
	                } else {
	                  OBDal.getInstance().commitAndClose();
	                  GenerarFacturaData.methodUpdateOrder(this, "--", strOrder);
	                  myError = new OBError();
	                  myError.setType("Success");
	                  myError.setTitle(Utility.messageBD(this, "Success", vars.getLanguage()));
	                }
	              } else {
	                OBDal.getInstance().rollbackAndClose();

	                GenerarFacturaData.methodUpdateOrder(this, "SP", strOrder);
	                GenerarFacturaData.methodDeleteOrder(this, strOrder);

	                conn = OBDal.getInstance().getConnection();          

	                ps.close();
	                OBDal.getInstance().commitAndClose();

	                myError = new OBError();
	                myError.setType("Error");
	                myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));

	                conn.close();
	                conn = null;
	              }

	              if (auxerrormsg != null) {
	                auxerrormsg = auxerrormsg.replaceAll("@ERROR=", "");
	              } else {
	                auxerrormsg = "@Documento procesado@";
	              }

	              String auxerrormsgTrad = "";

	              while (auxerrormsg.indexOf("@") != -1) {
	                int auxIni = auxerrormsg.indexOf("@");
	                int auxFin = auxerrormsg.indexOf("@", auxIni + 1);

	                String auxParteInicio = auxerrormsg.substring(0, auxIni);
	                String auxParte = auxerrormsg.substring(auxIni + 1, auxFin);

	                auxParte = Utility.messageBD(this, auxParte, vars.getLanguage());

	                auxerrormsgTrad = auxerrormsgTrad + auxParteInicio + auxParte;

	                auxerrormsg = auxerrormsg.substring(auxFin + 1, auxerrormsg.length());
	              }

	              auxerrormsgTrad = auxerrormsgTrad + auxerrormsg;

	              myError.setMessage(auxerrormsgTrad);
	              return myError;
	            }
	          }
	          
	          OBDal.getInstance().commitAndClose();
	      /*Fin procesar factura*/
	      
	      
	    } catch (Exception e) {
	      OBDal.getInstance().rollbackAndClose();

	      try {
	    	  GenerarFacturaData.methodUpdateOrder(this, "SP", strOrder);
	    	  GenerarFacturaData.methodDeleteOrder(this, strOrder);
	      } catch (ServletException e1) {
	        // TODO Auto-generated catch block
	        e1.printStackTrace();
	      }

	      e.printStackTrace();
	      log4j.warn("Rollback in transaction");
	      myError = new OBError();
	      myError.setType("Error");
	      myError.setTitle(Utility.messageBD(this, "Error", vars.getLanguage()));
	      myError.setMessage("El proceso no se ejecuto correctamente: " + e.getMessage());
	      // myError = Utility.translateError(this, vars, vars.getLanguage(), "ProcessRunError");
	    } finally {
	      OBContext.restorePreviousMode();

	      if (conn != null) {
	        try {
	          conn.close();
	        } catch (Exception e) {
	        }
	      }
	    }

	    return myError;
	  }
}
