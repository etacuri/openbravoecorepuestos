package com.atrums.ventas.pedido.ad_forms;

import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.StringTokenizer;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.jfree.util.Log;
import org.openbravo.base.filter.IsIDFilter;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.dal.core.OBContext;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.erpCommon.utility.ComboTableData;
import org.openbravo.erpCommon.utility.LeftTabsBar;
import org.openbravo.erpCommon.utility.NavigationBar;
import org.openbravo.erpCommon.utility.OBError;
import org.openbravo.erpCommon.utility.ToolBar;
import org.openbravo.erpCommon.utility.Utility;
import org.openbravo.utils.Replace;
import org.openbravo.xmlEngine.XmlDocument;

public class FacturaRapida extends HttpSecureAppServlet {

  /**
   *  
   */
  private static final long serialVersionUID = 1L;

  @Override
  public void init(ServletConfig config) {
    super.init(config);
    boolHist = false;
  }

  public void doPost(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);

    OBContext.setAdminMode(true);

    try {
      if (vars.commandIn("DEFAULT")) {
        printPageDataSheet(response, vars, "DEFAULT");
      } else if (vars.commandIn("REGISTRAR")) {
        printPageDataSheet(response, vars, "REGISTRAR");
      } else if (vars.commandIn("BUSCAR")) {
        printPageDataSheet(response, vars, "BUSCAR");
      } else if (vars.commandIn("LIMPIAR")) {
        printPageDataSheet(response, vars, "LIMPIAR");
      } else if (vars.commandIn("ENVIAR")) {
        printPageDataSheet(response, vars, "ENVIAR");
      } else if (vars.commandIn("ELIMINAR")) {
        printPageDataSheet(response, vars, "ELIMINAR");
      } else if (vars.commandIn("ACTUALIZAR")) {
        printPageDataSheet(response, vars, "ACTUALIZAR");
      } else if (vars.commandIn("METODOPAGO")) {
        printPageDataSheet(response, vars, "METODOPAGO");
      } else if (vars.commandIn("FILTRAR")) {
        printPageDataSheet(response, vars, "FILTRAR");
      } else if (vars.commandIn("FILTRARLIMPIAR")) {
        printPageDataSheet(response, vars, "FILTRARLIMPIAR");
      } else if (vars.commandIn("OBTENER_FAMILIA") || vars.commandIn("OBTENER_CATEGORIA")
          || vars.commandIn("OBTENER_MARCA") || vars.commandIn("OBTENER_MODELO")) {
        printPageDataSheet(response, vars, "OBTENER_CATEGORIA");
      } else {
        pageError(response);
      }
    } finally {
      // TODO: handle finally clause
      OBContext.restorePreviousMode();
    }

  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars,
      String strCommand) throws IOException, ServletException {
    // TODO Auto-generated method stub
    log4j.debug("Output: dataSheet");

    String strMProductWarehouseName = vars.getStringParameter("inpmProductWarehouseName");
    String strFpvFacturaCotId = vars.getStringParameter("inpfpvFacturaCotId");
    String auxFinPaymentmethod = vars.getStringParameter("inpMetodoPago");

    if (strCommand.equals("LIMPIAR")) {
      strFpvFacturaCotId = "";
    }

    if (strCommand.equals("ACTUALIZAR")) {
      String strfpvFacturaCotLinId = "";

      try {
        strfpvFacturaCotLinId = vars.getRequiredInStringParameter("inpfpvFacturaCotLinId",
            IsIDFilter.instance);
      } catch (Exception ex) {
        // TODO: handle exception
        Log.error(ex.getMessage(), ex);
      }

      if (strfpvFacturaCotLinId.startsWith("("))
        strfpvFacturaCotLinId = strfpvFacturaCotLinId.substring(1,
            strfpvFacturaCotLinId.length() - 1);

      if (!strfpvFacturaCotLinId.equals("")) {
        strfpvFacturaCotLinId = Replace.replace(strfpvFacturaCotLinId, "'", "");
        StringTokenizer st = new StringTokenizer(strfpvFacturaCotLinId, ",", false);

        boolean ingresoExito = false;

        while (st.hasMoreTokens()) {
          String strpFpvFacturaCotLinId = st.nextToken().trim();

          if (!strpFpvFacturaCotLinId.equals("")) {
            String strCantidadOrdenada = vars
                .getStringParameter("inpfpvCantidadOrdenada" + strpFpvFacturaCotLinId);
            String strProductPrice = vars
                .getStringParameter("inpfpvProductPrice" + strpFpvFacturaCotLinId);
            String strTotalCantidad = vars
                .getStringParameter("inpfpvTotalLinea" + strpFpvFacturaCotLinId);

            BigDecimal bdmCantidad = new BigDecimal(strCantidadOrdenada);

            if (bdmCantidad.doubleValue() > 0) {
              FacturaRapidaData.methodActualizarTemporalFacuraCot(this, strpFpvFacturaCotLinId,
                  strProductPrice, strCantidadOrdenada, strTotalCantidad);

              ingresoExito = true;
            }
          }
        }

        if (ingresoExito) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s actualizado/s", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage.setMessage(
              Utility.messageBD(this, "Producto/s no actualizado/s", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        }
      }
    }

    if (strCommand.equals("ELIMINAR")) {
      String strfpvFacturaCotLinId = "";

      try {
        strfpvFacturaCotLinId = vars.getRequiredInStringParameter("inpfpvFacturaCotLinId",
            IsIDFilter.instance);
      } catch (Exception ex) {
        // TODO: handle exception
        Log.error(ex.getMessage(), ex);
      }

      if (strfpvFacturaCotLinId.startsWith("("))
        strfpvFacturaCotLinId = strfpvFacturaCotLinId.substring(1,
            strfpvFacturaCotLinId.length() - 1);

      if (!strfpvFacturaCotLinId.equals("")) {
        strfpvFacturaCotLinId = Replace.replace(strfpvFacturaCotLinId, "'", "");
        StringTokenizer st = new StringTokenizer(strfpvFacturaCotLinId, ",", false);

        boolean ingresoExito = false;

        while (st.hasMoreTokens()) {
          String strpFpvFacturaCotLinId = st.nextToken().trim();

          if (!strpFpvFacturaCotLinId.equals("")) {
            FacturaRapidaData.methodBorrarTemporalFacuraCot(this, strpFpvFacturaCotLinId);

            ingresoExito = true;
          }
        }

        if (ingresoExito) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s eliminado/s", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s no eliminado/s", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        }
      }
    }

    if (strCommand.equals("REGISTRAR")) {
      String strmProductStockId = "";

      try {
        strmProductStockId = vars.getRequiredInStringParameter("inpProductStockId",
            IsIDFilter.instance);
      } catch (Exception ex) {
        // TODO: handle exception
        Log.error(ex.getMessage(), ex);
      }

      if (strmProductStockId.startsWith("("))
        strmProductStockId = strmProductStockId.substring(1, strmProductStockId.length() - 1);

      if (!strmProductStockId.equals("")) {
        strmProductStockId = Replace.replace(strmProductStockId, "'", "");
        StringTokenizer st = new StringTokenizer(strmProductStockId, ",", false);

        boolean ingresoExito = false;
        boolean ingresoStock = false;
        boolean ingresoValorZero = false;
        boolean ingresoDescuento = false;

        while (st.hasMoreTokens()) {
          String strMProductStockId = st.nextToken().trim();

          if (!strMProductStockId.equals("")) {
            String strCantidadOrdenada = vars
                .getStringParameter("inpCantidadOrdenada" + strMProductStockId);
            String strTotalCantidad = vars
                .getStringParameter("inpTotalCantidad" + strMProductStockId);
            String strProductPrice = vars
                .getStringParameter("inpmProductPrice" + strMProductStockId);
            String strTotalStock = vars.getStringParameter("inpTotalStock" + strMProductStockId);
            String strMProductId = vars.getStringParameter("inpMProductId" + strMProductStockId);
            String strDescuento = vars.getStringParameter("inpDescuento" + strMProductStockId);

            BigDecimal bdmCantidad = new BigDecimal(strCantidadOrdenada);
            BigDecimal bdmTotalCantidad = new BigDecimal(strTotalCantidad);
            BigDecimal bdmTotalStock = new BigDecimal(strTotalStock);
            BigDecimal bdmDescuento = new BigDecimal(strDescuento);

            if (bdmCantidad.doubleValue() <= bdmTotalStock.doubleValue()) {
              if (bdmDescuento.doubleValue() >= 0 && bdmDescuento.doubleValue() <= 10) {
                if (strFpvFacturaCotId.equals("") && bdmCantidad.doubleValue() > 0
                    && bdmTotalCantidad.doubleValue() > 0) {
                  FacturaRapidaData[] fpvFacturaCotIdData = FacturaRapidaData
                      .methodSeleccionarTemporalFacuraCot(this, vars.getClient(), vars.getOrg(), vars.getUser());

                  if (fpvFacturaCotIdData.length > 0) {
                    strFpvFacturaCotId = fpvFacturaCotIdData[0].fpvfacturacotid;

                    FacturaRapidaData.methodSeleccionarTemporalFacuraCot(this, vars.getClient(),
                        vars.getOrg(), strFpvFacturaCotId, strMProductId, vars.getWarehouse(),
                        strProductPrice, strCantidadOrdenada, strTotalCantidad, strDescuento);

                    ingresoExito = true;
                  }
                } else if (!strFpvFacturaCotId.equals("") && bdmCantidad.doubleValue() > 0
                    && bdmTotalCantidad.doubleValue() > 0) {
                  FacturaRapidaData.methodSeleccionarTemporalFacuraCot(this, vars.getClient(),
                      vars.getOrg(), strFpvFacturaCotId, strMProductId, vars.getWarehouse(),
                      strProductPrice, strCantidadOrdenada, strTotalCantidad, strDescuento);

                  ingresoExito = true;
                } else if (bdmTotalCantidad.doubleValue() <= 0) {
                  ingresoValorZero = true;
                }
              } else {
                ingresoDescuento = true;
              }
            } else {
              ingresoStock = true;
            }
          }
        }

        if (ingresoStock) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this,
              "Producto/s no registrado/s, porque la cantidad supera el stock",
              vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else if (ingresoExito) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage
              .setMessage(Utility.messageBD(this, "Producto/s registrado/s", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else if (ingresoValorZero) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage.setMessage(Utility.messageBD(this,
              "Producto/s no registrado/s, porque el total es de 0", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else if (ingresoDescuento) {
          OBError myMessage = new OBError();
          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this,
              "Producto/s no registrado/s, El descuento por producto no puede ser mayor al 10%",
              vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage.setMessage(
              Utility.messageBD(this, "Producto/s no registrado/s", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        }
      }
    }

    if (strCommand.equals("METODOPAGO")) {
      FacturaRapidaData[] DataMetodo = null;

      String strmensaje = null;

      try {
        DataMetodo = FacturaRapidaData.methodActualizarMetodoFacuraCot(this, strFpvFacturaCotId,
            auxFinPaymentmethod);
      } catch (Exception e) {
        // TODO: handle exception
        Log.error(e.getMessage());
        strmensaje = e.getMessage();
      }

      if (strmensaje != null) {
        OBError myMessage = new OBError();

        myMessage.setTitle("Error");
        myMessage.setType("ERROR");
        myMessage.setMessage(Utility.messageBD(this,
            "Precio no actualizado, " + strmensaje.replaceAll("@CODE=0@ERROR: ", ""),
            vars.getLanguage()));

        vars.setMessage("FacturaRapida", myMessage);
      } else if (DataMetodo.length > 0) {
        OBError myMessage = new OBError();

        myMessage.setTitle("Success");
        myMessage.setType("SUCCESS");
        myMessage.setMessage(Utility.messageBD(this, "Precio actualizado", vars.getLanguage()));

        vars.setMessage("FacturaRapida", myMessage);

      } else {
        OBError myMessage = new OBError();

        myMessage.setTitle("Error");
        myMessage.setType("ERROR");
        myMessage.setMessage(Utility.messageBD(this, "Precio no actualizado", vars.getLanguage()));

        vars.setMessage("FacturaRapida", myMessage);
      }
    }

    if (strCommand.equals("ENVIAR")) {

      if (!strFpvFacturaCotId.equals("")) {
        FacturaRapidaData[] enviarData = null;

        String strmensaje = null;

        try {
          enviarData = FacturaRapidaData.methodSeleccionarTemporalFacuraCli(this,
              strFpvFacturaCotId, vars.getWarehouse(), auxFinPaymentmethod, vars.getUser());
        } catch (Exception e) {
          // TODO: handle exception
          Log.error(e.getMessage());
          strmensaje = e.getMessage();
        }

        if (strmensaje != null) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this,
              "Documento No Creado, " + strmensaje.replaceAll("@CODE=0@ERROR: ", ""),
              vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        } else if (enviarData.length > 0) {
          OBError myMessage = new OBError();

          myMessage.setTitle("Success");
          myMessage.setType("SUCCESS");
          myMessage.setMessage(Utility.messageBD(this,
              "Documento Creado # " + enviarData[0].fpvdocumentno, vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);

        } else {
          OBError myMessage = new OBError();

          myMessage.setTitle("Error");
          myMessage.setType("ERROR");
          myMessage.setMessage(Utility.messageBD(this, "Documento No Creado", vars.getLanguage()));

          vars.setMessage("FacturaRapida", myMessage);
        }
      } else {
        OBError myMessage = new OBError();

        myMessage.setTitle("Error");
        myMessage.setType("ERROR");
        myMessage.setMessage(Utility.messageBD(this, "No hay productos para generar la factura",
            vars.getLanguage()));

        vars.setMessage("FacturaRapida", myMessage);
      }
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();

    /**
     * Contruyendo el base de XML para el HTMLss
     **/
    XmlDocument xmlDocument = xmlEngine
        .readXmlTemplate("com/atrums/ventas/pedido/ad_forms/FacturaRapida").createXmlDocument();

    ToolBar toolbar = new ToolBar(this, vars.getLanguage(), "FacturaRapida", false, "", "", "",
        false, "ad_forms", strReplaceWith, false, true);
    toolbar.prepareSimpleToolBarTemplate();
    xmlDocument.setParameter("toolbar", toolbar.toString());

    OBContext.setAdminMode(true);

    try {
      WindowTabs tabs = new WindowTabs(this, vars,
          "com.atrums.ventas.pedido.ad_forms.FacturaRapida");
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      xmlDocument.setParameter("theme", vars.getTheme());
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "FacturaRapida.html",
          classInfo.id, classInfo.type, strReplaceWith, tabs.breadcrumb());
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "FacturaRapida.html",
          strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.manualTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    } finally {
      OBContext.restorePreviousMode();
    }

    if (strMProductWarehouseName.equals("")) {
      FacturaRapidaData[] mWarehouseNameData = FacturaRapidaData.methodSeleccionarProductosOrg(this,
          vars.getWarehouse());

      if (mWarehouseNameData.length > 0) {
        strMProductWarehouseName = mWarehouseNameData[0].mproductwarehousename;
      } else {
        strMProductWarehouseName = "%";
      }
    }

    OBError myMessage = vars.getMessage("FacturaRapida");
    vars.removeMessage("FacturaRapida");
    if (myMessage != null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }

    ComboTableData comboTableDataMetodoPago;

    try {
      if (auxFinPaymentmethod.equals("")) {
        FacturaRapidaData[] auxMetodoData = FacturaRapidaData.methodMetodoPagoAlto(this,
            vars.getClient());

        if (auxMetodoData.length > 0) {
          auxFinPaymentmethod = auxMetodoData[0].fpvmetodopagovid;
        }
      }

      // Filtros
      ComboTableData comboTableDataFamilia;
      ComboTableData comboTableDataCategoria;
      ComboTableData comboTableDataMarca;
      ComboTableData comboTableDataModelo;

      if (vars.getStringParameter("inpsearchAteccoFamilia") != ""
          && !(strCommand.equals("FILTRARLIMPIAR") || strCommand.equals("LIMPIAR"))) {
        comboTableDataCategoria = new ComboTableData(vars, this, "TABLEDIR", "FPV_CATEGORIA_ID", "",
            "ATECCO_CATEGORIA FAMILIA", Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      } else {
        comboTableDataCategoria = new ComboTableData(vars, this, "TABLEDIR", "FPV_CATEGORIA_ID", "",
            null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      }

      if (vars.getStringParameter("inpsearchAteccoCategoria") != ""
          && !(strCommand.equals("FILTRARLIMPIAR") || strCommand.equals("LIMPIAR"))) {
        comboTableDataFamilia = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_FAMILIA_ID", "",
            "ATECCO_FAMILIA CATEGORIA", Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      } else {
        comboTableDataFamilia = new ComboTableData(vars, this, "TABLEDIR", "ATECCO_FAMILIA_ID", "",
            null, Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      }

      if (vars.getStringParameter("inpsearchProductMarca") != ""
          && !(strCommand.equals("FILTRARLIMPIAR") || strCommand.equals("LIMPIAR"))) {
        comboTableDataModelo = new ComboTableData(vars, this, "TABLEDIR", "FPV_MODELO_ID", "",
            "ATECCO_MODELO MARCA", Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      } else {
        comboTableDataModelo = new ComboTableData(vars, this, "TABLEDIR", "FPV_MODELO_ID", "", null,
            Utility.getReferenceableOrg(vars, vars.getOrg()),
            Utility.getContext(this, vars, "#User_Client", null), 0);
      }

      comboTableDataMarca = new ComboTableData(vars, this, "TABLEDIR", "M_BRAND_ID", "", null,
          Utility.getReferenceableOrg(vars, vars.getOrg()),
          Utility.getContext(this, vars, "#User_Client", null), 0);

      Utility.fillSQLParameters(this, vars, null, comboTableDataFamilia, null, null);

      xmlDocument.setData("reportAteccoFamilia_ID", "liststructure",
          comboTableDataFamilia.select(false));

      Utility.fillSQLParameters(this, vars, null, comboTableDataCategoria, null, null);

      xmlDocument.setData("reportAteccoCategoria_ID", "liststructure",
          comboTableDataCategoria.select(false));

      Utility.fillSQLParameters(this, vars, null, comboTableDataMarca, null, null);

      xmlDocument.setData("reportProductMarca_ID", "liststructure",
          comboTableDataMarca.select(false));

      Utility.fillSQLParameters(this, vars, null, comboTableDataModelo, null, null);

      xmlDocument.setData("reportProductModelo_ID", "liststructure",
          comboTableDataModelo.select(false));

      // Fin Flltros

      comboTableDataMetodoPago = new ComboTableData(vars, this, "TABLEDIR", "FPV_METODOPAGO_V_ID",
          "", null, Utility.getReferenceableOrg(vars, vars.getOrg()),
          Utility.getContext(this, vars, "#User_Client", null), 0);

      Utility.fillSQLParameters(this, vars, null, comboTableDataMetodoPago, null, null);
      xmlDocument.setData("reportFPVMetodoPago_ID", "liststructure",
          comboTableDataMetodoPago.select(false));

      xmlDocument.setParameter("metodopagoid", auxFinPaymentmethod);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    String strMProductValue = "%";
    String strMProductUpc = "%";
    String strMProductName = "%";
    String strMProductReferenceCode = "%";
    String strMProductFamiliaName = "%";
    String strMProductGenericoName = "%";

    String strMProductCategoriaName = "%";
    String strMProductSubCategoriaName = "%";
    String strMProductMarcaName = "%";
    String strMProductModeloName = "%";
    String strMProductYear = "0";
    String strMProductCilindraje = "0";

    String strMProductFamiliaId = "%";
    String strMProductCategoriaId = "%";
    String strMProductMarcaId = "%";
    String strMProductModeloId = "%";

    String strSearchMProductMarcaName = vars.getStringParameter("inpsearchMProductMarcaName");
    String strSearchMProductModeloName = vars.getStringParameter("inpsearchMProductModeloName");
    String strSearchMProductName = vars.getStringParameter("inpsearchMProductName");
    String strSearchMProductValue = vars.getStringParameter("inpsearchMProductValue");
    String strSearchMProductYear = vars.getStringParameter("inpsearchMProductYear");
    String strSearchMProductCilindraje = vars.getStringParameter("inpsearchMProductCilindraje");

    String strSearchMProductFamiliaId = vars.getStringParameter("inpsearchAteccoFamilia");
    String strSearchMProductCategoriaId = vars.getStringParameter("inpsearchAteccoCategoria");
    String strSearchMProductMarcaId = vars.getStringParameter("inpsearchProductMarca");
    String strSearchMProductModeloId = vars.getStringParameter("inpsearchProductModelo");

    if (strCommand.equals("DEFAULT") || strCommand.equals("LIMPIAR")
        || strCommand.equals("FILTRARLIMPIAR")) {
      xmlDocument.setParameter("searchProductMarcaName", "");
      xmlDocument.setParameter("searchProductModeloName", "");
      xmlDocument.setParameter("searchProductName", "");
      xmlDocument.setParameter("searchProductValue", "");
      xmlDocument.setParameter("searchProductYear", "");
      xmlDocument.setParameter("searchProductCilindraje", "");

      xmlDocument.setParameter("ateccofamiliaid", "");
      xmlDocument.setParameter("ateccocategoriaid", "");
      xmlDocument.setParameter("productmarcaid", "");
      xmlDocument.setParameter("productmodeloid", "");

      strSearchMProductMarcaName = "";
      strSearchMProductModeloName = "";
      strSearchMProductName = "";
      strSearchMProductValue = "";
      strSearchMProductYear = "";
      strSearchMProductCilindraje = "";

      strSearchMProductFamiliaId = "";
      strSearchMProductCategoriaId = "";
      strSearchMProductMarcaId = "";
      strSearchMProductModeloId = "";

      strMProductMarcaName = "";
      strMProductModeloName = "";
      strMProductName = "";
      strMProductValue = "";
      strMProductYear = "";
      strMProductCilindraje = "";
    } else {
      strMProductMarcaName = vars.getStringParameter("inpmProductMarcaName");
      strMProductModeloName = vars.getStringParameter("inpmProductModeloName");
      strMProductName = vars.getStringParameter("inpmProductName");
      strMProductValue = vars.getStringParameter("inpmProductValue");
      strMProductYear = vars.getStringParameter("inpmProductYear");
      strMProductCilindraje = vars.getStringParameter("inpmProductCilindraje");

      strMProductFamiliaId = vars.getStringParameter("inpsearchAteccoFamilia");
      strMProductCategoriaId = vars.getStringParameter("inpsearchAteccoCategoria");
      strMProductMarcaId = vars.getStringParameter("inpsearchProductMarca");
      strMProductModeloId = vars.getStringParameter("inpsearchProductModelo");

      if (!strMProductFamiliaId.equals("") && !strMProductFamiliaId.equals("%")) {
        xmlDocument.setParameter("ateccofamiliaid", strMProductFamiliaId);
        strMProductFamiliaId = "%" + strMProductFamiliaId + "%";
      } else {
        strMProductFamiliaId = "%";
      }

      if (!strMProductCategoriaId.equals("") && !strMProductCategoriaId.equals("%")) {
        xmlDocument.setParameter("ateccocategoriaid", strMProductCategoriaId);
        strMProductCategoriaId = "%" + strMProductCategoriaId + "%";
      } else {
        strMProductCategoriaId = "%";
      }

      if (!strMProductMarcaId.equals("") && !strMProductMarcaId.equals("%")) {
        xmlDocument.setParameter("productmarcaid", strMProductMarcaId);
        strMProductMarcaId = "%" + strMProductMarcaId + "%";
      } else {
        strMProductMarcaId = "%";
      }

      if (!strMProductModeloId.equals("") && !strMProductModeloId.equals("%")) {
        xmlDocument.setParameter("productmodeloid", strMProductModeloId);
        strMProductModeloId = strMProductModeloId;
      } else {
        strMProductModeloId = "%";
      }

      if (!strMProductMarcaName.equals("") && !strMProductMarcaName.equals("%")) {
        xmlDocument.setParameter("productMarcaName", strMProductMarcaName.toUpperCase());
        strMProductMarcaName = "%" + strMProductMarcaName.toUpperCase() + "%";
      } else {
        strMProductMarcaName = "%";
      }

      if (!strMProductModeloName.equals("") && !strMProductModeloName.equals("%")) {
        xmlDocument.setParameter("productModeloName", strMProductModeloName.toUpperCase());
        strMProductModeloName = "%" + strMProductModeloName.toUpperCase() + "%";
      } else {
        strMProductModeloName = "%";
      }

      if (!strMProductReferenceCode.equals("") && !strMProductReferenceCode.equals("%")) {
        xmlDocument.setParameter("productProductReferenceCode",
            strMProductReferenceCode.toUpperCase());
        strMProductReferenceCode = "%" + strMProductReferenceCode.toUpperCase() + "%";
      } else {
        strMProductReferenceCode = "%";
      }

      if (!strMProductName.equals("") && !strMProductName.equals("%")) {
        xmlDocument.setParameter("productName", strMProductName.toUpperCase());
        strMProductName = "%" + strMProductName.toUpperCase() + "%";
      } else {
        strMProductName = "%";
      }

      if (!strMProductValue.equals("") && !strMProductValue.equals("%")) {
        xmlDocument.setParameter("productValueName", strMProductValue.toUpperCase());
        strMProductValue = "%" + strMProductValue.toUpperCase() + "%";
      } else {
        strMProductValue = "%";
      }

      if (!strMProductYear.equals("") && !strMProductYear.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductYear);
      } else {
        strMProductYear = "0";
      }

      if (!strMProductCilindraje.equals("") && !strMProductCilindraje.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductCilindraje);
      } else {
        strMProductCilindraje = "0";
      }
    }

    if (strCommand.equals("FILTRAR")) {
      strMProductMarcaName = strSearchMProductMarcaName;
      strMProductModeloName = strSearchMProductModeloName;
      strMProductName = strSearchMProductName;
      strMProductValue = strSearchMProductValue;
      strMProductYear = strSearchMProductYear;
      strMProductCilindraje = strSearchMProductCilindraje;

      strMProductFamiliaId = strSearchMProductFamiliaId;
      strMProductCategoriaId = strSearchMProductCategoriaId;
      strMProductMarcaId = strSearchMProductMarcaId;
      strMProductModeloId = strSearchMProductModeloId;

      if (!strMProductFamiliaId.equals("") && !strMProductFamiliaId.equals("%")) {
        xmlDocument.setParameter("ateccofamiliaid", strMProductFamiliaId);
        strMProductFamiliaId = "%" + strMProductFamiliaId + "%";
      } else {
        strMProductFamiliaId = "%";
      }

      if (!strMProductCategoriaId.equals("") && !strMProductCategoriaId.equals("%")) {
        xmlDocument.setParameter("ateccocategoriaid", strMProductCategoriaId);
        strMProductCategoriaId = "%" + strMProductCategoriaId + "%";
      } else {
        strMProductCategoriaId = "%";
      }

      if (!strMProductMarcaId.equals("") && !strMProductMarcaId.equals("%")) {
        xmlDocument.setParameter("productmarcaid", strMProductMarcaId);
        strMProductMarcaId = "%" + strMProductMarcaId + "%";
      } else {
        strMProductMarcaId = "%";
      }

      if (!strMProductModeloId.equals("") && !strMProductModeloId.equals("%")) {
        xmlDocument.setParameter("productmodeloid", strMProductModeloId);
        strMProductModeloId = strMProductModeloId;
      } else {
        strMProductModeloId = "%";
      }

      if (!strMProductMarcaName.equals("") && !strMProductMarcaName.equals("%")) {
        xmlDocument.setParameter("productMarcaName", strMProductMarcaName.toUpperCase());
        strMProductMarcaName = "%" + strMProductMarcaName.toUpperCase() + "%";
      } else {
        strMProductMarcaName = "%";
      }

      if (!strMProductModeloName.equals("") && !strMProductModeloName.equals("%")) {
        xmlDocument.setParameter("productModeloName", strMProductModeloName.toUpperCase());
        strMProductModeloName = "%" + strMProductModeloName.toUpperCase() + "%";
      } else {
        strMProductModeloName = "%";
      }

      if (!strMProductName.equals("") && !strMProductName.equals("%")) {
        xmlDocument.setParameter("productName", strMProductName.toUpperCase());
        strMProductName = "%" + strMProductName.toUpperCase() + "%";
      } else {
        strMProductName = "%";
      }

      if (!strMProductValue.equals("") && !strMProductValue.equals("%")) {
        xmlDocument.setParameter("productValue", strMProductValue.toUpperCase());
        strMProductValue = "%" + strMProductValue.toUpperCase() + "%";
      } else {
        strMProductValue = "%";
      }

      if (!strMProductYear.equals("") && !strMProductYear.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductYear);
      } else {
        strMProductYear = "0";
      }

      if (!strMProductCilindraje.equals("") && !strMProductCilindraje.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductCilindraje);
      } else {
        strMProductCilindraje = "0";
      }
    }

    xmlDocument.setParameter("searchProductMarcaName", strSearchMProductMarcaName.toUpperCase());
    xmlDocument.setParameter("searchProductModeloName", strSearchMProductModeloName.toUpperCase());
    xmlDocument.setParameter("searchProductName", strSearchMProductName.toUpperCase());
    xmlDocument.setParameter("searchProductValue", strSearchMProductValue.toUpperCase());
    xmlDocument.setParameter("searchProductYear", strSearchMProductYear);
    xmlDocument.setParameter("searchProductCilindraje", strSearchMProductCilindraje);

    xmlDocument.setParameter("productYear", "");
    xmlDocument.setParameter("productCilindraje", "");
    xmlDocument.setParameter("productModeloName", "");
    xmlDocument.setParameter("productMarcaName", "");
    xmlDocument.setParameter("productSubCategoriaName", "");
    xmlDocument.setParameter("productCategoriaName", "");
    xmlDocument.setParameter("productValue", "");
    xmlDocument.setParameter("productUpc", "");
    xmlDocument.setParameter("productName", "");
    xmlDocument.setParameter("productFamiliaName", "");
    xmlDocument.setParameter("productGenericoName", "");

    if (strCommand.equals("BUSCAR")) {
      strMProductValue = vars.getStringParameter("inpmProductValue");
      strMProductUpc = vars.getStringParameter("inpmProductUpc");
      strMProductName = vars.getStringParameter("inpmProductName");
      strMProductFamiliaName = vars.getStringParameter("inpmProductFamiliaName");
      strMProductGenericoName = vars.getStringParameter("inpmProductGenericoName");

      strMProductCategoriaName = vars.getStringParameter("inpmProductCategoriaName");
      strMProductSubCategoriaName = vars.getStringParameter("inpmProductSubCategoriaName");
      strMProductReferenceCode = vars.getStringParameter("inpmProductReferenceCode");
      strMProductMarcaName = vars.getStringParameter("inpmProductMarcaName");
      strMProductModeloName = vars.getStringParameter("inpmProductModeloName");
      strMProductYear = vars.getStringParameter("inpmProductYear");
      strMProductCilindraje = vars.getStringParameter("inpmProductCilindraje");

      strMProductFamiliaId = vars.getStringParameter("inpsearchAteccoFamilia");
      strMProductCategoriaId = vars.getStringParameter("inpsearchAteccoCategoria");
      strMProductMarcaId = vars.getStringParameter("inpsearchProductMarca");
      strMProductModeloId = vars.getStringParameter("inpsearchProductModelo");

      if (!strMProductFamiliaId.equals("") && !strMProductFamiliaId.equals("%")) {
        xmlDocument.setParameter("ateccofamiliaid", strMProductFamiliaId);
        strMProductFamiliaId = "%" + strMProductFamiliaId + "%";
      } else {
        strMProductFamiliaId = "%";
      }

      if (!strMProductCategoriaId.equals("") && !strMProductCategoriaId.equals("%")) {
        xmlDocument.setParameter("ateccocategoriaid", strMProductCategoriaId);
        strMProductCategoriaId = "%" + strMProductCategoriaId + "%";
      } else {
        strMProductCategoriaId = "%";
      }

      if (!strMProductMarcaId.equals("") && !strMProductMarcaId.equals("%")) {
        xmlDocument.setParameter("productmarcaid", strMProductMarcaId);
        strMProductMarcaId = "%" + strMProductMarcaId + "%";
      } else {
        strMProductMarcaId = "%";
      }

      if (!strMProductModeloId.equals("") && !strMProductModeloId.equals("%")) {
        xmlDocument.setParameter("productmodeloid", strMProductModeloId);
        strMProductModeloId = strMProductModeloId;
      } else {
        strMProductModeloId = "%";
      }

      if (!strMProductYear.equals("") && !strMProductYear.equals("0")) {
        xmlDocument.setParameter("productYear", strMProductYear);
      } else {
        strMProductYear = "0";
      }

      if (!strMProductCilindraje.equals("") && !strMProductCilindraje.equals("0")) {
        xmlDocument.setParameter("productCilindraje", strMProductCilindraje);
      } else {
        strMProductCilindraje = "0";
      }

      if (!strMProductModeloName.equals("") && !strMProductModeloName.equals("%")) {
        xmlDocument.setParameter("productModeloName", strMProductModeloName.toUpperCase());
        strMProductModeloName = "%" + strMProductModeloName.toUpperCase() + "%";
      } else {
        strMProductModeloName = "%";
      }

      if (!strMProductMarcaName.equals("") && !strMProductMarcaName.equals("%")) {
        xmlDocument.setParameter("productMarcaName", strMProductMarcaName.toUpperCase());
        strMProductMarcaName = "%" + strMProductMarcaName.toUpperCase() + "%";
      } else {
        strMProductMarcaName = "%";
      }

      if (!strMProductSubCategoriaName.equals("") && !strMProductSubCategoriaName.equals("%")) {
        xmlDocument.setParameter("productSubCategoriaName",
            strMProductSubCategoriaName.toUpperCase());
        strMProductSubCategoriaName = "%" + strMProductSubCategoriaName.toUpperCase() + "%";
      } else {
        strMProductSubCategoriaName = "%";
      }

      if (!strMProductCategoriaName.equals("") && !strMProductCategoriaName.equals("%")) {
        xmlDocument.setParameter("productCategoriaName", strMProductCategoriaName.toUpperCase());
        strMProductCategoriaName = "%" + strMProductCategoriaName.toUpperCase() + "%";
      } else {
        strMProductCategoriaName = "%";
      }

      if (!strMProductValue.equals("") && !strMProductValue.equals("%")) {
        xmlDocument.setParameter("productValue", strMProductValue.toUpperCase());
        strMProductValue = "%" + strMProductValue.toUpperCase() + "%";
      } else {
        strMProductValue = "%";
      }

      if (!strMProductUpc.equals("") && !strMProductUpc.equals("%")) {
        xmlDocument.setParameter("productUpc", strMProductUpc.toUpperCase());
        strMProductUpc = "%" + strMProductUpc.toUpperCase() + "%";
      } else {
        strMProductUpc = "%";
      }

      if (!strMProductName.equals("") && !strMProductName.equals("%")) {
        xmlDocument.setParameter("productName", strMProductName.toUpperCase());
        strMProductName = "%" + strMProductName.toUpperCase() + "%";
      } else {
        strMProductName = "%";
      }

      if (!strMProductFamiliaName.equals("") && !strMProductFamiliaName.equals("%")) {
        xmlDocument.setParameter("productFamiliaName", strMProductFamiliaName.toUpperCase());
        strMProductFamiliaName = "%" + strMProductFamiliaName.toUpperCase() + "%";
      } else {
        strMProductFamiliaName = "%";
      }

      if (!strMProductGenericoName.equals("") && !strMProductGenericoName.equals("%")) {
        xmlDocument.setParameter("productGenericoName", strMProductGenericoName.toUpperCase());
        strMProductGenericoName = "%" + strMProductGenericoName.toUpperCase() + "%";
      } else {
        strMProductGenericoName = "%";
      }

      if (!strMProductReferenceCode.equals("") && !strMProductReferenceCode.equals("%")) {
        xmlDocument.setParameter("productReferenceCode", strMProductReferenceCode.toUpperCase());
        strMProductReferenceCode = "%" + strMProductReferenceCode.toUpperCase() + "%";
      } else {
        strMProductReferenceCode = "%";
      }
    }

    FacturaRapidaData[] data1 = null;

    if (strCommand.equals("OBTENER_FAMILIA") || strCommand.equals("OBTENER_CATEGORIA")
        || strCommand.equals("OBTENER_MARCA") || strCommand.equals("OBTENER_MODELO")) {
      data1 = FacturaRapidaData.methodSeleccionarProductosOrg(this, vars.getClient(),
          strMProductWarehouseName, strMProductName, strMProductValue, strMProductSubCategoriaName,
          "", "", "", "", "");
    } else {
      data1 = FacturaRapidaData.methodSeleccionarProductosOrg(this, vars.getClient(),
          strMProductWarehouseName, strMProductName, strMProductValue, strMProductSubCategoriaName,
          strMProductFamiliaId, strMProductCategoriaId, strMProductMarcaId, strMProductModeloId,
          strMProductReferenceCode);
    }

    FacturaRapidaData[] data2 = FacturaRapidaData.methodSeleccionarProductosTemporales(this,
        strFpvFacturaCotId);

    FacturaRapidaData[] data3 = FacturaRapidaData.methodTotalFacuraCot(this, strFpvFacturaCotId);

    if (data3.length > 0) {
      xmlDocument.setParameter("facturaCotTotal", data3[0].mproductpricetotal);
    } else {
      xmlDocument.setParameter("facturaCotTotal", "0");
    }

    FacturaRapidaData[] data4 = FacturaRapidaData.methodTotalEfecFacuraCot(this,
        strFpvFacturaCotId);

    if (data4.length > 0) {
      xmlDocument.setParameter("facturaCotTotalEfec", data4[0].mproductpricetotal);
    } else {
      xmlDocument.setParameter("facturaCotTotalEfec", "0");
    }

    xmlDocument.setParameter("productWarehouseName", strMProductWarehouseName);
    xmlDocument.setParameter("fpvFacturaCotId", strFpvFacturaCotId);

    xmlDocument.setParameter("calendar", vars.getLanguage().substring(0, 2));
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("paramLanguage", "defaultLang=\"" + vars.getLanguage() + "\";");

    xmlDocument.setData("structure1", data1);
    xmlDocument.setData("structure2", data2);

    out.println(xmlDocument.print());
    out.close();
  }
}