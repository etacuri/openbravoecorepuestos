//Sqlc generated V1.O00-1
package com.atrums.declaraciontributaria.ecuador.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class CrearAnexoData implements FieldProvider {
static Logger log4j = Logger.getLogger(CrearAnexoData.class);
  private String InitRecordNumber="0";
  public String nombreXml;
  public String nombreArchivo;
  public String tipoInformacion;
  public String procedimiento;
  public String startdate;
  public String enddate;
  public String adOrgId;
  public String semestral;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("nombre_xml") || fieldName.equals("nombreXml"))
      return nombreXml;
    else if (fieldName.equalsIgnoreCase("nombre_archivo") || fieldName.equals("nombreArchivo"))
      return nombreArchivo;
    else if (fieldName.equalsIgnoreCase("tipo_informacion") || fieldName.equals("tipoInformacion"))
      return tipoInformacion;
    else if (fieldName.equalsIgnoreCase("procedimiento"))
      return procedimiento;
    else if (fieldName.equalsIgnoreCase("startdate"))
      return startdate;
    else if (fieldName.equalsIgnoreCase("enddate"))
      return enddate;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("semestral"))
      return semestral;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static CrearAnexoData[] select(ConnectionProvider connectionProvider, String adProcesaId)    throws ServletException {
    return select(connectionProvider, adProcesaId, 0, 0);
  }

  public static CrearAnexoData[] select(ConnectionProvider connectionProvider, String adProcesaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                SELECT A.NOMBRE_XML, " +
      "               A.NOMBRE_ARCHIVO, " +
      "               AC.TIPO_INFORMACION, " +
      "               AC.PROCEDIMIENTO," +
      "               (CASE WHEN PA.SEMESTRAL='Y' AND PS.PERIODNO=6 THEN" +
      "                    (SELECT X.STARTDATE FROM C_PERIOD X WHERE X.C_YEAR_ID = PS.C_YEAR_ID AND X.PERIODNO=1)" +
      "                    WHEN PA.SEMESTRAL='Y' AND PS.PERIODNO=12 THEN" +
      "                    (SELECT X.STARTDATE FROM C_PERIOD X WHERE X.C_YEAR_ID = PS.C_YEAR_ID AND X.PERIODNO=7)" +
      "               ELSE" +
      "                    PS.STARTDATE" +
      "               END) AS STARTDATE, " +
      "               PS.ENDDATE," +
      "               PA.AD_ORG_ID," +
      "               PA.SEMESTRAL" +
      "          FROM ATS_PROCESA_ANEXO PA,C_PERIOD PS,ATS_ANEXO A,ATS_ANEXO_CONFIG AC,C_YEAR Y" +
      "         WHERE PA.ATS_PROCESA_ANEXO_ID = ?" +
      "           AND PA.ATS_ANEXO_ID = A.ATS_ANEXO_ID" +
      "           AND AC.ATS_ANEXO_ID = A.ATS_ANEXO_ID" +
      "           AND A.ISACTIVE = 'Y'" +
      "           AND PA.C_PERIOD_ID = PS.C_PERIOD_ID" +
      "           AND PS.C_YEAR_ID = Y.C_YEAR_ID" +
      "         ORDER BY AC.LINE ASC";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adProcesaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CrearAnexoData objectCrearAnexoData = new CrearAnexoData();
        objectCrearAnexoData.nombreXml = UtilSql.getValue(result, "nombre_xml");
        objectCrearAnexoData.nombreArchivo = UtilSql.getValue(result, "nombre_archivo");
        objectCrearAnexoData.tipoInformacion = UtilSql.getValue(result, "tipo_informacion");
        objectCrearAnexoData.procedimiento = UtilSql.getValue(result, "procedimiento");
        objectCrearAnexoData.startdate = UtilSql.getDateValue(result, "startdate", "dd-MM-yyyy");
        objectCrearAnexoData.enddate = UtilSql.getDateValue(result, "enddate", "dd-MM-yyyy");
        objectCrearAnexoData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCrearAnexoData.semestral = UtilSql.getValue(result, "semestral");
        objectCrearAnexoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCrearAnexoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CrearAnexoData objectCrearAnexoData[] = new CrearAnexoData[vector.size()];
    vector.copyInto(objectCrearAnexoData);
    return(objectCrearAnexoData);
  }
}
