//Sqlc generated V1.O00-1
package com.atrums.declaraciontributaria.ecuador.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class BuscaFacturaData implements FieldProvider {
static Logger log4j = Logger.getLogger(BuscaFacturaData.class);
  private String InitRecordNumber="0";
  public String cBpartnerId;
  public String docbasetype;
  public String cInvoiceId;
  public String total;
  public String tpidcliente;
  public String idcliente;
  public String tipocomprobante;
  public String numerocomprobantes;
  public String basenograiva;
  public String baseimponible;
  public String baseimpgrav;
  public String montoiva;
  public String valorretiva;
  public String valorretrenta;
  public String fechainicio;
  public String fechafin;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("docbasetype"))
      return docbasetype;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("tpidcliente"))
      return tpidcliente;
    else if (fieldName.equalsIgnoreCase("idcliente"))
      return idcliente;
    else if (fieldName.equalsIgnoreCase("tipocomprobante"))
      return tipocomprobante;
    else if (fieldName.equalsIgnoreCase("numerocomprobantes"))
      return numerocomprobantes;
    else if (fieldName.equalsIgnoreCase("basenograiva"))
      return basenograiva;
    else if (fieldName.equalsIgnoreCase("baseimponible"))
      return baseimponible;
    else if (fieldName.equalsIgnoreCase("baseimpgrav"))
      return baseimpgrav;
    else if (fieldName.equalsIgnoreCase("montoiva"))
      return montoiva;
    else if (fieldName.equalsIgnoreCase("valorretiva"))
      return valorretiva;
    else if (fieldName.equalsIgnoreCase("valorretrenta"))
      return valorretrenta;
    else if (fieldName.equalsIgnoreCase("fechainicio"))
      return fechainicio;
    else if (fieldName.equalsIgnoreCase("fechafin"))
      return fechafin;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static BuscaFacturaData[] select(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssotrx)    throws ServletException {
    return select(connectionProvider, fechaInicio, fechaFin, adClient, adIssotrx, 0, 0);
  }

  public static BuscaFacturaData[] select(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssotrx, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "          SELECT  " +
      "          I.C_BPARTNER_ID," +
      "          D.DOCBASETYPE," +
      "          I.C_INVOICE_ID," +
      "          '' as TOTAL," +
      "          '' as tpIdCliente," +
      "          '' as idCliente," +
      "          '' as tipoComprobante," +
      "          '' as numeroComprobantes," +
      "          '' as baseNoGraIva," +
      "          '' as baseImponible," +
      "          '' as baseImpGrav," +
      "          '' as montoIva," +
      "          '' as valorRetIva," +
      "          '' as valorRetRenta," +
      "          '' as FechaInicio," +
      "          '' as FechaFin" +
      "          FROM C_INVOICE I, C_DOCTYPE D" +
      "         WHERE DOCSTATUS ='CO'" +
      "           AND DOCACTION = 'RE'" +
      "           AND PROCESSED = 'Y'" +
      "           AND DATEINVOICED >= ?" +
      "           AND DATEINVOICED <= ?" +
      "           AND I.AD_CLIENT_ID = ?" +
      "           AND I.ISSOTRX = ?" +
      "           AND I.C_DOCTYPETARGET_ID = D.C_DOCTYPE_ID" +
      "           AND D.EM_ATECFE_FAC_ELEC = 'N'" +
      "           and D.EM_CO_TP_COMP_AUTORIZADOR_SRI not in ('0')";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClient);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adIssotrx);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        BuscaFacturaData objectBuscaFacturaData = new BuscaFacturaData();
        objectBuscaFacturaData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectBuscaFacturaData.docbasetype = UtilSql.getValue(result, "docbasetype");
        objectBuscaFacturaData.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectBuscaFacturaData.total = UtilSql.getValue(result, "total");
        objectBuscaFacturaData.tpidcliente = UtilSql.getValue(result, "tpidcliente");
        objectBuscaFacturaData.idcliente = UtilSql.getValue(result, "idcliente");
        objectBuscaFacturaData.tipocomprobante = UtilSql.getValue(result, "tipocomprobante");
        objectBuscaFacturaData.numerocomprobantes = UtilSql.getValue(result, "numerocomprobantes");
        objectBuscaFacturaData.basenograiva = UtilSql.getValue(result, "basenograiva");
        objectBuscaFacturaData.baseimponible = UtilSql.getValue(result, "baseimponible");
        objectBuscaFacturaData.baseimpgrav = UtilSql.getValue(result, "baseimpgrav");
        objectBuscaFacturaData.montoiva = UtilSql.getValue(result, "montoiva");
        objectBuscaFacturaData.valorretiva = UtilSql.getValue(result, "valorretiva");
        objectBuscaFacturaData.valorretrenta = UtilSql.getValue(result, "valorretrenta");
        objectBuscaFacturaData.fechainicio = UtilSql.getValue(result, "fechainicio");
        objectBuscaFacturaData.fechafin = UtilSql.getValue(result, "fechafin");
        objectBuscaFacturaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectBuscaFacturaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    BuscaFacturaData objectBuscaFacturaData[] = new BuscaFacturaData[vector.size()];
    vector.copyInto(objectBuscaFacturaData);
    return(objectBuscaFacturaData);
  }

  public static BuscaFacturaData[] selectVentas(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssotrx)    throws ServletException {
    return selectVentas(connectionProvider, fechaInicio, fechaFin, adClient, adIssotrx, 0, 0);
  }

  public static BuscaFacturaData[] selectVentas(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssotrx, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select D.tpidcliente, " +
      "               D.idcliente, " +
      "               D.tipocomprobante, " +
      "        sum(to_number(D.numerocomprobantes)) as numerocomprobantes, " +
      "        sum(to_number(D.baseNoGraIva)) as baseNoGraIva, " +
      "        sum(to_number(D.baseImponible)) as baseImponible, " +
      "        sum(to_number(D.baseImpGrav)) as baseImpGrav, " +
      "        sum(to_number(D.montoIva)) as montoIva, " +
      "        D.valorRetIva," +
      "        D.valorRetRenta," +
      "        D.FechaInicio," +
      "        D.FechaFin" +
      "    from" +
      "    (      " +
      "    select C.tpidcliente, " +
      "        C.idcliente, " +
      "        (case when C.tipocomprobante = '1' then '18' else C.tipocomprobante end) as tipocomprobante, " +
      "        C.numerocomprobantes, " +
      "        C.baseNoGraIva, " +
      "        C.baseImponible, " +
      "        C.baseImpGrav, " +
      "        C.montoIva, " +
      "        C.valorRetIva, " +
      "        C.valorRetRenta," +
      "        C.FechaInicio," +
      "        C.FechaFin      " +
      "    from" +
      "    (     " +
      "        select B.tpidcliente, " +
      "        B.idcliente, " +
      "        B.tipocomprobante, " +
      "        ats_num_comprobantes(B.c_bpartner_id, B.FechaInicio, B.FechaFin, b.tipoComprobante) AS numerocomprobantes, " +
      "        ATS_calculo_base(B.c_bpartner_id, B.FechaInicio, B.FechaFin, '1', b.tipoComprobante) AS baseNoGraIva, " +
      "        ATS_calculo_base(B.c_bpartner_id, B.FechaInicio, B.FechaFin, '2', b.tipoComprobante) as baseImponible, " +
      "        ATS_calculo_base(B.c_bpartner_id, B.FechaInicio, B.FechaFin, '3', b.tipoComprobante) as baseImpGrav, " +
      "        ats_calculo_iva(B.c_bpartner_id, B.FechaInicio, B.FechaFin, b.tipoComprobante) as montoIva, " +
      "        (case when B.tipocomprobante = '04' then 0 else ats_valor_retencion_venta(B.c_bpartner_id, B.FechaInicio, B.FechaFin, 'IVA') end) as valorRetIva, " +
      "        (case when B.tipocomprobante = '04' then 0 else ats_valor_retencion_venta(B.c_bpartner_id, B.FechaInicio, B.FechaFin, 'FUENTE') end) as valorRetRenta," +
      "        DATE(B.FechaInicio) as FechaInicio,         " +
      "        DATE(B.FechaFin) as FechaFin" +
      "    from ( " +
      "    select  a.c_bpartner_id, " +
      "            (case when (select em_co_tipo_identificacion from c_bpartner where c_bpartner_id = A.C_BPARTNER_ID) != '07' " +
      "                    then ltrim(to_char(to_number((select em_co_tipo_identificacion from c_bpartner where c_bpartner_id = A.C_BPARTNER_ID))+3,'00')) " +
      "                    else (select em_co_tipo_identificacion from c_bpartner where c_bpartner_id = A.C_BPARTNER_ID) end) as tpIdCliente, " +
      "            (case when (select em_co_tipo_identificacion from c_bpartner where c_bpartner_id = A.C_BPARTNER_ID) = '07' " +
      "                    then '9999999999999' else (select taxid from c_bpartner where c_bpartner_id = A.C_BPARTNER_ID) end) as idCliente, " +
      "            A.DOCBASETYPE as tipoComprobante, " +
      "            A.FechaInicio, " +
      "            A.FechaFin   " +
      "    from ( " +
      "         (SELECT I.C_BPARTNER_ID,  " +
      "                (case when (select docbasetype from c_doctype where c_doctype_id = i.c_doctypetarget_id and isreturn = 'N' and em_atecfe_fac_elec = 'N')='ARI' then '18' " +
      "                      when (select docbasetype from c_doctype where c_doctype_id = i.c_doctypetarget_id and isreturn = 'N' and em_atecfe_fac_elec = 'Y')='ARI' then '1'" +
      "                      when (select docbasetype from c_doctype where c_doctype_id = i.c_doctypetarget_id and isreturn = 'Y') = 'ARI' then '04' " +
      "                      when (select docbasetype from c_doctype where c_doctype_id = i.c_doctypetarget_id) = 'ARC' then '04' " +
      "                      end) as DOCBASETYPE, " +
      "                '1' as Fact, " +
      "                ? as FechaInicio, " +
      "                ? as FechaFin " +
      "         FROM C_INVOICE I, C_DOCTYPE DT  " +
      "         WHERE I.docstatus ='CO' " +
      "           AND I.C_DOCTYPETARGET_ID = DT.C_DOCTYPE_ID" +
      "           AND DT.EM_ATECFE_FAC_ELEC =  'N'" +
      "           AND date(I.DATEINVOICED) >=  DATE(?) " +
      "           AND date(I.DATEINVOICED) <=  DATE(?) " +
      "           AND I.AD_CLIENT_ID = ?  " +
      "           AND I.ISSOTRX = ? " +
      "           and DT.em_co_tp_comp_autorizador_sri  <>  0  " +
      "         order by 1,2) " +
      "         UNION " +
      "         (SELECT (select c_bpartner_id from c_invoice where rv.c_invoice_id = c_invoice_id) as C_BPARTNER_ID,   " +
      "                '18' as DOCBASETYPE, " +
      "                '2' as Fact, " +
      "                ? as FechaInicio, " +
      "                ? as FechaFin " +
      "         FROM co_retencion_venta rv  " +
      "         WHERE date(rv.fecha_emision) >= date(?) " +
      "           AND date(rv.fecha_emision) <= date(?) " +
      "           AND rv.AD_CLIENT_ID = ? " +
      "         order by 1,2) " +
      "        ) A " +
      "    group by 1,2,3,4,5,6 " +
      "    order by 3,2 " +
      "    ) B " +
      "    order by 3,2 " +
      "    ) C" +
      "    order by 3,2 " +
      "    )D   " +
      "group by 1,2,3,9,10,11,12 " +
      "order by 3,2 ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClient);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adIssotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClient);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        BuscaFacturaData objectBuscaFacturaData = new BuscaFacturaData();
        objectBuscaFacturaData.tpidcliente = UtilSql.getValue(result, "tpidcliente");
        objectBuscaFacturaData.idcliente = UtilSql.getValue(result, "idcliente");
        objectBuscaFacturaData.tipocomprobante = UtilSql.getValue(result, "tipocomprobante");
        objectBuscaFacturaData.numerocomprobantes = UtilSql.getValue(result, "numerocomprobantes");
        objectBuscaFacturaData.basenograiva = UtilSql.getValue(result, "basenograiva");
        objectBuscaFacturaData.baseimponible = UtilSql.getValue(result, "baseimponible");
        objectBuscaFacturaData.baseimpgrav = UtilSql.getValue(result, "baseimpgrav");
        objectBuscaFacturaData.montoiva = UtilSql.getValue(result, "montoiva");
        objectBuscaFacturaData.valorretiva = UtilSql.getValue(result, "valorretiva");
        objectBuscaFacturaData.valorretrenta = UtilSql.getValue(result, "valorretrenta");
        objectBuscaFacturaData.fechainicio = UtilSql.getDateValue(result, "fechainicio", "dd-MM-yyyy");
        objectBuscaFacturaData.fechafin = UtilSql.getDateValue(result, "fechafin", "dd-MM-yyyy");
        objectBuscaFacturaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectBuscaFacturaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    BuscaFacturaData objectBuscaFacturaData[] = new BuscaFacturaData[vector.size()];
    vector.copyInto(objectBuscaFacturaData);
    return(objectBuscaFacturaData);
  }

  public static BuscaFacturaData[] selectInvoice(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssotrx)    throws ServletException {
    return selectInvoice(connectionProvider, fechaInicio, fechaFin, adClient, adIssotrx, 0, 0);
  }

  public static BuscaFacturaData[] selectInvoice(ConnectionProvider connectionProvider, String fechaInicio, String fechaFin, String adClient, String adIssotrx, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "          SELECT C_INVOICE_ID" +
      "          FROM C_INVOICE C, C_DOCTYPE DT" +
      "         WHERE C.DOCSTATUS ='CO'" +
      "           AND C.DOCACTION = 'RE'" +
      "           AND C.PROCESSED = 'Y'" +
      "           AND C.C_DOCTYPE_ID = DT.C_DOCTYPE_ID" +
      "           AND TO_DATE(C.DATEINVOICED+0) >= TO_DATE(?)" +
      "           AND TO_DATE(C.DATEINVOICED+0) <= TO_DATE(?)" +
      "           AND C.AD_CLIENT_ID = ?" +
      "           AND C.ISSOTRX = ?" +
      "           and DT.em_co_tp_comp_autorizador_sri not in ('0')";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClient);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adIssotrx);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        BuscaFacturaData objectBuscaFacturaData = new BuscaFacturaData();
        objectBuscaFacturaData.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectBuscaFacturaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectBuscaFacturaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    BuscaFacturaData objectBuscaFacturaData[] = new BuscaFacturaData[vector.size()];
    vector.copyInto(objectBuscaFacturaData);
    return(objectBuscaFacturaData);
  }
}
