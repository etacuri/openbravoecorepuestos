//Sqlc generated V1.O00-1
package com.atrums.declaraciontributaria.ecuador.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ATSReembolsoVentasData implements FieldProvider {
static Logger log4j = Logger.getLogger(ATSReembolsoVentasData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;
  public String dato9;
  public String dato10;
  public String dato11;
  public String dato12;
  public String dato13;
  public String dato14;
  public String dato15;
  public String dato16;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
    else if (fieldName.equalsIgnoreCase("dato9"))
      return dato9;
    else if (fieldName.equalsIgnoreCase("dato10"))
      return dato10;
    else if (fieldName.equalsIgnoreCase("dato11"))
      return dato11;
    else if (fieldName.equalsIgnoreCase("dato12"))
      return dato12;
    else if (fieldName.equalsIgnoreCase("dato13"))
      return dato13;
    else if (fieldName.equalsIgnoreCase("dato14"))
      return dato14;
    else if (fieldName.equalsIgnoreCase("dato15"))
      return dato15;
    else if (fieldName.equalsIgnoreCase("dato16"))
      return dato16;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ATSReembolsoVentasData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String atsReembolsoId)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, atsReembolsoId, 0, 0);
  }

  public static ATSReembolsoVentasData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String atsReembolsoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select d.dummy as dato1, " +
      "			 d.dummy as dato2, " +
      "			 d.dummy as dato3, " +
      "			 d.dummy as dato4," +
      "			 d.dummy as dato5," +
      "			 d.dummy as dato6," +
      "			 d.dummy as dato7," +
      "			 d.dummy as dato8," +
      "			 d.dummy as dato9," +
      "			 d.dummy as dato10," +
      "			 d.dummy as dato11," +
      "			 d.dummy as dato12," +
      "			 d.dummy as dato13," +
      "			 d.dummy as dato14," +
      "			 d.dummy as dato15," +
      "			 d.dummy as dato16" +
      "	  from dual d " +
      "	  where d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atsReembolsoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATSReembolsoVentasData objectATSReembolsoVentasData = new ATSReembolsoVentasData();
        objectATSReembolsoVentasData.dato1 = UtilSql.getValue(result, "dato1");
        objectATSReembolsoVentasData.dato2 = UtilSql.getValue(result, "dato2");
        objectATSReembolsoVentasData.dato3 = UtilSql.getValue(result, "dato3");
        objectATSReembolsoVentasData.dato4 = UtilSql.getValue(result, "dato4");
        objectATSReembolsoVentasData.dato5 = UtilSql.getValue(result, "dato5");
        objectATSReembolsoVentasData.dato6 = UtilSql.getValue(result, "dato6");
        objectATSReembolsoVentasData.dato7 = UtilSql.getValue(result, "dato7");
        objectATSReembolsoVentasData.dato8 = UtilSql.getValue(result, "dato8");
        objectATSReembolsoVentasData.dato9 = UtilSql.getValue(result, "dato9");
        objectATSReembolsoVentasData.dato10 = UtilSql.getValue(result, "dato10");
        objectATSReembolsoVentasData.dato11 = UtilSql.getValue(result, "dato11");
        objectATSReembolsoVentasData.dato12 = UtilSql.getValue(result, "dato12");
        objectATSReembolsoVentasData.dato13 = UtilSql.getValue(result, "dato13");
        objectATSReembolsoVentasData.dato14 = UtilSql.getValue(result, "dato14");
        objectATSReembolsoVentasData.dato15 = UtilSql.getValue(result, "dato15");
        objectATSReembolsoVentasData.dato16 = UtilSql.getValue(result, "dato16");
        objectATSReembolsoVentasData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATSReembolsoVentasData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATSReembolsoVentasData objectATSReembolsoVentasData[] = new ATSReembolsoVentasData[vector.size()];
    vector.copyInto(objectATSReembolsoVentasData);
    return(objectATSReembolsoVentasData);
  }

  public static ATSReembolsoVentasData[] selectReembolso(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return selectReembolso(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATSReembolsoVentasData[] selectReembolso(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select a.c_doctype_id as dato1, " +
      "		       c.em_co_tipo_identificacion as dato2," +
      "		       c.taxid as dato3, " +
      "		       a.em_co_nro_estab as dato4, " +
      "		       a.em_co_punto_emision as dato5," +
      "		       lpad(coalesce(a.documentno,''),9,'0')  as dato6, " +
      "		       a.dateinvoiced as dato7, " +
      "		       a.em_co_nro_aut_sri as dato8," +
      "		       sum((case when d.rate = 0 and lower(d.name) like lower('%IVA%') then b.taxbaseamt else 0 end)) as dato9, " +
      "		       sum((case when d.rate = 12 and lower(d.name) like lower('%IVA%') then b.taxbaseamt else 0 end)) as dato10, " +
      "		       sum((case when d.rate = 0 and lower(d.name) not like lower('%IVA%') then b.taxbaseamt else 0 end)) as dato11, " +
      "		       sum((case when d.rate = 15 and lower(d.name) like lower('%ICE%') then b.taxamt else 0 end)) as dato12," +
      "		       sum((case when d.rate = 12 and lower(d.name) like lower('%IVA%') then b.taxamt else 0 end)) as dato13," +
      "		       c.c_bpartner_id as dato14," +
      "		       a.grandtotal as dato15," +
      "		       a.totallines as dato16" +
      "		  from c_invoice a, c_invoicetax b, c_bpartner c, c_tax d" +
      "		 where a.c_invoice_id = b.c_invoice_id" +
      "		   and a.c_bpartner_id = c.c_bpartner_id" +
      "		   and b.c_tax_id = d.c_tax_id" +
      "		   and a.c_invoice_id = ?" +
      "		 group by 1,2,3,4,5,6,7,8,14,15,16";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATSReembolsoVentasData objectATSReembolsoVentasData = new ATSReembolsoVentasData();
        objectATSReembolsoVentasData.dato1 = UtilSql.getValue(result, "dato1");
        objectATSReembolsoVentasData.dato2 = UtilSql.getValue(result, "dato2");
        objectATSReembolsoVentasData.dato3 = UtilSql.getValue(result, "dato3");
        objectATSReembolsoVentasData.dato4 = UtilSql.getValue(result, "dato4");
        objectATSReembolsoVentasData.dato5 = UtilSql.getValue(result, "dato5");
        objectATSReembolsoVentasData.dato6 = UtilSql.getValue(result, "dato6");
        objectATSReembolsoVentasData.dato7 = UtilSql.getDateValue(result, "dato7", "dd-MM-yyyy");
        objectATSReembolsoVentasData.dato8 = UtilSql.getValue(result, "dato8");
        objectATSReembolsoVentasData.dato9 = UtilSql.getValue(result, "dato9");
        objectATSReembolsoVentasData.dato10 = UtilSql.getValue(result, "dato10");
        objectATSReembolsoVentasData.dato11 = UtilSql.getValue(result, "dato11");
        objectATSReembolsoVentasData.dato12 = UtilSql.getValue(result, "dato12");
        objectATSReembolsoVentasData.dato13 = UtilSql.getValue(result, "dato13");
        objectATSReembolsoVentasData.dato14 = UtilSql.getValue(result, "dato14");
        objectATSReembolsoVentasData.dato15 = UtilSql.getValue(result, "dato15");
        objectATSReembolsoVentasData.dato16 = UtilSql.getValue(result, "dato16");
        objectATSReembolsoVentasData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATSReembolsoVentasData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATSReembolsoVentasData objectATSReembolsoVentasData[] = new ATSReembolsoVentasData[vector.size()];
    vector.copyInto(objectATSReembolsoVentasData);
    return(objectATSReembolsoVentasData);
  }
}
