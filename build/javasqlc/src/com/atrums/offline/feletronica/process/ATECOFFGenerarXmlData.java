//Sqlc generated V1.O00-1
package com.atrums.offline.feletronica.process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ATECOFFGenerarXmlData implements FieldProvider {
static Logger log4j = Logger.getLogger(ATECOFFGenerarXmlData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;
  public String dato9;
  public String dato10;
  public String dato11;
  public String dato12;
  public String dato13;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
    else if (fieldName.equalsIgnoreCase("dato9"))
      return dato9;
    else if (fieldName.equalsIgnoreCase("dato10"))
      return dato10;
    else if (fieldName.equalsIgnoreCase("dato11"))
      return dato11;
    else if (fieldName.equalsIgnoreCase("dato12"))
      return dato12;
    else if (fieldName.equalsIgnoreCase("dato13"))
      return dato13;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ATECOFFGenerarXmlData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionardummy(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select d.dummy as dato1, " +
      "			 d.dummy as dato2, " +
      "			 d.dummy as dato3, " +
      "			 d.dummy as dato4," +
      "			 d.dummy as dato5," +
      "			 d.dummy as dato6," +
      "			 d.dummy as dato7," +
      "			 d.dummy as dato8," +
      "			 d.dummy as dato9," +
      "			 d.dummy as dato10," +
      "			 d.dummy as dato11," +
      "			 d.dummy as dato12," +
      "			 d.dummy as dato13" +
      "	  from dual d " +
      "	  where d.dummy = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.dato10 = UtilSql.getValue(result, "dato10");
        objectATECOFFGenerarXmlData.dato11 = UtilSql.getValue(result, "dato11");
        objectATECOFFGenerarXmlData.dato12 = UtilSql.getValue(result, "dato12");
        objectATECOFFGenerarXmlData.dato13 = UtilSql.getValue(result, "dato13");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodVerificarGuia(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodVerificarGuia(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodVerificarGuia(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select mi.m_inout_id as dato1 " +
      "      from c_invoice ci " +
      "      inner join c_invoiceline cil on (ci.c_invoice_id = cil.c_invoice_id)" +
      "      inner join c_orderline col on (cil.c_orderline_id = col.c_orderline_id)" +
      "      inner join c_order co on (col.c_order_id = co.c_order_id) " +
      "      inner join m_inout mi on (co.c_order_id = mi.c_order_id) " +
      "      where ci.c_invoice_id = ?" +
      "      limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDetallesGuia(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarDetallesGuia(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDetallesGuia(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select " +
      "      coalesce(p.value,'') as dato1, " +
      "      coalesce(p.description,coalesce(p.name,'')) as dato3, " +
      "      round(coalesce(mil.movementqty,0),2) as dato4 " +
      "      from m_inoutline mil " +
      "      inner join m_product p on (mil.m_product_id = p.m_product_id) " +
      "      where mil.m_inout_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDatoSusteno(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarDatoSusteno(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDatoSusteno(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select  ci.em_co_nro_estab || '-' || ci.em_co_nro_estab || '-' || (case when length(ci.documentno) = 9 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 8 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 7 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 6 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 5 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 4 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 3 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 2 then ci.documentno else '0' || " +
      "      (case when length(ci.documentno) = 1 then ci.documentno else '000000001' end) end) end) end) end) end) end) end) end) as dato1, to_char(ci.dateinvoiced, 'DD/MM/YYYY') as dato2 " +
      "      from  c_invoice ci " +
      "      inner join m_inout mi on (ci.c_order_id = mi.c_order_id) " +
      "      where ci.issotrx = 'Y' " +
      "      and mi.m_inout_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSelectTransport(ConnectionProvider connectionProvider)    throws ServletException {
    return methodSelectTransport(connectionProvider, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSelectTransport(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select ms.description as dato1  " +
      "      from m_shipper ms " +
      "      where upper(ms.name) like '%TRANSPORTISTA%' ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirecDestinatario(ConnectionProvider connectionProvider, String adOrgId)    throws ServletException {
    return methodSeleccionarDirecDestinatario(connectionProvider, adOrgId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirecDestinatario(ConnectionProvider connectionProvider, String adOrgId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select (coalesce(l.address1,'') || CASE WHEN l.address2 is null THEN '' ELSE ' = ' END || coalesce(l.address2,'') || CASE WHEN l.postal is null THEN '' ELSE ' - ' END || coalesce(l.postal,'') || CASE WHEN l.city is null THEN '' ELSE ', ' END || coalesce(l.city,'') || CASE WHEN c.name is null THEN '' ELSE ' - ' END || coalesce(c.name,'')) as dato1" +
      "      from c_location l inner join " +
      "        c_country c on (l.c_country_id = c.c_country_id) inner join " +
      "        c_bpartner_location cbl on (cbl.c_location_id = l.c_location_id)" +
      "      where cbl.isshipto = 'Y' and cbl.isactive = 'Y' and cbl.c_bpartner_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDocumentoPendientes(ConnectionProvider connectionProvider)    throws ServletException {
    return methodSeleccionarDocumentoPendientes(connectionProvider, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDocumentoPendientes(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select a.tipodocumento as dato1, " +
      "        a.id as dato2, " +
      "        a.fecha as dato3, " +
      "        a.em_atecoff_docstatus as dato4 " +
      "        from (" +
      "        select 'FV' as tipodocumento, ci.c_invoice_id as id, ci.dateinvoiced as fecha, ci.em_atecoff_docstatus, ci.updated " +
      "        from c_invoice ci " +
      "        where ci.em_atecoff_docstatus = 'PD' " +
      "        and ci.issotrx = 'Y'" +
      "        union all" +
      "        select 'LC' as tipodocumento, ci.c_invoice_id as id, ci.dateinvoiced as fecha, ci.em_atecoff_docstatus, ci.updated " +
      "        from c_invoice ci " +
      "        where ci.em_atecoff_docstatus = 'PD' " +
      "        and ci.issotrx = 'N'        " +
      "        union all" +
      "        select 'RT' as tipodocumento, crc.co_retencion_compra_id as id, crc.fecha_emision as fecha, crc.em_atecoff_docstatus, crc.updated " +
      "        from co_retencion_compra crc" +
      "        where crc.em_atecoff_docstatus = 'PD'" +
      "        union all " +
      "        select 'GD' as tipodocumento, mi.m_inout_id as id, mi.movementdate as fecha, mi.em_atecoff_docstatus, mi.updated " +
      "        from m_inout mi " +
      "        where mi.em_atecoff_docstatus = 'PD') as a" +
      "        order by a.updated asc ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getDateValue(result, "dato3", "dd-MM-yyyy");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarInvo(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarInvo(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarInvo(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select encode(em_atecfe_documento_xml,'base64') as dato1 " +
      "	  from c_invoice " +
      "	  where c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSelDirMatriz(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return methodSelDirMatriz(connectionProvider, adClientId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSelDirMatriz(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select coalesce(c.address1,coalesce(c.address2,'')) as dato1, " +
      "		       coalesce((case when (select split_part(cli.value,'*',2)" +
      "		                                      from ad_client cli" +
      "		                                     where cli.ad_client_id = og.ad_client_id) = '1' then " +
      "		                              'CONTRIBUYENTE R\u00c9GIMEN MICROEMPRESAS'" +
      "		                          else " +
      "		                              '' " +
      "		                          end),'') as dato2, " +
      "		       coalesce((case when (select split_part(cli.value,'*',3)" +
      "		                                      from ad_client cli" +
      "		                                     where cli.ad_client_id = og.ad_client_id) = '2' then " +
      "		                              '00000001' " +
      "		                          else " +
      "		                              '' " +
      "		                          end),'') as dato3" +
      "		  from ad_orginfo ad, ad_org og, c_location c" +
      "		 where ad.ad_org_id = og.ad_org_id" +
      "		   and og.issummary = 'Y'" +
      "		   and og.ad_orgtype_id = '1'" +
      "		   and c.c_location_id = ad.c_location_id" +
      "		   and og.ad_client_id = ?" +
      "		 limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarImpues(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarImpues(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarImpues(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select coalesce(tc.em_atecfe_codtax,'') as dato1, " +
      "			 coalesce(t.em_atecfe_tartax,'') as dato2, " +
      "			 round(coalesce(it.taxbaseamt,0),2) as dato3, " +
      "			 round(coalesce(it.taxamt,0),2) as dato4, " +
      "			 round(coalesce(t.rate,0),2) as dato5" +
      "	  from c_invoice i " +
      "		   inner join c_invoicetax it on (i.c_invoice_id = it.c_invoice_id) " +
      "		   inner join c_tax t on (it.c_tax_id = t.c_tax_id)" +
      "           inner join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)" +
      "	  where tc.em_atecfe_codtax is not null and i.c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarImpuesReten(ConnectionProvider connectionProvider, String co_retencion_compra_id)    throws ServletException {
    return methodSeleccionarImpuesReten(connectionProvider, co_retencion_compra_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarImpuesReten(ConnectionProvider connectionProvider, String co_retencion_compra_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select case when upper(crcl.tipo) = 'FUENTE' then '1'" +
      "            when upper(crcl.tipo) = 'IVA' then '2'" +
      "            when upper(crcl.tipo) = 'ISD' then '6'" +
      "			end as dato1," +
      "			coalesce(ctr.tiporetencionfuente,coalesce(ctr.tiporetencioniva,'')) as dato2," +
      "			coalesce(crcl.base_imp_retencion,0) as dato3," +
      "			coalesce(ctr.porcentaje,0) as dato4," +
      "			coalesce(crcl.valor_retencion,0) as dato5," +
      "			'01' as dato6" +
      "		from co_retencion_compra_linea crcl " +
      "			inner join co_bp_retencion_compra cbrc on (crcl.co_bp_retencion_compra_id = cbrc.co_bp_retencion_compra_id)" +
      "			inner join co_tipo_retencion ctr on (cbrc.co_tipo_retencion_id = ctr.co_tipo_retencion_id)" +
      "		where crcl.co_retencion_compra_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, co_retencion_compra_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDetalles(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarDetalles(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDetalles(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select coalesce(p.value,'') as dato1, " +
      "			 coalesce(null,'') as dato2, " +
      "			 coalesce(p.description,coalesce(p.name,'')) as dato3," +
      "			 round(coalesce(il.qtyinvoiced,0),2) as dato4, " +
      "			 (case when il.em_atecfe_descuento <> 0 then " +
      "             			             round(coalesce(il.em_fpv_precio_unitario,0),4)" +
      "             			        else" +
      "                                     round(coalesce(il.priceactual,0),4)" +
      "                                 end)  as dato5," +
      "			 round((case when il.em_atecfe_descuento <> 0 then " +
      "			             (em_fpv_precio_unitario - priceactual)" +
      "			        else" +
      "                         0" +
      "                    end),2)  as dato6," +
      "			 round(coalesce(il.linenetamt,0),2) as dato7," +
      "			 coalesce(il.c_invoiceline_id,'') as dato8," +
      "			 coalesce(il.description,'') as dato9" +
      "	  from c_invoiceline il" +
      "	       inner join m_product p on (il.m_product_id = p.m_product_id)" +
      "	  where il.c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDetalTax(ConnectionProvider connectionProvider, String cInvoicelineId)    throws ServletException {
    return methodSeleccionarDetalTax(connectionProvider, cInvoicelineId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDetalTax(ConnectionProvider connectionProvider, String cInvoicelineId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select coalesce(tc.em_atecfe_codtax,'') as dato1, " +
      "			 coalesce(t.em_atecfe_tartax,'') as dato2," +
      "			 round(coalesce(t.rate, 0),2) as dato3, " +
      "			 round(coalesce(ilt.taxbaseamt, 0),2) as dato4, " +
      "			 round(coalesce(ilt.taxamt, 0),2) as dato5" +
      "	  from c_invoicelinetax ilt" +
      "		   inner join c_tax t on (ilt.c_tax_id = t.c_tax_id)" +
      "           inner join c_taxcategory tc on (t.c_taxcategory_id = tc.c_taxcategory_id)" +
      "	  where tc.em_atecfe_codtax is not null and ilt.c_invoiceline_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoicelineId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarAdicional(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return methodSeleccionarAdicional(connectionProvider, adClientId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarAdicional(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select coalesce(null,(case when (select split_part(cli.value,'*',3)" +
      "                                           from ad_client cli" +
      "                                          where cli.ad_client_id = ?) = '2' then " +
      "                                   '4' " +
      "                               else " +
      "                                   '3' " +
      "                               end)) as dato1, " +
      "			 coalesce(null,'Pagina web') as dato2,  " +
      "			 coalesce(d.dummy,'') as dato3," +
      "			 coalesce(null,'rucFirmante') as dato4,  " +
      "			 coalesce(d.dummy,'') as dato5," +
      "			 coalesce(null,'Contribuyente Regimen Microempresas') as dato6,  " +
      "			 coalesce((case when (select split_part(cli.value,'*',2)" +
      "                                           from ad_client cli" +
      "                                          where cli.ad_client_id = ?) = '1' then " +
      "                                   'Contribuyente Regimen Microempresas' " +
      "                               else " +
      "                                   '' " +
      "                               end),'') as dato7," +
      "			 coalesce(null,'Agente de Retencion') as dato8,  " +
      "			 coalesce((case when (select split_part(cli.value,'*',3)" +
      "                                           from ad_client cli" +
      "                                          where cli.ad_client_id = ?) = '2' then " +
      "                                   'No. Resolucion: 1' " +
      "                               else " +
      "                                   '' " +
      "                               end),'') as dato9" +
      "	  from dual d" +
      "	  where d.dummy <> ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirec(ConnectionProvider connectionProvider, String adOrgId)    throws ServletException {
    return methodSeleccionarDirec(connectionProvider, adOrgId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirec(ConnectionProvider connectionProvider, String adOrgId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select (coalesce(l.address1,coalesce(l.address2,'')) || ', ' || coalesce(l.city,'') || '-' || coalesce(c.name,'')) as dato1 " +
      "	  from ad_orginfo oi " +
      "		   inner join c_location l on (oi.c_location_id = l.c_location_id) " +
      "		   inner join c_country c on (l.c_country_id = c.c_country_id) " +
      "	  where oi.ad_org_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarFirma(ConnectionProvider connectionProvider, String adUserId, String adClientId)    throws ServletException {
    return methodSeleccionarFirma(connectionProvider, adUserId, adClientId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarFirma(ConnectionProvider connectionProvider, String adUserId, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		  select acf.atecfe_conf_firma_id as dato1" +
      "		    from atecfe_conf_firma acf, atecfe_conf_firma_lineas acfl " +
      "		   where acf.atecfe_conf_firma_id = acfl.atecfe_conf_firma_id" +
      "		     and acf.isactive = 'Y'  " +
      "		     and acfl.isactive = 'Y'" +
      "		     and acfl.ad_user_id = ?" +
      "		     and acfl.ad_client_id = ?" +
      "		   limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracion(ConnectionProvider connectionProvider, String c_invoice_id)    throws ServletException {
    return methodSeleccionarConfiguracion(connectionProvider, c_invoice_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracion(ConnectionProvider connectionProvider, String c_invoice_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select acs.direc_serv_transa as dato1, " +
      "        acs.direc_serv_consul as dato2, " +
      "        acs.nombre_bdd as dato3, " +
      "        acs.puerto_bdd as dato4, " +
      "        acs.usuario_bdd as dato5, " +
      "        acs.password_bdd as dato6, " +
      "        b.taxid as dato7," +
      "		u.email as dato8," +
      "		pc.smtpserverpassword as dato9" +
      "		from c_invoice i " +
      "			 inner join atecfe_conf_servidor acs ON (i.ad_client_id = acs.ad_client_id)" +
      "			 left join c_bpartner b ON (i.c_bpartner_id = b.c_bpartner_id)" +
      "			 left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y')" +
      "			 left join c_poc_configuration pc ON (i.ad_client_id = pc.ad_client_id)" +
      "		where i.c_invoice_id = ?" +
      "		limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirecBodega(ConnectionProvider connectionProvider, String adOrgId)    throws ServletException {
    return methodSeleccionarDirecBodega(connectionProvider, adOrgId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirecBodega(ConnectionProvider connectionProvider, String adOrgId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select (coalesce(l.address1,'') || CASE WHEN l.address2 is null THEN '' ELSE ' - ' END || coalesce(l.address2,'') || CASE WHEN l.postal is null THEN '' ELSE ' - ' END || coalesce(l.postal,'') || CASE WHEN l.city is null THEN '' ELSE ', ' END || coalesce(l.city,'') || CASE WHEN c.name is null THEN '' ELSE ' - ' END || coalesce(c.name,'')) as dato1" +
      "      from c_location l inner join " +
      "         c_country c on (l.c_country_id = c.c_country_id) inner join " +
      "         m_warehouse mw on (mw.c_location_id = l.c_location_id)" +
      "      where mw.m_warehouse_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracionTrasn(ConnectionProvider connectionProvider, String ad_client_id)    throws ServletException {
    return methodSeleccionarConfiguracionTrasn(connectionProvider, ad_client_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracionTrasn(ConnectionProvider connectionProvider, String ad_client_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select acs.direc_serv_transa as dato1" +
      "		from atecfe_conf_servidor acs" +
      "		where acs.ad_client_id = ?" +
      "		limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracionReCompra(ConnectionProvider connectionProvider, String co_retencion_compra_id)    throws ServletException {
    return methodSeleccionarConfiguracionReCompra(connectionProvider, co_retencion_compra_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracionReCompra(ConnectionProvider connectionProvider, String co_retencion_compra_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select acs.direc_serv_transa as dato1, " +
      "        acs.direc_serv_consul as dato2, " +
      "        acs.nombre_bdd as dato3, " +
      "        acs.puerto_bdd as dato4, " +
      "        acs.usuario_bdd as dato5, " +
      "        acs.password_bdd as dato6, " +
      "        b.taxid as dato7," +
      "		u.email as dato8," +
      "		pc.smtpserverpassword as dato9" +
      "		from co_retencion_compra rc " +
      "			 inner join atecfe_conf_servidor acs ON (rc.ad_client_id = acs.ad_client_id)" +
      "			 left join c_bpartner b ON (rc.c_bpartner_id = b.c_bpartner_id)" +
      "			 left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y')" +
      "			 left join c_poc_configuration pc ON (rc.ad_client_id = pc.ad_client_id)" +
      "		where rc.co_retencion_compra_id = ? " +
      "		limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, co_retencion_compra_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracionMinout(ConnectionProvider connectionProvider, String m_inout_id)    throws ServletException {
    return methodSeleccionarConfiguracionMinout(connectionProvider, m_inout_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarConfiguracionMinout(ConnectionProvider connectionProvider, String m_inout_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select acs.direc_serv_transa as dato1, " +
      "        acs.direc_serv_consul as dato2, " +
      "        acs.nombre_bdd as dato3, " +
      "        acs.puerto_bdd as dato4, " +
      "        acs.usuario_bdd as dato5, " +
      "        acs.password_bdd as dato6, " +
      "        b.taxid as dato7," +
      "		u.email as dato8," +
      "		pc.smtpserverpassword as dato9" +
      "		from m_inout mi " +
      "			 inner join atecfe_conf_servidor acs ON (mi.ad_client_id = acs.ad_client_id)" +
      "			 left join c_bpartner b ON (mi.c_bpartner_id = b.c_bpartner_id)" +
      "			 left join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id and u.em_atecfe_check_email = 'Y')" +
      "			 left join c_poc_configuration pc ON (mi.ad_client_id = pc.ad_client_id)" +
      "		where mi.m_inout_id = ? " +
      "		limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, m_inout_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodFacturaElect(ConnectionProvider connectionProvider, String c_doctype_id)    throws ServletException {
    return methodFacturaElect(connectionProvider, c_doctype_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodFacturaElect(ConnectionProvider connectionProvider, String c_doctype_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select d.em_atecfe_fac_elec as dato1, " +
      "		coalesce(dt.templatelocation,'') as dato2, " +
      "		coalesce(dt.templatefilename,'') as dato3, " +
      "		coalesce(dt.reportfilename,'') as dato4" +
      "	from c_doctype d" +
      "         left join c_poc_doctype_template dt on (dt.c_doctype_id = d.c_doctype_id)" +
      "	where d.c_doctype_id = ?" +
      "	limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_doctype_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarEmail(ConnectionProvider connectionProvider, String c_bpartner_id)    throws ServletException {
    return methodSeleccionarEmail(connectionProvider, c_bpartner_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarEmail(ConnectionProvider connectionProvider, String c_bpartner_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select (select array_to_string(ARRAY_AGG(u.email),',') from ad_user u where (b.c_bpartner_id = u.c_bpartner_id) and u.em_atecfe_check_email = 'Y') as dato1, " +
      "		   replace(c.name,'''','''''') as dato2, " +
      "		   replace(b.name,'''','''''') as dato3, " +
      "		   cs.direc_serv_consul as dato4, " +
      "		   b.taxid as dato5 " +
      "	from c_bpartner b" +
      "		 inner join ad_user u ON (b.c_bpartner_id = u.c_bpartner_id)" +
      "		 inner join ad_client c ON (b.ad_client_id = c.ad_client_id)" +
      "		 inner join atecfe_conf_servidor cs ON (c.ad_client_id = cs.ad_client_id)" +
      "	where b.c_bpartner_id = ?" +
      "	limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_bpartner_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarProcess(ConnectionProvider connectionProvider, String nameprocedure, String nameproce)    throws ServletException {
    return methodSeleccionarProcess(connectionProvider, nameprocedure, nameproce, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarProcess(ConnectionProvider connectionProvider, String nameprocedure, String nameproce, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select ad_process_id as dato1" +
      "	from ad_process where upper(procedurename) = upper(?)" +
      "		and upper(name) = upper(?)";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nameprocedure);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, nameproce);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessInvo(ConnectionProvider connectionProvider, String ad_pinstance_id, String c_invoice_id, String ad_dummy_id)    throws ServletException {
    return methodEjecutarProcessInvo(connectionProvider, ad_pinstance_id, c_invoice_id, ad_dummy_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessInvo(ConnectionProvider connectionProvider, String ad_pinstance_id, String c_invoice_id, String ad_dummy_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select c_invoice_post(?,?) as dato1" +
      "	from dual d" +
      "	where d.dummy <> ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_pinstance_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_dummy_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessRet(ConnectionProvider connectionProvider, String ad_pinstance_id, String ad_dummy_id)    throws ServletException {
    return methodEjecutarProcessRet(connectionProvider, ad_pinstance_id, ad_dummy_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessRet(ConnectionProvider connectionProvider, String ad_pinstance_id, String ad_dummy_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select co_ejecuta_retencion_compra(?) as dato1" +
      "	from dual d" +
      "	where d.dummy <> ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_pinstance_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_dummy_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessGuia(ConnectionProvider connectionProvider, String ad_pinstance_id, String m_inout_id, String ad_dummy_id)    throws ServletException {
    return methodEjecutarProcessGuia(connectionProvider, ad_pinstance_id, m_inout_id, ad_dummy_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessGuia(ConnectionProvider connectionProvider, String ad_pinstance_id, String m_inout_id, String ad_dummy_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    select m_inout_post(?,?) as dato1" +
      "    from dual d" +
      "    where d.dummy <> ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_pinstance_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, m_inout_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_dummy_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessInsepInpara(ConnectionProvider connectionProvider, String ad_pinstance_id, String ad_dummy_id)    throws ServletException {
    return methodEjecutarProcessInsepInpara(connectionProvider, ad_pinstance_id, ad_dummy_id, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodEjecutarProcessInsepInpara(ConnectionProvider connectionProvider, String ad_pinstance_id, String ad_dummy_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select ad_pinstance_para_insert(?, '10', 'AccionRet', 'CO', null, '0', '0', null, null) as dato1" +
      "	from dual d" +
      "	where d.dummy <> ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_pinstance_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_dummy_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static int methodInsertarPInst(ConnectionProvider connectionProvider, String ad_pinstance_id, String ad_process_id, String record_id, String ad_user_id, String ad_client_id, String ad_org_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  Insert into ad_pinstance(ad_pinstance_id, ad_process_id, record_id, isprocessing, created, " +
      "                 ad_user_id, updated, result, errormsg, ad_client_id, " +
      "                 ad_org_id, createdby, updatedby, isactive)" +
      "      values (?, ?, ?, 'N', now(), " +
      "                 ?, now(),'0','', ?, " +
      "                 ?, '0', '0', 'Y');";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_pinstance_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_process_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, record_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_user_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_client_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ad_org_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarInvo(ConnectionProvider connectionProvider, String emAtecfeClave, String emAtecfeDocumentoXml, String emAtecfeMenobserror, String emAtecfeDocaction, String emAtecfeDocstatus, String emAtecoffDocaction, String emAtecoffDocstatus, String emCoNroAutSri, String emAtecfeFechaAutori, String emAtecfeFechaAutoriz, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  Update c_invoice set em_atecfe_codigo_acc = ?, " +
      "	  em_atecfe_documento_xml = decode(?,'base64'), " +
      "	  em_atecfe_menobserror_sri = ?, " +
      "	  em_atecfe_docaction = ?, " +
      "	  em_atecfe_docstatus = ?, " +
      "	  em_atecoff_docaction = ?, " +
      "      em_atecoff_docstatus = ?, " +
      "	  em_co_nro_aut_sri = ?, " +
      "	  em_atecfe_fecha_autori = ?, " +
      "	  em_co_vencimiento_aut_sri = date(?)   " +
      "	  where c_invoice_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeClave);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocumentoXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserror);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutori);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutoriz);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarInvoOffline(ConnectionProvider connectionProvider, String emAtecfeDocumentoXml, String emAtecfeClave, String emAtecfeMenobserror, String emAtecfeDocaction, String emAtecfeDocstatus, String emAtecoffDocaction, String emAtecoffDocstatus, String emCoNroAutSri, String emAtecfeFechaAutori, String emAtecfeFechaAutoriz, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      Update c_invoice set em_atecfe_documento_xml = decode(?,'base64'),  " +
      "      em_atecfe_codigo_acc = ?, " +
      "      em_atecfe_menobserror_sri = ?, " +
      "      em_atecfe_docaction = ?, " +
      "      em_atecfe_docstatus = ?, " +
      "      em_atecoff_docaction = ?,  " +
      "      em_atecoff_docstatus = ?, " +
      "      em_co_nro_aut_sri = ?, " +
      "      em_atecfe_fecha_autori = ?, " +
      "      em_co_vencimiento_aut_sri = date(?)   " +
      "      where c_invoice_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocumentoXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeClave);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserror);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutori);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutoriz);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarReten(ConnectionProvider connectionProvider, String emAtecfeClave, String emAtecfeDocumentoXml, String emAtecfeMenobserror, String emAtecfeDocaction, String emAtecfeDocstatus, String emAtecoffDocaction, String emAtecoffDocstatus, String noAutorizacion, String emAtecfeFechaAutori, String coretencionid)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  Update co_retencion_compra set em_atecfe_codigo_acc = ?, " +
      "	  em_atecfe_documento_xml = decode(?,'base64'), " +
      "	  em_atecfe_menobserror_sri = ?, " +
      "	  em_atecfe_docaction = ?, " +
      "	  em_atecfe_docstatus = ?, " +
      "	  em_atecoff_docaction = ?, " +
      "      em_atecoff_docstatus = ?, " +
      "	  no_autorizacion = ?, " +
      "	  em_atecfe_fecha_autori = ? " +
      "	  where co_retencion_compra_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeClave);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocumentoXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserror);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutori);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coretencionid);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarRetenOffline(ConnectionProvider connectionProvider, String emAtecfeDocumentoXml, String emAtecfeClave, String emAtecfeMenobserror, String emAtecfeDocaction, String emAtecfeDocstatus, String emAtecoffDocaction, String emAtecoffDocstatus, String noAutorizacion, String emAtecfeFechaAutori, String coretencionid)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      Update co_retencion_compra set em_atecfe_documento_xml = decode(?,'base64'), " +
      "      em_atecfe_codigo_acc = ?, " +
      "      em_atecfe_menobserror_sri = ?, " +
      "      em_atecfe_docaction = ?, " +
      "      em_atecfe_docstatus = ?, " +
      "      em_atecoff_docaction = ?,  " +
      "      em_atecoff_docstatus = ?, " +
      "      no_autorizacion = ?, " +
      "      em_atecfe_fecha_autori = ? " +
      "      where co_retencion_compra_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocumentoXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeClave);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserror);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutori);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coretencionid);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarMinout(ConnectionProvider connectionProvider, String emAtecfeClave, String emAtecfeDocumentoXml, String emAtecfeMenobserror, String emAtecfeDocaction, String emAtecfeDocstatus, String emCoNroAutSri, String emAtecfeFechaAutori, String mInoutId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      Update m_inout set em_atecfe_codigo_acc = ?, " +
      "      em_atecfe_documento_xml = decode(?,'base64'), " +
      "      em_atecfe_menobserror_sri = ?, " +
      "      em_atecfe_docaction = ?, " +
      "      em_atecfe_docstatus = ?, " +
      "      em_atecfe_nro_aut_sri = ?, " +
      "      em_atecfe_fecha_autori = ?  " +
      "      where m_inout_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeClave);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocumentoXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserror);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutori);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarMinoutOffline(ConnectionProvider connectionProvider, String emAtecfeDocumentoXml, String emAtecfeClave, String emAtecfeMenobserror, String emAtecfeDocaction, String emAtecfeDocstatus, String emAtecoffDocaction, String emAtecoffDocstatus, String emCoNroAutSri, String emAtecfeFechaAutori, String mInoutId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      Update m_inout set em_atecfe_documento_xml = decode(?,'base64'), " +
      "      em_atecfe_codigo_acc = ?, " +
      "      em_atecfe_menobserror_sri = ?, " +
      "      em_atecfe_docaction = ?, " +
      "      em_atecfe_docstatus = ?, " +
      "      em_atecoff_docaction = ?,  " +
      "      em_atecoff_docstatus = ?, " +
      "      em_atecfe_nro_aut_sri = ?, " +
      "      em_atecfe_fecha_autori = ?  " +
      "      where m_inout_id = ?";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocumentoXml);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeClave);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserror);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoNroAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeFechaAutori);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarInvEstadoOffline(ConnectionProvider connectionProvider, String emAtecoffDocstatus, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      update c_invoice set em_atecoff_docstatus = ?, updated = now() where c_invoice_id = ? ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarRetEstadoOffline(ConnectionProvider connectionProvider, String emAtecoffDocstatus, String coRetencionCompraIdd)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      update co_retencion_compra set em_atecoff_docstatus = ?, updated = now() where co_retencion_compra_id = ? ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionCompraIdd);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int methodActualizarGuiEstadoOffline(ConnectionProvider connectionProvider, String emAtecoffDocstatus, String mInoutId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      update m_inout set em_atecoff_docstatus = ?, updated = now() where m_inout_id = ? ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static ATECOFFGenerarXmlData[] methodSelecDetallesLiqui(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSelecDetallesLiqui(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSelecDetallesLiqui(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select *" +
      "       from (select r.dato1, r.dato2, r.dato3, r.dato4, r.dato5, r.dato6, r.dato7, r.dato8, " +
      "        (select name from c_uom_trl where c_uom_id = r.dato9 order by created limit 1) as dato9" +
      "       from (select coalesce(p.value,'') as dato1, " +
      "        	 coalesce(null,'') as dato2, " +
      "        	 coalesce(p.description,coalesce(p.name,'')) as dato3," +
      "        	 round(coalesce(il.qtyinvoiced,0),2) as dato4, " +
      "        	 round(coalesce(il.priceactual,0),2) as dato5," +
      "        	 round(coalesce(((il.em_atecfe_descuento * il.priceactual) / 100) * il.qtyinvoiced, 0),2) as dato6, " +
      "        	 round(coalesce(il.linenetamt,0),2) as dato7," +
      "        	 coalesce(il.c_invoiceline_id,'') as dato8," +
      "        	 u.c_uom_id as dato9" +
      "      from c_invoiceline il" +
      "           inner join m_product p on (il.m_product_id = p.m_product_id)" +
      "           left join c_uom u on (p.c_uom_id=u.c_uom_id)" +
      "           where il.c_invoice_id = ?) as r" +
      "      union all   " +
      "      select substring(g.name from 1 for 5) as dato1, " +
      "        	 coalesce(null,'')  as dato2, " +
      "        	 coalesce(g.description,coalesce(g.name,'')) as dato3," +
      "        	 round(coalesce(il.qtyinvoiced,0),2) as dato4, " +
      "        	 round(coalesce(il.priceactual,0),2) as dato5," +
      "        	 round(coalesce(((il.em_atecfe_descuento * il.priceactual) / 100) * il.qtyinvoiced, 0),2) as dato6, " +
      "        	 round(coalesce(il.linenetamt,0),2) as dato7," +
      "        	 coalesce(il.c_invoiceline_id,'') as dato8," +
      "        	 'Unidad' as dato9" +
      "      from c_invoiceline il" +
      "           inner join c_glitem g on (il.account_id = g.c_glitem_id)" +
      "           where il.c_invoice_id = ?) as datos";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirecProveedor(ConnectionProvider connectionProvider, String cBpartnerIdLocationId)    throws ServletException {
    return methodSeleccionarDirecProveedor(connectionProvider, cBpartnerIdLocationId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDirecProveedor(ConnectionProvider connectionProvider, String cBpartnerIdLocationId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select (coalesce(l.address1,'') || CASE WHEN l.address2 is null THEN '' ELSE ' = ' END || coalesce(l.address2,'') || CASE WHEN l.postal is null THEN '' ELSE ' - ' END || coalesce(l.postal,'') || CASE WHEN l.city is null THEN '' ELSE ', ' END || coalesce(l.city,'') || CASE WHEN c.name is null THEN '' ELSE ' - ' END || coalesce(c.name,'')) as dato1" +
      "      from c_location l inner join " +
      "        c_country c on (l.c_country_id = c.c_country_id) inner join " +
      "        c_bpartner_location cbl on (cbl.c_location_id = l.c_location_id)" +
      "      where cbl.c_bpartner_location_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerIdLocationId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarTotImpReembolso(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarTotImpReembolso(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarTotImpReembolso(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select coalesce(round((res.base + res.valor),2),0) as dato1, " +
      "             coalesce(round(res.base,2),0) as dato2, " +
      "             coalesce(round(res.valor,2),0) as dato3 " +
      "      from (select sum(datos.base0 + datos.base12 + datos.base_exento) as base, " +
      "                   sum(datos.valor_ice + datos.valor_iva) as valor" +
      "            from (select sum(base0) as base0, sum(base12) as base12, sum(base_exento) as base_exento, " +
      "                         sum(valor_ice) as valor_ice, sum(valor_iva) as valor_iva" +
      "                 from ats_reembolso " +
      "                 where c_invoice_id = ?" +
      "                 ) as datos) res";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDescuentoFactura(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarDescuentoFactura(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarDescuentoFactura(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select coalesce(sum(em_atecfe_descuento),0) as dato1" +
      "      from c_invoiceline " +
      "        where c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarReembolsos(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarReembolsos(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarReembolsos(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select re.tipo_identificacion as dato1, re.identificacion as dato2, '593' as dato3," +
      "               (LPAD((to_char(d.em_co_tp_comp_autorizador_sri)), 2, '0')) as dato4, re.establecimiento as dato5, " +
      "               re.emision as dato6, re.secuencial as dato7, to_char(re.fecha_emision, 'DD/MM/YYYY') as dato8, re.autorizacion_sri as dato9," +
      "               re.ats_reembolso_id as dato10" +
      "        from ats_reembolso re" +
      "        inner join c_doctype d on (re.c_doctype_id=d.c_doctype_id)" +
      "        where re.c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.dato10 = UtilSql.getValue(result, "dato10");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarImpRee(ConnectionProvider connectionProvider, String adClientId, String atsReembolsoId)    throws ServletException {
    return methodSeleccionarImpRee(connectionProvider, adClientId, atsReembolsoId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarImpRee(ConnectionProvider connectionProvider, String adClientId, String atsReembolsoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	select (select coalesce(c.em_atecfe_codtax,'')" +
      "	               from c_tax t" +
      "	               inner join c_taxcategory c on (t.c_taxcategory_id=c.c_taxcategory_id)" +
      "	               where t.ad_client_id = ?" +
      "	               and lower(c.name) like '%' || res.iva || '%' and t.isactive='Y' limit 1) as dato1," +
      "	       (select coalesce(t.em_atecfe_tartax,'')" +
      "	               from c_tax t" +
      "	               inner join c_taxcategory c on (t.c_taxcategory_id=c.c_taxcategory_id)" +
      "	               where t.ad_client_id = ? " +
      "	               and lower(c.name) like '%' || res.iva || '%' and t.isactive='Y' limit 1) as dato2," +
      "	       (select coalesce(t.rate,0)" +
      "	               from c_tax t" +
      "	               inner join c_taxcategory c on (t.c_taxcategory_id=c.c_taxcategory_id)" +
      "	               where t.ad_client_id = ?" +
      "	               and lower(c.name) like '%' || res.iva || '%' and t.isactive='Y' limit 1) as dato3, " +
      "	        res.base as dato4, res.valor as dato5" +
      "	from (" +
      "	select 'iva 0' as iva, re.base0 as base, 0 as valor " +
      "	        from ats_reembolso re where re.ats_reembolso_id = ? " +
      "	        and re.base0 > 0" +
      "	union all" +
      "	select 'iva 12' as iva, re.base12 as base, valor_iva as valor" +
      "	        from ats_reembolso re where re.ats_reembolso_id = ?" +
      "	        and re.base12 > 0" +
      "	union all" +
      "	select 'excento' as iva, re.base_exento as base, 0 as valor" +
      "	        from ats_reembolso re where re.ats_reembolso_id = ?" +
      "	        and re.base_exento > 0" +
      "	union all" +
      "	select 'ice' as iva, re.base12 as base, re.valor_ice as valor" +
      "	        from ats_reembolso re where re.ats_reembolso_id = ?" +
      "	        and re.valor_ice > 0) as res	     ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atsReembolsoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atsReembolsoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atsReembolsoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atsReembolsoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodTotales(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodTotales(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodTotales(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select (select coalesce(round((sum(taxbaseamt)),2),0)" +
      "                     from c_invoicetax where c_invoice_id=r.c_invoice_id) as dato1," +
      "             (select coalesce(round((sum(taxamt)),2),0)" +
      "                     from c_invoicetax where c_invoice_id=r.c_invoice_id) as dato2," +
      "             coalesce((round(sum(r.base0+r.base12+r.base_exento),2)),0) as dato3," +
      "             coalesce((round(sum(r.valor_ice+r.valor_iva),2)),0) as dato4" +
      "      from (select c_invoice_id," +
      "             sum(base0) as base0, " +
      "             sum(base12) as base12, " +
      "             sum(base_exento) as base_exento, " +
      "             sum(valor_ice) as valor_ice, " +
      "             sum(valor_iva) as valor_iva" +
      "             from ats_reembolso" +
      "             where c_invoice_id = ?" +
      "             group by 1) as r" +
      "             group by 1,2";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarFact(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodSeleccionarFact(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarFact(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	    select encode(a.documento_xml,'base64') as dato1, " +
      "			   b.em_atecfe_codigo_acc as dato2," +
      "			   coalesce((SELECT array_to_string(array_agg(u.email), ';') " +
      "		       		       FROM ad_user as u " +
      "		       			  WHERE u.c_bpartner_id = b.c_bpartner_id " +
      "		       			    AND u.em_atecfe_check_email = 'Y'),' ') as dato3," +
      "		       (case when c.em_co_tp_comp_autorizador_sri = '1' then " +
      "		                'FACTURA DE VENTA ELECTRONICA'" +
      "		            when c.em_co_tp_comp_autorizador_sri = '4' then" +
      "		                'NOTA DE CREDITO ELECTRONICA'" +
      "		            when c.em_co_tp_comp_autorizador_sri = '3' then" +
      "		                'LIQUIDACION DE COMPRA ELECTRONICA'" +
      "		            else" +
      "		                'DOCUMENTO ELECTRÃ“NICO'" +
      "		       end) as dato4," +
      "		       c.em_co_tp_comp_autorizador_sri as dato5," +
      "		       d.name2 as dato6," +
      "		       e.name as dato7," +
      "		       e.ad_client_id as dato8 " +
      "		  from atecfe_c_invoice a, c_invoice b, c_doctype c, c_bpartner d, ad_client e " +
      "		 where a.c_invoice_id = b.c_invoice_id" +
      "		   and b.c_doctypetarget_id = c.c_doctype_id" +
      "		   and b.c_bpartner_id = d.c_bpartner_id" +
      "		   and b.ad_client_id = e.ad_client_id" +
      "		   and a.c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarRete(ConnectionProvider connectionProvider, String cRetencionId)    throws ServletException {
    return methodSeleccionarRete(connectionProvider, cRetencionId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarRete(ConnectionProvider connectionProvider, String cRetencionId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select encode(r.em_atecfe_documento_xml,'base64') as dato1, " +
      "			 r.em_atecfe_codigo_acc as dato2," +
      "			 coalesce((SELECT array_to_string(array_agg(u.email), ';') " +
      "       		       FROM ad_user as u " +
      "       			  WHERE u.c_bpartner_id = r.c_bpartner_id " +
      "       			    AND u.em_atecfe_check_email = 'Y'),' ') as dato3," +
      "       		'RETENCION DE COMPRA ELECTRONICA' as dato4," +
      "       		c.em_co_tp_comp_autorizador_sri as dato5," +
      "		    d.name2 as dato6," +
      "		    e.name as dato7 " +
      "	  from co_retencion_compra r, c_doctype c, c_bpartner d, ad_client e" +
      "	  where c.c_doctype_id = r.c_doctype_id" +
      "		and d.c_bpartner_id = r.c_bpartner_id" +
      "		and e.ad_client_id = r.ad_client_id" +
      "		and r.co_retencion_compra_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRetencionId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarGuia(ConnectionProvider connectionProvider, String mInoutId)    throws ServletException {
    return methodSeleccionarGuia(connectionProvider, mInoutId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarGuia(ConnectionProvider connectionProvider, String mInoutId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select encode(m.em_atecfe_documento_xml,'base64') as dato1, " +
      "			 m.em_atecfe_codigo_acc as dato2," +
      "			 coalesce((SELECT array_to_string(array_agg(u.email), ';') " +
      "       		       FROM ad_user as u " +
      "       			  WHERE u.c_bpartner_id = m.c_bpartner_id " +
      "       			    AND u.em_atecfe_check_email = 'Y'),' ') as dato3," +
      "       	     'GUIA DE REMISION ELECTRONICA' as dato4," +
      "       	     c.em_co_tp_comp_autorizador_sri as dato5," +
      "		     d.name2 as dato6," +
      "		     e.name as dato7 " +
      "	  from m_inout m, c_doctype c, c_bpartner d, ad_client e" +
      "	  where c.c_doctype_id = m.c_doctype_id" +
      "		and d.c_bpartner_id = m.c_bpartner_id" +
      "		and e.ad_client_id = m.ad_client_id" +
      "	    and m.m_inout_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodResumenReembolso(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodResumenReembolso(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodResumenReembolso(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  	select a.c_invoice_id as dato1, " +
      "	  	       (sum(c.taxbaseamt)+sum(c.taxamt)) as dato2, " +
      "	  	       sum(c.taxbaseamt) as dato3, " +
      "	  	       sum(c.taxamt) as dato4 " +
      "		  from ats_reembolso_ventas a, c_invoice b, c_invoicetax c" +
      "		 where a.c_invoice_compra_id = b.c_invoice_id" +
      "		   and b.c_invoice_id = c.c_invoice_id" +
      "		   and a.c_invoice_id = ?" +
      "		 group by 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodDetalleReembolso(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodDetalleReembolso(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodDetalleReembolso(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		 select d.em_co_tipo_identificacion as dato1, " +
      "		        d.taxid as dato2," +
      "		        (case when d.em_co_natural_juridico = 'PN' then '01'" +
      "		             when d.em_co_natural_juridico = 'PJ' then '02'" +
      "		             else" +
      "		             '00'" +
      "		        end) as dato3," +
      "		        lpad(to_char(e.em_co_tp_comp_autorizador_sri), 2, '0') as dato4," +
      "		        b.em_co_nro_estab as dato5, " +
      "		        b.em_co_punto_emision as dato6," +
      "		        lpad(to_char(b.documentno), 9, '0') as dato7," +
      "		        to_char(b.dateinvoiced, 'DD/MM/YYYY') as dato8," +
      "		        b.em_co_nro_aut_sri as dato9," +
      "		        sum(b.grandtotal) as dato10, " +
      "		        sum(c.taxbaseamt) as dato11, " +
      "		        sum(c.taxamt) as dato12," +
      "		        a.c_invoice_compra_id as dato13 " +
      "		   from ats_reembolso_ventas a, c_invoice b, c_invoicetax c, c_bpartner d," +
      "		        c_doctype e" +
      "		  where a.c_invoice_compra_id = b.c_invoice_id" +
      "		    and b.c_invoice_id = c.c_invoice_id" +
      "		    and b.c_bpartner_id = d.c_bpartner_id" +
      "		    and e.c_doctype_id = b.c_doctypetarget_id" +
      "		    and a.c_invoice_id = ?" +
      "		  group by 1,2,3,4,5,6,7,8,9,13";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECOFFGenerarXmlData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECOFFGenerarXmlData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECOFFGenerarXmlData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECOFFGenerarXmlData.dato10 = UtilSql.getValue(result, "dato10");
        objectATECOFFGenerarXmlData.dato11 = UtilSql.getValue(result, "dato11");
        objectATECOFFGenerarXmlData.dato12 = UtilSql.getValue(result, "dato12");
        objectATECOFFGenerarXmlData.dato13 = UtilSql.getValue(result, "dato13");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodDetImpReembolso(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodDetImpReembolso(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodDetImpReembolso(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		  select b.taxbaseamt as dato1, " +
      "		         b.taxamt as dato2, " +
      "		         c.rate as dato3, " +
      "		         c.em_atecfe_tartax as dato4, " +
      "		         d.em_atecfe_codtax as dato5" +
      "		    from c_invoice a, c_invoicetax b, c_tax c, c_taxcategory d" +
      "		   where a.c_invoice_id = b.c_invoice_id" +
      "		     and b.c_tax_id = c.c_tax_id" +
      "		     and c.c_taxcategory_id = d.c_taxcategory_id" +
      "		     and a.c_invoice_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodValidarReembolso(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodValidarReembolso(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodValidarReembolso(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		  select totallines as dato1, grandtotal as dato2," +
      "		         coalesce((base0+base12+base_exento),0) as dato3," +
      "		         coalesce((base0+base12+base_exento+valor_iva)) as dato4" +
      "		   from(" +
      "		         select a.totallines, a.grandtotal, " +
      "		                sum(b.base0) as base0, sum(b.base12) as base12, " +
      "		                sum(base_exento) as base_exento, " +
      "		                sum(valor_iva) as valor_iva" +
      "		           from c_invoice a, ats_reembolso_ventas b" +
      "		          where a.c_invoice_id = b.c_invoice_id" +
      "		            and a.c_invoice_id = ?" +
      "		         group by 1,2" +
      "		       ) as datos";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarAdicionalDesc(ConnectionProvider connectionProvider, String adClientId, String adInvoiceId)    throws ServletException {
    return methodSeleccionarAdicionalDesc(connectionProvider, adClientId, adInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodSeleccionarAdicionalDesc(ConnectionProvider connectionProvider, String adClientId, String adInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  select coalesce(null,(case when (select split_part(cli.value,'*',3)" +
      "                                           from ad_client cli" +
      "                                          where cli.ad_client_id = ?) = '2' then " +
      "                                   '2' " +
      "                               else " +
      "                                   '1' " +
      "                               end)) as dato1, " +
      "             coalesce(null,'PAGINA WEB') as dato2,  " +
      "			 coalesce('N/A','') as dato3," +
      "			 coalesce(null,'DESCRIPCION') as dato4,  " +
      "			 coalesce((SELECT coalesce(description,'')" +
      "                        FROM c_invoice" +
      "                       WHERE c_invoice_id = ?),'X') as dato5" +
      "	  from dual d" +
      "	  where d.dummy <> ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECOFFGenerarXmlData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECOFFGenerarXmlData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }

  public static ATECOFFGenerarXmlData[] methodValidarConsumidor(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return methodValidarConsumidor(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECOFFGenerarXmlData[] methodValidarConsumidor(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select round(sum(coalesce(b.taxbaseamt,0)+coalesce(b.taxamt,0)),2) as dato1, " +
      "      c.em_co_tipo_identificacion as dato2 " +
      "	  from c_invoice a " +
      "		left join c_invoicelinetax b on b.c_invoice_id=a.c_invoice_id" +
      "		left join c_bpartner c on c.c_bpartner_id=a.c_bpartner_id" +
      "		where a.c_invoice_id = ?" +
      "		group by 2";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECOFFGenerarXmlData objectATECOFFGenerarXmlData = new ATECOFFGenerarXmlData();
        objectATECOFFGenerarXmlData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECOFFGenerarXmlData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECOFFGenerarXmlData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECOFFGenerarXmlData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECOFFGenerarXmlData objectATECOFFGenerarXmlData[] = new ATECOFFGenerarXmlData[vector.size()];
    vector.copyInto(objectATECOFFGenerarXmlData);
    return(objectATECOFFGenerarXmlData);
  }
}
