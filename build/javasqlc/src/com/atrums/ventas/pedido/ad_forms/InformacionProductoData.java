//Sqlc generated V1.O00-1
package com.atrums.ventas.pedido.ad_forms;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class InformacionProductoData implements FieldProvider {
static Logger log4j = Logger.getLogger(InformacionProductoData.class);
  private String InitRecordNumber="0";
  public String mproductmarca;
  public String mproductmodelo;
  public String mproductvalue;
  public String mproductname;
  public String mproductimageid;
  public String mproductdescripcion;
  public String date;
  public String rownum;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("mproductmarca"))
      return mproductmarca;
    else if (fieldName.equalsIgnoreCase("mproductmodelo"))
      return mproductmodelo;
    else if (fieldName.equalsIgnoreCase("mproductvalue"))
      return mproductvalue;
    else if (fieldName.equalsIgnoreCase("mproductname"))
      return mproductname;
    else if (fieldName.equalsIgnoreCase("mproductimageid"))
      return mproductimageid;
    else if (fieldName.equalsIgnoreCase("mproductdescripcion"))
      return mproductdescripcion;
    else if (fieldName.equalsIgnoreCase("date"))
      return date;
    else if (fieldName.equals("rownum"))
      return rownum;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static InformacionProductoData[] selectInfoProductos(ConnectionProvider connectionProvider, String mProductId)    throws ServletException {
    return selectInfoProductos(connectionProvider, mProductId, 0, 0);
  }

  public static InformacionProductoData[] selectInfoProductos(ConnectionProvider connectionProvider, String mProductId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      		SELECT " +
      "			    emv.nombre AS mProductMarca, " +
      "      		    emm.modelo AS mProductModelo, " +
      "      		    mp.value AS mProductValue, " +
      "      		    mp.name AS mProductName," +
      "      		    mp.ad_image_id AS mProductImageId," +
      "      		    mp.description AS mProductDescripcion," +
      "      		    to_char(now(),'dd/mm/yy-HH24:MI:SS') as date" +
      "      		FROM m_product mp " +
      "      		LEFT JOIN ecom_posicion_modelo epm ON (epm.m_product_id = mp.m_product_id) " +
      "      		LEFT JOIN ecom_marca_vehiculo emv ON (emv.ecom_marca_vehiculo_id = epm.ecom_marca_vehiculo_id) " +
      "      		LEFT JOIN ecom_modelo_marcas emm ON (emm.ecom_modelo_marcas_id = epm.ecom_modelo_marcas_id) " +
      "      		LEFT JOIN ad_image im on (mp.ad_image_id = im.ad_image_id)" +
      "      		WHERE mp.m_product_id = ? " +
      "      		limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        InformacionProductoData objectInformacionProductoData = new InformacionProductoData();
        objectInformacionProductoData.mproductmarca = UtilSql.getValue(result, "mproductmarca");
        objectInformacionProductoData.mproductmodelo = UtilSql.getValue(result, "mproductmodelo");
        objectInformacionProductoData.mproductvalue = UtilSql.getValue(result, "mproductvalue");
        objectInformacionProductoData.mproductname = UtilSql.getValue(result, "mproductname");
        objectInformacionProductoData.mproductimageid = UtilSql.getValue(result, "mproductimageid");
        objectInformacionProductoData.mproductdescripcion = UtilSql.getValue(result, "mproductdescripcion");
        objectInformacionProductoData.date = UtilSql.getValue(result, "date");
        objectInformacionProductoData.rownum = Long.toString(countRecord);
        objectInformacionProductoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectInformacionProductoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    InformacionProductoData objectInformacionProductoData[] = new InformacionProductoData[vector.size()];
    vector.copyInto(objectInformacionProductoData);
    return(objectInformacionProductoData);
  }
}
