//Sqlc generated V1.O00-1
package com.atrums.ventas.pedido.ad_forms;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class FacturaPosData implements FieldProvider {
static Logger log4j = Logger.getLogger(FacturaPosData.class);
  private String InitRecordNumber="0";
  public String mproducmarcamodelo;
  public String mproductid;
  public String mproductorgname;
  public String mproductname;
  public String mproductwarehouseid;
  public String mproductwarehousename;
  public String mproductvalue;
  public String mproductupc;
  public String mecomproductmarcaname;
  public String mecomproductmodeloname;
  public String mproductfamiliaid;
  public String mproductfamilianame;
  public String mproductcategoriaid;
  public String mproductcategorianame;
  public String mproductgenericoname;
  public String mproductmarcaname;
  public String mproductsubcategorianame;
  public String mproductmodeloname;
  public String mproductreferencecode;
  public String mproductprice;
  public String mproductpriceefec;
  public String mproductcantidadstock;
  public String mproducttax;
  public String mproductimageid;
  public String mproductimage;
  public String mproductstockid;
  public String mproductinicial;
  public String mproductpricetotal;
  public String mproducttotalcant;
  public String mproducttotalimp;
  public String mproducttotal;
  public String mproducttotaldescuento;
  public String fpvproductvalue;
  public String fpvmetodopagovid;
  public String fpvmetodopagovname;
  public String fpvfacturacotid;
  public String fpvfacturacotlinid;
  public String fpvproductname;
  public String fpvproductprice;
  public String fpvcantidadordenada;
  public String fpvtotallinea;
  public String fpvdocumentno;
  public String stocktienda;
  public String stockcliente;
  public String fpvproductpriceefectivo;
  public String fpvproductdescuento;
  public String fpvproductvalordescuento;
  public String nameclient;
  public String idenclient;
  public String telefonoclient;
  public String emailclient;
  public String direccionclient;
  public String paymentmethodid;
  public String paymentmethodname;
  public String total;
  public String tipoIden;
  public String rownum;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("mproducmarcamodelo"))
      return mproducmarcamodelo;
    else if (fieldName.equalsIgnoreCase("mproductid"))
      return mproductid;
    else if (fieldName.equalsIgnoreCase("mproductorgname"))
      return mproductorgname;
    else if (fieldName.equalsIgnoreCase("mproductname"))
      return mproductname;
    else if (fieldName.equalsIgnoreCase("mproductwarehouseid"))
      return mproductwarehouseid;
    else if (fieldName.equalsIgnoreCase("mproductwarehousename"))
      return mproductwarehousename;
    else if (fieldName.equalsIgnoreCase("mproductvalue"))
      return mproductvalue;
    else if (fieldName.equalsIgnoreCase("mproductupc"))
      return mproductupc;
    else if (fieldName.equalsIgnoreCase("mecomproductmarcaname"))
      return mecomproductmarcaname;
    else if (fieldName.equalsIgnoreCase("mecomproductmodeloname"))
      return mecomproductmodeloname;
    else if (fieldName.equalsIgnoreCase("mproductfamiliaid"))
      return mproductfamiliaid;
    else if (fieldName.equalsIgnoreCase("mproductfamilianame"))
      return mproductfamilianame;
    else if (fieldName.equalsIgnoreCase("mproductcategoriaid"))
      return mproductcategoriaid;
    else if (fieldName.equalsIgnoreCase("mproductcategorianame"))
      return mproductcategorianame;
    else if (fieldName.equalsIgnoreCase("mproductgenericoname"))
      return mproductgenericoname;
    else if (fieldName.equalsIgnoreCase("mproductmarcaname"))
      return mproductmarcaname;
    else if (fieldName.equalsIgnoreCase("mproductsubcategorianame"))
      return mproductsubcategorianame;
    else if (fieldName.equalsIgnoreCase("mproductmodeloname"))
      return mproductmodeloname;
    else if (fieldName.equalsIgnoreCase("mproductreferencecode"))
      return mproductreferencecode;
    else if (fieldName.equalsIgnoreCase("mproductprice"))
      return mproductprice;
    else if (fieldName.equalsIgnoreCase("mproductpriceefec"))
      return mproductpriceefec;
    else if (fieldName.equalsIgnoreCase("mproductcantidadstock"))
      return mproductcantidadstock;
    else if (fieldName.equalsIgnoreCase("mproducttax"))
      return mproducttax;
    else if (fieldName.equalsIgnoreCase("mproductimageid"))
      return mproductimageid;
    else if (fieldName.equalsIgnoreCase("mproductimage"))
      return mproductimage;
    else if (fieldName.equalsIgnoreCase("mproductstockid"))
      return mproductstockid;
    else if (fieldName.equalsIgnoreCase("mproductinicial"))
      return mproductinicial;
    else if (fieldName.equalsIgnoreCase("mproductpricetotal"))
      return mproductpricetotal;
    else if (fieldName.equalsIgnoreCase("mproducttotalcant"))
      return mproducttotalcant;
    else if (fieldName.equalsIgnoreCase("mproducttotalimp"))
      return mproducttotalimp;
    else if (fieldName.equalsIgnoreCase("mproducttotal"))
      return mproducttotal;
    else if (fieldName.equalsIgnoreCase("mproducttotaldescuento"))
      return mproducttotaldescuento;
    else if (fieldName.equalsIgnoreCase("fpvproductvalue"))
      return fpvproductvalue;
    else if (fieldName.equalsIgnoreCase("fpvmetodopagovid"))
      return fpvmetodopagovid;
    else if (fieldName.equalsIgnoreCase("fpvmetodopagovname"))
      return fpvmetodopagovname;
    else if (fieldName.equalsIgnoreCase("fpvfacturacotid"))
      return fpvfacturacotid;
    else if (fieldName.equalsIgnoreCase("fpvfacturacotlinid"))
      return fpvfacturacotlinid;
    else if (fieldName.equalsIgnoreCase("fpvproductname"))
      return fpvproductname;
    else if (fieldName.equalsIgnoreCase("fpvproductprice"))
      return fpvproductprice;
    else if (fieldName.equalsIgnoreCase("fpvcantidadordenada"))
      return fpvcantidadordenada;
    else if (fieldName.equalsIgnoreCase("fpvtotallinea"))
      return fpvtotallinea;
    else if (fieldName.equalsIgnoreCase("fpvdocumentno"))
      return fpvdocumentno;
    else if (fieldName.equalsIgnoreCase("stocktienda"))
      return stocktienda;
    else if (fieldName.equalsIgnoreCase("stockcliente"))
      return stockcliente;
    else if (fieldName.equalsIgnoreCase("fpvproductpriceefectivo"))
      return fpvproductpriceefectivo;
    else if (fieldName.equalsIgnoreCase("fpvproductdescuento"))
      return fpvproductdescuento;
    else if (fieldName.equalsIgnoreCase("fpvproductvalordescuento"))
      return fpvproductvalordescuento;
    else if (fieldName.equalsIgnoreCase("nameclient"))
      return nameclient;
    else if (fieldName.equalsIgnoreCase("idenclient"))
      return idenclient;
    else if (fieldName.equalsIgnoreCase("telefonoclient"))
      return telefonoclient;
    else if (fieldName.equalsIgnoreCase("emailclient"))
      return emailclient;
    else if (fieldName.equalsIgnoreCase("direccionclient"))
      return direccionclient;
    else if (fieldName.equalsIgnoreCase("paymentmethodid"))
      return paymentmethodid;
    else if (fieldName.equalsIgnoreCase("paymentmethodname"))
      return paymentmethodname;
    else if (fieldName.equalsIgnoreCase("total"))
      return total;
    else if (fieldName.equalsIgnoreCase("tipo_iden") || fieldName.equals("tipoIden"))
      return tipoIden;
    else if (fieldName.equals("rownum"))
      return rownum;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static FacturaPosData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String adClientId, String mProductWarehouseName, String mProductCategoriaId, String mProductName, String mProductValue, String mProductMarcaName, String mProductUpc)    throws ServletException {
    return methodSeleccionarProductosOrg(connectionProvider, adClientId, mProductWarehouseName, mProductCategoriaId, mProductName, mProductValue, mProductMarcaName, mProductUpc, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String adClientId, String mProductWarehouseName, String mProductCategoriaId, String mProductName, String mProductValue, String mProductMarcaName, String mProductUpc, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "    SELECT *, get_uuid() AS mProductStockId, " +
      "          0 AS mProductInicial, 0 AS mProductPriceTotal, 0 AS mProductTotalCant, 0 AS mProductTotalImp, 0 AS mProductTotal, 0 AS mProductTotalDescuento," +
      "          null AS fpvProductValue, null AS fpvMetodoPagovId, null AS fpvMetodoPagovName, null AS fpvFacturaCotId, null AS fpvFacturaCotLinId, " +
      "          null AS fpvProductName, null AS fpvProductPrice, null AS fpvCantidadOrdenada, null AS fpvTotalLinea, null AS fpvDocumentno, " +
      "          0 AS stocktienda, 0 AS stockcliente, 0 AS fpvProductPriceEfectivo, 0 AS fpvProductDescuento, 0 AS fpvProductValorDescuento," +
      "          null AS nameClient, null AS idenClient, null AS telefonoClient, null AS emailClient, null AS direccionClient, null AS paymentmethodId, null AS paymentmethodName," +
      "          '' as total, '' as tipo_iden   " +
      "    FROM (SELECT distinct(r.mProductId || '-' || coalesce(r.mEcomProductMarcaName, '') || '-' || coalesce(r.mEcomProductModeloName, '')) AS mProducMarcaModelo, *," +
      "       (case when r.mProductImageId <> 'null' then" +
      "                  ('<img src=../utility/ShowImage?id='|| r.mProductImageId ||'&amp;nocache='|| to_char(now(),'dd/mm/yy-HH24:MI:SS') ||' width=60 height=60 />') " +
      "             else ('<img src=../utility/ShowImage?id=0 width=60 height=60 />') end) AS mProductImage " +
      "    FROM (" +
      "    (SELECT  	" +
      "            mps.m_product_id AS mProductId, " +
      "            ao.name AS mProductOrgName, " +
      "            upper(mp.name) AS mProductName, " +
      "            mw.m_warehouse_id AS mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName,  " +
      "            mp.value AS mProductValue, " +
      "            mp.upc AS mProductUpc,  " +
      "            emv.nombre AS mEcomProductMarcaName, " +
      "            emm.modelo AS mEcomProductModeloName, " +
      "            mp.em_atecco_familia_id AS mProductFamiliaId, " +
      "            af.nombre As mProductFamiliaName,   " +
      "            mp.em_atecco_categoria_id AS mProductCategoriaId, " +
      "            ac.nombre AS mProductCategoriaName,  " +
      "            ag.nombre AS mProductGenericoName, " +
      "            mb.name AS mProductMarcaName, " +
      "            sc.nombre AS mProductSubCategoriaName, " +
      "            matm.name AS mProductModeloName,  " +
      "            mp.em_atecco_codigo_edimca as mProductReferenceCode, " +
      "            (select atecco_productuni(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPrice, " +
      "            (select atecco_productpriceeffec(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPriceEfec," +
      "            (select atecco_productstocktienda(mps.m_product_id, mw.m_warehouse_id) from dual) AS mProductCantidadStock," +
      "            (select atecco_producttaxt(mps.m_product_id) from dual) AS mProductTax," +
      "            mp.ad_image_id AS mProductImageId  " +
      "        FROM m_product_stock_v mps " +
      "        LEFT JOIN m_product mp ON (mp.m_product_id = mps.m_product_id) " +
      "        LEFT JOIN m_locator ml ON (ml.m_locator_id = mps.m_locator_id) " +
      "        LEFT JOIN m_warehouse mw ON (mw.m_warehouse_id = ml.m_warehouse_id) " +
      "        LEFT JOIN ad_org ao ON (ao.ad_org_id = mw.ad_org_id) " +
      "        LEFT JOIN atecco_familia af ON (af.atecco_familia_id = mp.em_atecco_familia_id) " +
      "        LEFT JOIN atecco_categoria ac ON (ac.atecco_categoria_id = mp.em_atecco_categoria_id) " +
      "        LEFT JOiN ecom_posicion_modelo epm ON (epm.m_product_id = mp.m_product_id) " +
      "        LEFT JOIN ecom_marca_vehiculo emv ON (emv.ecom_marca_vehiculo_id = epm.ecom_marca_vehiculo_id) " +
      "        LEFT JOIN ecom_modelo_marcas emm ON (emm.ecom_modelo_marcas_id = epm.ecom_modelo_marcas_id) " +
      "        LEFT JOIN atecco_sub_categoria sc ON (sc.atecco_sub_categoria_id = mp.em_atecco_sub_categoria_id) " +
      "        LEFT JOIN atecco_generico ag ON (ag.atecco_generico_id = mp.em_atecco_generico_id) " +
      "        LEFT JOIN m_brand mb ON (mb.m_brand_id = mp.m_brand_id) " +
      "        LEFT JOIN m_attributevalue matm ON (matm.m_attributevalue_id = mp.em_atecco_modelo)" +
      "        WHERE mps.ad_client_id = ?  " +
      "            AND coalesce(mw.name,'') LIKE ? " +
      "            AND coalesce(trim(ac.nombre),'') LIKE ?" +
      "            AND upper(coalesce(mp.name,'')) LIKE ?" +
      "        GROUP BY mps.m_product_id, mw.m_warehouse_id, mp.upc, ao.name, mp.name, mp.value, mp.em_atecco_familia_id, af.nombre, " +
      "        mp.em_atecco_categoria_id, ac.nombre, sc.nombre, emv.nombre, emm.modelo, ag.nombre, " +
      "        mb.name, matm.name, mp.em_atecco_codigo_edimca, mp.ad_image_id " +
      "        ORDER BY mp.name,emv.nombre)" +
      "    UNION ALL" +
      "    (SELECT  	" +
      "            mps.m_product_id AS mProductId, " +
      "            ao.name AS mProductOrgName, " +
      "            upper(mp.name) AS mProductName, " +
      "            mw.m_warehouse_id AS mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName,  " +
      "            mp.value AS mProductValue, " +
      "            mp.upc AS mProductUpc,  " +
      "            emv.nombre AS mEcomProductMarcaName, " +
      "            emm.modelo AS mEcomProductModeloName, " +
      "            mp.em_atecco_familia_id AS mProductFamiliaId, " +
      "            af.nombre As mProductFamiliaName, " +
      "            mp.em_atecco_categoria_id AS mProductCategoriaId, " +
      "            ac.nombre AS mProductCategoriaName,  " +
      "            ag.nombre AS mProductGenericoName, " +
      "            mb.name AS mProductMarcaName, " +
      "            sc.nombre AS mProductSubCategoriaName, " +
      "            matm.name AS mProductModeloName," +
      "            mp.em_atecco_codigo_edimca as mProductReferenceCode, " +
      "            (select atecco_productuni(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPrice, " +
      "            (select atecco_productpriceeffec(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPriceEfec," +
      "            (select atecco_productstocktienda(mps.m_product_id, mw.m_warehouse_id) from dual) AS mProductCantidadStock," +
      "            (select atecco_producttaxt(mps.m_product_id) from dual) AS mProductTax," +
      "            mp.ad_image_id AS mProductImageId  " +
      "        FROM m_product_stock_v mps " +
      "        LEFT JOIN m_product mp ON (mp.m_product_id = mps.m_product_id) " +
      "        LEFT JOIN m_locator ml ON (ml.m_locator_id = mps.m_locator_id) " +
      "        LEFT JOIN m_warehouse mw ON (mw.m_warehouse_id = ml.m_warehouse_id) " +
      "        LEFT JOIN ad_org ao ON (ao.ad_org_id = mw.ad_org_id) " +
      "        LEFT JOIN atecco_familia af ON (af.atecco_familia_id = mp.em_atecco_familia_id) " +
      "        LEFT JOIN atecco_categoria ac ON (ac.atecco_categoria_id = mp.em_atecco_categoria_id) " +
      "        LEFT JOiN ecom_posicion_modelo epm ON (epm.m_product_id = mp.m_product_id) " +
      "        LEFT JOIN ecom_marca_vehiculo emv ON (emv.ecom_marca_vehiculo_id = epm.ecom_marca_vehiculo_id) " +
      "        LEFT JOIN ecom_modelo_marcas emm ON (emm.ecom_modelo_marcas_id = epm.ecom_modelo_marcas_id) " +
      "        LEFT JOIN atecco_sub_categoria sc ON (sc.atecco_sub_categoria_id = mp.em_atecco_sub_categoria_id) " +
      "        LEFT JOIN atecco_generico ag ON (ag.atecco_generico_id = mp.em_atecco_generico_id) " +
      "        LEFT JOIN m_brand mb ON (mb.m_brand_id = mp.m_brand_id) " +
      "        LEFT JOIN m_attributevalue matm ON (matm.m_attributevalue_id = mp.em_atecco_modelo)" +
      "        WHERE mps.ad_client_id = ?  " +
      "            AND coalesce(mw.name,'') LIKE ?" +
      "            AND coalesce(trim(ac.nombre),'') LIKE ?" +
      "            AND upper(coalesce(mp.value,'')) LIKE ?" +
      "        GROUP BY mps.m_product_id, mw.m_warehouse_id, mp.upc, ao.name, mp.name, mp.value, mp.em_atecco_familia_id, af.nombre, " +
      "        mp.em_atecco_categoria_id, ac.nombre, sc.nombre, emv.nombre, emm.modelo, ag.nombre, " +
      "        mb.name, matm.name, mp.em_atecco_codigo_edimca, mp.ad_image_id " +
      "        ORDER BY mp.name,emv.nombre)" +
      "    UNION ALL" +
      "    (SELECT  	" +
      "            mps.m_product_id AS mProductId, " +
      "            ao.name AS mProductOrgName, " +
      "            upper(mp.name) AS mProductName, " +
      "            mw.m_warehouse_id AS mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName,  " +
      "            mp.value AS mProductValue, " +
      "            mp.upc AS mProductUpc,  " +
      "            emv.nombre AS mEcomProductMarcaName, " +
      "            emm.modelo AS mEcomProductModeloName, " +
      "            mp.em_atecco_familia_id AS mProductFamiliaId, " +
      "            af.nombre As mProductFamiliaName,  " +
      "            mp.em_atecco_categoria_id AS mProductCategoriaId, " +
      "            ac.nombre AS mProductCategoriaName,  " +
      "            ag.nombre AS mProductGenericoName, " +
      "            mb.name AS mProductMarcaName, " +
      "            sc.nombre AS mProductSubCategoriaName, " +
      "            matm.name AS mProductModeloName," +
      "            mp.em_atecco_codigo_edimca as mProductReferenceCode, " +
      "            (select atecco_productuni(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPrice, " +
      "            (select atecco_productpriceeffec(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPriceEfec," +
      "            (select atecco_productstocktienda(mps.m_product_id, mw.m_warehouse_id) from dual) AS mProductCantidadStock," +
      "            (select atecco_producttaxt(mps.m_product_id) from dual) AS mProductTax," +
      "            mp.ad_image_id AS mProductImageId  " +
      "        FROM m_product_stock_v mps " +
      "        LEFT JOIN m_product mp ON (mp.m_product_id = mps.m_product_id) " +
      "        LEFT JOIN m_locator ml ON (ml.m_locator_id = mps.m_locator_id) " +
      "        LEFT JOIN m_warehouse mw ON (mw.m_warehouse_id = ml.m_warehouse_id) " +
      "        LEFT JOIN ad_org ao ON (ao.ad_org_id = mw.ad_org_id) " +
      "        LEFT JOIN atecco_familia af ON (af.atecco_familia_id = mp.em_atecco_familia_id) " +
      "        LEFT JOIN atecco_categoria ac ON (ac.atecco_categoria_id = mp.em_atecco_categoria_id) " +
      "        LEFT JOiN ecom_posicion_modelo epm ON (epm.m_product_id = mp.m_product_id) " +
      "        LEFT JOIN ecom_marca_vehiculo emv ON (emv.ecom_marca_vehiculo_id = epm.ecom_marca_vehiculo_id) " +
      "        LEFT JOIN ecom_modelo_marcas emm ON (emm.ecom_modelo_marcas_id = epm.ecom_modelo_marcas_id) " +
      "        LEFT JOIN atecco_sub_categoria sc ON (sc.atecco_sub_categoria_id = mp.em_atecco_sub_categoria_id)" +
      "        LEFT JOIN atecco_generico ag ON (ag.atecco_generico_id = mp.em_atecco_generico_id) " +
      "        LEFT JOIN m_brand mb ON (mb.m_brand_id = mp.m_brand_id) " +
      "        LEFT JOIN m_attributevalue matm ON (matm.m_attributevalue_id = mp.em_atecco_modelo)" +
      "        WHERE mps.ad_client_id = ?  " +
      "            AND coalesce(mw.name,'') LIKE ?" +
      "            AND coalesce(trim(ac.nombre),'') LIKE ?" +
      "            AND upper(coalesce(mb.name,'')) LIKE ?" +
      "        GROUP BY mps.m_product_id, mw.m_warehouse_id, mp.upc, ao.name, mp.name, mp.value, mp.em_atecco_familia_id, af.nombre, " +
      "        mp.em_atecco_categoria_id, ac.nombre, sc.nombre, emv.nombre, emm.modelo, ag.nombre, " +
      "        mb.name, matm.name, mp.em_atecco_codigo_edimca, mp.ad_image_id " +
      "        ORDER BY mp.name,emv.nombre)" +
      "   UNION ALL" +
      "   (SELECT  	" +
      "            mps.m_product_id AS mProductId, " +
      "            ao.name AS mProductOrgName, " +
      "            upper(mp.name) AS mProductName, " +
      "            mw.m_warehouse_id AS mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName,  " +
      "            mp.value AS mProductValue, " +
      "            mp.upc AS mProductUpc,  " +
      "            emv.nombre AS mEcomProductMarcaName, " +
      "            emm.modelo AS mEcomProductModeloName, " +
      "            mp.em_atecco_familia_id AS mProductFamiliaId, " +
      "            af.nombre As mProductFamiliaName, " +
      "            mp.em_atecco_categoria_id AS mProductCategoriaId, " +
      "            ac.nombre AS mProductCategoriaName,  " +
      "            ag.nombre AS mProductGenericoName, " +
      "            mb.name AS mProductMarcaName, " +
      "            sc.nombre AS mProductSubCategoriaName, " +
      "            matm.name AS mProductModeloName, " +
      "            mp.em_atecco_codigo_edimca as mProductReferenceCode, " +
      "            (select atecco_productuni(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPrice, " +
      "            (select atecco_productpriceeffec(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPriceEfec," +
      "            (select atecco_productstocktienda(mps.m_product_id, mw.m_warehouse_id) from dual) AS mProductCantidadStock," +
      "            (select atecco_producttaxt(mps.m_product_id) from dual) AS mProductTax," +
      "            mp.ad_image_id AS mProductImageId  " +
      "        FROM m_product_stock_v mps " +
      "        LEFT JOIN m_product mp ON (mp.m_product_id = mps.m_product_id) " +
      "        LEFT JOIN m_locator ml ON (ml.m_locator_id = mps.m_locator_id) " +
      "        LEFT JOIN m_warehouse mw ON (mw.m_warehouse_id = ml.m_warehouse_id) " +
      "        LEFT JOIN ad_org ao ON (ao.ad_org_id = mw.ad_org_id) " +
      "        LEFT JOIN atecco_familia af ON (af.atecco_familia_id = mp.em_atecco_familia_id) " +
      "        LEFT JOIN atecco_categoria ac ON (ac.atecco_categoria_id = mp.em_atecco_categoria_id) " +
      "        LEFT JOiN ecom_posicion_modelo epm ON (epm.m_product_id = mp.m_product_id) " +
      "        LEFT JOIN ecom_marca_vehiculo emv ON (emv.ecom_marca_vehiculo_id = epm.ecom_marca_vehiculo_id) " +
      "        LEFT JOIN ecom_modelo_marcas emm ON (emm.ecom_modelo_marcas_id = epm.ecom_modelo_marcas_id) " +
      "        LEFT JOIN atecco_sub_categoria sc ON (sc.atecco_sub_categoria_id = mp.em_atecco_sub_categoria_id)  " +
      "        LEFT JOIN atecco_generico ag ON (ag.atecco_generico_id = mp.em_atecco_generico_id) " +
      "        LEFT JOIN m_brand mb ON (mb.m_brand_id = mp.m_brand_id) " +
      "        LEFT JOIN m_attributevalue matm ON (matm.m_attributevalue_id = mp.em_atecco_modelo)" +
      "        WHERE mps.ad_client_id = ?  " +
      "            AND coalesce(mw.name,'') LIKE ?" +
      "            AND coalesce(trim(ac.nombre),'') LIKE ?" +
      "            AND upper(coalesce(trim(mp.upc),'')) LIKE ?" +
      "        GROUP BY mps.m_product_id, mw.m_warehouse_id, mp.upc, ao.name, mp.name, mp.value, mp.em_atecco_familia_id, af.nombre, " +
      "        mp.em_atecco_categoria_id, ac.nombre, sc.nombre, emv.nombre, emm.modelo, ag.nombre, " +
      "        mb.name, matm.name, mp.em_atecco_codigo_edimca, mp.ad_image_id " +
      "        ORDER BY mp.name,emv.nombre)" +
      "        ) as r) as f";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductValue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductMarcaName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUpc);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.mproducmarcamodelo = UtilSql.getValue(result, "mproducmarcamodelo");
        objectFacturaPosData.mproductid = UtilSql.getValue(result, "mproductid");
        objectFacturaPosData.mproductorgname = UtilSql.getValue(result, "mproductorgname");
        objectFacturaPosData.mproductname = UtilSql.getValue(result, "mproductname");
        objectFacturaPosData.mproductwarehouseid = UtilSql.getValue(result, "mproductwarehouseid");
        objectFacturaPosData.mproductwarehousename = UtilSql.getValue(result, "mproductwarehousename");
        objectFacturaPosData.mproductvalue = UtilSql.getValue(result, "mproductvalue");
        objectFacturaPosData.mproductupc = UtilSql.getValue(result, "mproductupc");
        objectFacturaPosData.mecomproductmarcaname = UtilSql.getValue(result, "mecomproductmarcaname");
        objectFacturaPosData.mecomproductmodeloname = UtilSql.getValue(result, "mecomproductmodeloname");
        objectFacturaPosData.mproductfamiliaid = UtilSql.getValue(result, "mproductfamiliaid");
        objectFacturaPosData.mproductfamilianame = UtilSql.getValue(result, "mproductfamilianame");
        objectFacturaPosData.mproductcategoriaid = UtilSql.getValue(result, "mproductcategoriaid");
        objectFacturaPosData.mproductcategorianame = UtilSql.getValue(result, "mproductcategorianame");
        objectFacturaPosData.mproductgenericoname = UtilSql.getValue(result, "mproductgenericoname");
        objectFacturaPosData.mproductmarcaname = UtilSql.getValue(result, "mproductmarcaname");
        objectFacturaPosData.mproductsubcategorianame = UtilSql.getValue(result, "mproductsubcategorianame");
        objectFacturaPosData.mproductmodeloname = UtilSql.getValue(result, "mproductmodeloname");
        objectFacturaPosData.mproductreferencecode = UtilSql.getValue(result, "mproductreferencecode");
        objectFacturaPosData.mproductprice = UtilSql.getValue(result, "mproductprice");
        objectFacturaPosData.mproductpriceefec = UtilSql.getValue(result, "mproductpriceefec");
        objectFacturaPosData.mproductcantidadstock = UtilSql.getValue(result, "mproductcantidadstock");
        objectFacturaPosData.mproducttax = UtilSql.getValue(result, "mproducttax");
        objectFacturaPosData.mproductimageid = UtilSql.getValue(result, "mproductimageid");
        objectFacturaPosData.mproductimage = UtilSql.getValue(result, "mproductimage");
        objectFacturaPosData.mproductstockid = UtilSql.getValue(result, "mproductstockid");
        objectFacturaPosData.mproductinicial = UtilSql.getValue(result, "mproductinicial");
        objectFacturaPosData.mproductpricetotal = UtilSql.getValue(result, "mproductpricetotal");
        objectFacturaPosData.mproducttotalcant = UtilSql.getValue(result, "mproducttotalcant");
        objectFacturaPosData.mproducttotalimp = UtilSql.getValue(result, "mproducttotalimp");
        objectFacturaPosData.mproducttotal = UtilSql.getValue(result, "mproducttotal");
        objectFacturaPosData.mproducttotaldescuento = UtilSql.getValue(result, "mproducttotaldescuento");
        objectFacturaPosData.fpvproductvalue = UtilSql.getValue(result, "fpvproductvalue");
        objectFacturaPosData.fpvmetodopagovid = UtilSql.getValue(result, "fpvmetodopagovid");
        objectFacturaPosData.fpvmetodopagovname = UtilSql.getValue(result, "fpvmetodopagovname");
        objectFacturaPosData.fpvfacturacotid = UtilSql.getValue(result, "fpvfacturacotid");
        objectFacturaPosData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaPosData.fpvproductname = UtilSql.getValue(result, "fpvproductname");
        objectFacturaPosData.fpvproductprice = UtilSql.getValue(result, "fpvproductprice");
        objectFacturaPosData.fpvcantidadordenada = UtilSql.getValue(result, "fpvcantidadordenada");
        objectFacturaPosData.fpvtotallinea = UtilSql.getValue(result, "fpvtotallinea");
        objectFacturaPosData.fpvdocumentno = UtilSql.getValue(result, "fpvdocumentno");
        objectFacturaPosData.stocktienda = UtilSql.getValue(result, "stocktienda");
        objectFacturaPosData.stockcliente = UtilSql.getValue(result, "stockcliente");
        objectFacturaPosData.fpvproductpriceefectivo = UtilSql.getValue(result, "fpvproductpriceefectivo");
        objectFacturaPosData.fpvproductdescuento = UtilSql.getValue(result, "fpvproductdescuento");
        objectFacturaPosData.fpvproductvalordescuento = UtilSql.getValue(result, "fpvproductvalordescuento");
        objectFacturaPosData.nameclient = UtilSql.getValue(result, "nameclient");
        objectFacturaPosData.idenclient = UtilSql.getValue(result, "idenclient");
        objectFacturaPosData.telefonoclient = UtilSql.getValue(result, "telefonoclient");
        objectFacturaPosData.emailclient = UtilSql.getValue(result, "emailclient");
        objectFacturaPosData.direccionclient = UtilSql.getValue(result, "direccionclient");
        objectFacturaPosData.paymentmethodid = UtilSql.getValue(result, "paymentmethodid");
        objectFacturaPosData.paymentmethodname = UtilSql.getValue(result, "paymentmethodname");
        objectFacturaPosData.total = UtilSql.getValue(result, "total");
        objectFacturaPosData.tipoIden = UtilSql.getValue(result, "tipo_iden");
        objectFacturaPosData.rownum = Long.toString(countRecord);
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] selectStockTienda(ConnectionProvider connectionProvider, String mProductId, String mProductWarehouseId)    throws ServletException {
    return selectStockTienda(connectionProvider, mProductId, mProductWarehouseId, 0, 0);
  }

  public static FacturaPosData[] selectStockTienda(ConnectionProvider connectionProvider, String mProductId, String mProductWarehouseId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            SUM(COALESCE(msd.qtyonhand,0)) AS stocktienda " +
      "      FROM m_storage_detail msd " +
      "      INNER JOIN m_locator ml on (ml.m_locator_id=msd.m_locator_id)" +
      "      INNER JOIN m_warehouse mw on (mw.m_warehouse_id=ml.m_warehouse_id) " +
      "      WHERE msd.m_product_id = ?" +
      "      AND mw.m_warehouse_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.stocktienda = UtilSql.getValue(result, "stocktienda");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] selectStockCliente(ConnectionProvider connectionProvider, String mProductId)    throws ServletException {
    return selectStockCliente(connectionProvider, mProductId, 0, 0);
  }

  public static FacturaPosData[] selectStockCliente(ConnectionProvider connectionProvider, String mProductId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            SUM(COALESCE(msd.qtyonhand,0)) AS stockcliente " +
      "      FROM m_storage_detail msd " +
      "      INNER JOIN m_locator ml on (ml.m_locator_id=msd.m_locator_id)" +
      "      WHERE msd.m_product_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.stockcliente = UtilSql.getValue(result, "stockcliente");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String mProductWarehouseId)    throws ServletException {
    return methodSeleccionarProductosOrg(connectionProvider, mProductWarehouseId, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String mProductWarehouseId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            mw.m_warehouse_id as mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName " +
      "      FROM m_warehouse mw " +
      "      WHERE mw.m_warehouse_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.mproductwarehouseid = UtilSql.getValue(result, "mproductwarehouseid");
        objectFacturaPosData.mproductwarehousename = UtilSql.getValue(result, "mproductwarehousename");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodSeleccionarProductosTemporales(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodSeleccionarProductosTemporales(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarProductosTemporales(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            ffc.fpv_factura_cot_lin_id AS fpvFacturaCotLinId, " +
      "            upper(mp.name) AS fpvProductName, " +
      "            mp.value AS fpvProductValue, " +
      "            round(ffc.precio,2) AS fpvProductPrice, " +
      "            ffc.cantidad AS fpvCantidadOrdenada, " +
      "            ffc.subtotal AS fpvTotalLinea, " +
      "            ffc.subtotal AS fpvProductPriceEfectivo," +
      "            ffc.descuento as fpvProductDescuento,  " +
      "            ((precio * cantidad) - subtotal) as fpvProductValorDescuento" +
      "      FROM fpv_factura_cot_lin ffc " +
      "      LEFT JOIN m_product mp ON (ffc.m_product_id = mp.m_product_id) " +
      "      WHERE ffc.fpv_factura_cot_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaPosData.fpvproductname = UtilSql.getValue(result, "fpvproductname");
        objectFacturaPosData.fpvproductvalue = UtilSql.getValue(result, "fpvproductvalue");
        objectFacturaPosData.fpvproductprice = UtilSql.getValue(result, "fpvproductprice");
        objectFacturaPosData.fpvcantidadordenada = UtilSql.getValue(result, "fpvcantidadordenada");
        objectFacturaPosData.fpvtotallinea = UtilSql.getValue(result, "fpvtotallinea");
        objectFacturaPosData.fpvproductpriceefectivo = UtilSql.getValue(result, "fpvproductpriceefectivo");
        objectFacturaPosData.fpvproductdescuento = UtilSql.getValue(result, "fpvproductdescuento");
        objectFacturaPosData.fpvproductvalordescuento = UtilSql.getValue(result, "fpvproductvalordescuento");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String adUserId)    throws ServletException {
    return methodSeleccionarTemporalFacuraCot(connectionProvider, adClientId, adOrgId, adUserId, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_facturacot(?,?,?) as fpvFacturaCotId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvfacturacotid = UtilSql.getValue(result, "fpvfacturacotid");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String fpvFacturaCotId, String mProductStockId, String mWarehouseId, String precio, String cantidad, String subtotal, String descuento)    throws ServletException {
    return methodSeleccionarTemporalFacuraCot(connectionProvider, adClientId, adOrgId, fpvFacturaCotId, mProductStockId, mWarehouseId, precio, cantidad, subtotal, descuento, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String fpvFacturaCotId, String mProductStockId, String mWarehouseId, String precio, String cantidad, String subtotal, String descuento, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_facturacotline(?,?,?,?,?,?,?,?,?) as fpvFacturaCotLinId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductStockId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, precio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descuento);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodActualizarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, String precio, String cantidad, String subtotal)    throws ServletException {
    return methodActualizarTemporalFacuraCot(connectionProvider, fpvFacturaCotLinId, precio, cantidad, subtotal, 0, 0);
  }

  public static FacturaPosData[] methodActualizarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, String precio, String cantidad, String subtotal, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_actua_facturacotline(?,?,?,?) as fpvFacturaCotLinId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotLinId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, precio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotal);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodActualizarMetodoFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId, String finPaymentMethodId)    throws ServletException {
    return methodActualizarMetodoFacuraCot(connectionProvider, fpvFacturaCotId, finPaymentMethodId, 0, 0);
  }

  public static FacturaPosData[] methodActualizarMetodoFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId, String finPaymentMethodId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_updte_metodo(?,?) as fpvFacturaCotId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentMethodId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvfacturacotid = UtilSql.getValue(result, "fpvfacturacotid");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodBorrarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId)    throws ServletException {
    return methodBorrarTemporalFacuraCot(connectionProvider, fpvFacturaCotLinId, 0, 0);
  }

  public static FacturaPosData[] methodBorrarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_dele_facturacotlinea(?) as fpvFacturaCotLinId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotLinId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodSeleccionarTemporalFacuraCli(ConnectionProvider connectionProvider, String fpvFacturaCotId, String mWarehouseId, String finPaymentmethodId, String adUserId)    throws ServletException {
    return methodSeleccionarTemporalFacuraCli(connectionProvider, fpvFacturaCotId, mWarehouseId, finPaymentmethodId, adUserId, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarTemporalFacuraCli(ConnectionProvider connectionProvider, String fpvFacturaCotId, String mWarehouseId, String finPaymentmethodId, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_facturacli(?, ?, ?, ?) as fpvDocumentno  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvdocumentno = UtilSql.getValue(result, "fpvdocumentno");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodMetodoPagoAlto(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return methodMetodoPagoAlto(connectionProvider, adClientId, 0, 0);
  }

  public static FacturaPosData[] methodMetodoPagoAlto(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "      fmp.fpv_metodopago_v_id AS fpvMetodoPagovId, " +
      "      fmp.name AS fpvMetodoPagovName " +
      "      FROM fpv_metodopago_v fmp " +
      "      WHERE fmp.ad_client_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvmetodopagovid = UtilSql.getValue(result, "fpvmetodopagovid");
        objectFacturaPosData.fpvmetodopagovname = UtilSql.getValue(result, "fpvmetodopagovname");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodDatosCliente(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodDatosCliente(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static FacturaPosData[] methodDatosCliente(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT  " +
      "      cb.name as nameClient, " +
      "      cb.value as idenClient, " +
      "      coalesce(cbl.phone,cbl.phone2) as telefonoClient,      " +
      "      us.email as emailClient, " +
      "      cl.address1 as direccionClient " +
      "      FROM fpv_factura_cot fc " +
      "      LEFT JOIN c_bpartner cb ON (fc.c_bpartner_id = cb.c_bpartner_id)" +
      "      LEFT JOIN fpv_tipopersona ftp ON (cb.em_co_natural_juridico = ftp.value AND ftp.isactive = 'Y') " +
      "      LEFT JOIN fpv_tipoidentificacion fti ON (cb.em_co_tipo_identificacion = fti.value AND fti.isactive = 'Y') " +
      "      LEFT JOIN c_bpartner_location cbl ON (cbl.c_bpartner_location_id = fc.c_bpartner_location_id AND cbl.isactive = 'Y') " +
      "      LEFT JOIN ad_user us on (us.ad_user_id = fc.ad_user_id)" +
      "      LEFT JOIN c_location cl ON (cl.c_location_id = cbl.c_location_id)" +
      "      WHERE fc.fpv_factura_cot_id = ?" +
      "      order by cb.created desc limit 1";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.nameclient = UtilSql.getValue(result, "nameclient");
        objectFacturaPosData.idenclient = UtilSql.getValue(result, "idenclient");
        objectFacturaPosData.telefonoclient = UtilSql.getValue(result, "telefonoclient");
        objectFacturaPosData.emailclient = UtilSql.getValue(result, "emailclient");
        objectFacturaPosData.direccionclient = UtilSql.getValue(result, "direccionclient");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodSeleccionarTemporalPedido(ConnectionProvider connectionProvider, String fpvFacturaCotId, String mWarehouseId, String adUserId)    throws ServletException {
    return methodSeleccionarTemporalPedido(connectionProvider, fpvFacturaCotId, mWarehouseId, adUserId, 0, 0);
  }

  public static FacturaPosData[] methodSeleccionarTemporalPedido(ConnectionProvider connectionProvider, String fpvFacturaCotId, String mWarehouseId, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_pedido(?, ?, ?) as fpvDocumentno  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.fpvdocumentno = UtilSql.getValue(result, "fpvdocumentno");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] methodTotales(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodTotales(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static FacturaPosData[] methodTotales(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		       select coalesce(sum(r.cantidad),0) as mProductTotalCant, " +
      "		              coalesce(round(sum(r.subtotal),2),0) as mProductPriceTotal," +
      "		              coalesce(round(sum(r.precio_iva),2),0) as mProductTotalImp, " +
      "		              coalesce(round(sum(total_prod),2),0) as mProductTotal," +
      "		              coalesce(round(sum(r.valor_desc),2),0) as mProductTotalDescuento" +
      "		        from (select cantidad, " +
      "		                     subtotal, " +
      "		                     precio_iva, " +
      "		                     (subtotal + round(precio_iva,4)) as total_prod, " +
      "		                     ((precio * cantidad) - subtotal) as valor_desc       	       " +
      "		                from fpv_factura_cot_lin " +
      "		               where fpv_factura_cot_id = ?) as r";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.mproducttotalcant = UtilSql.getValue(result, "mproducttotalcant");
        objectFacturaPosData.mproductpricetotal = UtilSql.getValue(result, "mproductpricetotal");
        objectFacturaPosData.mproducttotalimp = UtilSql.getValue(result, "mproducttotalimp");
        objectFacturaPosData.mproducttotal = UtilSql.getValue(result, "mproducttotal");
        objectFacturaPosData.mproducttotaldescuento = UtilSql.getValue(result, "mproducttotaldescuento");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static FacturaPosData[] buscarMetodoPago(ConnectionProvider connectionProvider, String adClienteId)    throws ServletException {
    return buscarMetodoPago(connectionProvider, adClienteId, 0, 0);
  }

  public static FacturaPosData[] buscarMetodoPago(ConnectionProvider connectionProvider, String adClienteId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT fpm.fin_paymentmethod_id as paymentmethodId, " +
      "	        (case when fpm.name like 'Transferencia/Dep%' then ('Transferencia / ' || SUBSTRING(fpm.name, 15, 22)) else fpm.name end) AS paymentmethodName" +
      "	     FROM fin_paymentmethod as fpm" +
      "	     WHERE fpm.em_atecco_vizualizacion = 'Y'::bpchar AND fpm.isactive = 'Y'::bpchar" +
      "           and fpm.ad_client_id = ? " +
      "	     ORDER BY fpm.name;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClienteId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.paymentmethodid = UtilSql.getValue(result, "paymentmethodid");
        objectFacturaPosData.paymentmethodname = UtilSql.getValue(result, "paymentmethodname");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }

  public static int methodUpdateMetodoPago(ConnectionProvider connectionProvider, String adclientid, String cotizacionid)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      update fpv_factura_cot " +
      "      set fin_paymentmethod_id = (SELECT fpm.fin_paymentmethod_id" +
      "                                  FROM fin_paymentmethod as fpm" +
      "                                  WHERE fpm.em_atecco_vizualizacion = 'Y' AND fpm.isactive = 'Y'" +
      "                                  and fpm.ad_client_id = ? " +
      "                                  and upper(fpm.name) like 'EFECTIVO' limit 1) " +
      "      where fpv_factura_cot_id = ?;";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adclientid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cotizacionid);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static FacturaPosData[] methodValidarConsumidor(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodValidarConsumidor(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static FacturaPosData[] methodValidarConsumidor(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "		        select round(sum(a.subtotal+a.precio_iva),2) as total, " +
      "		        	   c.em_co_tipo_identificacion as tipo_iden" +
      "				from fpv_factura_cot_lin a" +
      "						inner join fpv_factura_cot b on b.fpv_factura_cot_id=a.fpv_factura_cot_id" +
      "						inner join c_bpartner c on c.c_bpartner_id=b.c_bpartner_id" +
      "				where a.fpv_factura_cot_id= ?" +
      "						group by 2;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaPosData objectFacturaPosData = new FacturaPosData();
        objectFacturaPosData.total = UtilSql.getValue(result, "total");
        objectFacturaPosData.tipoIden = UtilSql.getValue(result, "tipo_iden");
        objectFacturaPosData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaPosData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaPosData objectFacturaPosData[] = new FacturaPosData[vector.size()];
    vector.copyInto(objectFacturaPosData);
    return(objectFacturaPosData);
  }
}
