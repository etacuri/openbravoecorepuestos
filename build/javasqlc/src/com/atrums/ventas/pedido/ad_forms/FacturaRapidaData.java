//Sqlc generated V1.O00-1
package com.atrums.ventas.pedido.ad_forms;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class FacturaRapidaData implements FieldProvider {
static Logger log4j = Logger.getLogger(FacturaRapidaData.class);
  private String InitRecordNumber="0";
  public String mproducmarcamodelo;
  public String mproductstockid;
  public String mproductid;
  public String mproductorgname;
  public String mproductname;
  public String mproductwarehouseid;
  public String mproductwarehousename;
  public String mproductvalue;
  public String mproductupc;
  public String mecomproductmarcaname;
  public String mecomproductmodeloname;
  public String mproductfamiliaid;
  public String mproductfamilianame;
  public String mproducttexturaid;
  public String mproducttexturaname;
  public String mproductcategoriaid;
  public String mproductcategorianame;
  public String mproductgenericoname;
  public String mproductmarcaname;
  public String mproductsubcategorianame;
  public String mproductmodeloname;
  public String mproductinicial;
  public String mproductpricetotal;
  public String fpvproductvalue;
  public String fpvmetodopagovid;
  public String fpvmetodopagovname;
  public String fpvfacturacotid;
  public String fpvfacturacotlinid;
  public String fpvproductname;
  public String fpvproductprice;
  public String fpvcantidadordenada;
  public String fpvtotallinea;
  public String fpvdocumentno;
  public String stocktienda;
  public String stockcliente;
  public String fpvproductpriceefectivo;
  public String mecomproductyear;
  public String mecomproductcilindraje;
  public String mproductreferencecode;
  public String mproductprice;
  public String mproductpriceefec;
  public String mproductcantidadstock;
  public String mproducttax;
  public String mproductimage;
  public String rownum;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("mproducmarcamodelo"))
      return mproducmarcamodelo;
    else if (fieldName.equalsIgnoreCase("mproductstockid"))
      return mproductstockid;
    else if (fieldName.equalsIgnoreCase("mproductid"))
      return mproductid;
    else if (fieldName.equalsIgnoreCase("mproductorgname"))
      return mproductorgname;
    else if (fieldName.equalsIgnoreCase("mproductname"))
      return mproductname;
    else if (fieldName.equalsIgnoreCase("mproductwarehouseid"))
      return mproductwarehouseid;
    else if (fieldName.equalsIgnoreCase("mproductwarehousename"))
      return mproductwarehousename;
    else if (fieldName.equalsIgnoreCase("mproductvalue"))
      return mproductvalue;
    else if (fieldName.equalsIgnoreCase("mproductupc"))
      return mproductupc;
    else if (fieldName.equalsIgnoreCase("mecomproductmarcaname"))
      return mecomproductmarcaname;
    else if (fieldName.equalsIgnoreCase("mecomproductmodeloname"))
      return mecomproductmodeloname;
    else if (fieldName.equalsIgnoreCase("mproductfamiliaid"))
      return mproductfamiliaid;
    else if (fieldName.equalsIgnoreCase("mproductfamilianame"))
      return mproductfamilianame;
    else if (fieldName.equalsIgnoreCase("mproducttexturaid"))
      return mproducttexturaid;
    else if (fieldName.equalsIgnoreCase("mproducttexturaname"))
      return mproducttexturaname;
    else if (fieldName.equalsIgnoreCase("mproductcategoriaid"))
      return mproductcategoriaid;
    else if (fieldName.equalsIgnoreCase("mproductcategorianame"))
      return mproductcategorianame;
    else if (fieldName.equalsIgnoreCase("mproductgenericoname"))
      return mproductgenericoname;
    else if (fieldName.equalsIgnoreCase("mproductmarcaname"))
      return mproductmarcaname;
    else if (fieldName.equalsIgnoreCase("mproductsubcategorianame"))
      return mproductsubcategorianame;
    else if (fieldName.equalsIgnoreCase("mproductmodeloname"))
      return mproductmodeloname;
    else if (fieldName.equalsIgnoreCase("mproductinicial"))
      return mproductinicial;
    else if (fieldName.equalsIgnoreCase("mproductpricetotal"))
      return mproductpricetotal;
    else if (fieldName.equalsIgnoreCase("fpvproductvalue"))
      return fpvproductvalue;
    else if (fieldName.equalsIgnoreCase("fpvmetodopagovid"))
      return fpvmetodopagovid;
    else if (fieldName.equalsIgnoreCase("fpvmetodopagovname"))
      return fpvmetodopagovname;
    else if (fieldName.equalsIgnoreCase("fpvfacturacotid"))
      return fpvfacturacotid;
    else if (fieldName.equalsIgnoreCase("fpvfacturacotlinid"))
      return fpvfacturacotlinid;
    else if (fieldName.equalsIgnoreCase("fpvproductname"))
      return fpvproductname;
    else if (fieldName.equalsIgnoreCase("fpvproductprice"))
      return fpvproductprice;
    else if (fieldName.equalsIgnoreCase("fpvcantidadordenada"))
      return fpvcantidadordenada;
    else if (fieldName.equalsIgnoreCase("fpvtotallinea"))
      return fpvtotallinea;
    else if (fieldName.equalsIgnoreCase("fpvdocumentno"))
      return fpvdocumentno;
    else if (fieldName.equalsIgnoreCase("stocktienda"))
      return stocktienda;
    else if (fieldName.equalsIgnoreCase("stockcliente"))
      return stockcliente;
    else if (fieldName.equalsIgnoreCase("fpvproductpriceefectivo"))
      return fpvproductpriceefectivo;
    else if (fieldName.equalsIgnoreCase("mecomproductyear"))
      return mecomproductyear;
    else if (fieldName.equalsIgnoreCase("mecomproductcilindraje"))
      return mecomproductcilindraje;
    else if (fieldName.equalsIgnoreCase("mproductreferencecode"))
      return mproductreferencecode;
    else if (fieldName.equalsIgnoreCase("mproductprice"))
      return mproductprice;
    else if (fieldName.equalsIgnoreCase("mproductpriceefec"))
      return mproductpriceefec;
    else if (fieldName.equalsIgnoreCase("mproductcantidadstock"))
      return mproductcantidadstock;
    else if (fieldName.equalsIgnoreCase("mproducttax"))
      return mproducttax;
    else if (fieldName.equalsIgnoreCase("mproductimage"))
      return mproductimage;
    else if (fieldName.equals("rownum"))
      return rownum;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static FacturaRapidaData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String adClientId, String mProductWarehouseName, String mProductName, String mProductValue, String mProductSubCategoriaName, String mProductFamiliaId, String mProductCategoriaId, String mProductMacaId, String mProductModeloId, String mProductReferenceCode)    throws ServletException {
    return methodSeleccionarProductosOrg(connectionProvider, adClientId, mProductWarehouseName, mProductName, mProductValue, mProductSubCategoriaName, mProductFamiliaId, mProductCategoriaId, mProductMacaId, mProductModeloId, mProductReferenceCode, 0, 0);
  }

  public static FacturaRapidaData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String adClientId, String mProductWarehouseName, String mProductName, String mProductValue, String mProductSubCategoriaName, String mProductFamiliaId, String mProductCategoriaId, String mProductMacaId, String mProductModeloId, String mProductReferenceCode, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	    SELECT " +
      "	distinct(mps.m_product_id || '-' || coalesce(emv.nombre, '') || '-' || coalesce(emm.modelo, '')) AS mProducMarcaModelo," +
      "	get_uuid() AS mProductStockId, 	" +
      "            mps.m_product_id AS mProductId, " +
      "            ao.name AS mProductOrgName, " +
      "            mp.name AS mProductName, " +
      "            mw.m_warehouse_id AS mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName,  " +
      "            mp.value AS mProductValue, " +
      "            mp.upc AS mProductUpc,  " +
      "            emv.nombre AS mEcomProductMarcaName, " +
      "            emm.modelo AS mEcomProductModeloName, " +
      "            mp.em_atecco_familia_id AS mProductFamiliaId, " +
      "            af.nombre As mProductFamiliaName, " +
      "            mp.em_atecco_textura AS mProductTexturaId, " +
      "            mat.name AS mProductTexturaName,  " +
      "            mp.em_atecco_categoria_id AS mProductCategoriaId, " +
      "            ac.nombre AS mProductCategoriaName,  " +
      "            ag.nombre AS mProductGenericoName, " +
      "            mb.name AS mProductMarcaName, " +
      "            sc.nombre AS mProductSubCategoriaName, " +
      "            matm.name AS mProductModeloName, " +
      "            0 AS mProductInicial, " +
      "            0 AS mProductPriceTotal, " +
      "            null AS fpvProductValue, " +
      "            null AS fpvMetodoPagovId, " +
      "            null AS fpvMetodoPagovName, " +
      "            null AS fpvFacturaCotId, " +
      "            null AS fpvFacturaCotLinId, " +
      "            null AS fpvProductName, " +
      "            null AS fpvProductPrice, " +
      "            null AS fpvCantidadOrdenada, " +
      "            null AS fpvTotalLinea, " +
      "            null AS fpvDocumentno, " +
      "            0 AS stocktienda, " +
      "            0 AS stockcliente, " +
      "            0 AS fpvProductPriceEfectivo, " +
      "            0 AS mEcomProductYear, " +
      "            0 AS mEcomProductCilindraje," +
      "            mp.em_atecco_codigo_edimca as mProductReferenceCode, " +
      "            (select atecco_productprice(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPrice, " +
      "            (select atecco_productpriceeffec(mps.m_product_id, mw.ad_org_id) from dual) AS mProductPriceEfec," +
      "            (select atecco_productstocktienda(mps.m_product_id, mw.m_warehouse_id) from dual) AS mProductCantidadStock," +
      "            (select atecco_producttaxt(mps.m_product_id) from dual) AS mProductTax," +
      "            (case when mp.ad_image_id <> 'null' then" +
      "                       ('<img src=../utility/ShowImage?id='|| mp.ad_image_id ||'&amp;nocache='|| to_char(now(),'dd/mm/yy-HH24:MI:SS') ||' width=50 height=50 />') " +
      "                  else ('<img src=../utility/ShowImage?id=0 width=50 height=50 />') end) AS mProductImage  " +
      "        FROM m_product_stock_v mps " +
      "        LEFT JOIN m_product mp ON (mp.m_product_id = mps.m_product_id) " +
      "        LEFT JOIN m_locator ml ON (ml.m_locator_id = mps.m_locator_id) " +
      "        LEFT JOIN m_warehouse mw ON (mw.m_warehouse_id = ml.m_warehouse_id) " +
      "        LEFT JOIN ad_org ao ON (ao.ad_org_id = mw.ad_org_id) " +
      "        LEFT JOIN atecco_familia af ON (af.atecco_familia_id = mp.em_atecco_familia_id) " +
      "        LEFT JOIN atecco_categoria ac ON (ac.atecco_categoria_id = mp.em_atecco_categoria_id) " +
      "        LEFT JOiN ecom_posicion_modelo epm ON (epm.m_product_id = mp.m_product_id) " +
      "        LEFT JOIN ecom_marca_vehiculo emv ON (emv.ecom_marca_vehiculo_id = epm.ecom_marca_vehiculo_id) " +
      "        LEFT JOIN ecom_modelo_marcas emm ON (emm.ecom_modelo_marcas_id = epm.ecom_modelo_marcas_id) " +
      "        LEFT JOIN atecco_sub_categoria sc ON (sc.atecco_sub_categoria_id = mp.em_atecco_sub_categoria_id) " +
      "        LEFT JOIN m_attributevalue mat ON (mat.m_attributevalue_id = mp.em_atecco_textura) " +
      "        LEFT JOIN atecco_generico ag ON (ag.atecco_generico_id = mp.em_atecco_generico_id) " +
      "        LEFT JOIN m_brand mb ON (mb.m_brand_id = mp.m_brand_id) " +
      "        LEFT JOIN m_attributevalue matm ON (matm.m_attributevalue_id = mp.em_atecco_modelo)" +
      "        WHERE mps.ad_client_id = ? " +
      "            AND coalesce(mw.name,'') LIKE ? " +
      "            AND coalesce(mp.name,'') LIKE ?  " +
      "            AND coalesce(mp.value,'') LIKE ?  " +
      "            AND coalesce(sc.nombre,'') LIKE ? " +
      "            AND coalesce(af.atecco_familia_id,'') LIKE ? " +
      "            AND coalesce(trim(ac.nombre),'') LIKE ? " +
      "            AND coalesce(mb.m_brand_id,'') LIKE ? " +
      "            AND coalesce(trim(emm.modelo),'') LIKE ?" +
      "            AND coalesce(trim(mp.em_atecco_codigo_edimca),'') LIKE ? " +
      "        GROUP BY mps.m_product_id, " +
      "        mw.m_warehouse_id, " +
      "        mp.upc, " +
      "        ao.name, " +
      "        mp.name, " +
      "        mp.value, " +
      "        mp.em_atecco_familia_id, " +
      "        af.nombre, " +
      "        mp.em_atecco_categoria_id, " +
      "        ac.nombre, " +
      "        sc.nombre, " +
      "        emv.nombre, " +
      "        emm.modelo, " +
      "        mp.em_atecco_textura, " +
      "        mat.name, " +
      "        ag.nombre, " +
      "        mb.name, " +
      "        matm.name," +
      "        mp.em_atecco_codigo_edimca," +
      "        mp.ad_image_id " +
      "        ORDER BY mp.name,emv.nombre, emm.modelo;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductValue);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductSubCategoriaName);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductFamiliaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductCategoriaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductMacaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductModeloId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductReferenceCode);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.mproducmarcamodelo = UtilSql.getValue(result, "mproducmarcamodelo");
        objectFacturaRapidaData.mproductstockid = UtilSql.getValue(result, "mproductstockid");
        objectFacturaRapidaData.mproductid = UtilSql.getValue(result, "mproductid");
        objectFacturaRapidaData.mproductorgname = UtilSql.getValue(result, "mproductorgname");
        objectFacturaRapidaData.mproductname = UtilSql.getValue(result, "mproductname");
        objectFacturaRapidaData.mproductwarehouseid = UtilSql.getValue(result, "mproductwarehouseid");
        objectFacturaRapidaData.mproductwarehousename = UtilSql.getValue(result, "mproductwarehousename");
        objectFacturaRapidaData.mproductvalue = UtilSql.getValue(result, "mproductvalue");
        objectFacturaRapidaData.mproductupc = UtilSql.getValue(result, "mproductupc");
        objectFacturaRapidaData.mecomproductmarcaname = UtilSql.getValue(result, "mecomproductmarcaname");
        objectFacturaRapidaData.mecomproductmodeloname = UtilSql.getValue(result, "mecomproductmodeloname");
        objectFacturaRapidaData.mproductfamiliaid = UtilSql.getValue(result, "mproductfamiliaid");
        objectFacturaRapidaData.mproductfamilianame = UtilSql.getValue(result, "mproductfamilianame");
        objectFacturaRapidaData.mproducttexturaid = UtilSql.getValue(result, "mproducttexturaid");
        objectFacturaRapidaData.mproducttexturaname = UtilSql.getValue(result, "mproducttexturaname");
        objectFacturaRapidaData.mproductcategoriaid = UtilSql.getValue(result, "mproductcategoriaid");
        objectFacturaRapidaData.mproductcategorianame = UtilSql.getValue(result, "mproductcategorianame");
        objectFacturaRapidaData.mproductgenericoname = UtilSql.getValue(result, "mproductgenericoname");
        objectFacturaRapidaData.mproductmarcaname = UtilSql.getValue(result, "mproductmarcaname");
        objectFacturaRapidaData.mproductsubcategorianame = UtilSql.getValue(result, "mproductsubcategorianame");
        objectFacturaRapidaData.mproductmodeloname = UtilSql.getValue(result, "mproductmodeloname");
        objectFacturaRapidaData.mproductinicial = UtilSql.getValue(result, "mproductinicial");
        objectFacturaRapidaData.mproductpricetotal = UtilSql.getValue(result, "mproductpricetotal");
        objectFacturaRapidaData.fpvproductvalue = UtilSql.getValue(result, "fpvproductvalue");
        objectFacturaRapidaData.fpvmetodopagovid = UtilSql.getValue(result, "fpvmetodopagovid");
        objectFacturaRapidaData.fpvmetodopagovname = UtilSql.getValue(result, "fpvmetodopagovname");
        objectFacturaRapidaData.fpvfacturacotid = UtilSql.getValue(result, "fpvfacturacotid");
        objectFacturaRapidaData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaRapidaData.fpvproductname = UtilSql.getValue(result, "fpvproductname");
        objectFacturaRapidaData.fpvproductprice = UtilSql.getValue(result, "fpvproductprice");
        objectFacturaRapidaData.fpvcantidadordenada = UtilSql.getValue(result, "fpvcantidadordenada");
        objectFacturaRapidaData.fpvtotallinea = UtilSql.getValue(result, "fpvtotallinea");
        objectFacturaRapidaData.fpvdocumentno = UtilSql.getValue(result, "fpvdocumentno");
        objectFacturaRapidaData.stocktienda = UtilSql.getValue(result, "stocktienda");
        objectFacturaRapidaData.stockcliente = UtilSql.getValue(result, "stockcliente");
        objectFacturaRapidaData.fpvproductpriceefectivo = UtilSql.getValue(result, "fpvproductpriceefectivo");
        objectFacturaRapidaData.mecomproductyear = UtilSql.getValue(result, "mecomproductyear");
        objectFacturaRapidaData.mecomproductcilindraje = UtilSql.getValue(result, "mecomproductcilindraje");
        objectFacturaRapidaData.mproductreferencecode = UtilSql.getValue(result, "mproductreferencecode");
        objectFacturaRapidaData.mproductprice = UtilSql.getValue(result, "mproductprice");
        objectFacturaRapidaData.mproductpriceefec = UtilSql.getValue(result, "mproductpriceefec");
        objectFacturaRapidaData.mproductcantidadstock = UtilSql.getValue(result, "mproductcantidadstock");
        objectFacturaRapidaData.mproducttax = UtilSql.getValue(result, "mproducttax");
        objectFacturaRapidaData.mproductimage = UtilSql.getValue(result, "mproductimage");
        objectFacturaRapidaData.rownum = Long.toString(countRecord);
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] selectStockTienda(ConnectionProvider connectionProvider, String mProductId, String mProductWarehouseId)    throws ServletException {
    return selectStockTienda(connectionProvider, mProductId, mProductWarehouseId, 0, 0);
  }

  public static FacturaRapidaData[] selectStockTienda(ConnectionProvider connectionProvider, String mProductId, String mProductWarehouseId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            SUM(COALESCE(msd.qtyonhand,0)) AS stocktienda " +
      "      FROM m_storage_detail msd " +
      "      INNER JOIN m_locator ml on (ml.m_locator_id=msd.m_locator_id)" +
      "      INNER JOIN m_warehouse mw on (mw.m_warehouse_id=ml.m_warehouse_id) " +
      "      WHERE msd.m_product_id = ?" +
      "      AND mw.m_warehouse_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.stocktienda = UtilSql.getValue(result, "stocktienda");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] selectStockCliente(ConnectionProvider connectionProvider, String mProductId)    throws ServletException {
    return selectStockCliente(connectionProvider, mProductId, 0, 0);
  }

  public static FacturaRapidaData[] selectStockCliente(ConnectionProvider connectionProvider, String mProductId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            SUM(COALESCE(msd.qtyonhand,0)) AS stockcliente " +
      "      FROM m_storage_detail msd " +
      "      INNER JOIN m_locator ml on (ml.m_locator_id=msd.m_locator_id)" +
      "      WHERE msd.m_product_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.stockcliente = UtilSql.getValue(result, "stockcliente");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String mProductWarehouseId)    throws ServletException {
    return methodSeleccionarProductosOrg(connectionProvider, mProductWarehouseId, 0, 0);
  }

  public static FacturaRapidaData[] methodSeleccionarProductosOrg(ConnectionProvider connectionProvider, String mProductWarehouseId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            mw.m_warehouse_id as mProductWarehouseId, " +
      "            mw.name AS mProductWarehouseName " +
      "      FROM m_warehouse mw " +
      "      WHERE mw.m_warehouse_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductWarehouseId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.mproductwarehouseid = UtilSql.getValue(result, "mproductwarehouseid");
        objectFacturaRapidaData.mproductwarehousename = UtilSql.getValue(result, "mproductwarehousename");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodSeleccionarProductosTemporales(ConnectionProvider connectionProvider, String fpvFacturaCotLinId)    throws ServletException {
    return methodSeleccionarProductosTemporales(connectionProvider, fpvFacturaCotLinId, 0, 0);
  }

  public static FacturaRapidaData[] methodSeleccionarProductosTemporales(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "            ffc.fpv_factura_cot_lin_id AS fpvFacturaCotLinId, " +
      "            mp.name AS fpvProductName, " +
      "            mp.value AS fpvProductValue, " +
      "            ffc.precio_iva AS fpvProductPrice, " +
      "            ffc.cantidad AS fpvCantidadOrdenada, " +
      "            ffc.subtotal AS fpvTotalLinea, " +
      "           (ffc.precio_iva * ffc.cantidad) AS fpvProductPriceEfectivo  " +
      "      FROM fpv_factura_cot_lin ffc " +
      "      LEFT JOIN m_product mp ON (ffc.m_product_id = mp.m_product_id) " +
      "      WHERE ffc.fpv_factura_cot_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotLinId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaRapidaData.fpvproductname = UtilSql.getValue(result, "fpvproductname");
        objectFacturaRapidaData.fpvproductvalue = UtilSql.getValue(result, "fpvproductvalue");
        objectFacturaRapidaData.fpvproductprice = UtilSql.getValue(result, "fpvproductprice");
        objectFacturaRapidaData.fpvcantidadordenada = UtilSql.getValue(result, "fpvcantidadordenada");
        objectFacturaRapidaData.fpvtotallinea = UtilSql.getValue(result, "fpvtotallinea");
        objectFacturaRapidaData.fpvproductpriceefectivo = UtilSql.getValue(result, "fpvproductpriceefectivo");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String adUserId)    throws ServletException {
    return methodSeleccionarTemporalFacuraCot(connectionProvider, adClientId, adOrgId, adUserId, 0, 0);
  }

  public static FacturaRapidaData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_facturacot(?,?,?) as fpvFacturaCotId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvfacturacotid = UtilSql.getValue(result, "fpvfacturacotid");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String fpvFacturaCotId, String mProductStockId, String mWarehouseId, String precio, String cantidad, String subtotal, String descuento)    throws ServletException {
    return methodSeleccionarTemporalFacuraCot(connectionProvider, adClientId, adOrgId, fpvFacturaCotId, mProductStockId, mWarehouseId, precio, cantidad, subtotal, descuento, 0, 0);
  }

  public static FacturaRapidaData[] methodSeleccionarTemporalFacuraCot(ConnectionProvider connectionProvider, String adClientId, String adOrgId, String fpvFacturaCotId, String mProductStockId, String mWarehouseId, String precio, String cantidad, String subtotal, String descuento, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_facturacotlinea(?,?,?,?,?,?,?,?,?) as fpvFacturaCotLinId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductStockId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, precio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, descuento);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodActualizarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, String precio, String cantidad, String subtotal)    throws ServletException {
    return methodActualizarTemporalFacuraCot(connectionProvider, fpvFacturaCotLinId, precio, cantidad, subtotal, 0, 0);
  }

  public static FacturaRapidaData[] methodActualizarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, String precio, String cantidad, String subtotal, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_actua_facturacotlinea(?,?,?,?) as fpvFacturaCotLinId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotLinId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, precio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cantidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, subtotal);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodActualizarMetodoFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId, String finPaymentMethodId)    throws ServletException {
    return methodActualizarMetodoFacuraCot(connectionProvider, fpvFacturaCotId, finPaymentMethodId, 0, 0);
  }

  public static FacturaRapidaData[] methodActualizarMetodoFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId, String finPaymentMethodId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_updte_metodo(?,?) as fpvFacturaCotId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentMethodId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvfacturacotid = UtilSql.getValue(result, "fpvfacturacotid");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodBorrarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId)    throws ServletException {
    return methodBorrarTemporalFacuraCot(connectionProvider, fpvFacturaCotLinId, 0, 0);
  }

  public static FacturaRapidaData[] methodBorrarTemporalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotLinId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_dele_facturacotlinea(?) as fpvFacturaCotLinId  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotLinId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvfacturacotlinid = UtilSql.getValue(result, "fpvfacturacotlinid");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodTotalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodTotalFacuraCot(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static FacturaRapidaData[] methodTotalFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT atecco_totalproductprice(?) as mProductPriceTotal  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.mproductpricetotal = UtilSql.getValue(result, "mproductpricetotal");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodTotalEfecFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId)    throws ServletException {
    return methodTotalEfecFacuraCot(connectionProvider, fpvFacturaCotId, 0, 0);
  }

  public static FacturaRapidaData[] methodTotalEfecFacuraCot(ConnectionProvider connectionProvider, String fpvFacturaCotId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT atecco_totalefecproductprice(?) as mProductPriceTotal  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.mproductpricetotal = UtilSql.getValue(result, "mproductpricetotal");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodSeleccionarTemporalFacuraCli(ConnectionProvider connectionProvider, String fpvFacturaCotId, String mWarehouseId, String finPaymentmethodId, String adUserId)    throws ServletException {
    return methodSeleccionarTemporalFacuraCli(connectionProvider, fpvFacturaCotId, mWarehouseId, finPaymentmethodId, adUserId, 0, 0);
  }

  public static FacturaRapidaData[] methodSeleccionarTemporalFacuraCli(ConnectionProvider connectionProvider, String fpvFacturaCotId, String mWarehouseId, String finPaymentmethodId, String adUserId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT fpv_crear_facturacli(?, ?, ?, ?) as fpvDocumentno  " +
      "      FROM dual;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fpvFacturaCotId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvdocumentno = UtilSql.getValue(result, "fpvdocumentno");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }

  public static FacturaRapidaData[] methodMetodoPagoAlto(ConnectionProvider connectionProvider, String adClientId)    throws ServletException {
    return methodMetodoPagoAlto(connectionProvider, adClientId, 0, 0);
  }

  public static FacturaRapidaData[] methodMetodoPagoAlto(ConnectionProvider connectionProvider, String adClientId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT " +
      "      fmp.fpv_metodopago_v_id AS fpvMetodoPagovId, " +
      "      fmp.name AS fpvMetodoPagovName " +
      "      FROM fpv_metodopago_v fmp " +
      "      WHERE fmp.ad_client_id = ?;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        FacturaRapidaData objectFacturaRapidaData = new FacturaRapidaData();
        objectFacturaRapidaData.fpvmetodopagovid = UtilSql.getValue(result, "fpvmetodopagovid");
        objectFacturaRapidaData.fpvmetodopagovname = UtilSql.getValue(result, "fpvmetodopagovname");
        objectFacturaRapidaData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectFacturaRapidaData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    FacturaRapidaData objectFacturaRapidaData[] = new FacturaRapidaData[vector.size()];
    vector.copyInto(objectFacturaRapidaData);
    return(objectFacturaRapidaData);
  }
}
