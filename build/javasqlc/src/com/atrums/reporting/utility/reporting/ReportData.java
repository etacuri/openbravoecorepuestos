//Sqlc generated V1.O00-1
package com.atrums.reporting.utility.reporting;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ReportData implements FieldProvider {
static Logger log4j = Logger.getLogger(ReportData.class);
  private String InitRecordNumber="0";
  public String adOrgId;
  public String documentId;
  public String docstatus;
  public String doctypetargetid;
  public String ourreference;
  public String cusreference;
  public String bpartnerId;
  public String bpartnerLanguage;
  public String issalesordertransaction;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("document_id") || fieldName.equals("documentId"))
      return documentId;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("doctypetargetid"))
      return doctypetargetid;
    else if (fieldName.equalsIgnoreCase("ourreference"))
      return ourreference;
    else if (fieldName.equalsIgnoreCase("cusreference"))
      return cusreference;
    else if (fieldName.equalsIgnoreCase("bpartner_id") || fieldName.equals("bpartnerId"))
      return bpartnerId;
    else if (fieldName.equalsIgnoreCase("bpartner_language") || fieldName.equals("bpartnerLanguage"))
      return bpartnerLanguage;
    else if (fieldName.equalsIgnoreCase("issalesordertransaction"))
      return issalesordertransaction;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ReportData[] dummy(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    return dummy(connectionProvider, cOrderId, 0, 0);
  }

  public static ReportData[] dummy(ConnectionProvider connectionProvider, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "			select" +
      "				'' as ad_Org_Id," +
      "				'' as document_id," +
      "				'' as docstatus," +
      "				'' as docTypeTargetId," +
      "				'' as ourreference," +
      "				'' as cusreference," +
      "				'' as bpartner_id," +
      "				'' as bpartner_language," +
      "				'' as isSalesOrderTransaction" +
      "			from" +
      "				c_order" +
      "			where" +
      "				1=1";
    strSql = strSql + ((cOrderId==null || cOrderId.equals(""))?"":"  			 and c_order.c_order_id in  		" + cOrderId);

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (cOrderId != null && !(cOrderId.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getValue(result, "ourreference");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }

  public static ReportData[] getRetencionInfo(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return getRetencionInfo(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ReportData[] getRetencionInfo(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "            select" +
      "                co_retencion_compra.ad_org_id," +
      "                co_retencion_compra.c_invoice_id as document_id," +
      "                co_retencion_compra.docstatus," +
      "                co_retencion_compra.c_doctype_id as docTypeTargetId," +
      "                co_retencion_compra.documentno as ourreference," +
      "                null as cusreference," +
      "                co_retencion_compra.c_bpartner_id as bpartner_id," +
      "                'N' as isSalesOrderTransaction," +
      "                c_bpartner.ad_language as bpartner_language" +
      "           from co_retencion_compra left join c_doctype on co_retencion_compra.c_doctype_id = c_doctype.c_doctype_id" +
      "                                    left join c_invoice on co_retencion_compra.c_invoice_id = c_invoice.c_invoice_id" +
      "                                    left join c_bpartner on c_invoice.c_bpartner_id = c_bpartner.c_bpartner_id" +
      "            where co_retencion_compra.co_retencion_compra_id = ?" +
      "              and co_retencion_compra.docstatus='CO'";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getValue(result, "ourreference");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }

  public static ReportData[] getMovimientoInfo(ConnectionProvider connectionProvider, String mMovementId)    throws ServletException {
    return getMovimientoInfo(connectionProvider, mMovementId, 0, 0);
  }

  public static ReportData[] getMovimientoInfo(ConnectionProvider connectionProvider, String mMovementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                select  m.ad_org_id," +
      "                        m.m_movement_id as document_id," +
      "                        'CO' as docstatus," +
      "                        m.EM_Co_Doctype_ID as docTypeTargetId," +
      "                        m.documentno as ourreference," +
      "                        null as cusreference," +
      "                        null as bpartner_id," +
      "                        'N' as isSalesOrderTransaction," +
      "                        'es_EC' as bpartner_language" +
      "                   from M_MOVEMENT m" +
      "                  where m.m_movement_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mMovementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getValue(result, "ourreference");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }

  public static ReportData[] getCierreCajaInfo(ConnectionProvider connectionProvider, String ateccoCierrecajaId)    throws ServletException {
    return getCierreCajaInfo(connectionProvider, ateccoCierrecajaId, 0, 0);
  }

  public static ReportData[] getCierreCajaInfo(ConnectionProvider connectionProvider, String ateccoCierrecajaId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "                select  c.ad_org_id," +
      "                        c.atecco_cierrecaja_id as document_id," +
      "                        'CO' as docstatus," +
      "                        c.c_doctype_id as docTypeTargetId," +
      "                        c.fecha_apertura as ourreference," +
      "                        null as cusreference," +
      "                        null as bpartner_id," +
      "                        'N' as isSalesOrderTransaction," +
      "                        'es_EC' as bpartner_language" +
      "                   from atecco_cierrecaja c" +
      "                  where c.atecco_cierrecaja_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ReportData objectReportData = new ReportData();
        objectReportData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectReportData.documentId = UtilSql.getValue(result, "document_id");
        objectReportData.docstatus = UtilSql.getValue(result, "docstatus");
        objectReportData.doctypetargetid = UtilSql.getValue(result, "doctypetargetid");
        objectReportData.ourreference = UtilSql.getDateValue(result, "ourreference", "dd-MM-yyyy");
        objectReportData.cusreference = UtilSql.getValue(result, "cusreference");
        objectReportData.bpartnerId = UtilSql.getValue(result, "bpartner_id");
        objectReportData.issalesordertransaction = UtilSql.getValue(result, "issalesordertransaction");
        objectReportData.bpartnerLanguage = UtilSql.getValue(result, "bpartner_language");
        objectReportData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectReportData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ReportData objectReportData[] = new ReportData[vector.size()];
    vector.copyInto(objectReportData);
    return(objectReportData);
  }
}
