//Sqlc generated V1.O00-1
package com.atrums.compras.montos.ad_callouts;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ATECCOOrderProductData implements FieldProvider {
static Logger log4j = Logger.getLogger(ATECCOOrderProductData.class);
  private String InitRecordNumber="0";
  public String id;
  public String dateordered;
  public String cBpartnerId;
  public String mPricelistId;
  public String dateinvoiced;
  public String stocktienda;
  public String stockcliente;
  public String precioefectivo;
  public String totalefectivo;
  public String preciotarjeta;
  public String totaltarjeta;
  public String loc1;
  public String loc2;
  public String cTaxId;
  public String qtyonhand;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("id"))
      return id;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("dateinvoiced"))
      return dateinvoiced;
    else if (fieldName.equalsIgnoreCase("stocktienda"))
      return stocktienda;
    else if (fieldName.equalsIgnoreCase("stockcliente"))
      return stockcliente;
    else if (fieldName.equalsIgnoreCase("precioefectivo"))
      return precioefectivo;
    else if (fieldName.equalsIgnoreCase("totalefectivo"))
      return totalefectivo;
    else if (fieldName.equalsIgnoreCase("preciotarjeta"))
      return preciotarjeta;
    else if (fieldName.equalsIgnoreCase("totaltarjeta"))
      return totaltarjeta;
    else if (fieldName.equalsIgnoreCase("loc1"))
      return loc1;
    else if (fieldName.equalsIgnoreCase("loc2"))
      return loc2;
    else if (fieldName.equalsIgnoreCase("c_tax_id") || fieldName.equals("cTaxId"))
      return cTaxId;
    else if (fieldName.equalsIgnoreCase("qtyonhand"))
      return qtyonhand;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ATECCOOrderProductData[] select(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    return select(connectionProvider, cOrderId, 0, 0);
  }

  public static ATECCOOrderProductData[] select(ConnectionProvider connectionProvider, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT C_ORDER_ID AS ID, DATEORDERED, C_BPARTNER_ID, M_PRICELIST_ID, " +
      "        '' AS DATEINVOICED, " +
      "        '' AS STOCKTIENDA, " +
      "        '' AS STOCKCLIENTE, " +
      "        '' AS PRECIOEFECTIVO, " +
      "        '' AS TOTALEFECTIVO," +
      "        '' AS PRECIOTARJETA, " +
      "        '' AS TOTALTARJETA, " +
      "        '' AS LOC1," +
      "        '' AS LOC2, " +
      "        '' AS C_TAX_ID,  " +
      "        0 AS qtyonhand " +
      "        FROM C_ORDER WHERE C_ORDER_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.id = UtilSql.getValue(result, "id");
        objectATECCOOrderProductData.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectATECCOOrderProductData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectATECCOOrderProductData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectATECCOOrderProductData.dateinvoiced = UtilSql.getValue(result, "dateinvoiced");
        objectATECCOOrderProductData.stocktienda = UtilSql.getValue(result, "stocktienda");
        objectATECCOOrderProductData.stockcliente = UtilSql.getValue(result, "stockcliente");
        objectATECCOOrderProductData.precioefectivo = UtilSql.getValue(result, "precioefectivo");
        objectATECCOOrderProductData.totalefectivo = UtilSql.getValue(result, "totalefectivo");
        objectATECCOOrderProductData.preciotarjeta = UtilSql.getValue(result, "preciotarjeta");
        objectATECCOOrderProductData.totaltarjeta = UtilSql.getValue(result, "totaltarjeta");
        objectATECCOOrderProductData.loc1 = UtilSql.getValue(result, "loc1");
        objectATECCOOrderProductData.loc2 = UtilSql.getValue(result, "loc2");
        objectATECCOOrderProductData.cTaxId = UtilSql.getValue(result, "c_tax_id");
        objectATECCOOrderProductData.qtyonhand = UtilSql.getValue(result, "qtyonhand");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static ATECCOOrderProductData[] selectStockTienda(ConnectionProvider connectionProvider, String mProductId, String cOrderId)    throws ServletException {
    return selectStockTienda(connectionProvider, mProductId, cOrderId, 0, 0);
  }

  public static ATECCOOrderProductData[] selectStockTienda(ConnectionProvider connectionProvider, String mProductId, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select sum(coalesce(msd.qtyonhand,0)) AS STOCKTIENDA " +
      "        from m_storage_detail msd " +
      "        inner join m_locator ml on (ml.m_locator_id=msd.m_locator_id)" +
      "        inner join m_warehouse mw on (mw.m_warehouse_id=ml.m_warehouse_id) " +
      "		inner join ad_org_warehouse aow on (aow.m_warehouse_id = mw.m_warehouse_id) " +
      "        where msd.m_product_id=? " +
      "        and aow.ad_org_id IN (SELECT o.ad_org_id FROM c_order AS o WHERE o.c_order_id=?)";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.stocktienda = UtilSql.getValue(result, "stocktienda");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static ATECCOOrderProductData[] selectLocatorOrgDes(ConnectionProvider connectionProvider, String mProductId1, String mProductId2, String mMovementId)    throws ServletException {
    return selectLocatorOrgDes(connectionProvider, mProductId1, mProductId2, mMovementId, 0, 0);
  }

  public static ATECCOOrderProductData[] selectLocatorOrgDes(ConnectionProvider connectionProvider, String mProductId1, String mProductId2, String mMovementId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT apu1.m_locator_id AS LOC1, " +
      "         apu2.m_locator_id AS LOC2, " +
      "         apu1.qtyonhand  " +
      "         FROM m_movement mm " +
      "         LEFT JOIN m_product_stock_v apu1 ON (apu1.m_product_id = ? AND apu1.m_locator_id IN (SELECT ml.m_locator_id " +
      "         FROM m_locator ml " +
      "         WHERE ml.m_warehouse_id = mm.em_atecco_warehouse_id)) " +
      "         LEFT JOIN atecco_product_ubic apu2 ON (mm.em_atecco_warehouseto_id = apu2.m_warehouse_id " +
      "         AND apu2.m_product_id = ?) " +
      "         WHERE mm.m_movement_id = ? ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId2);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mMovementId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.loc1 = UtilSql.getValue(result, "loc1");
        objectATECCOOrderProductData.loc2 = UtilSql.getValue(result, "loc2");
        objectATECCOOrderProductData.qtyonhand = UtilSql.getValue(result, "qtyonhand");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static ATECCOOrderProductData[] selectPrecioEfectivo(ConnectionProvider connectionProvider, String priceactual, String cOrderId)    throws ServletException {
    return selectPrecioEfectivo(connectionProvider, priceactual, cOrderId, 0, 0);
  }

  public static ATECCOOrderProductData[] selectPrecioEfectivo(ConnectionProvider connectionProvider, String priceactual, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT (CAST(? AS NUMERIC) * ((100 + fpm.em_atecco_porcentaje) / 100)) AS PRECIOEFECTIVO " +
      "      FROM c_order co " +
      "      INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id) " +
      "      WHERE co.c_order_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priceactual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.precioefectivo = UtilSql.getValue(result, "precioefectivo");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static ATECCOOrderProductData[] selectTotalEfectivo(ConnectionProvider connectionProvider, String priceactual, String qtyorder, String cOrderId)    throws ServletException {
    return selectTotalEfectivo(connectionProvider, priceactual, qtyorder, cOrderId, 0, 0);
  }

  public static ATECCOOrderProductData[] selectTotalEfectivo(ConnectionProvider connectionProvider, String priceactual, String qtyorder, String cOrderId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT ((CAST(? AS NUMERIC) * ((100 + fpm.em_atecco_porcentaje) / 100)) * CAST(? AS NUMERIC)) AS TOTALEFECTIVO " +
      "      FROM c_order co  " +
      "      INNER JOIN fin_paymentmethod fpm ON (co.fin_paymentmethod_id = fpm.fin_paymentmethod_id)" +
      "      WHERE co.c_order_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priceactual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyorder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.totalefectivo = UtilSql.getValue(result, "totalefectivo");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static ATECCOOrderProductData[] selectStockCliente(ConnectionProvider connectionProvider, String mProductId)    throws ServletException {
    return selectStockCliente(connectionProvider, mProductId, 0, 0);
  }

  public static ATECCOOrderProductData[] selectStockCliente(ConnectionProvider connectionProvider, String mProductId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select sum(coalesce(msd.qtyonhand,0)) AS STOCKCLIENTE " +
      "        from m_storage_detail msd " +
      "        inner join m_locator ml on (ml.m_locator_id=msd.m_locator_id)" +
      "        where msd.m_product_id=?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.stockcliente = UtilSql.getValue(result, "stockcliente");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static ATECCOOrderProductData[] selectInvoice(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    return selectInvoice(connectionProvider, cInvoiceId, 0, 0);
  }

  public static ATECCOOrderProductData[] selectInvoice(ConnectionProvider connectionProvider, String cInvoiceId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT C_INVOICE_ID AS ID, DATEINVOICED, C_BPARTNER_ID, M_PRICELIST_ID" +
      "      	FROM C_INVOICE WHERE C_INVOICE_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.id = UtilSql.getValue(result, "id");
        objectATECCOOrderProductData.dateinvoiced = UtilSql.getDateValue(result, "dateinvoiced", "dd-MM-yyyy");
        objectATECCOOrderProductData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectATECCOOrderProductData.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }

  public static String hasSecondaryUOM(ConnectionProvider connectionProvider, String mProductId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      SELECT (CASE COUNT(*) WHEN 0 THEN 0 ELSE 1 END) AS TOTAL " +
      "      FROM M_PRODUCT_UOM " +
      "      WHERE M_PRODUCT_ID = ?" +
      "      AND ISACTIVE = 'Y'";

    ResultSet result;
    String strReturn = "0";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "total");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getOrgLocationId(ConnectionProvider connectionProvider, String adUserClient, String adUserOrg)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  	SELECT C_LOCATION_ID " +
      "	  		FROM AD_ORGINFO " +
      "	  		WHERE AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "	  		AND AD_Org_ID IN (";
    strSql = strSql + ((adUserOrg==null || adUserOrg.equals(""))?"":adUserOrg);
    strSql = strSql + 
      ")";

    ResultSet result;
    String strReturn = "0";
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adUserOrg != null && !(adUserOrg.equals(""))) {
        }

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_location_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getWarehouseOrg(ConnectionProvider connectionProvider, String cWarehouseID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "	  SELECT AD_ORG_ID" +
      "      FROM M_WAREHOUSE" +
      "      WHERE M_WAREHOUSE_ID = ?";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cWarehouseID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getWarehouseOfOrg(ConnectionProvider connectionProvider, String adClientId, String adOrgId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select min(w.m_warehouse_id)" +
      "        from m_warehouse w" +
      "        where w.ad_client_id=?" +
      "          and (ad_isorgincluded(?,w.ad_org_id,?)<>-1" +
      "            or ad_isorgincluded(w.ad_org_id,?,?)<>-1)";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "min");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static ATECCOOrderProductData[] selectCTax(ConnectionProvider connectionProvider, String MProductID)    throws ServletException {
    return selectCTax(connectionProvider, MProductID, 0, 0);
  }

  public static ATECCOOrderProductData[] selectCTax(ConnectionProvider connectionProvider, String MProductID, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT ct.C_TAX_ID " +
      "        FROM m_product mp " +
      "        LEFT JOIN c_tax ct on (mp.c_taxcategory_id = ct.c_taxcategory_id)" +
      "        WHERE mp.m_product_id = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, MProductID);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOOrderProductData objectATECCOOrderProductData = new ATECCOOrderProductData();
        objectATECCOOrderProductData.cTaxId = UtilSql.getValue(result, "c_tax_id");
        objectATECCOOrderProductData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOOrderProductData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOOrderProductData objectATECCOOrderProductData[] = new ATECCOOrderProductData[vector.size()];
    vector.copyInto(objectATECCOOrderProductData);
    return(objectATECCOOrderProductData);
  }
}
