//Sqlc generated V1.O00-1
package com.atrums.compras.montos.ad_actionButton;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class ATECCOInvoiceData implements FieldProvider {
static Logger log4j = Logger.getLogger(ATECCOInvoiceData.class);
  private String InitRecordNumber="0";
  public String dato1;
  public String dato2;
  public String dato3;
  public String dato4;
  public String dato5;
  public String dato6;
  public String dato7;
  public String dato8;
  public String dato9;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("dato1"))
      return dato1;
    else if (fieldName.equalsIgnoreCase("dato2"))
      return dato2;
    else if (fieldName.equalsIgnoreCase("dato3"))
      return dato3;
    else if (fieldName.equalsIgnoreCase("dato4"))
      return dato4;
    else if (fieldName.equalsIgnoreCase("dato5"))
      return dato5;
    else if (fieldName.equalsIgnoreCase("dato6"))
      return dato6;
    else if (fieldName.equalsIgnoreCase("dato7"))
      return dato7;
    else if (fieldName.equalsIgnoreCase("dato8"))
      return dato8;
    else if (fieldName.equalsIgnoreCase("dato9"))
      return dato9;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static ATECCOInvoiceData[] methodSeleccionardummy(ConnectionProvider connectionProvider)    throws ServletException {
    return methodSeleccionardummy(connectionProvider, 0, 0);
  }

  public static ATECCOInvoiceData[] methodSeleccionardummy(ConnectionProvider connectionProvider, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select d.dummy as dato1, " +
      "             d.dummy as dato2, " +
      "             d.dummy as dato3, " +
      "             d.dummy as dato4," +
      "             d.dummy as dato5," +
      "             d.dummy as dato6," +
      "             d.dummy as dato7," +
      "             d.dummy as dato8," +
      "             d.dummy as dato9" +
      "      from dual d ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOInvoiceData objectATECCOInvoiceData = new ATECCOInvoiceData();
        objectATECCOInvoiceData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECCOInvoiceData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECCOInvoiceData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECCOInvoiceData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECCOInvoiceData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECCOInvoiceData.dato6 = UtilSql.getValue(result, "dato6");
        objectATECCOInvoiceData.dato7 = UtilSql.getValue(result, "dato7");
        objectATECCOInvoiceData.dato8 = UtilSql.getValue(result, "dato8");
        objectATECCOInvoiceData.dato9 = UtilSql.getValue(result, "dato9");
        objectATECCOInvoiceData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOInvoiceData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOInvoiceData objectATECCOInvoiceData[] = new ATECCOInvoiceData[vector.size()];
    vector.copyInto(objectATECCOInvoiceData);
    return(objectATECCOInvoiceData);
  }

  public static ATECCOInvoiceData[] methodSeleccionarInvoiceOrg(ConnectionProvider connectionProvider, String c_order_id)    throws ServletException {
    return methodSeleccionarInvoiceOrg(connectionProvider, c_order_id, 0, 0);
  }

  public static ATECCOInvoiceData[] methodSeleccionarInvoiceOrg(ConnectionProvider connectionProvider, String c_order_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select ci.documentno as dato1, " +
      "             ci.c_invoice_id as dato2, " +
      "             ci2.documentno as dato3, " +
      "             ci2.c_invoice_id as dato4, " +
      "             crr.name as dato5 " +
      "       from c_order co " +
      "       left join c_return_reason crr on (co.c_return_reason_id = crr.c_return_reason_id) " +
      "       inner join c_orderline col on (co.c_order_id = col.c_order_id) " +
      "       inner join c_invoiceline cil on (col.c_orderline_id = cil.c_orderline_id) " +
      "       inner join c_invoice ci ON (cil.c_invoice_id = ci.c_invoice_id)" +
      "       inner join c_invoice ci2 ON (col.em_atecco_c_invoice_id = ci2.c_invoice_id)" +
      "       where co.c_order_id = ?  ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOInvoiceData objectATECCOInvoiceData = new ATECCOInvoiceData();
        objectATECCOInvoiceData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECCOInvoiceData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECCOInvoiceData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECCOInvoiceData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECCOInvoiceData.dato5 = UtilSql.getValue(result, "dato5");
        objectATECCOInvoiceData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOInvoiceData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOInvoiceData objectATECCOInvoiceData[] = new ATECCOInvoiceData[vector.size()];
    vector.copyInto(objectATECCOInvoiceData);
    return(objectATECCOInvoiceData);
  }

  public static ATECCOInvoiceData[] methodSeleccionarDocTypeElectronica(ConnectionProvider connectionProvider, String c_order_id)    throws ServletException {
    return methodSeleccionarDocTypeElectronica(connectionProvider, c_order_id, 0, 0);
  }

  public static ATECCOInvoiceData[] methodSeleccionarDocTypeElectronica(ConnectionProvider connectionProvider, String c_order_id, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      select ci.c_invoice_id as dato1, " +
      "             cd.em_atecfe_fac_elec as dato2, " +
      "             cd2.em_atecfe_fac_elec as dato3, " +
      "             cd2.name as dato4 " +
      "      from c_order co " +
      "      inner join c_invoice ci on (co.c_order_id = ci.c_order_id) " +
      "      inner join c_doctype cd on (co.c_doctype_id = cd.c_doctype_id) " +
      "      inner join c_doctype cd2 on (ci.c_doctype_id = cd2.c_doctype_id) " +
      "      where co.c_order_id = ? " +
      "      and ci.em_atecfe_docaction = 'PR' " +
      "      limit 1;";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_order_id);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ATECCOInvoiceData objectATECCOInvoiceData = new ATECCOInvoiceData();
        objectATECCOInvoiceData.dato1 = UtilSql.getValue(result, "dato1");
        objectATECCOInvoiceData.dato2 = UtilSql.getValue(result, "dato2");
        objectATECCOInvoiceData.dato3 = UtilSql.getValue(result, "dato3");
        objectATECCOInvoiceData.dato4 = UtilSql.getValue(result, "dato4");
        objectATECCOInvoiceData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectATECCOInvoiceData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ATECCOInvoiceData objectATECCOInvoiceData[] = new ATECCOInvoiceData[vector.size()];
    vector.copyInto(objectATECCOInvoiceData);
    return(objectATECCOInvoiceData);
  }

  public static int methodUpdateCInvoice(ConnectionProvider connectionProvider, String c_invoiceto_id, String description, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      UPDATE c_invoice SET em_atecfe_c_invoice_id = ?, description = ? WHERE c_invoice_id = ?;";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoiceto_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
