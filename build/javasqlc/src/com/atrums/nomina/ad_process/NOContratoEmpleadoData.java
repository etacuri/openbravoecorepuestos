//Sqlc generated V1.O00-1
package com.atrums.nomina.ad_process;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

class NOContratoEmpleadoData implements FieldProvider {
static Logger log4j = Logger.getLogger(NOContratoEmpleadoData.class);
  private String InitRecordNumber="0";
  public String noContratoEmpleadoId;
  public String adClientId;
  public String adOrgId;
  public String isactive;
  public String created;
  public String createdby;
  public String updated;
  public String updatedby;
  public String fechaInicio;
  public String fechaFin;
  public String documentno;
  public String cDoctypeId;
  public String cBpartnerId;
  public String salario;
  public String finPaymentmethodId;
  public String tipoContrato;
  public String pagofondoreserva;
  public String finFinancialAccountId;
  public String cCurrencyId;
  public String isImpuestoAsumido;
  public String aplicaUtilidad;
  public String emAtnorhCargoId;
  public String liquidacionEmpleado;
  public String docstatus;
  public String docactionno;
  public String emNeSissalnet;
  public String emNeAreaEmpresaId;
  public String emNeMotivoSalida;
  public String emNeIsJornadaParcial;
  public String emNeNumHorasParciales;
  public String emNeVacacionProp;
  public String emNeVacacionTom;
  public String emNeVacacionRes;
  public String emNeRegion;
  public String emNeObservaciones;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("no_contrato_empleado_id") || fieldName.equals("noContratoEmpleadoId"))
      return noContratoEmpleadoId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdby"))
      return createdby;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("salario"))
      return salario;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("tipo_contrato") || fieldName.equals("tipoContrato"))
      return tipoContrato;
    else if (fieldName.equalsIgnoreCase("pagofondoreserva"))
      return pagofondoreserva;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("is_impuesto_asumido") || fieldName.equals("isImpuestoAsumido"))
      return isImpuestoAsumido;
    else if (fieldName.equalsIgnoreCase("aplica_utilidad") || fieldName.equals("aplicaUtilidad"))
      return aplicaUtilidad;
    else if (fieldName.equalsIgnoreCase("em_atnorh_cargo_id") || fieldName.equals("emAtnorhCargoId"))
      return emAtnorhCargoId;
    else if (fieldName.equalsIgnoreCase("liquidacion_empleado") || fieldName.equals("liquidacionEmpleado"))
      return liquidacionEmpleado;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("docactionno"))
      return docactionno;
    else if (fieldName.equalsIgnoreCase("em_ne_sissalnet") || fieldName.equals("emNeSissalnet"))
      return emNeSissalnet;
    else if (fieldName.equalsIgnoreCase("em_ne_area_empresa_id") || fieldName.equals("emNeAreaEmpresaId"))
      return emNeAreaEmpresaId;
    else if (fieldName.equalsIgnoreCase("em_ne_motivo_salida") || fieldName.equals("emNeMotivoSalida"))
      return emNeMotivoSalida;
    else if (fieldName.equalsIgnoreCase("em_ne_is_jornada_parcial") || fieldName.equals("emNeIsJornadaParcial"))
      return emNeIsJornadaParcial;
    else if (fieldName.equalsIgnoreCase("em_ne_num_horas_parciales") || fieldName.equals("emNeNumHorasParciales"))
      return emNeNumHorasParciales;
    else if (fieldName.equalsIgnoreCase("em_ne_vacacion_prop") || fieldName.equals("emNeVacacionProp"))
      return emNeVacacionProp;
    else if (fieldName.equalsIgnoreCase("em_ne_vacacion_tom") || fieldName.equals("emNeVacacionTom"))
      return emNeVacacionTom;
    else if (fieldName.equalsIgnoreCase("em_ne_vacacion_res") || fieldName.equals("emNeVacacionRes"))
      return emNeVacacionRes;
    else if (fieldName.equalsIgnoreCase("em_ne_region") || fieldName.equals("emNeRegion"))
      return emNeRegion;
    else if (fieldName.equalsIgnoreCase("em_ne_observaciones") || fieldName.equals("emNeObservaciones"))
      return emNeObservaciones;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

  public static NOContratoEmpleadoData[] select(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    return select(connectionProvider, noContratoEmpleadoId, 0, 0);
  }

  public static NOContratoEmpleadoData[] select(ConnectionProvider connectionProvider, String noContratoEmpleadoId, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "     	SELECT * FROM NO_CONTRATO_EMPLEADO  " +
      "     	WHERE NO_CONTRATO_EMPLEADO_ID = ?";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        NOContratoEmpleadoData objectNOContratoEmpleadoData = new NOContratoEmpleadoData();
        objectNOContratoEmpleadoData.noContratoEmpleadoId = UtilSql.getValue(result, "no_contrato_empleado_id");
        objectNOContratoEmpleadoData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectNOContratoEmpleadoData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectNOContratoEmpleadoData.isactive = UtilSql.getValue(result, "isactive");
        objectNOContratoEmpleadoData.created = UtilSql.getDateValue(result, "created", "dd-MM-yyyy");
        objectNOContratoEmpleadoData.createdby = UtilSql.getValue(result, "createdby");
        objectNOContratoEmpleadoData.updated = UtilSql.getDateValue(result, "updated", "dd-MM-yyyy");
        objectNOContratoEmpleadoData.updatedby = UtilSql.getValue(result, "updatedby");
        objectNOContratoEmpleadoData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectNOContratoEmpleadoData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectNOContratoEmpleadoData.documentno = UtilSql.getValue(result, "documentno");
        objectNOContratoEmpleadoData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectNOContratoEmpleadoData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectNOContratoEmpleadoData.salario = UtilSql.getValue(result, "salario");
        objectNOContratoEmpleadoData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectNOContratoEmpleadoData.tipoContrato = UtilSql.getValue(result, "tipo_contrato");
        objectNOContratoEmpleadoData.pagofondoreserva = UtilSql.getValue(result, "pagofondoreserva");
        objectNOContratoEmpleadoData.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectNOContratoEmpleadoData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectNOContratoEmpleadoData.isImpuestoAsumido = UtilSql.getValue(result, "is_impuesto_asumido");
        objectNOContratoEmpleadoData.aplicaUtilidad = UtilSql.getValue(result, "aplica_utilidad");
        objectNOContratoEmpleadoData.emAtnorhCargoId = UtilSql.getValue(result, "em_atnorh_cargo_id");
        objectNOContratoEmpleadoData.liquidacionEmpleado = UtilSql.getValue(result, "liquidacion_empleado");
        objectNOContratoEmpleadoData.docstatus = UtilSql.getValue(result, "docstatus");
        objectNOContratoEmpleadoData.docactionno = UtilSql.getValue(result, "docactionno");
        objectNOContratoEmpleadoData.emNeSissalnet = UtilSql.getValue(result, "em_ne_sissalnet");
        objectNOContratoEmpleadoData.emNeAreaEmpresaId = UtilSql.getValue(result, "em_ne_area_empresa_id");
        objectNOContratoEmpleadoData.emNeMotivoSalida = UtilSql.getValue(result, "em_ne_motivo_salida");
        objectNOContratoEmpleadoData.emNeIsJornadaParcial = UtilSql.getValue(result, "em_ne_is_jornada_parcial");
        objectNOContratoEmpleadoData.emNeNumHorasParciales = UtilSql.getValue(result, "em_ne_num_horas_parciales");
        objectNOContratoEmpleadoData.emNeVacacionProp = UtilSql.getValue(result, "em_ne_vacacion_prop");
        objectNOContratoEmpleadoData.emNeVacacionTom = UtilSql.getValue(result, "em_ne_vacacion_tom");
        objectNOContratoEmpleadoData.emNeVacacionRes = UtilSql.getValue(result, "em_ne_vacacion_res");
        objectNOContratoEmpleadoData.emNeRegion = UtilSql.getValue(result, "em_ne_region");
        objectNOContratoEmpleadoData.emNeObservaciones = UtilSql.getValue(result, "em_ne_observaciones");
        objectNOContratoEmpleadoData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectNOContratoEmpleadoData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    NOContratoEmpleadoData objectNOContratoEmpleadoData[] = new NOContratoEmpleadoData[vector.size()];
    vector.copyInto(objectNOContratoEmpleadoData);
    return(objectNOContratoEmpleadoData);
  }

  public int actualizarContrato(ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "      UPDATE NO_CONTRATO_EMPLEADO " +
      "		SET DOCSTATUS = 'CO' " +
      "		WHERE NO_CONTRATO_EMPLEADO_ID= ?;";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }
}
