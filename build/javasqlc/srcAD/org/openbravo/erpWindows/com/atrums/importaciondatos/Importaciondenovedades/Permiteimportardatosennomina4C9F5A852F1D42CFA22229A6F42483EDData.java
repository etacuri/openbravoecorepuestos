//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.importaciondatos.Importaciondenovedades;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData implements FieldProvider {
static Logger log4j = Logger.getLogger(Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String iErrormsg;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String adOrgId;
  public String adOrgIdr;
  public String cedula;
  public String isactive;
  public String noTipoIngresoEgresoId;
  public String noTipoIngresoEgresoIdr;
  public String cPeriodId;
  public String cPeriodIdr;
  public String valor;
  public String iIsimported;
  public String novprocesada;
  public String procnovedad;
  public String idtNovedadId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("i_errormsg") || fieldName.equals("iErrormsg"))
      return iErrormsg;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("cedula"))
      return cedula;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_id") || fieldName.equals("noTipoIngresoEgresoId"))
      return noTipoIngresoEgresoId;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_idr") || fieldName.equals("noTipoIngresoEgresoIdr"))
      return noTipoIngresoEgresoIdr;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("c_period_idr") || fieldName.equals("cPeriodIdr"))
      return cPeriodIdr;
    else if (fieldName.equalsIgnoreCase("valor"))
      return valor;
    else if (fieldName.equalsIgnoreCase("i_isimported") || fieldName.equals("iIsimported"))
      return iIsimported;
    else if (fieldName.equalsIgnoreCase("novprocesada"))
      return novprocesada;
    else if (fieldName.equalsIgnoreCase("procnovedad"))
      return procnovedad;
    else if (fieldName.equalsIgnoreCase("idt_novedad_id") || fieldName.equals("idtNovedadId"))
      return idtNovedadId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(IDT_novedad.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IDT_novedad.CreatedBy) as CreatedByR, " +
      "        to_char(IDT_novedad.Updated, ?) as updated, " +
      "        to_char(IDT_novedad.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        IDT_novedad.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IDT_novedad.UpdatedBy) as UpdatedByR," +
      "        IDT_novedad.I_Errormsg, " +
      "IDT_novedad.C_Bpartner_ID, " +
      "(CASE WHEN IDT_novedad.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name2), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "IDT_novedad.AD_Org_ID, " +
      "(CASE WHEN IDT_novedad.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "IDT_novedad.Cedula, " +
      "COALESCE(IDT_novedad.Isactive, 'N') AS Isactive, " +
      "IDT_novedad.NO_Tipo_Ingreso_Egreso_ID, " +
      "(CASE WHEN IDT_novedad.NO_Tipo_Ingreso_Egreso_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS NO_Tipo_Ingreso_Egreso_IDR, " +
      "IDT_novedad.C_Period_ID, " +
      "(CASE WHEN IDT_novedad.C_Period_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_Period_IDR, " +
      "IDT_novedad.Valor, " +
      "COALESCE(IDT_novedad.I_Isimported, 'N') AS I_Isimported, " +
      "COALESCE(IDT_novedad.Novprocesada, 'N') AS Novprocesada, " +
      "IDT_novedad.Procnovedad, " +
      "IDT_novedad.IDT_Novedad_ID, " +
      "IDT_novedad.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM IDT_novedad left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table1 on (IDT_novedad.C_Bpartner_ID = table1.C_BPartner_ID) left join (select AD_Org_ID, Name from AD_Org) table2 on (IDT_novedad.AD_Org_ID = table2.AD_Org_ID) left join (select NO_Tipo_Ingreso_Egreso_ID, Name from NO_Tipo_Ingreso_Egreso) table3 on (IDT_novedad.NO_Tipo_Ingreso_Egreso_ID = table3.NO_Tipo_Ingreso_Egreso_ID) left join (select C_Period_ID, Name from C_Period) table4 on (IDT_novedad.C_Period_ID = table4.C_Period_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND IDT_novedad.IDT_Novedad_ID = ? " +
      "        AND IDT_novedad.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND IDT_novedad.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData = new Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData();
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.created = UtilSql.getValue(result, "created");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.updated = UtilSql.getValue(result, "updated");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.updatedby = UtilSql.getValue(result, "updatedby");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.iErrormsg = UtilSql.getValue(result, "i_errormsg");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.cedula = UtilSql.getValue(result, "cedula");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.isactive = UtilSql.getValue(result, "isactive");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.noTipoIngresoEgresoId = UtilSql.getValue(result, "no_tipo_ingreso_egreso_id");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.noTipoIngresoEgresoIdr = UtilSql.getValue(result, "no_tipo_ingreso_egreso_idr");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.cPeriodIdr = UtilSql.getValue(result, "c_period_idr");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.valor = UtilSql.getValue(result, "valor");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.iIsimported = UtilSql.getValue(result, "i_isimported");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.novprocesada = UtilSql.getValue(result, "novprocesada");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.procnovedad = UtilSql.getValue(result, "procnovedad");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.idtNovedadId = UtilSql.getValue(result, "idt_novedad_id");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.language = UtilSql.getValue(result, "language");
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.adUserClient = "";
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.adOrgClient = "";
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.createdby = "";
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.trBgcolor = "";
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.totalCount = "";
        objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[] = new Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[vector.size()];
    vector.copyInto(objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData);
    return(objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData);
  }

/**
Create a registry
 */
  public static Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[] set(String noTipoIngresoEgresoId, String cPeriodId, String iErrormsg, String valor, String adClientId, String novprocesada, String cBpartnerId, String cBpartnerIdr, String updatedby, String updatedbyr, String isactive, String cedula, String iIsimported, String adOrgId, String createdby, String createdbyr, String procnovedad, String idtNovedadId)    throws ServletException {
    Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[] = new Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[1];
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0] = new Permiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData();
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].created = "";
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].createdbyr = createdbyr;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].updated = "";
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].updatedTimeStamp = "";
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].updatedby = updatedby;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].updatedbyr = updatedbyr;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].iErrormsg = iErrormsg;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].cBpartnerId = cBpartnerId;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].cBpartnerIdr = cBpartnerIdr;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].adOrgId = adOrgId;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].adOrgIdr = "";
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].cedula = cedula;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].isactive = isactive;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].noTipoIngresoEgresoId = noTipoIngresoEgresoId;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].noTipoIngresoEgresoIdr = "";
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].cPeriodId = cPeriodId;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].cPeriodIdr = "";
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].valor = valor;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].iIsimported = iIsimported;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].novprocesada = novprocesada;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].procnovedad = procnovedad;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].idtNovedadId = idtNovedadId;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].adClientId = adClientId;
    objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData[0].language = "";
    return objectPermiteimportardatosennomina4C9F5A852F1D42CFA22229A6F42483EDData;
  }

/**
Select for auxiliar field
 */
  public static String selectDefB005003A62AA4802B975637B50FD6AFE_0(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefB0C76F5BB7CE472D913DF5C16FAD3FD4_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefD1DB08C80397422F8A8757231E083DF5_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE IDT_novedad" +
      "        SET I_Errormsg = (?) , C_Bpartner_ID = (?) , AD_Org_ID = (?) , Cedula = (?) , Isactive = (?) , NO_Tipo_Ingreso_Egreso_ID = (?) , C_Period_ID = (?) , Valor = TO_NUMBER(?) , I_Isimported = (?) , Novprocesada = (?) , Procnovedad = (?) , IDT_Novedad_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE IDT_novedad.IDT_Novedad_ID = ? " +
      "        AND IDT_novedad.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IDT_novedad.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iErrormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cedula);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iIsimported);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, novprocesada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procnovedad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtNovedadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtNovedadId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO IDT_novedad " +
      "        (I_Errormsg, C_Bpartner_ID, AD_Org_ID, Cedula, Isactive, NO_Tipo_Ingreso_Egreso_ID, C_Period_ID, Valor, I_Isimported, Novprocesada, Procnovedad, IDT_Novedad_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iErrormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cedula);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valor);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iIsimported);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, novprocesada);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procnovedad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtNovedadId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM IDT_novedad" +
      "        WHERE IDT_novedad.IDT_Novedad_ID = ? " +
      "        AND IDT_novedad.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IDT_novedad.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM IDT_novedad" +
      "         WHERE IDT_novedad.IDT_Novedad_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM IDT_novedad" +
      "         WHERE IDT_novedad.IDT_Novedad_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
