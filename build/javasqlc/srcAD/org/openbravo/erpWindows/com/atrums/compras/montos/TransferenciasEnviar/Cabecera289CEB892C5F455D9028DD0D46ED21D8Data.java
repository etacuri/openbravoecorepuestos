//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.TransferenciasEnviar;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Cabecera289CEB892C5F455D9028DD0D46ED21D8Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Cabecera289CEB892C5F455D9028DD0D46ED21D8Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String emCoDoctypeId;
  public String emCoDoctypeIdr;
  public String emAteccoWarehouseId;
  public String emAteccoWarehouseIdr;
  public String emAteccoWarehousetoId;
  public String emAteccoWarehousetoIdr;
  public String movementdate;
  public String documentno;
  public String name;
  public String description;
  public String emAteccoDocstatus;
  public String emAteccoDocstatusr;
  public String emAteccoDocaction;
  public String isactive;
  public String adOrgtrxId;
  public String cProjectId;
  public String cCostcenterId;
  public String aAssetId;
  public String cActivityId;
  public String cActivityIdr;
  public String cCampaignId;
  public String cCampaignIdr;
  public String user1Id;
  public String user2Id;
  public String emAteccoProcesarEnviar;
  public String emAteccoAdOrgtoId;
  public String processed;
  public String mMovementId;
  public String adClientId;
  public String emAteccoAdOrgId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("em_co_doctype_id") || fieldName.equals("emCoDoctypeId"))
      return emCoDoctypeId;
    else if (fieldName.equalsIgnoreCase("em_co_doctype_idr") || fieldName.equals("emCoDoctypeIdr"))
      return emCoDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_warehouse_id") || fieldName.equals("emAteccoWarehouseId"))
      return emAteccoWarehouseId;
    else if (fieldName.equalsIgnoreCase("em_atecco_warehouse_idr") || fieldName.equals("emAteccoWarehouseIdr"))
      return emAteccoWarehouseIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_warehouseto_id") || fieldName.equals("emAteccoWarehousetoId"))
      return emAteccoWarehousetoId;
    else if (fieldName.equalsIgnoreCase("em_atecco_warehouseto_idr") || fieldName.equals("emAteccoWarehousetoIdr"))
      return emAteccoWarehousetoIdr;
    else if (fieldName.equalsIgnoreCase("movementdate"))
      return movementdate;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("em_atecco_docstatus") || fieldName.equals("emAteccoDocstatus"))
      return emAteccoDocstatus;
    else if (fieldName.equalsIgnoreCase("em_atecco_docstatusr") || fieldName.equals("emAteccoDocstatusr"))
      return emAteccoDocstatusr;
    else if (fieldName.equalsIgnoreCase("em_atecco_docaction") || fieldName.equals("emAteccoDocaction"))
      return emAteccoDocaction;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("c_activity_idr") || fieldName.equals("cActivityIdr"))
      return cActivityIdr;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("c_campaign_idr") || fieldName.equals("cCampaignIdr"))
      return cCampaignIdr;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("em_atecco_procesar_enviar") || fieldName.equals("emAteccoProcesarEnviar"))
      return emAteccoProcesarEnviar;
    else if (fieldName.equalsIgnoreCase("em_atecco_ad_orgto_id") || fieldName.equals("emAteccoAdOrgtoId"))
      return emAteccoAdOrgtoId;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("m_movement_id") || fieldName.equals("mMovementId"))
      return mMovementId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("em_atecco_ad_org_id") || fieldName.equals("emAteccoAdOrgId"))
      return emAteccoAdOrgId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Cabecera289CEB892C5F455D9028DD0D46ED21D8Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Cabecera289CEB892C5F455D9028DD0D46ED21D8Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(M_Movement.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Movement.CreatedBy) as CreatedByR, " +
      "        to_char(M_Movement.Updated, ?) as updated, " +
      "        to_char(M_Movement.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        M_Movement.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = M_Movement.UpdatedBy) as UpdatedByR," +
      "        M_Movement.AD_Org_ID, " +
      "(CASE WHEN M_Movement.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "M_Movement.EM_Co_Doctype_ID, " +
      "(CASE WHEN M_Movement.EM_Co_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS EM_Co_Doctype_IDR, " +
      "M_Movement.EM_Atecco_Warehouse_ID, " +
      "(CASE WHEN M_Movement.EM_Atecco_Warehouse_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS EM_Atecco_Warehouse_IDR, " +
      "M_Movement.EM_Atecco_Warehouseto_ID, " +
      "(CASE WHEN M_Movement.EM_Atecco_Warehouseto_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS EM_Atecco_Warehouseto_IDR, " +
      "M_Movement.MovementDate, " +
      "M_Movement.DocumentNo, " +
      "M_Movement.Name, " +
      "M_Movement.Description, " +
      "M_Movement.EM_Atecco_Docstatus, " +
      "(CASE WHEN M_Movement.EM_Atecco_Docstatus IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS EM_Atecco_DocstatusR, " +
      "M_Movement.EM_Atecco_Docaction, " +
      "COALESCE(M_Movement.IsActive, 'N') AS IsActive, " +
      "M_Movement.AD_OrgTrx_ID, " +
      "M_Movement.C_Project_ID, " +
      "M_Movement.C_Costcenter_ID, " +
      "M_Movement.A_Asset_ID, " +
      "M_Movement.C_Activity_ID, " +
      "(CASE WHEN M_Movement.C_Activity_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS C_Activity_IDR, " +
      "M_Movement.C_Campaign_ID, " +
      "(CASE WHEN M_Movement.C_Campaign_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS C_Campaign_IDR, " +
      "M_Movement.User1_ID, " +
      "M_Movement.User2_ID, " +
      "M_Movement.EM_Atecco_Procesar_Enviar, " +
      "M_Movement.EM_Atecco_Ad_Orgto_ID, " +
      "COALESCE(M_Movement.Processed, 'N') AS Processed, " +
      "M_Movement.M_Movement_ID, " +
      "M_Movement.AD_Client_ID, " +
      "M_Movement.EM_Atecco_Ad_Org_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM M_Movement left join (select AD_Org_ID, Name from AD_Org) table1 on (M_Movement.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (M_Movement.EM_Co_Doctype_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select M_Warehouse_ID, Name from M_Warehouse) table4 on (M_Movement.EM_Atecco_Warehouse_ID =  table4.M_Warehouse_ID) left join (select M_Warehouse_ID, Name from M_Warehouse) table5 on (M_Movement.EM_Atecco_Warehouseto_ID =  table5.M_Warehouse_ID) left join ad_ref_list_v list1 on (M_Movement.EM_Atecco_Docstatus = list1.value and list1.ad_reference_id = 'F09025250F444F4CA184018603F54DC8' and list1.ad_language = ?)  left join (select C_Activity_ID, Name from C_Activity) table6 on (M_Movement.C_Activity_ID = table6.C_Activity_ID) left join (select C_Campaign_ID, Name from C_Campaign) table7 on (M_Movement.C_Campaign_ID = table7.C_Campaign_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND M_Movement.M_Movement_ID = ? " +
      "        AND M_Movement.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND M_Movement.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Cabecera289CEB892C5F455D9028DD0D46ED21D8Data objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data = new Cabecera289CEB892C5F455D9028DD0D46ED21D8Data();
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.created = UtilSql.getValue(result, "created");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.updated = UtilSql.getValue(result, "updated");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emCoDoctypeId = UtilSql.getValue(result, "em_co_doctype_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emCoDoctypeIdr = UtilSql.getValue(result, "em_co_doctype_idr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoWarehouseId = UtilSql.getValue(result, "em_atecco_warehouse_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoWarehouseIdr = UtilSql.getValue(result, "em_atecco_warehouse_idr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoWarehousetoId = UtilSql.getValue(result, "em_atecco_warehouseto_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoWarehousetoIdr = UtilSql.getValue(result, "em_atecco_warehouseto_idr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.movementdate = UtilSql.getDateValue(result, "movementdate", "dd-MM-yyyy");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.documentno = UtilSql.getValue(result, "documentno");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.name = UtilSql.getValue(result, "name");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.description = UtilSql.getValue(result, "description");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoDocstatus = UtilSql.getValue(result, "em_atecco_docstatus");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoDocstatusr = UtilSql.getValue(result, "em_atecco_docstatusr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoDocaction = UtilSql.getValue(result, "em_atecco_docaction");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.isactive = UtilSql.getValue(result, "isactive");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.cActivityIdr = UtilSql.getValue(result, "c_activity_idr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.cCampaignIdr = UtilSql.getValue(result, "c_campaign_idr");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.user1Id = UtilSql.getValue(result, "user1_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.user2Id = UtilSql.getValue(result, "user2_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoProcesarEnviar = UtilSql.getValue(result, "em_atecco_procesar_enviar");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoAdOrgtoId = UtilSql.getValue(result, "em_atecco_ad_orgto_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.processed = UtilSql.getValue(result, "processed");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.mMovementId = UtilSql.getValue(result, "m_movement_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.emAteccoAdOrgId = UtilSql.getValue(result, "em_atecco_ad_org_id");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.language = UtilSql.getValue(result, "language");
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.adUserClient = "";
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.adOrgClient = "";
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.createdby = "";
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.trBgcolor = "";
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.totalCount = "";
        objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Cabecera289CEB892C5F455D9028DD0D46ED21D8Data objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[] = new Cabecera289CEB892C5F455D9028DD0D46ED21D8Data[vector.size()];
    vector.copyInto(objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data);
    return(objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data);
  }

/**
Create a registry
 */
  public static Cabecera289CEB892C5F455D9028DD0D46ED21D8Data[] set(String emAteccoProcesarEnviar, String mMovementId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String name, String description, String movementdate, String processed, String emAteccoAdOrgtoId, String emAteccoWarehousetoId, String aAssetId, String documentno, String cActivityId, String user2Id, String user1Id, String cProjectId, String cCampaignId, String adOrgtrxId, String emAteccoWarehouseId, String emAteccoDocaction, String emAteccoDocstatus, String emCoDoctypeId, String emAteccoAdOrgId, String cCostcenterId)    throws ServletException {
    Cabecera289CEB892C5F455D9028DD0D46ED21D8Data objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[] = new Cabecera289CEB892C5F455D9028DD0D46ED21D8Data[1];
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0] = new Cabecera289CEB892C5F455D9028DD0D46ED21D8Data();
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].created = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].createdbyr = createdbyr;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].updated = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].updatedTimeStamp = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].updatedby = updatedby;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].updatedbyr = updatedbyr;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].adOrgId = adOrgId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].adOrgIdr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emCoDoctypeId = emCoDoctypeId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emCoDoctypeIdr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoWarehouseId = emAteccoWarehouseId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoWarehouseIdr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoWarehousetoId = emAteccoWarehousetoId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoWarehousetoIdr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].movementdate = movementdate;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].documentno = documentno;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].name = name;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].description = description;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoDocstatus = emAteccoDocstatus;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoDocstatusr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoDocaction = emAteccoDocaction;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].isactive = isactive;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].adOrgtrxId = adOrgtrxId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].cProjectId = cProjectId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].cCostcenterId = cCostcenterId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].aAssetId = aAssetId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].cActivityId = cActivityId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].cActivityIdr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].cCampaignId = cCampaignId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].cCampaignIdr = "";
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].user1Id = user1Id;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].user2Id = user2Id;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoProcesarEnviar = emAteccoProcesarEnviar;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoAdOrgtoId = emAteccoAdOrgtoId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].processed = processed;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].mMovementId = mMovementId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].adClientId = adClientId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].emAteccoAdOrgId = emAteccoAdOrgId;
    objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data[0].language = "";
    return objectCabecera289CEB892C5F455D9028DD0D46ED21D8Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef3574_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3575_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE M_Movement" +
      "        SET AD_Org_ID = (?) , EM_Co_Doctype_ID = (?) , EM_Atecco_Warehouse_ID = (?) , EM_Atecco_Warehouseto_ID = (?) , MovementDate = TO_DATE(?) , DocumentNo = (?) , Name = (?) , Description = (?) , EM_Atecco_Docstatus = (?) , EM_Atecco_Docaction = (?) , IsActive = (?) , AD_OrgTrx_ID = (?) , C_Project_ID = (?) , C_Costcenter_ID = (?) , A_Asset_ID = (?) , C_Activity_ID = (?) , C_Campaign_ID = (?) , User1_ID = (?) , User2_ID = (?) , EM_Atecco_Procesar_Enviar = (?) , EM_Atecco_Ad_Orgto_ID = (?) , Processed = (?) , M_Movement_ID = (?) , AD_Client_ID = (?) , EM_Atecco_Ad_Org_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE M_Movement.M_Movement_ID = ? " +
      "        AND M_Movement.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Movement.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoWarehousetoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, movementdate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoProcesarEnviar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAdOrgtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mMovementId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAdOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mMovementId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO M_Movement " +
      "        (AD_Org_ID, EM_Co_Doctype_ID, EM_Atecco_Warehouse_ID, EM_Atecco_Warehouseto_ID, MovementDate, DocumentNo, Name, Description, EM_Atecco_Docstatus, EM_Atecco_Docaction, IsActive, AD_OrgTrx_ID, C_Project_ID, C_Costcenter_ID, A_Asset_ID, C_Activity_ID, C_Campaign_ID, User1_ID, User2_ID, EM_Atecco_Procesar_Enviar, EM_Atecco_Ad_Orgto_ID, Processed, M_Movement_ID, AD_Client_ID, EM_Atecco_Ad_Org_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emCoDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoWarehousetoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, movementdate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoProcesarEnviar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAdOrgtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mMovementId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAdOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM M_Movement" +
      "        WHERE M_Movement.M_Movement_ID = ? " +
      "        AND M_Movement.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND M_Movement.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM M_Movement" +
      "         WHERE M_Movement.M_Movement_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM M_Movement" +
      "         WHERE M_Movement.M_Movement_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
