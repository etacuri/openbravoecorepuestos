//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.importaciondatos.Importaciondecontratos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData implements FieldProvider {
static Logger log4j = Logger.getLogger(ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String finFinancialAccountId;
  public String finFinancialAccountIdr;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String tipoContrato;
  public String iErrormsg;
  public String isImpuestoAsumido;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String fechaInicio;
  public String fechaFin;
  public String atnorhCargoId;
  public String isactive;
  public String neVacacionProp;
  public String neVacacionTom;
  public String neVacacionRes;
  public String salario;
  public String cCurrencyId;
  public String cCurrencyIdr;
  public String neIsJornadaParcial;
  public String neNumHorasParciales;
  public String neSissalnet;
  public String pagofondoreserva;
  public String aplicaUtilidad;
  public String neMotivoSalida;
  public String neObservaciones;
  public String btnproceso;
  public String iIsimported;
  public String procesado;
  public String neRegion;
  public String neRegionr;
  public String noAreaEmpresaId;
  public String idtContratoId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_id") || fieldName.equals("finFinancialAccountId"))
      return finFinancialAccountId;
    else if (fieldName.equalsIgnoreCase("fin_financial_account_idr") || fieldName.equals("finFinancialAccountIdr"))
      return finFinancialAccountIdr;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("tipo_contrato") || fieldName.equals("tipoContrato"))
      return tipoContrato;
    else if (fieldName.equalsIgnoreCase("i_errormsg") || fieldName.equals("iErrormsg"))
      return iErrormsg;
    else if (fieldName.equalsIgnoreCase("is_impuesto_asumido") || fieldName.equals("isImpuestoAsumido"))
      return isImpuestoAsumido;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("fecha_inicio") || fieldName.equals("fechaInicio"))
      return fechaInicio;
    else if (fieldName.equalsIgnoreCase("fecha_fin") || fieldName.equals("fechaFin"))
      return fechaFin;
    else if (fieldName.equalsIgnoreCase("atnorh_cargo_id") || fieldName.equals("atnorhCargoId"))
      return atnorhCargoId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ne_vacacion_prop") || fieldName.equals("neVacacionProp"))
      return neVacacionProp;
    else if (fieldName.equalsIgnoreCase("ne_vacacion_tom") || fieldName.equals("neVacacionTom"))
      return neVacacionTom;
    else if (fieldName.equalsIgnoreCase("ne_vacacion_res") || fieldName.equals("neVacacionRes"))
      return neVacacionRes;
    else if (fieldName.equalsIgnoreCase("salario"))
      return salario;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_currency_idr") || fieldName.equals("cCurrencyIdr"))
      return cCurrencyIdr;
    else if (fieldName.equalsIgnoreCase("ne_is_jornada_parcial") || fieldName.equals("neIsJornadaParcial"))
      return neIsJornadaParcial;
    else if (fieldName.equalsIgnoreCase("ne_num_horas_parciales") || fieldName.equals("neNumHorasParciales"))
      return neNumHorasParciales;
    else if (fieldName.equalsIgnoreCase("ne_sissalnet") || fieldName.equals("neSissalnet"))
      return neSissalnet;
    else if (fieldName.equalsIgnoreCase("pagofondoreserva"))
      return pagofondoreserva;
    else if (fieldName.equalsIgnoreCase("aplica_utilidad") || fieldName.equals("aplicaUtilidad"))
      return aplicaUtilidad;
    else if (fieldName.equalsIgnoreCase("ne_motivo_salida") || fieldName.equals("neMotivoSalida"))
      return neMotivoSalida;
    else if (fieldName.equalsIgnoreCase("ne_observaciones") || fieldName.equals("neObservaciones"))
      return neObservaciones;
    else if (fieldName.equalsIgnoreCase("btnproceso"))
      return btnproceso;
    else if (fieldName.equalsIgnoreCase("i_isimported") || fieldName.equals("iIsimported"))
      return iIsimported;
    else if (fieldName.equalsIgnoreCase("procesado"))
      return procesado;
    else if (fieldName.equalsIgnoreCase("ne_region") || fieldName.equals("neRegion"))
      return neRegion;
    else if (fieldName.equalsIgnoreCase("ne_regionr") || fieldName.equals("neRegionr"))
      return neRegionr;
    else if (fieldName.equalsIgnoreCase("no_area_empresa_id") || fieldName.equals("noAreaEmpresaId"))
      return noAreaEmpresaId;
    else if (fieldName.equalsIgnoreCase("idt_contrato_id") || fieldName.equals("idtContratoId"))
      return idtContratoId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(IDT_contrato.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IDT_contrato.CreatedBy) as CreatedByR, " +
      "        to_char(IDT_contrato.Updated, ?) as updated, " +
      "        to_char(IDT_contrato.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        IDT_contrato.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = IDT_contrato.UpdatedBy) as UpdatedByR," +
      "        IDT_contrato.AD_Org_ID, " +
      "(CASE WHEN IDT_contrato.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "IDT_contrato.FIN_Financial_Account_ID, " +
      "(CASE WHEN IDT_contrato.FIN_Financial_Account_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.ISO_Code), ''))),'') ) END) AS FIN_Financial_Account_IDR, " +
      "IDT_contrato.FIN_Paymentmethod_ID, " +
      "(CASE WHEN IDT_contrato.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "IDT_contrato.Tipo_Contrato, " +
      "IDT_contrato.I_Errormsg, " +
      "COALESCE(IDT_contrato.IS_Impuesto_Asumido, 'N') AS IS_Impuesto_Asumido, " +
      "IDT_contrato.C_Doctype_ID, " +
      "(CASE WHEN IDT_contrato.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "IDT_contrato.Documentno, " +
      "IDT_contrato.C_Bpartner_ID, " +
      "(CASE WHEN IDT_contrato.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name2), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "IDT_contrato.Fecha_Inicio, " +
      "IDT_contrato.Fecha_Fin, " +
      "IDT_contrato.Atnorh_Cargo_ID, " +
      "COALESCE(IDT_contrato.Isactive, 'N') AS Isactive, " +
      "IDT_contrato.NE_Vacacion_Prop, " +
      "IDT_contrato.NE_Vacacion_Tom, " +
      "IDT_contrato.NE_Vacacion_Res, " +
      "IDT_contrato.Salario, " +
      "IDT_contrato.C_Currency_ID, " +
      "(CASE WHEN IDT_contrato.C_Currency_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.ISO_Code), ''))),'') ) END) AS C_Currency_IDR, " +
      "COALESCE(IDT_contrato.NE_Is_Jornada_Parcial, 'N') AS NE_Is_Jornada_Parcial, " +
      "IDT_contrato.NE_Num_Horas_Parciales, " +
      "IDT_contrato.NE_Sissalnet, " +
      "COALESCE(IDT_contrato.Pagofondoreserva, 'N') AS Pagofondoreserva, " +
      "COALESCE(IDT_contrato.Aplica_Utilidad, 'N') AS Aplica_Utilidad, " +
      "IDT_contrato.NE_Motivo_Salida, " +
      "IDT_contrato.NE_Observaciones, " +
      "IDT_contrato.Btnproceso, " +
      "COALESCE(IDT_contrato.I_Isimported, 'N') AS I_Isimported, " +
      "COALESCE(IDT_contrato.Procesado, 'N') AS Procesado, " +
      "IDT_contrato.NE_Region, " +
      "(CASE WHEN IDT_contrato.NE_Region IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS NE_RegionR, " +
      "IDT_contrato.NO_Area_Empresa_ID, " +
      "IDT_contrato.IDT_Contrato_ID, " +
      "IDT_contrato.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM IDT_contrato left join (select AD_Org_ID, Name from AD_Org) table1 on (IDT_contrato.AD_Org_ID = table1.AD_Org_ID) left join (select FIN_Financial_Account_ID, Name, C_Currency_ID from FIN_Financial_Account) table2 on (IDT_contrato.FIN_Financial_Account_ID = table2.FIN_Financial_Account_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table3 on (table2.C_Currency_ID = table3.C_Currency_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table4 on (IDT_contrato.FIN_Paymentmethod_ID = table4.FIN_Paymentmethod_ID) left join (select C_DocType_ID, Name from C_DocType) table5 on (IDT_contrato.C_Doctype_ID =  table5.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL5 on (table5.C_DocType_ID = tableTRL5.C_DocType_ID and tableTRL5.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table7 on (IDT_contrato.C_Bpartner_ID = table7.C_BPartner_ID) left join (select C_Currency_ID, ISO_Code from C_Currency) table8 on (IDT_contrato.C_Currency_ID = table8.C_Currency_ID) left join ad_ref_list_v list1 on (IDT_contrato.NE_Region = list1.value and list1.ad_reference_id = '5399B74563094E35B96D005012B4A6D9' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND IDT_contrato.IDT_Contrato_ID = ? " +
      "        AND IDT_contrato.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND IDT_contrato.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData = new ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData();
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.created = UtilSql.getValue(result, "created");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.updated = UtilSql.getValue(result, "updated");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.updatedby = UtilSql.getValue(result, "updatedby");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.finFinancialAccountId = UtilSql.getValue(result, "fin_financial_account_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.finFinancialAccountIdr = UtilSql.getValue(result, "fin_financial_account_idr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.tipoContrato = UtilSql.getValue(result, "tipo_contrato");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.iErrormsg = UtilSql.getValue(result, "i_errormsg");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.isImpuestoAsumido = UtilSql.getValue(result, "is_impuesto_asumido");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.documentno = UtilSql.getValue(result, "documentno");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.fechaInicio = UtilSql.getDateValue(result, "fecha_inicio", "dd-MM-yyyy");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.fechaFin = UtilSql.getDateValue(result, "fecha_fin", "dd-MM-yyyy");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.atnorhCargoId = UtilSql.getValue(result, "atnorh_cargo_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.isactive = UtilSql.getValue(result, "isactive");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neVacacionProp = UtilSql.getValue(result, "ne_vacacion_prop");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neVacacionTom = UtilSql.getValue(result, "ne_vacacion_tom");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neVacacionRes = UtilSql.getValue(result, "ne_vacacion_res");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.salario = UtilSql.getValue(result, "salario");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.cCurrencyIdr = UtilSql.getValue(result, "c_currency_idr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neIsJornadaParcial = UtilSql.getValue(result, "ne_is_jornada_parcial");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neNumHorasParciales = UtilSql.getValue(result, "ne_num_horas_parciales");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neSissalnet = UtilSql.getValue(result, "ne_sissalnet");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.pagofondoreserva = UtilSql.getValue(result, "pagofondoreserva");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.aplicaUtilidad = UtilSql.getValue(result, "aplica_utilidad");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neMotivoSalida = UtilSql.getValue(result, "ne_motivo_salida");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neObservaciones = UtilSql.getValue(result, "ne_observaciones");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.btnproceso = UtilSql.getValue(result, "btnproceso");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.iIsimported = UtilSql.getValue(result, "i_isimported");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.procesado = UtilSql.getValue(result, "procesado");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neRegion = UtilSql.getValue(result, "ne_region");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.neRegionr = UtilSql.getValue(result, "ne_regionr");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.noAreaEmpresaId = UtilSql.getValue(result, "no_area_empresa_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.idtContratoId = UtilSql.getValue(result, "idt_contrato_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.language = UtilSql.getValue(result, "language");
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.adUserClient = "";
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.adOrgClient = "";
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.createdby = "";
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.trBgcolor = "";
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.totalCount = "";
        objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[] = new ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[vector.size()];
    vector.copyInto(objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData);
    return(objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData);
  }

/**
Create a registry
 */
  public static ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[] set(String finPaymentmethodId, String neMotivoSalida, String neSissalnet, String aplicaUtilidad, String pagofondoreserva, String neObservaciones, String updatedby, String updatedbyr, String iIsimported, String finFinancialAccountId, String btnproceso, String cBpartnerId, String cBpartnerIdr, String neVacacionTom, String tipoContrato, String neVacacionRes, String adOrgId, String noAreaEmpresaId, String fechaFin, String procesado, String isImpuestoAsumido, String idtContratoId, String documentno, String neRegion, String cDoctypeId, String fechaInicio, String cCurrencyId, String neIsJornadaParcial, String createdby, String createdbyr, String neVacacionProp, String iErrormsg, String atnorhCargoId, String neNumHorasParciales, String salario, String isactive, String adClientId)    throws ServletException {
    ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[] = new ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[1];
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0] = new ImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData();
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].created = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].createdbyr = createdbyr;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].updated = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].updatedTimeStamp = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].updatedby = updatedby;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].updatedbyr = updatedbyr;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].adOrgId = adOrgId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].adOrgIdr = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].finFinancialAccountId = finFinancialAccountId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].finFinancialAccountIdr = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].finPaymentmethodId = finPaymentmethodId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].finPaymentmethodIdr = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].tipoContrato = tipoContrato;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].iErrormsg = iErrormsg;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].isImpuestoAsumido = isImpuestoAsumido;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].cDoctypeId = cDoctypeId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].cDoctypeIdr = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].documentno = documentno;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].cBpartnerId = cBpartnerId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].cBpartnerIdr = cBpartnerIdr;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].fechaInicio = fechaInicio;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].fechaFin = fechaFin;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].atnorhCargoId = atnorhCargoId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].isactive = isactive;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neVacacionProp = neVacacionProp;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neVacacionTom = neVacacionTom;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neVacacionRes = neVacacionRes;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].salario = salario;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].cCurrencyId = cCurrencyId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].cCurrencyIdr = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neIsJornadaParcial = neIsJornadaParcial;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neNumHorasParciales = neNumHorasParciales;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neSissalnet = neSissalnet;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].pagofondoreserva = pagofondoreserva;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].aplicaUtilidad = aplicaUtilidad;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neMotivoSalida = neMotivoSalida;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neObservaciones = neObservaciones;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].btnproceso = btnproceso;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].iIsimported = iIsimported;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].procesado = procesado;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neRegion = neRegion;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].neRegionr = "";
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].noAreaEmpresaId = noAreaEmpresaId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].idtContratoId = idtContratoId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].adClientId = adClientId;
    objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData[0].language = "";
    return objectImportaciondecontratosC6B91FD2AA954C5ABCCBE73BEC16BCDFData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef27FF0982875E499A889BA66DB7179111_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef664B47419B554B13BBCE655C4F933E7B_1(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefC00E46C43D6D4A798642FF13BB860C43_2(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE IDT_contrato" +
      "        SET AD_Org_ID = (?) , FIN_Financial_Account_ID = (?) , FIN_Paymentmethod_ID = (?) , Tipo_Contrato = (?) , I_Errormsg = (?) , IS_Impuesto_Asumido = (?) , C_Doctype_ID = (?) , Documentno = (?) , C_Bpartner_ID = (?) , Fecha_Inicio = TO_DATE(?) , Fecha_Fin = TO_DATE(?) , Atnorh_Cargo_ID = (?) , Isactive = (?) , NE_Vacacion_Prop = TO_NUMBER(?) , NE_Vacacion_Tom = TO_NUMBER(?) , NE_Vacacion_Res = TO_NUMBER(?) , Salario = TO_NUMBER(?) , C_Currency_ID = (?) , NE_Is_Jornada_Parcial = (?) , NE_Num_Horas_Parciales = TO_NUMBER(?) , NE_Sissalnet = (?) , Pagofondoreserva = (?) , Aplica_Utilidad = (?) , NE_Motivo_Salida = (?) , NE_Observaciones = (?) , Btnproceso = (?) , I_Isimported = (?) , Procesado = (?) , NE_Region = (?) , NO_Area_Empresa_ID = (?) , IDT_Contrato_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE IDT_contrato.IDT_Contrato_ID = ? " +
      "        AND IDT_contrato.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IDT_contrato.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoContrato);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iErrormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isImpuestoAsumido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atnorhCargoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neVacacionProp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neVacacionTom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neVacacionRes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salario);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neIsJornadaParcial);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neNumHorasParciales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neSissalnet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagofondoreserva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aplicaUtilidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neMotivoSalida);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neObservaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnproceso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iIsimported);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neRegion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtContratoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtContratoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO IDT_contrato " +
      "        (AD_Org_ID, FIN_Financial_Account_ID, FIN_Paymentmethod_ID, Tipo_Contrato, I_Errormsg, IS_Impuesto_Asumido, C_Doctype_ID, Documentno, C_Bpartner_ID, Fecha_Inicio, Fecha_Fin, Atnorh_Cargo_ID, Isactive, NE_Vacacion_Prop, NE_Vacacion_Tom, NE_Vacacion_Res, Salario, C_Currency_ID, NE_Is_Jornada_Parcial, NE_Num_Horas_Parciales, NE_Sissalnet, Pagofondoreserva, Aplica_Utilidad, NE_Motivo_Salida, NE_Observaciones, Btnproceso, I_Isimported, Procesado, NE_Region, NO_Area_Empresa_ID, IDT_Contrato_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), TO_DATE(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finFinancialAccountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoContrato);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iErrormsg);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isImpuestoAsumido);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, atnorhCargoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neVacacionProp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neVacacionTom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neVacacionRes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salario);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neIsJornadaParcial);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neNumHorasParciales);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neSissalnet);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pagofondoreserva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aplicaUtilidad);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neMotivoSalida);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neObservaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, btnproceso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iIsimported);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, neRegion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, idtContratoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM IDT_contrato" +
      "        WHERE IDT_contrato.IDT_Contrato_ID = ? " +
      "        AND IDT_contrato.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND IDT_contrato.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM IDT_contrato" +
      "         WHERE IDT_contrato.IDT_Contrato_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM IDT_contrato" +
      "         WHERE IDT_contrato.IDT_Contrato_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
