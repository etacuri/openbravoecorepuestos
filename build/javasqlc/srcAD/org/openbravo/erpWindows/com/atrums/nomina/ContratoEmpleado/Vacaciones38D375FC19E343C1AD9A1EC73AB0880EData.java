//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.ContratoEmpleado;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData implements FieldProvider {
static Logger log4j = Logger.getLogger(Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String emNeLinea;
  public String emNeFechaInicio;
  public String emNeFechaFin;
  public String emNeTotalDias;
  public String emNeObservaciones;
  public String emNeProcesar;
  public String emNeProcesarBtn;
  public String saldo;
  public String anio;
  public String adOrgId;
  public String noVacacionId;
  public String noContratoEmpleadoId;
  public String adClientId;
  public String bonificaciones;
  public String emNeDocstatus;
  public String isactive;
  public String vacaciones;
  public String cBpartnerId;
  public String emNeEstado;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("em_ne_linea") || fieldName.equals("emNeLinea"))
      return emNeLinea;
    else if (fieldName.equalsIgnoreCase("em_ne_fecha_inicio") || fieldName.equals("emNeFechaInicio"))
      return emNeFechaInicio;
    else if (fieldName.equalsIgnoreCase("em_ne_fecha_fin") || fieldName.equals("emNeFechaFin"))
      return emNeFechaFin;
    else if (fieldName.equalsIgnoreCase("em_ne_total_dias") || fieldName.equals("emNeTotalDias"))
      return emNeTotalDias;
    else if (fieldName.equalsIgnoreCase("em_ne_observaciones") || fieldName.equals("emNeObservaciones"))
      return emNeObservaciones;
    else if (fieldName.equalsIgnoreCase("em_ne_procesar") || fieldName.equals("emNeProcesar"))
      return emNeProcesar;
    else if (fieldName.equalsIgnoreCase("em_ne_procesar_btn") || fieldName.equals("emNeProcesarBtn"))
      return emNeProcesarBtn;
    else if (fieldName.equalsIgnoreCase("saldo"))
      return saldo;
    else if (fieldName.equalsIgnoreCase("anio"))
      return anio;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("no_vacacion_id") || fieldName.equals("noVacacionId"))
      return noVacacionId;
    else if (fieldName.equalsIgnoreCase("no_contrato_empleado_id") || fieldName.equals("noContratoEmpleadoId"))
      return noContratoEmpleadoId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("bonificaciones"))
      return bonificaciones;
    else if (fieldName.equalsIgnoreCase("em_ne_docstatus") || fieldName.equals("emNeDocstatus"))
      return emNeDocstatus;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("vacaciones"))
      return vacaciones;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("em_ne_estado") || fieldName.equals("emNeEstado"))
      return emNeEstado;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noContratoEmpleadoId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, noContratoEmpleadoId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noContratoEmpleadoId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_vacacion.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_vacacion.CreatedBy) as CreatedByR, " +
      "        to_char(no_vacacion.Updated, ?) as updated, " +
      "        to_char(no_vacacion.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_vacacion.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_vacacion.UpdatedBy) as UpdatedByR," +
      "        no_vacacion.em_ne_linea, " +
      "no_vacacion.em_ne_fecha_inicio, " +
      "no_vacacion.em_ne_fecha_fin, " +
      "no_vacacion.em_ne_total_dias, " +
      "no_vacacion.em_ne_observaciones, " +
      "no_vacacion.em_ne_procesar, " +
      "list1.name as em_ne_procesar_BTN, " +
      "no_vacacion.saldo, " +
      "no_vacacion.Anio, " +
      "no_vacacion.AD_Org_ID, " +
      "no_vacacion.NO_Vacacion_ID, " +
      "no_vacacion.no_contrato_empleado_id, " +
      "no_vacacion.AD_Client_ID, " +
      "no_vacacion.bonificaciones, " +
      "no_vacacion.em_ne_docstatus, " +
      "COALESCE(no_vacacion.Isactive, 'N') AS Isactive, " +
      "no_vacacion.vacaciones, " +
      "no_vacacion.C_Bpartner_ID, " +
      "no_vacacion.em_ne_estado, " +
      "        ? AS LANGUAGE " +
      "        FROM no_vacacion left join ad_ref_list_v list1 on (list1.ad_reference_id = 'D90979EA306F493EBD3EF5D201355C56' and list1.ad_language = ?  AND no_vacacion.em_ne_procesar = TO_CHAR(list1.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((noContratoEmpleadoId==null || noContratoEmpleadoId.equals(""))?"":"  AND no_vacacion.no_contrato_empleado_id = ?  ");
    strSql = strSql + 
      "        AND no_vacacion.NO_Vacacion_ID = ? " +
      "        AND no_vacacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_vacacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (noContratoEmpleadoId != null && !(noContratoEmpleadoId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData = new Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData();
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.created = UtilSql.getValue(result, "created");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.updated = UtilSql.getValue(result, "updated");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.updatedby = UtilSql.getValue(result, "updatedby");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeLinea = UtilSql.getValue(result, "em_ne_linea");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeFechaInicio = UtilSql.getDateValue(result, "em_ne_fecha_inicio", "dd-MM-yyyy");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeFechaFin = UtilSql.getDateValue(result, "em_ne_fecha_fin", "dd-MM-yyyy");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeTotalDias = UtilSql.getValue(result, "em_ne_total_dias");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeObservaciones = UtilSql.getValue(result, "em_ne_observaciones");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeProcesar = UtilSql.getValue(result, "em_ne_procesar");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeProcesarBtn = UtilSql.getValue(result, "em_ne_procesar_btn");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.saldo = UtilSql.getValue(result, "saldo");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.anio = UtilSql.getValue(result, "anio");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.noVacacionId = UtilSql.getValue(result, "no_vacacion_id");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.noContratoEmpleadoId = UtilSql.getValue(result, "no_contrato_empleado_id");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.bonificaciones = UtilSql.getValue(result, "bonificaciones");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeDocstatus = UtilSql.getValue(result, "em_ne_docstatus");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.isactive = UtilSql.getValue(result, "isactive");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.vacaciones = UtilSql.getValue(result, "vacaciones");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.emNeEstado = UtilSql.getValue(result, "em_ne_estado");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.language = UtilSql.getValue(result, "language");
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.adUserClient = "";
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.adOrgClient = "";
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.createdby = "";
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.trBgcolor = "";
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.totalCount = "";
        objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[] = new Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData[vector.size()];
    vector.copyInto(objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData);
    return(objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData);
  }

/**
Create a registry
 */
  public static Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData[] set(String noContratoEmpleadoId, String anio, String createdby, String createdbyr, String bonificaciones, String vacaciones, String emNeDocstatus, String emNeLinea, String emNeFechaInicio, String emNeEstado, String isactive, String noVacacionId, String adClientId, String emNeFechaFin, String emNeProcesar, String emNeProcesarBtn, String emNeObservaciones, String saldo, String adOrgId, String cBpartnerId, String updatedby, String updatedbyr, String emNeTotalDias)    throws ServletException {
    Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[] = new Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData[1];
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0] = new Vacaciones38D375FC19E343C1AD9A1EC73AB0880EData();
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].created = "";
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].createdbyr = createdbyr;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].updated = "";
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].updatedTimeStamp = "";
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].updatedby = updatedby;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].updatedbyr = updatedbyr;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeLinea = emNeLinea;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeFechaInicio = emNeFechaInicio;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeFechaFin = emNeFechaFin;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeTotalDias = emNeTotalDias;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeObservaciones = emNeObservaciones;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeProcesar = emNeProcesar;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeProcesarBtn = emNeProcesarBtn;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].saldo = saldo;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].anio = anio;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].adOrgId = adOrgId;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].noVacacionId = noVacacionId;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].noContratoEmpleadoId = noContratoEmpleadoId;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].adClientId = adClientId;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].bonificaciones = bonificaciones;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeDocstatus = emNeDocstatus;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].isactive = isactive;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].vacaciones = vacaciones;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].cBpartnerId = cBpartnerId;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].emNeEstado = emNeEstado;
    objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData[0].language = "";
    return objectVacaciones38D375FC19E343C1AD9A1EC73AB0880EData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef0909E63254424FA5A6B97C8E9E701409_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef44713C42126B4362A7D9F8040A9124DE(ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(em_ne_linea),0)+10 AS DefaultValue FROM no_vacacion ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefDE5682896BC94605947571648B7782BF_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT no_vacacion.no_contrato_empleado_id AS NAME" +
      "        FROM no_vacacion" +
      "        WHERE no_vacacion.NO_Vacacion_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM no_contrato_empleado left join (select no_contrato_empleado_id, Documentno from no_contrato_empleado) table1 on (no_contrato_empleado.no_contrato_empleado_id = table1.no_contrato_empleado_id) WHERE no_contrato_empleado.no_contrato_empleado_id = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM no_contrato_empleado left join (select no_contrato_empleado_id, Documentno from no_contrato_empleado) table1 on (no_contrato_empleado.no_contrato_empleado_id = table1.no_contrato_empleado_id) WHERE no_contrato_empleado.no_contrato_empleado_id = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_vacacion" +
      "        SET em_ne_linea = TO_NUMBER(?) , em_ne_fecha_inicio = TO_DATE(?) , em_ne_fecha_fin = TO_DATE(?) , em_ne_total_dias = TO_NUMBER(?) , em_ne_observaciones = (?) , em_ne_procesar = (?) , saldo = TO_NUMBER(?) , Anio = (?) , AD_Org_ID = (?) , NO_Vacacion_ID = (?) , no_contrato_empleado_id = (?) , AD_Client_ID = (?) , bonificaciones = TO_NUMBER(?) , em_ne_docstatus = (?) , Isactive = (?) , vacaciones = TO_NUMBER(?) , C_Bpartner_ID = (?) , em_ne_estado = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_vacacion.NO_Vacacion_ID = ? " +
      "                 AND no_vacacion.no_contrato_empleado_id = ? " +
      "        AND no_vacacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_vacacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeLinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeFechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeFechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeTotalDias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeObservaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, saldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bonificaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vacaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeEstado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_vacacion " +
      "        (em_ne_linea, em_ne_fecha_inicio, em_ne_fecha_fin, em_ne_total_dias, em_ne_observaciones, em_ne_procesar, saldo, Anio, AD_Org_ID, NO_Vacacion_ID, no_contrato_empleado_id, AD_Client_ID, bonificaciones, em_ne_docstatus, Isactive, vacaciones, C_Bpartner_ID, em_ne_estado, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_NUMBER(?), TO_DATE(?), TO_DATE(?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), TO_NUMBER(?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeLinea);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeFechaInicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeFechaFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeTotalDias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeObservaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, saldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, anio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noVacacionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bonificaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, vacaciones);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeEstado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String noContratoEmpleadoId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_vacacion" +
      "        WHERE no_vacacion.NO_Vacacion_ID = ? " +
      "                 AND no_vacacion.no_contrato_empleado_id = ? " +
      "        AND no_vacacion.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_vacacion.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_vacacion" +
      "         WHERE no_vacacion.NO_Vacacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_vacacion" +
      "         WHERE no_vacacion.NO_Vacacion_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
