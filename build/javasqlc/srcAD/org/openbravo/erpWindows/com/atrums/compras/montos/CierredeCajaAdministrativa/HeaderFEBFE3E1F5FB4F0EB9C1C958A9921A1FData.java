//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.CierredeCajaAdministrativa;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData implements FieldProvider {
static Logger log4j = Logger.getLogger(HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String docstatus;
  public String fechaApertura;
  public String usuarioAperturaId;
  public String usuarioAperturaIdr;
  public String respoAperturaId;
  public String respoAperturaIdr;
  public String observacionApertura;
  public String fechaCierre;
  public String usuarioCierreId;
  public String usuarioCierreIdr;
  public String respoCierreId;
  public String respoCierreIdr;
  public String observacionCierre;
  public String valorApertura;
  public String totalFacturas;
  public String valorDeposito;
  public String valorCaja;
  public String valorCierre;
  public String procesar;
  public String procesarBtn;
  public String procesarValidar;
  public String procesarValidarBtn;
  public String procesarRetiro;
  public String isactive;
  public String ateccoCierrecajaId;
  public String adClientId;
  public String cDoctypeId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("fecha_apertura") || fieldName.equals("fechaApertura"))
      return fechaApertura;
    else if (fieldName.equalsIgnoreCase("usuario_apertura_id") || fieldName.equals("usuarioAperturaId"))
      return usuarioAperturaId;
    else if (fieldName.equalsIgnoreCase("usuario_apertura_idr") || fieldName.equals("usuarioAperturaIdr"))
      return usuarioAperturaIdr;
    else if (fieldName.equalsIgnoreCase("respo_apertura_id") || fieldName.equals("respoAperturaId"))
      return respoAperturaId;
    else if (fieldName.equalsIgnoreCase("respo_apertura_idr") || fieldName.equals("respoAperturaIdr"))
      return respoAperturaIdr;
    else if (fieldName.equalsIgnoreCase("observacion_apertura") || fieldName.equals("observacionApertura"))
      return observacionApertura;
    else if (fieldName.equalsIgnoreCase("fecha_cierre") || fieldName.equals("fechaCierre"))
      return fechaCierre;
    else if (fieldName.equalsIgnoreCase("usuario_cierre_id") || fieldName.equals("usuarioCierreId"))
      return usuarioCierreId;
    else if (fieldName.equalsIgnoreCase("usuario_cierre_idr") || fieldName.equals("usuarioCierreIdr"))
      return usuarioCierreIdr;
    else if (fieldName.equalsIgnoreCase("respo_cierre_id") || fieldName.equals("respoCierreId"))
      return respoCierreId;
    else if (fieldName.equalsIgnoreCase("respo_cierre_idr") || fieldName.equals("respoCierreIdr"))
      return respoCierreIdr;
    else if (fieldName.equalsIgnoreCase("observacion_cierre") || fieldName.equals("observacionCierre"))
      return observacionCierre;
    else if (fieldName.equalsIgnoreCase("valor_apertura") || fieldName.equals("valorApertura"))
      return valorApertura;
    else if (fieldName.equalsIgnoreCase("total_facturas") || fieldName.equals("totalFacturas"))
      return totalFacturas;
    else if (fieldName.equalsIgnoreCase("valor_deposito") || fieldName.equals("valorDeposito"))
      return valorDeposito;
    else if (fieldName.equalsIgnoreCase("valor_caja") || fieldName.equals("valorCaja"))
      return valorCaja;
    else if (fieldName.equalsIgnoreCase("valor_cierre") || fieldName.equals("valorCierre"))
      return valorCierre;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("procesar_btn") || fieldName.equals("procesarBtn"))
      return procesarBtn;
    else if (fieldName.equalsIgnoreCase("procesar_validar") || fieldName.equals("procesarValidar"))
      return procesarValidar;
    else if (fieldName.equalsIgnoreCase("procesar_validar_btn") || fieldName.equals("procesarValidarBtn"))
      return procesarValidarBtn;
    else if (fieldName.equalsIgnoreCase("procesar_retiro") || fieldName.equals("procesarRetiro"))
      return procesarRetiro;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("atecco_cierrecaja_id") || fieldName.equals("ateccoCierrecajaId"))
      return ateccoCierrecajaId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(atecco_cierrecaja.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_cierrecaja.CreatedBy) as CreatedByR, " +
      "        to_char(atecco_cierrecaja.Updated, ?) as updated, " +
      "        to_char(atecco_cierrecaja.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        atecco_cierrecaja.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = atecco_cierrecaja.UpdatedBy) as UpdatedByR," +
      "        atecco_cierrecaja.AD_Org_ID, " +
      "(CASE WHEN atecco_cierrecaja.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "atecco_cierrecaja.Docstatus, " +
      "TO_CHAR(atecco_cierrecaja.Fecha_Apertura, ?) AS Fecha_Apertura, " +
      "atecco_cierrecaja.Usuario_Apertura_ID, " +
      "(CASE WHEN atecco_cierrecaja.Usuario_Apertura_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))),'') ) END) AS Usuario_Apertura_IDR, " +
      "atecco_cierrecaja.Respo_Apertura_ID, " +
      "(CASE WHEN atecco_cierrecaja.Respo_Apertura_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'') ) END) AS Respo_Apertura_IDR, " +
      "atecco_cierrecaja.Observacion_Apertura, " +
      "TO_CHAR(atecco_cierrecaja.Fecha_Cierre, ?) AS Fecha_Cierre, " +
      "atecco_cierrecaja.Usuario_Cierre_ID, " +
      "(CASE WHEN atecco_cierrecaja.Usuario_Cierre_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS Usuario_Cierre_IDR, " +
      "atecco_cierrecaja.Respo_Cierre_ID, " +
      "(CASE WHEN atecco_cierrecaja.Respo_Cierre_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS Respo_Cierre_IDR, " +
      "atecco_cierrecaja.Observacion_Cierre, " +
      "atecco_cierrecaja.Valor_Apertura, " +
      "atecco_cierrecaja.Total_Facturas, " +
      "atecco_cierrecaja.Valor_Deposito, " +
      "atecco_cierrecaja.Valor_Caja, " +
      "atecco_cierrecaja.Valor_Cierre, " +
      "atecco_cierrecaja.Procesar, " +
      "list1.name as Procesar_BTN, " +
      "atecco_cierrecaja.Procesar_Validar, " +
      "list2.name as Procesar_Validar_BTN, " +
      "atecco_cierrecaja.Procesar_Retiro, " +
      "COALESCE(atecco_cierrecaja.Isactive, 'N') AS Isactive, " +
      "atecco_cierrecaja.Atecco_Cierrecaja_ID, " +
      "atecco_cierrecaja.AD_Client_ID, " +
      "atecco_cierrecaja.C_Doctype_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM atecco_cierrecaja left join (select AD_Org_ID, Name from AD_Org) table1 on (atecco_cierrecaja.AD_Org_ID = table1.AD_Org_ID) left join (select AD_User_ID, Name from AD_User) table2 on (atecco_cierrecaja.Usuario_Apertura_ID =  table2.AD_User_ID) left join (select AD_User_ID, Name from AD_User) table3 on (atecco_cierrecaja.Respo_Apertura_ID =  table3.AD_User_ID) left join (select AD_User_ID, Name from AD_User) table4 on (atecco_cierrecaja.Usuario_Cierre_ID =  table4.AD_User_ID) left join (select AD_User_ID, Name from AD_User) table5 on (atecco_cierrecaja.Respo_Cierre_ID =  table5.AD_User_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '69732EF9EAF24E18B046FA2AC155E98D' and list1.ad_language = ?  AND atecco_cierrecaja.Procesar = TO_CHAR(list1.value)) left join ad_ref_list_v list2 on (list2.ad_reference_id = '69732EF9EAF24E18B046FA2AC155E98D' and list2.ad_language = ?  AND atecco_cierrecaja.Procesar_Validar = TO_CHAR(list2.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND atecco_cierrecaja.Atecco_Cierrecaja_ID = ? " +
      "        AND atecco_cierrecaja.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND atecco_cierrecaja.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData = new HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData();
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.created = UtilSql.getValue(result, "created");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.updated = UtilSql.getValue(result, "updated");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.updatedby = UtilSql.getValue(result, "updatedby");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.docstatus = UtilSql.getValue(result, "docstatus");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.fechaApertura = UtilSql.getValue(result, "fecha_apertura");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.usuarioAperturaId = UtilSql.getValue(result, "usuario_apertura_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.usuarioAperturaIdr = UtilSql.getValue(result, "usuario_apertura_idr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.respoAperturaId = UtilSql.getValue(result, "respo_apertura_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.respoAperturaIdr = UtilSql.getValue(result, "respo_apertura_idr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.observacionApertura = UtilSql.getValue(result, "observacion_apertura");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.fechaCierre = UtilSql.getValue(result, "fecha_cierre");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.usuarioCierreId = UtilSql.getValue(result, "usuario_cierre_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.usuarioCierreIdr = UtilSql.getValue(result, "usuario_cierre_idr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.respoCierreId = UtilSql.getValue(result, "respo_cierre_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.respoCierreIdr = UtilSql.getValue(result, "respo_cierre_idr");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.observacionCierre = UtilSql.getValue(result, "observacion_cierre");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.valorApertura = UtilSql.getValue(result, "valor_apertura");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.totalFacturas = UtilSql.getValue(result, "total_facturas");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.valorDeposito = UtilSql.getValue(result, "valor_deposito");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.valorCaja = UtilSql.getValue(result, "valor_caja");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.valorCierre = UtilSql.getValue(result, "valor_cierre");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.procesar = UtilSql.getValue(result, "procesar");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.procesarBtn = UtilSql.getValue(result, "procesar_btn");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.procesarValidar = UtilSql.getValue(result, "procesar_validar");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.procesarValidarBtn = UtilSql.getValue(result, "procesar_validar_btn");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.procesarRetiro = UtilSql.getValue(result, "procesar_retiro");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.isactive = UtilSql.getValue(result, "isactive");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.ateccoCierrecajaId = UtilSql.getValue(result, "atecco_cierrecaja_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.language = UtilSql.getValue(result, "language");
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.adUserClient = "";
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.adOrgClient = "";
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.createdby = "";
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.trBgcolor = "";
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.totalCount = "";
        objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[] = new HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[vector.size()];
    vector.copyInto(objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData);
    return(objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData);
  }

/**
Create a registry
 */
  public static HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[] set(String valorDeposito, String usuarioCierreId, String adOrgId, String respoAperturaId, String procesar, String procesarBtn, String fechaApertura, String observacionApertura, String respoCierreId, String updatedby, String updatedbyr, String fechaCierre, String ateccoCierrecajaId, String createdby, String createdbyr, String docstatus, String isactive, String valorCierre, String totalFacturas, String valorCaja, String cDoctypeId, String procesarRetiro, String procesarValidar, String procesarValidarBtn, String valorApertura, String observacionCierre, String adClientId, String usuarioAperturaId)    throws ServletException {
    HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[] = new HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[1];
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0] = new HeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData();
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].created = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].createdbyr = createdbyr;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].updated = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].updatedTimeStamp = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].updatedby = updatedby;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].updatedbyr = updatedbyr;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].adOrgId = adOrgId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].adOrgIdr = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].docstatus = docstatus;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].fechaApertura = fechaApertura;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].usuarioAperturaId = usuarioAperturaId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].usuarioAperturaIdr = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].respoAperturaId = respoAperturaId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].respoAperturaIdr = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].observacionApertura = observacionApertura;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].fechaCierre = fechaCierre;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].usuarioCierreId = usuarioCierreId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].usuarioCierreIdr = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].respoCierreId = respoCierreId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].respoCierreIdr = "";
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].observacionCierre = observacionCierre;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].valorApertura = valorApertura;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].totalFacturas = totalFacturas;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].valorDeposito = valorDeposito;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].valorCaja = valorCaja;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].valorCierre = valorCierre;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].procesar = procesar;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].procesarBtn = procesarBtn;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].procesarValidar = procesarValidar;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].procesarValidarBtn = procesarValidarBtn;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].procesarRetiro = procesarRetiro;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].isactive = isactive;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].ateccoCierrecajaId = ateccoCierrecajaId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].adClientId = adClientId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].cDoctypeId = cDoctypeId;
    objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData[0].language = "";
    return objectHeaderFEBFE3E1F5FB4F0EB9C1C958A9921A1FData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef43E94606E81946C793C9628174B1825F_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef560B3C922DFB49DBAB57DCB54393DE23_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE atecco_cierrecaja" +
      "        SET AD_Org_ID = (?) , Docstatus = (?) , Fecha_Apertura = TO_TIMESTAMP(?, ?) , Usuario_Apertura_ID = (?) , Respo_Apertura_ID = (?) , Observacion_Apertura = (?) , Fecha_Cierre = TO_TIMESTAMP(?, ?) , Usuario_Cierre_ID = (?) , Respo_Cierre_ID = (?) , Observacion_Cierre = (?) , Valor_Apertura = TO_NUMBER(?) , Total_Facturas = TO_NUMBER(?) , Valor_Deposito = TO_NUMBER(?) , Valor_Caja = TO_NUMBER(?) , Valor_Cierre = TO_NUMBER(?) , Procesar = (?) , Procesar_Validar = (?) , Procesar_Retiro = (?) , Isactive = (?) , Atecco_Cierrecaja_ID = (?) , AD_Client_ID = (?) , C_Doctype_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE atecco_cierrecaja.Atecco_Cierrecaja_ID = ? " +
      "        AND atecco_cierrecaja.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_cierrecaja.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaApertura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, usuarioAperturaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, respoAperturaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacionApertura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, usuarioCierreId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, respoCierreId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacionCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorApertura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalFacturas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorCaja);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarRetiro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO atecco_cierrecaja " +
      "        (AD_Org_ID, Docstatus, Fecha_Apertura, Usuario_Apertura_ID, Respo_Apertura_ID, Observacion_Apertura, Fecha_Cierre, Usuario_Cierre_ID, Respo_Cierre_ID, Observacion_Cierre, Valor_Apertura, Total_Facturas, Valor_Deposito, Valor_Caja, Valor_Cierre, Procesar, Procesar_Validar, Procesar_Retiro, Isactive, Atecco_Cierrecaja_ID, AD_Client_ID, C_Doctype_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), TO_TIMESTAMP(?, ?), (?), (?), (?), TO_TIMESTAMP(?, ?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaApertura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, usuarioAperturaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, respoAperturaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacionApertura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, usuarioCierreId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, respoCierreId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, observacionCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorApertura);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalFacturas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorDeposito);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorCaja);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, valorCierre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesarRetiro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ateccoCierrecajaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM atecco_cierrecaja" +
      "        WHERE atecco_cierrecaja.Atecco_Cierrecaja_ID = ? " +
      "        AND atecco_cierrecaja.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND atecco_cierrecaja.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM atecco_cierrecaja" +
      "         WHERE atecco_cierrecaja.Atecco_Cierrecaja_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM atecco_cierrecaja" +
      "         WHERE atecco_cierrecaja.Atecco_Cierrecaja_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
