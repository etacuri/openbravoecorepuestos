//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.cargareembolso.CargaReembolso;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data implements FieldProvider {
static Logger log4j = Logger.getLogger(CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String mensaje;
  public String adOrgId;
  public String isactive;
  public String factNumEstab;
  public String factPuntoEmision;
  public String factDocumento;
  public String factTaxid;
  public String areCDoctypeId;
  public String areCDoctypeIdr;
  public String areTipoId;
  public String areTipoIdr;
  public String areTaxid;
  public String areNumEstab;
  public String arePuntoEmision;
  public String areDocumentno;
  public String areFechaEmision;
  public String areAutSri;
  public String areBaseCero;
  public String areBaseDoce;
  public String areBaseExento;
  public String areValorIva;
  public String areValorIce;
  public String procesar;
  public String adClientId;
  public String areIReembolsoId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("mensaje"))
      return mensaje;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("fact_num_estab") || fieldName.equals("factNumEstab"))
      return factNumEstab;
    else if (fieldName.equalsIgnoreCase("fact_punto_emision") || fieldName.equals("factPuntoEmision"))
      return factPuntoEmision;
    else if (fieldName.equalsIgnoreCase("fact_documento") || fieldName.equals("factDocumento"))
      return factDocumento;
    else if (fieldName.equalsIgnoreCase("fact_taxid") || fieldName.equals("factTaxid"))
      return factTaxid;
    else if (fieldName.equalsIgnoreCase("are_c_doctype_id") || fieldName.equals("areCDoctypeId"))
      return areCDoctypeId;
    else if (fieldName.equalsIgnoreCase("are_c_doctype_idr") || fieldName.equals("areCDoctypeIdr"))
      return areCDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("are_tipo_id") || fieldName.equals("areTipoId"))
      return areTipoId;
    else if (fieldName.equalsIgnoreCase("are_tipo_idr") || fieldName.equals("areTipoIdr"))
      return areTipoIdr;
    else if (fieldName.equalsIgnoreCase("are_taxid") || fieldName.equals("areTaxid"))
      return areTaxid;
    else if (fieldName.equalsIgnoreCase("are_num_estab") || fieldName.equals("areNumEstab"))
      return areNumEstab;
    else if (fieldName.equalsIgnoreCase("are_punto_emision") || fieldName.equals("arePuntoEmision"))
      return arePuntoEmision;
    else if (fieldName.equalsIgnoreCase("are_documentno") || fieldName.equals("areDocumentno"))
      return areDocumentno;
    else if (fieldName.equalsIgnoreCase("are_fecha_emision") || fieldName.equals("areFechaEmision"))
      return areFechaEmision;
    else if (fieldName.equalsIgnoreCase("are_aut_sri") || fieldName.equals("areAutSri"))
      return areAutSri;
    else if (fieldName.equalsIgnoreCase("are_base_cero") || fieldName.equals("areBaseCero"))
      return areBaseCero;
    else if (fieldName.equalsIgnoreCase("are_base_doce") || fieldName.equals("areBaseDoce"))
      return areBaseDoce;
    else if (fieldName.equalsIgnoreCase("are_base_exento") || fieldName.equals("areBaseExento"))
      return areBaseExento;
    else if (fieldName.equalsIgnoreCase("are_valor_iva") || fieldName.equals("areValorIva"))
      return areValorIva;
    else if (fieldName.equalsIgnoreCase("are_valor_ice") || fieldName.equals("areValorIce"))
      return areValorIce;
    else if (fieldName.equalsIgnoreCase("procesar"))
      return procesar;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("are_i_reembolso_id") || fieldName.equals("areIReembolsoId"))
      return areIReembolsoId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(are_i_reembolso.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = are_i_reembolso.CreatedBy) as CreatedByR, " +
      "        to_char(are_i_reembolso.Updated, ?) as updated, " +
      "        to_char(are_i_reembolso.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        are_i_reembolso.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = are_i_reembolso.UpdatedBy) as UpdatedByR," +
      "        are_i_reembolso.Mensaje, " +
      "are_i_reembolso.AD_Org_ID, " +
      "COALESCE(are_i_reembolso.Isactive, 'N') AS Isactive, " +
      "are_i_reembolso.Fact_Num_Estab, " +
      "are_i_reembolso.Fact_Punto_Emision, " +
      "are_i_reembolso.Fact_Documento, " +
      "are_i_reembolso.Fact_Taxid, " +
      "are_i_reembolso.ARE_C_Doctype_ID, " +
      "(CASE WHEN are_i_reembolso.ARE_C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS ARE_C_Doctype_IDR, " +
      "are_i_reembolso.ARE_Tipo_ID, " +
      "(CASE WHEN are_i_reembolso.ARE_Tipo_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS ARE_Tipo_IDR, " +
      "are_i_reembolso.ARE_Taxid, " +
      "are_i_reembolso.ARE_Num_Estab, " +
      "are_i_reembolso.ARE_Punto_Emision, " +
      "are_i_reembolso.ARE_Documentno, " +
      "are_i_reembolso.ARE_Fecha_Emision, " +
      "are_i_reembolso.ARE_Aut_Sri, " +
      "are_i_reembolso.ARE_Base_Cero, " +
      "are_i_reembolso.ARE_Base_Doce, " +
      "are_i_reembolso.ARE_Base_Exento, " +
      "are_i_reembolso.ARE_Valor_Iva, " +
      "are_i_reembolso.ARE_Valor_Ice, " +
      "are_i_reembolso.Procesar, " +
      "are_i_reembolso.AD_Client_ID, " +
      "are_i_reembolso.ARE_I_Reembolso_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM are_i_reembolso left join (select C_DocType_ID, Name from C_DocType) table1 on (are_i_reembolso.ARE_C_Doctype_ID =  table1.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL1 on (table1.C_DocType_ID = tableTRL1.C_DocType_ID and tableTRL1.AD_Language = ?)  left join ad_ref_list_v list1 on (are_i_reembolso.ARE_Tipo_ID = list1.value and list1.ad_reference_id = 'E8F31E09E4EB4943A6F2091166C1043D' and list1.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND are_i_reembolso.ARE_I_Reembolso_ID = ? " +
      "        AND are_i_reembolso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND are_i_reembolso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data = new CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data();
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.created = UtilSql.getValue(result, "created");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.updated = UtilSql.getValue(result, "updated");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.mensaje = UtilSql.getValue(result, "mensaje");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.isactive = UtilSql.getValue(result, "isactive");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.factNumEstab = UtilSql.getValue(result, "fact_num_estab");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.factPuntoEmision = UtilSql.getValue(result, "fact_punto_emision");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.factDocumento = UtilSql.getValue(result, "fact_documento");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.factTaxid = UtilSql.getValue(result, "fact_taxid");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areCDoctypeId = UtilSql.getValue(result, "are_c_doctype_id");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areCDoctypeIdr = UtilSql.getValue(result, "are_c_doctype_idr");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areTipoId = UtilSql.getValue(result, "are_tipo_id");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areTipoIdr = UtilSql.getValue(result, "are_tipo_idr");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areTaxid = UtilSql.getValue(result, "are_taxid");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areNumEstab = UtilSql.getValue(result, "are_num_estab");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.arePuntoEmision = UtilSql.getValue(result, "are_punto_emision");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areDocumentno = UtilSql.getValue(result, "are_documentno");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areFechaEmision = UtilSql.getDateValue(result, "are_fecha_emision", "dd-MM-yyyy");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areAutSri = UtilSql.getValue(result, "are_aut_sri");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areBaseCero = UtilSql.getValue(result, "are_base_cero");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areBaseDoce = UtilSql.getValue(result, "are_base_doce");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areBaseExento = UtilSql.getValue(result, "are_base_exento");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areValorIva = UtilSql.getValue(result, "are_valor_iva");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areValorIce = UtilSql.getValue(result, "are_valor_ice");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.procesar = UtilSql.getValue(result, "procesar");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.areIReembolsoId = UtilSql.getValue(result, "are_i_reembolso_id");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.language = UtilSql.getValue(result, "language");
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.adUserClient = "";
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.adOrgClient = "";
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.createdby = "";
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.trBgcolor = "";
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.totalCount = "";
        objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[] = new CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[vector.size()];
    vector.copyInto(objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data);
    return(objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data);
  }

/**
Create a registry
 */
  public static CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[] set(String createdby, String createdbyr, String mensaje, String procesar, String areDocumentno, String isactive, String areAutSri, String factTaxid, String adClientId, String areBaseCero, String areValorIva, String adOrgId, String factPuntoEmision, String areIReembolsoId, String areBaseExento, String factDocumento, String areBaseDoce, String areCDoctypeId, String arePuntoEmision, String areNumEstab, String updatedby, String updatedbyr, String areFechaEmision, String areTipoId, String areValorIce, String areTaxid, String factNumEstab)    throws ServletException {
    CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[] = new CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[1];
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0] = new CargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data();
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].created = "";
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].createdbyr = createdbyr;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].updated = "";
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].updatedTimeStamp = "";
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].updatedby = updatedby;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].updatedbyr = updatedbyr;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].mensaje = mensaje;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].adOrgId = adOrgId;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].isactive = isactive;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].factNumEstab = factNumEstab;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].factPuntoEmision = factPuntoEmision;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].factDocumento = factDocumento;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].factTaxid = factTaxid;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areCDoctypeId = areCDoctypeId;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areCDoctypeIdr = "";
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areTipoId = areTipoId;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areTipoIdr = "";
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areTaxid = areTaxid;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areNumEstab = areNumEstab;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].arePuntoEmision = arePuntoEmision;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areDocumentno = areDocumentno;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areFechaEmision = areFechaEmision;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areAutSri = areAutSri;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areBaseCero = areBaseCero;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areBaseDoce = areBaseDoce;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areBaseExento = areBaseExento;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areValorIva = areValorIva;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areValorIce = areValorIce;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].procesar = procesar;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].adClientId = adClientId;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].areIReembolsoId = areIReembolsoId;
    objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data[0].language = "";
    return objectCargaReembolso116F88967A3B49FCB24E7B0DA2C9A605Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef1EAF4C85B83B49D6944B5E1D3F2C0278_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefCFC5A4DC138A491690E5A2E3FCD81129_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE are_i_reembolso" +
      "        SET Mensaje = (?) , AD_Org_ID = (?) , Isactive = (?) , Fact_Num_Estab = (?) , Fact_Punto_Emision = (?) , Fact_Documento = (?) , Fact_Taxid = (?) , ARE_C_Doctype_ID = (?) , ARE_Tipo_ID = (?) , ARE_Taxid = (?) , ARE_Num_Estab = (?) , ARE_Punto_Emision = (?) , ARE_Documentno = (?) , ARE_Fecha_Emision = TO_DATE(?) , ARE_Aut_Sri = (?) , ARE_Base_Cero = TO_NUMBER(?) , ARE_Base_Doce = TO_NUMBER(?) , ARE_Base_Exento = TO_NUMBER(?) , ARE_Valor_Iva = TO_NUMBER(?) , ARE_Valor_Ice = TO_NUMBER(?) , Procesar = (?) , AD_Client_ID = (?) , ARE_I_Reembolso_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE are_i_reembolso.ARE_I_Reembolso_ID = ? " +
      "        AND are_i_reembolso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND are_i_reembolso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mensaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factNumEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factPuntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factDocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factTaxid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areTipoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areTaxid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areNumEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, arePuntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areDocumentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areFechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areBaseCero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areBaseDoce);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areBaseExento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areValorIva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areValorIce);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areIReembolsoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areIReembolsoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO are_i_reembolso " +
      "        (Mensaje, AD_Org_ID, Isactive, Fact_Num_Estab, Fact_Punto_Emision, Fact_Documento, Fact_Taxid, ARE_C_Doctype_ID, ARE_Tipo_ID, ARE_Taxid, ARE_Num_Estab, ARE_Punto_Emision, ARE_Documentno, ARE_Fecha_Emision, ARE_Aut_Sri, ARE_Base_Cero, ARE_Base_Doce, ARE_Base_Exento, ARE_Valor_Iva, ARE_Valor_Ice, Procesar, AD_Client_ID, ARE_I_Reembolso_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mensaje);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factNumEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factPuntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factDocumento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, factTaxid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areTipoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areTaxid);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areNumEstab);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, arePuntoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areDocumentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areFechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areAutSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areBaseCero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areBaseDoce);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areBaseExento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areValorIva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areValorIce);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, areIReembolsoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM are_i_reembolso" +
      "        WHERE are_i_reembolso.ARE_I_Reembolso_ID = ? " +
      "        AND are_i_reembolso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND are_i_reembolso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM are_i_reembolso" +
      "         WHERE are_i_reembolso.ARE_I_Reembolso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM are_i_reembolso" +
      "         WHERE are_i_reembolso.ARE_I_Reembolso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
