//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.ContratoEmpleado;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData implements FieldProvider {
static Logger log4j = Logger.getLogger(PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String diaspermiso;
  public String fechaPermiso;
  public String fechaPermisoFin;
  public String tipoPermiso;
  public String tipoPermisor;
  public String motivoPermiso;
  public String motivoPermisor;
  public String dias;
  public String horas;
  public String isactive;
  public String estado;
  public String emNeEstado;
  public String emNeEstador;
  public String emNeObservacion;
  public String emNeProcesar;
  public String emNeProcesarBtn;
  public String processed;
  public String noContratoEmpleadoId;
  public String cBpartnerId;
  public String adOrgId;
  public String noPermisoId;
  public String procesado;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("diaspermiso"))
      return diaspermiso;
    else if (fieldName.equalsIgnoreCase("fecha_permiso") || fieldName.equals("fechaPermiso"))
      return fechaPermiso;
    else if (fieldName.equalsIgnoreCase("fecha_permiso_fin") || fieldName.equals("fechaPermisoFin"))
      return fechaPermisoFin;
    else if (fieldName.equalsIgnoreCase("tipo_permiso") || fieldName.equals("tipoPermiso"))
      return tipoPermiso;
    else if (fieldName.equalsIgnoreCase("tipo_permisor") || fieldName.equals("tipoPermisor"))
      return tipoPermisor;
    else if (fieldName.equalsIgnoreCase("motivo_permiso") || fieldName.equals("motivoPermiso"))
      return motivoPermiso;
    else if (fieldName.equalsIgnoreCase("motivo_permisor") || fieldName.equals("motivoPermisor"))
      return motivoPermisor;
    else if (fieldName.equalsIgnoreCase("dias"))
      return dias;
    else if (fieldName.equalsIgnoreCase("horas"))
      return horas;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("estado"))
      return estado;
    else if (fieldName.equalsIgnoreCase("em_ne_estado") || fieldName.equals("emNeEstado"))
      return emNeEstado;
    else if (fieldName.equalsIgnoreCase("em_ne_estador") || fieldName.equals("emNeEstador"))
      return emNeEstador;
    else if (fieldName.equalsIgnoreCase("em_ne_observacion") || fieldName.equals("emNeObservacion"))
      return emNeObservacion;
    else if (fieldName.equalsIgnoreCase("em_ne_procesar") || fieldName.equals("emNeProcesar"))
      return emNeProcesar;
    else if (fieldName.equalsIgnoreCase("em_ne_procesar_btn") || fieldName.equals("emNeProcesarBtn"))
      return emNeProcesarBtn;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("no_contrato_empleado_id") || fieldName.equals("noContratoEmpleadoId"))
      return noContratoEmpleadoId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("no_permiso_id") || fieldName.equals("noPermisoId"))
      return noPermisoId;
    else if (fieldName.equalsIgnoreCase("procesado"))
      return procesado;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noContratoEmpleadoId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, noContratoEmpleadoId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String noContratoEmpleadoId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_permiso.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_permiso.CreatedBy) as CreatedByR, " +
      "        to_char(no_permiso.Updated, ?) as updated, " +
      "        to_char(no_permiso.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_permiso.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_permiso.UpdatedBy) as UpdatedByR," +
      "        COALESCE(no_permiso.Diaspermiso, 'N') AS Diaspermiso, " +
      "no_permiso.Fecha_Permiso, " +
      "no_permiso.Fecha_Permiso_Fin, " +
      "no_permiso.Tipo_Permiso, " +
      "(CASE WHEN no_permiso.Tipo_Permiso IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_PermisoR, " +
      "no_permiso.Motivo_Permiso, " +
      "(CASE WHEN no_permiso.Motivo_Permiso IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS Motivo_PermisoR, " +
      "no_permiso.Dias, " +
      "no_permiso.Horas, " +
      "COALESCE(no_permiso.Isactive, 'N') AS Isactive, " +
      "no_permiso.Estado, " +
      "no_permiso.em_ne_estado, " +
      "(CASE WHEN no_permiso.em_ne_estado IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list3.name),'') ) END) AS em_ne_estadoR, " +
      "no_permiso.em_ne_observacion, " +
      "no_permiso.em_ne_procesar, " +
      "list4.name as em_ne_procesar_BTN, " +
      "no_permiso.Processed, " +
      "no_permiso.no_contrato_empleado_id, " +
      "no_permiso.C_Bpartner_ID, " +
      "no_permiso.AD_Org_ID, " +
      "no_permiso.NO_Permiso_ID, " +
      "COALESCE(no_permiso.Procesado, 'N') AS Procesado, " +
      "no_permiso.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_permiso left join ad_ref_list_v list1 on (no_permiso.Tipo_Permiso = list1.value and list1.ad_reference_id = '7FB5608D7ED64B0DA88308BE0234600A' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (no_permiso.Motivo_Permiso = list2.value and list2.ad_reference_id = '5BCEFDAAF41642E5BA3E334D4C810E6D' and list2.ad_language = ?)  left join ad_ref_list_v list3 on (no_permiso.em_ne_estado = list3.value and list3.ad_reference_id = '8C83DEB7CA664B359FD14679B64236A0' and list3.ad_language = ?)  left join ad_ref_list_v list4 on (list4.ad_reference_id = '8C83DEB7CA664B359FD14679B64236A0' and list4.ad_language = ?  AND no_permiso.em_ne_procesar = TO_CHAR(list4.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((noContratoEmpleadoId==null || noContratoEmpleadoId.equals(""))?"":"  AND no_permiso.no_contrato_empleado_id = ?  ");
    strSql = strSql + 
      "        AND no_permiso.NO_Permiso_ID = ? " +
      "        AND no_permiso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_permiso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (noContratoEmpleadoId != null && !(noContratoEmpleadoId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData = new PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData();
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.created = UtilSql.getValue(result, "created");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.updated = UtilSql.getValue(result, "updated");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.updatedby = UtilSql.getValue(result, "updatedby");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.diaspermiso = UtilSql.getValue(result, "diaspermiso");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.fechaPermiso = UtilSql.getDateValue(result, "fecha_permiso", "dd-MM-yyyy");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.fechaPermisoFin = UtilSql.getDateValue(result, "fecha_permiso_fin", "dd-MM-yyyy");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.tipoPermiso = UtilSql.getValue(result, "tipo_permiso");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.tipoPermisor = UtilSql.getValue(result, "tipo_permisor");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.motivoPermiso = UtilSql.getValue(result, "motivo_permiso");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.motivoPermisor = UtilSql.getValue(result, "motivo_permisor");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.dias = UtilSql.getValue(result, "dias");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.horas = UtilSql.getValue(result, "horas");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.isactive = UtilSql.getValue(result, "isactive");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.estado = UtilSql.getValue(result, "estado");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.emNeEstado = UtilSql.getValue(result, "em_ne_estado");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.emNeEstador = UtilSql.getValue(result, "em_ne_estador");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.emNeObservacion = UtilSql.getValue(result, "em_ne_observacion");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.emNeProcesar = UtilSql.getValue(result, "em_ne_procesar");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.emNeProcesarBtn = UtilSql.getValue(result, "em_ne_procesar_btn");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.processed = UtilSql.getValue(result, "processed");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.noContratoEmpleadoId = UtilSql.getValue(result, "no_contrato_empleado_id");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.noPermisoId = UtilSql.getValue(result, "no_permiso_id");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.procesado = UtilSql.getValue(result, "procesado");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.language = UtilSql.getValue(result, "language");
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.adUserClient = "";
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.adOrgClient = "";
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.createdby = "";
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.trBgcolor = "";
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.totalCount = "";
        objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[] = new PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[vector.size()];
    vector.copyInto(objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData);
    return(objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData);
  }

/**
Create a registry
 */
  public static PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[] set(String noContratoEmpleadoId, String emNeProcesar, String emNeProcesarBtn, String motivoPermiso, String processed, String adOrgId, String fechaPermiso, String dias, String createdby, String createdbyr, String updatedby, String updatedbyr, String isactive, String emNeEstado, String cBpartnerId, String noPermisoId, String procesado, String emNeObservacion, String diaspermiso, String estado, String horas, String tipoPermiso, String adClientId, String fechaPermisoFin)    throws ServletException {
    PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[] = new PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[1];
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0] = new PermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData();
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].created = "";
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].createdbyr = createdbyr;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].updated = "";
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].updatedTimeStamp = "";
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].updatedby = updatedby;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].updatedbyr = updatedbyr;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].diaspermiso = diaspermiso;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].fechaPermiso = fechaPermiso;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].fechaPermisoFin = fechaPermisoFin;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].tipoPermiso = tipoPermiso;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].tipoPermisor = "";
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].motivoPermiso = motivoPermiso;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].motivoPermisor = "";
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].dias = dias;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].horas = horas;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].isactive = isactive;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].estado = estado;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].emNeEstado = emNeEstado;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].emNeEstador = "";
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].emNeObservacion = emNeObservacion;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].emNeProcesar = emNeProcesar;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].emNeProcesarBtn = emNeProcesarBtn;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].processed = processed;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].noContratoEmpleadoId = noContratoEmpleadoId;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].cBpartnerId = cBpartnerId;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].adOrgId = adOrgId;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].noPermisoId = noPermisoId;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].procesado = procesado;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].adClientId = adClientId;
    objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData[0].language = "";
    return objectPermisoEmpleados742FA61199A44DD3BB60DE3AFFB7A3DAData;
  }

/**
Select for auxiliar field
 */
  public static String selectDef3DE858E5D80845BD9146262347FBD737_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4095570FE9E54FECB4F9FAF7C2A44187_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT no_permiso.no_contrato_empleado_id AS NAME" +
      "        FROM no_permiso" +
      "        WHERE no_permiso.NO_Permiso_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM no_contrato_empleado left join (select no_contrato_empleado_id, Documentno from no_contrato_empleado) table1 on (no_contrato_empleado.no_contrato_empleado_id = table1.no_contrato_empleado_id) WHERE no_contrato_empleado.no_contrato_empleado_id = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String noContratoEmpleadoId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.Documentno), ''))) AS NAME FROM no_contrato_empleado left join (select no_contrato_empleado_id, Documentno from no_contrato_empleado) table1 on (no_contrato_empleado.no_contrato_empleado_id = table1.no_contrato_empleado_id) WHERE no_contrato_empleado.no_contrato_empleado_id = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_permiso" +
      "        SET Diaspermiso = (?) , Fecha_Permiso = TO_DATE(?) , Fecha_Permiso_Fin = TO_DATE(?) , Tipo_Permiso = (?) , Motivo_Permiso = (?) , Dias = TO_NUMBER(?) , Horas = TO_NUMBER(?) , Isactive = (?) , Estado = (?) , em_ne_estado = (?) , em_ne_observacion = (?) , em_ne_procesar = (?) , Processed = (?) , no_contrato_empleado_id = (?) , C_Bpartner_ID = (?) , AD_Org_ID = (?) , NO_Permiso_ID = (?) , Procesado = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_permiso.NO_Permiso_ID = ? " +
      "                 AND no_permiso.no_contrato_empleado_id = ? " +
      "        AND no_permiso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_permiso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, diaspermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPermisoFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, motivoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, horas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeEstado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeObservacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPermisoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPermisoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_permiso " +
      "        (Diaspermiso, Fecha_Permiso, Fecha_Permiso_Fin, Tipo_Permiso, Motivo_Permiso, Dias, Horas, Isactive, Estado, em_ne_estado, em_ne_observacion, em_ne_procesar, Processed, no_contrato_empleado_id, C_Bpartner_ID, AD_Org_ID, NO_Permiso_ID, Procesado, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), TO_DATE(?), TO_DATE(?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, diaspermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaPermisoFin);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, motivoPermiso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dias);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, horas);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, estado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeEstado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeObservacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noPermisoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, procesado);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String noContratoEmpleadoId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_permiso" +
      "        WHERE no_permiso.NO_Permiso_ID = ? " +
      "                 AND no_permiso.no_contrato_empleado_id = ? " +
      "        AND no_permiso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_permiso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noContratoEmpleadoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_permiso" +
      "         WHERE no_permiso.NO_Permiso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_permiso" +
      "         WHERE no_permiso.NO_Permiso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
