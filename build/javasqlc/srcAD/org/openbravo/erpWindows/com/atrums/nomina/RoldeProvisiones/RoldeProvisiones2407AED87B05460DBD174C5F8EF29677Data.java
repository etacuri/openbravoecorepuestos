//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.RoldeProvisiones;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data implements FieldProvider {
static Logger log4j = Logger.getLogger(RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String noAreaEmpresaId;
  public String noAreaEmpresaIdr;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String emNeNumContrato;
  public String cDoctypeId;
  public String documentno;
  public String totalNeto;
  public String docstatus;
  public String isactive;
  public String processed;
  public String docaccionno;
  public String docaccionnoBtn;
  public String payment;
  public String totalIngreso;
  public String totalEgreso;
  public String cPeriodId;
  public String inPayment;
  public String dateacct;
  public String noCDoctypeId;
  public String noCDoctypeIdr;
  public String ispago;
  public String adClientId;
  public String noRolPagoProvisionId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("no_area_empresa_id") || fieldName.equals("noAreaEmpresaId"))
      return noAreaEmpresaId;
    else if (fieldName.equalsIgnoreCase("no_area_empresa_idr") || fieldName.equals("noAreaEmpresaIdr"))
      return noAreaEmpresaIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("em_ne_num_contrato") || fieldName.equals("emNeNumContrato"))
      return emNeNumContrato;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("total_neto") || fieldName.equals("totalNeto"))
      return totalNeto;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("docaccionno"))
      return docaccionno;
    else if (fieldName.equalsIgnoreCase("docaccionno_btn") || fieldName.equals("docaccionnoBtn"))
      return docaccionnoBtn;
    else if (fieldName.equalsIgnoreCase("payment"))
      return payment;
    else if (fieldName.equalsIgnoreCase("total_ingreso") || fieldName.equals("totalIngreso"))
      return totalIngreso;
    else if (fieldName.equalsIgnoreCase("total_egreso") || fieldName.equals("totalEgreso"))
      return totalEgreso;
    else if (fieldName.equalsIgnoreCase("c_period_id") || fieldName.equals("cPeriodId"))
      return cPeriodId;
    else if (fieldName.equalsIgnoreCase("in_payment") || fieldName.equals("inPayment"))
      return inPayment;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("no_c_doctype_id") || fieldName.equals("noCDoctypeId"))
      return noCDoctypeId;
    else if (fieldName.equalsIgnoreCase("no_c_doctype_idr") || fieldName.equals("noCDoctypeIdr"))
      return noCDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("ispago"))
      return ispago;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("no_rol_pago_provision_id") || fieldName.equals("noRolPagoProvisionId"))
      return noRolPagoProvisionId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_rol_pago_provision.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_rol_pago_provision.CreatedBy) as CreatedByR, " +
      "        to_char(no_rol_pago_provision.Updated, ?) as updated, " +
      "        to_char(no_rol_pago_provision.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_rol_pago_provision.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_rol_pago_provision.UpdatedBy) as UpdatedByR," +
      "        no_rol_pago_provision.AD_Org_ID, " +
      "(CASE WHEN no_rol_pago_provision.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_rol_pago_provision.NO_Area_Empresa_ID, " +
      "(CASE WHEN no_rol_pago_provision.NO_Area_Empresa_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Nombre), ''))),'') ) END) AS NO_Area_Empresa_IDR, " +
      "no_rol_pago_provision.C_Bpartner_ID, " +
      "(CASE WHEN no_rol_pago_provision.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name2), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "no_rol_pago_provision.em_ne_num_contrato, " +
      "no_rol_pago_provision.C_Doctype_ID, " +
      "no_rol_pago_provision.Documentno, " +
      "no_rol_pago_provision.Total_Neto, " +
      "no_rol_pago_provision.Docstatus, " +
      "COALESCE(no_rol_pago_provision.Isactive, 'N') AS Isactive, " +
      "COALESCE(no_rol_pago_provision.Processed, 'N') AS Processed, " +
      "no_rol_pago_provision.Docaccionno, " +
      "list1.name as Docaccionno_BTN, " +
      "no_rol_pago_provision.Payment, " +
      "no_rol_pago_provision.Total_Ingreso, " +
      "no_rol_pago_provision.Total_Egreso, " +
      "no_rol_pago_provision.C_Period_ID, " +
      "COALESCE(no_rol_pago_provision.IN_Payment, 'N') AS IN_Payment, " +
      "no_rol_pago_provision.Dateacct, " +
      "no_rol_pago_provision.NO_C_Doctype_ID, " +
      "(CASE WHEN no_rol_pago_provision.NO_C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL4.Name IS NULL THEN TO_CHAR(table4.Name) ELSE TO_CHAR(tableTRL4.Name) END)), ''))),'') ) END) AS NO_C_Doctype_IDR, " +
      "COALESCE(no_rol_pago_provision.Ispago, 'N') AS Ispago, " +
      "no_rol_pago_provision.AD_Client_ID, " +
      "no_rol_pago_provision.NO_Rol_Pago_Provision_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_rol_pago_provision left join (select AD_Org_ID, Name from AD_Org) table1 on (no_rol_pago_provision.AD_Org_ID = table1.AD_Org_ID) left join (select NO_Area_Empresa_ID, Nombre from no_area_empresa) table2 on (no_rol_pago_provision.NO_Area_Empresa_ID =  table2.NO_Area_Empresa_ID) left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table3 on (no_rol_pago_provision.C_Bpartner_ID = table3.C_BPartner_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '31D050E5C2D843B99AD7E9470D9E8579' and list1.ad_language = ?  AND no_rol_pago_provision.Docaccionno = TO_CHAR(list1.value)) left join (select C_DocType_ID, Name from C_DocType) table4 on (no_rol_pago_provision.NO_C_Doctype_ID =  table4.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL4 on (table4.C_DocType_ID = tableTRL4.C_DocType_ID and tableTRL4.AD_Language = ?) " +
      "        WHERE 2=2 " +
      " AND No_Rol_Pago_Provision.isPago='N'" +
      "        AND 1=1 " +
      "        AND no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ? " +
      "        AND no_rol_pago_provision.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_rol_pago_provision.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data = new RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data();
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.created = UtilSql.getValue(result, "created");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.updated = UtilSql.getValue(result, "updated");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.noAreaEmpresaId = UtilSql.getValue(result, "no_area_empresa_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.noAreaEmpresaIdr = UtilSql.getValue(result, "no_area_empresa_idr");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.emNeNumContrato = UtilSql.getValue(result, "em_ne_num_contrato");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.documentno = UtilSql.getValue(result, "documentno");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.totalNeto = UtilSql.getValue(result, "total_neto");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.isactive = UtilSql.getValue(result, "isactive");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.processed = UtilSql.getValue(result, "processed");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.docaccionno = UtilSql.getValue(result, "docaccionno");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.docaccionnoBtn = UtilSql.getValue(result, "docaccionno_btn");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.payment = UtilSql.getValue(result, "payment");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.totalIngreso = UtilSql.getValue(result, "total_ingreso");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.totalEgreso = UtilSql.getValue(result, "total_egreso");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.cPeriodId = UtilSql.getValue(result, "c_period_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.inPayment = UtilSql.getValue(result, "in_payment");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.noCDoctypeId = UtilSql.getValue(result, "no_c_doctype_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.noCDoctypeIdr = UtilSql.getValue(result, "no_c_doctype_idr");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.ispago = UtilSql.getValue(result, "ispago");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.noRolPagoProvisionId = UtilSql.getValue(result, "no_rol_pago_provision_id");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.language = UtilSql.getValue(result, "language");
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.adUserClient = "";
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.adOrgClient = "";
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.createdby = "";
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.trBgcolor = "";
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.totalCount = "";
        objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[] = new RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[vector.size()];
    vector.copyInto(objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data);
    return(objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data);
  }

/**
Create a registry
 */
  public static RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[] set(String noRolPagoProvisionId, String processed, String totalIngreso, String inPayment, String adClientId, String emNeNumContrato, String createdby, String createdbyr, String totalNeto, String updatedby, String updatedbyr, String cBpartnerId, String cBpartnerIdr, String ispago, String noCDoctypeId, String dateacct, String docstatus, String docaccionno, String docaccionnoBtn, String documentno, String payment, String noAreaEmpresaId, String isactive, String cDoctypeId, String cPeriodId, String totalEgreso, String adOrgId)    throws ServletException {
    RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[] = new RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[1];
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0] = new RoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data();
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].created = "";
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].createdbyr = createdbyr;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].updated = "";
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].updatedTimeStamp = "";
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].updatedby = updatedby;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].updatedbyr = updatedbyr;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].adOrgId = adOrgId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].adOrgIdr = "";
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].noAreaEmpresaId = noAreaEmpresaId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].noAreaEmpresaIdr = "";
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].cBpartnerId = cBpartnerId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].cBpartnerIdr = cBpartnerIdr;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].emNeNumContrato = emNeNumContrato;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].cDoctypeId = cDoctypeId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].documentno = documentno;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].totalNeto = totalNeto;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].docstatus = docstatus;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].isactive = isactive;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].processed = processed;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].docaccionno = docaccionno;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].docaccionnoBtn = docaccionnoBtn;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].payment = payment;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].totalIngreso = totalIngreso;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].totalEgreso = totalEgreso;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].cPeriodId = cPeriodId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].inPayment = inPayment;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].dateacct = dateacct;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].noCDoctypeId = noCDoctypeId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].noCDoctypeIdr = "";
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].ispago = ispago;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].adClientId = adClientId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].noRolPagoProvisionId = noRolPagoProvisionId;
    objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data[0].language = "";
    return objectRoldeProvisiones2407AED87B05460DBD174C5F8EF29677Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef44057D41DF18403D8D3853403E452D2B_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef584B0B2374BB42A5AB1176EB27F0B063_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef64196704CBB64C56B398440A16A42DD5_2(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefC708EC749EE04D2E9D1245034DCFF2A7(ConnectionProvider connectionProvider, String AD_ORG_ID, String AD_CLIENT_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT P.C_PERIOD_ID AS DEFAULTVALUE FROM C_PERIOD P WHERE EXISTS (SELECT * FROM C_PERIODCONTROL PC WHERE P.C_PERIOD_ID=PC.C_PERIOD_ID AND UPPER(PC.PERIODSTATUS)='0') AND EXISTS(SELECT * FROM C_CALENDAR C, C_YEAR Y WHERE Y.C_CALENDAR_ID=C.C_CALENDAR_ID AND P.C_YEAR_ID=Y.C_YEAR_ID AND AD_ISORGINCLUDED(?, C.AD_ORG_ID, ?)<> -1) AND P.AD_CLIENT_ID=? AND NOW() BETWEEN STARTDATE AND ENDDATE ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_rol_pago_provision" +
      "        SET AD_Org_ID = (?) , NO_Area_Empresa_ID = (?) , C_Bpartner_ID = (?) , em_ne_num_contrato = (?) , C_Doctype_ID = (?) , Documentno = (?) , Total_Neto = TO_NUMBER(?) , Docstatus = (?) , Isactive = (?) , Processed = (?) , Docaccionno = (?) , Payment = (?) , Total_Ingreso = TO_NUMBER(?) , Total_Egreso = TO_NUMBER(?) , C_Period_ID = (?) , IN_Payment = (?) , Dateacct = TO_DATE(?) , NO_C_Doctype_ID = (?) , Ispago = (?) , AD_Client_ID = (?) , NO_Rol_Pago_Provision_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ? " +
      "        AND no_rol_pago_provision.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_rol_pago_provision.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeNumContrato);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNeto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, payment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalIngreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalEgreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inPayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_rol_pago_provision " +
      "        (AD_Org_ID, NO_Area_Empresa_ID, C_Bpartner_ID, em_ne_num_contrato, C_Doctype_ID, Documentno, Total_Neto, Docstatus, Isactive, Processed, Docaccionno, Payment, Total_Ingreso, Total_Egreso, C_Period_ID, IN_Payment, Dateacct, NO_C_Doctype_ID, Ispago, AD_Client_ID, NO_Rol_Pago_Provision_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), TO_DATE(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAreaEmpresaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emNeNumContrato);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalNeto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaccionno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, payment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalIngreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalEgreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPeriodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, inPayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noCDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, ispago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noRolPagoProvisionId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_rol_pago_provision" +
      "        WHERE no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ? " +
      "        AND no_rol_pago_provision.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_rol_pago_provision.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_rol_pago_provision" +
      "         WHERE no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_rol_pago_provision" +
      "         WHERE no_rol_pago_provision.NO_Rol_Pago_Provision_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
