//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.Anticipos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class CabeceraFB9EADFCA7364909B248605C11FEAD82Data implements FieldProvider {
static Logger log4j = Logger.getLogger(CabeceraFB9EADFCA7364909B248605C11FEAD82Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String cDoctypetargetId;
  public String cDoctypetargetIdr;
  public String cReturnReasonId;
  public String documentno;
  public String dateordered;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String rmPickfromshipment;
  public String rmReceivematerials;
  public String datepromised;
  public String finPaymentmethodId;
  public String finPaymentmethodIdr;
  public String rmCreateinvoice;
  public String mPricelistId;
  public String emAteccoBanco;
  public String emAteccoTipoTarjeta;
  public String cPaymenttermId;
  public String mWarehouseId;
  public String invoicerule;
  public String docstatus;
  public String emAteccoAnticipostatus;
  public String totallines;
  public String grandtotal;
  public String cCurrencyId;
  public String adUserId;
  public String cDoctypeId;
  public String soResStatus;
  public String poreference;
  public String emAteccoTransporte;
  public String salesrepId;
  public String salesrepIdr;
  public String description;
  public String billtoId;
  public String emAteccoAnticipo;
  public String deliveryLocationId;
  public String emAteccoIniciar;
  public String emAteccoIniciarBtn;
  public String emAteccoValidar;
  public String emAprmAddpayment;
  public String emAteccoProcesar;
  public String emAteccoProcesarBtn;
  public String docaction;
  public String emAteccoDocaction;
  public String copyfrom;
  public String emAteccoImprimir;
  public String copyfrompo;
  public String deliveryviarule;
  public String mShipperId;
  public String deliveryrule;
  public String freightcostrule;
  public String freightamt;
  public String isdiscountprinted;
  public String priorityrule;
  public String cCampaignId;
  public String chargeamt;
  public String cChargeId;
  public String cActivityId;
  public String adOrgtrxId;
  public String quotationId;
  public String quotationIdr;
  public String calculatePromotions;
  public String rmAddorphanline;
  public String convertquotation;
  public String cRejectReasonId;
  public String validuntil;
  public String cProjectId;
  public String cProjectIdr;
  public String cCostcenterId;
  public String aAssetId;
  public String user1Id;
  public String user2Id;
  public String dropshipUserId;
  public String dropshipLocationId;
  public String isselfservice;
  public String deliverynotes;
  public String cIncotermsId;
  public String incotermsdescription;
  public String isselected;
  public String istaxincluded;
  public String posted;
  public String paymentrule;
  public String issotrx;
  public String dateprinted;
  public String processed;
  public String processing;
  public String emAteccoDocstatus;
  public String isactive;
  public String adClientId;
  public String cOrderId;
  public String dateacct;
  public String isprinted;
  public String isinvoiced;
  public String isdelivered;
  public String generatetemplate;
  public String dropshipBpartnerId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_idr") || fieldName.equals("cDoctypetargetIdr"))
      return cDoctypetargetIdr;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("rm_pickfromshipment") || fieldName.equals("rmPickfromshipment"))
      return rmPickfromshipment;
    else if (fieldName.equalsIgnoreCase("rm_receivematerials") || fieldName.equals("rmReceivematerials"))
      return rmReceivematerials;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_id") || fieldName.equals("finPaymentmethodId"))
      return finPaymentmethodId;
    else if (fieldName.equalsIgnoreCase("fin_paymentmethod_idr") || fieldName.equals("finPaymentmethodIdr"))
      return finPaymentmethodIdr;
    else if (fieldName.equalsIgnoreCase("rm_createinvoice") || fieldName.equals("rmCreateinvoice"))
      return rmCreateinvoice;
    else if (fieldName.equalsIgnoreCase("m_pricelist_id") || fieldName.equals("mPricelistId"))
      return mPricelistId;
    else if (fieldName.equalsIgnoreCase("em_atecco_banco") || fieldName.equals("emAteccoBanco"))
      return emAteccoBanco;
    else if (fieldName.equalsIgnoreCase("em_atecco_tipo_tarjeta") || fieldName.equals("emAteccoTipoTarjeta"))
      return emAteccoTipoTarjeta;
    else if (fieldName.equalsIgnoreCase("c_paymentterm_id") || fieldName.equals("cPaymenttermId"))
      return cPaymenttermId;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("invoicerule"))
      return invoicerule;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("em_atecco_anticipostatus") || fieldName.equals("emAteccoAnticipostatus"))
      return emAteccoAnticipostatus;
    else if (fieldName.equalsIgnoreCase("totallines"))
      return totallines;
    else if (fieldName.equalsIgnoreCase("grandtotal"))
      return grandtotal;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("ad_user_id") || fieldName.equals("adUserId"))
      return adUserId;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("so_res_status") || fieldName.equals("soResStatus"))
      return soResStatus;
    else if (fieldName.equalsIgnoreCase("poreference"))
      return poreference;
    else if (fieldName.equalsIgnoreCase("em_atecco_transporte") || fieldName.equals("emAteccoTransporte"))
      return emAteccoTransporte;
    else if (fieldName.equalsIgnoreCase("salesrep_id") || fieldName.equals("salesrepId"))
      return salesrepId;
    else if (fieldName.equalsIgnoreCase("salesrep_idr") || fieldName.equals("salesrepIdr"))
      return salesrepIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("billto_id") || fieldName.equals("billtoId"))
      return billtoId;
    else if (fieldName.equalsIgnoreCase("em_atecco_anticipo") || fieldName.equals("emAteccoAnticipo"))
      return emAteccoAnticipo;
    else if (fieldName.equalsIgnoreCase("delivery_location_id") || fieldName.equals("deliveryLocationId"))
      return deliveryLocationId;
    else if (fieldName.equalsIgnoreCase("em_atecco_iniciar") || fieldName.equals("emAteccoIniciar"))
      return emAteccoIniciar;
    else if (fieldName.equalsIgnoreCase("em_atecco_iniciar_btn") || fieldName.equals("emAteccoIniciarBtn"))
      return emAteccoIniciarBtn;
    else if (fieldName.equalsIgnoreCase("em_atecco_validar") || fieldName.equals("emAteccoValidar"))
      return emAteccoValidar;
    else if (fieldName.equalsIgnoreCase("em_aprm_addpayment") || fieldName.equals("emAprmAddpayment"))
      return emAprmAddpayment;
    else if (fieldName.equalsIgnoreCase("em_atecco_procesar") || fieldName.equals("emAteccoProcesar"))
      return emAteccoProcesar;
    else if (fieldName.equalsIgnoreCase("em_atecco_procesar_btn") || fieldName.equals("emAteccoProcesarBtn"))
      return emAteccoProcesarBtn;
    else if (fieldName.equalsIgnoreCase("docaction"))
      return docaction;
    else if (fieldName.equalsIgnoreCase("em_atecco_docaction") || fieldName.equals("emAteccoDocaction"))
      return emAteccoDocaction;
    else if (fieldName.equalsIgnoreCase("copyfrom"))
      return copyfrom;
    else if (fieldName.equalsIgnoreCase("em_atecco_imprimir") || fieldName.equals("emAteccoImprimir"))
      return emAteccoImprimir;
    else if (fieldName.equalsIgnoreCase("copyfrompo"))
      return copyfrompo;
    else if (fieldName.equalsIgnoreCase("deliveryviarule"))
      return deliveryviarule;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("deliveryrule"))
      return deliveryrule;
    else if (fieldName.equalsIgnoreCase("freightcostrule"))
      return freightcostrule;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("isdiscountprinted"))
      return isdiscountprinted;
    else if (fieldName.equalsIgnoreCase("priorityrule"))
      return priorityrule;
    else if (fieldName.equalsIgnoreCase("c_campaign_id") || fieldName.equals("cCampaignId"))
      return cCampaignId;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("c_activity_id") || fieldName.equals("cActivityId"))
      return cActivityId;
    else if (fieldName.equalsIgnoreCase("ad_orgtrx_id") || fieldName.equals("adOrgtrxId"))
      return adOrgtrxId;
    else if (fieldName.equalsIgnoreCase("quotation_id") || fieldName.equals("quotationId"))
      return quotationId;
    else if (fieldName.equalsIgnoreCase("quotation_idr") || fieldName.equals("quotationIdr"))
      return quotationIdr;
    else if (fieldName.equalsIgnoreCase("calculate_promotions") || fieldName.equals("calculatePromotions"))
      return calculatePromotions;
    else if (fieldName.equalsIgnoreCase("rm_addorphanline") || fieldName.equals("rmAddorphanline"))
      return rmAddorphanline;
    else if (fieldName.equalsIgnoreCase("convertquotation"))
      return convertquotation;
    else if (fieldName.equalsIgnoreCase("c_reject_reason_id") || fieldName.equals("cRejectReasonId"))
      return cRejectReasonId;
    else if (fieldName.equalsIgnoreCase("validuntil"))
      return validuntil;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("dropship_user_id") || fieldName.equals("dropshipUserId"))
      return dropshipUserId;
    else if (fieldName.equalsIgnoreCase("dropship_location_id") || fieldName.equals("dropshipLocationId"))
      return dropshipLocationId;
    else if (fieldName.equalsIgnoreCase("isselfservice"))
      return isselfservice;
    else if (fieldName.equalsIgnoreCase("deliverynotes"))
      return deliverynotes;
    else if (fieldName.equalsIgnoreCase("c_incoterms_id") || fieldName.equals("cIncotermsId"))
      return cIncotermsId;
    else if (fieldName.equalsIgnoreCase("incotermsdescription"))
      return incotermsdescription;
    else if (fieldName.equalsIgnoreCase("isselected"))
      return isselected;
    else if (fieldName.equalsIgnoreCase("istaxincluded"))
      return istaxincluded;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("paymentrule"))
      return paymentrule;
    else if (fieldName.equalsIgnoreCase("issotrx"))
      return issotrx;
    else if (fieldName.equalsIgnoreCase("dateprinted"))
      return dateprinted;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("em_atecco_docstatus") || fieldName.equals("emAteccoDocstatus"))
      return emAteccoDocstatus;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("isprinted"))
      return isprinted;
    else if (fieldName.equalsIgnoreCase("isinvoiced"))
      return isinvoiced;
    else if (fieldName.equalsIgnoreCase("isdelivered"))
      return isdelivered;
    else if (fieldName.equalsIgnoreCase("generatetemplate"))
      return generatetemplate;
    else if (fieldName.equalsIgnoreCase("dropship_bpartner_id") || fieldName.equals("dropshipBpartnerId"))
      return dropshipBpartnerId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static CabeceraFB9EADFCA7364909B248605C11FEAD82Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static CabeceraFB9EADFCA7364909B248605C11FEAD82Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_Order.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.CreatedBy) as CreatedByR, " +
      "        to_char(C_Order.Updated, ?) as updated, " +
      "        to_char(C_Order.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_Order.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_Order.UpdatedBy) as UpdatedByR," +
      "        C_Order.AD_Org_ID, " +
      "(CASE WHEN C_Order.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "C_Order.C_DocTypeTarget_ID, " +
      "(CASE WHEN C_Order.C_DocTypeTarget_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))),'') ) END) AS C_DocTypeTarget_IDR, " +
      "C_Order.C_Return_Reason_ID, " +
      "C_Order.DocumentNo, " +
      "C_Order.DateOrdered, " +
      "C_Order.C_BPartner_ID, " +
      "(CASE WHEN C_Order.C_BPartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name2), ''))),'') ) END) AS C_BPartner_IDR, " +
      "C_Order.C_BPartner_Location_ID, " +
      "(CASE WHEN C_Order.C_BPartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table5.Name), ''))),'') ) END) AS C_BPartner_Location_IDR, " +
      "C_Order.RM_PickFromShipment, " +
      "C_Order.RM_ReceiveMaterials, " +
      "C_Order.DatePromised, " +
      "C_Order.FIN_Paymentmethod_ID, " +
      "(CASE WHEN C_Order.FIN_Paymentmethod_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table6.Name), ''))),'') ) END) AS FIN_Paymentmethod_IDR, " +
      "C_Order.RM_CreateInvoice, " +
      "C_Order.M_PriceList_ID, " +
      "C_Order.EM_Atecco_Banco, " +
      "C_Order.EM_Atecco_Tipo_Tarjeta, " +
      "C_Order.C_PaymentTerm_ID, " +
      "C_Order.M_Warehouse_ID, " +
      "C_Order.InvoiceRule, " +
      "C_Order.DocStatus, " +
      "C_Order.EM_Atecco_Anticipostatus, " +
      "C_Order.TotalLines, " +
      "C_Order.GrandTotal, " +
      "C_Order.C_Currency_ID, " +
      "C_Order.AD_User_ID, " +
      "C_Order.C_DocType_ID, " +
      "C_Order.SO_Res_Status, " +
      "C_Order.POReference, " +
      "COALESCE(C_Order.EM_Atecco_Transporte, 'N') AS EM_Atecco_Transporte, " +
      "C_Order.SalesRep_ID, " +
      "(CASE WHEN C_Order.SalesRep_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS SalesRep_IDR, " +
      "C_Order.Description, " +
      "C_Order.BillTo_ID, " +
      "C_Order.EM_Atecco_Anticipo, " +
      "C_Order.Delivery_Location_ID, " +
      "C_Order.EM_Atecco_Iniciar, " +
      "list1.name as EM_Atecco_Iniciar_BTN, " +
      "C_Order.EM_Atecco_Validar, " +
      "C_Order.EM_APRM_AddPayment, " +
      "C_Order.EM_Atecco_Procesar, " +
      "list2.name as EM_Atecco_Procesar_BTN, " +
      "C_Order.DocAction, " +
      "C_Order.EM_Atecco_Docaction, " +
      "C_Order.CopyFrom, " +
      "C_Order.EM_Atecco_Imprimir, " +
      "C_Order.CopyFromPO, " +
      "C_Order.DeliveryViaRule, " +
      "C_Order.M_Shipper_ID, " +
      "C_Order.DeliveryRule, " +
      "C_Order.FreightCostRule, " +
      "C_Order.FreightAmt, " +
      "COALESCE(C_Order.IsDiscountPrinted, 'N') AS IsDiscountPrinted, " +
      "C_Order.PriorityRule, " +
      "C_Order.C_Campaign_ID, " +
      "C_Order.ChargeAmt, " +
      "C_Order.C_Charge_ID, " +
      "C_Order.C_Activity_ID, " +
      "C_Order.AD_OrgTrx_ID, " +
      "C_Order.Quotation_ID, " +
      "(CASE WHEN C_Order.Quotation_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table8.DateOrdered, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.GrandTotal), ''))),'') ) END) AS Quotation_IDR, " +
      "C_Order.Calculate_Promotions, " +
      "C_Order.RM_AddOrphanLine, " +
      "C_Order.Convertquotation, " +
      "C_Order.C_Reject_Reason_ID, " +
      "C_Order.validuntil, " +
      "C_Order.C_Project_ID, " +
      "(CASE WHEN C_Order.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "C_Order.C_Costcenter_ID, " +
      "C_Order.A_Asset_ID, " +
      "C_Order.User1_ID, " +
      "C_Order.User2_ID, " +
      "C_Order.DropShip_User_ID, " +
      "C_Order.DropShip_Location_ID, " +
      "COALESCE(C_Order.IsSelfService, 'N') AS IsSelfService, " +
      "C_Order.Deliverynotes, " +
      "C_Order.C_Incoterms_ID, " +
      "C_Order.Incotermsdescription, " +
      "COALESCE(C_Order.IsSelected, 'N') AS IsSelected, " +
      "COALESCE(C_Order.IsTaxIncluded, 'N') AS IsTaxIncluded, " +
      "C_Order.Posted, " +
      "C_Order.PaymentRule, " +
      "COALESCE(C_Order.IsSOTrx, 'N') AS IsSOTrx, " +
      "C_Order.DatePrinted, " +
      "COALESCE(C_Order.Processed, 'N') AS Processed, " +
      "C_Order.Processing, " +
      "C_Order.EM_Atecco_Docstatus, " +
      "COALESCE(C_Order.IsActive, 'N') AS IsActive, " +
      "C_Order.AD_Client_ID, " +
      "C_Order.C_Order_ID, " +
      "C_Order.DateAcct, " +
      "COALESCE(C_Order.IsPrinted, 'N') AS IsPrinted, " +
      "COALESCE(C_Order.IsInvoiced, 'N') AS IsInvoiced, " +
      "COALESCE(C_Order.IsDelivered, 'N') AS IsDelivered, " +
      "C_Order.Generatetemplate, " +
      "C_Order.DropShip_BPartner_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM C_Order left join (select AD_Org_ID, Name from AD_Org) table1 on (C_Order.AD_Org_ID = table1.AD_Org_ID) left join (select C_DocType_ID, Name from C_DocType) table2 on (C_Order.C_DocTypeTarget_ID =  table2.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL2 on (table2.C_DocType_ID = tableTRL2.C_DocType_ID and tableTRL2.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table4 on (C_Order.C_BPartner_ID = table4.C_BPartner_ID) left join (select C_BPartner_Location_ID, Name from C_BPartner_Location) table5 on (C_Order.C_BPartner_Location_ID = table5.C_BPartner_Location_ID) left join (select FIN_Paymentmethod_ID, Name from FIN_Paymentmethod) table6 on (C_Order.FIN_Paymentmethod_ID = table6.FIN_Paymentmethod_ID) left join (select AD_User_ID, Name from AD_User) table7 on (C_Order.SalesRep_ID =  table7.AD_User_ID) left join ad_ref_list_v list1 on (list1.ad_reference_id = '00143645AE464184A81DA4AD0D22EF09' and list1.ad_language = ?  AND C_Order.EM_Atecco_Iniciar = TO_CHAR(list1.value)) left join ad_ref_list_v list2 on (list2.ad_reference_id = 'C2075E669BA7489CA61B85FA29883F1F' and list2.ad_language = ?  AND C_Order.EM_Atecco_Procesar = TO_CHAR(list2.value)) left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table8 on (C_Order.Quotation_ID = table8.C_Order_ID) left join (select C_Project_ID, Value, Name from C_Project) table9 on (C_Order.C_Project_ID = table9.C_Project_ID)" +
      "        WHERE 2=2 " +
      " AND C_Order.IsSOTrx='Y'" +
      "        AND 1=1 " +
      "        AND C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        CabeceraFB9EADFCA7364909B248605C11FEAD82Data objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data = new CabeceraFB9EADFCA7364909B248605C11FEAD82Data();
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.created = UtilSql.getValue(result, "created");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.updated = UtilSql.getValue(result, "updated");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cDoctypetargetIdr = UtilSql.getValue(result, "c_doctypetarget_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.documentno = UtilSql.getValue(result, "documentno");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.rmPickfromshipment = UtilSql.getValue(result, "rm_pickfromshipment");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.rmReceivematerials = UtilSql.getValue(result, "rm_receivematerials");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.finPaymentmethodId = UtilSql.getValue(result, "fin_paymentmethod_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.finPaymentmethodIdr = UtilSql.getValue(result, "fin_paymentmethod_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.rmCreateinvoice = UtilSql.getValue(result, "rm_createinvoice");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.mPricelistId = UtilSql.getValue(result, "m_pricelist_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoBanco = UtilSql.getValue(result, "em_atecco_banco");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoTipoTarjeta = UtilSql.getValue(result, "em_atecco_tipo_tarjeta");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cPaymenttermId = UtilSql.getValue(result, "c_paymentterm_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.invoicerule = UtilSql.getValue(result, "invoicerule");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoAnticipostatus = UtilSql.getValue(result, "em_atecco_anticipostatus");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.totallines = UtilSql.getValue(result, "totallines");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.grandtotal = UtilSql.getValue(result, "grandtotal");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adUserId = UtilSql.getValue(result, "ad_user_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.soResStatus = UtilSql.getValue(result, "so_res_status");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.poreference = UtilSql.getValue(result, "poreference");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoTransporte = UtilSql.getValue(result, "em_atecco_transporte");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.salesrepId = UtilSql.getValue(result, "salesrep_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.salesrepIdr = UtilSql.getValue(result, "salesrep_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.description = UtilSql.getValue(result, "description");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.billtoId = UtilSql.getValue(result, "billto_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoAnticipo = UtilSql.getValue(result, "em_atecco_anticipo");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.deliveryLocationId = UtilSql.getValue(result, "delivery_location_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoIniciar = UtilSql.getValue(result, "em_atecco_iniciar");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoIniciarBtn = UtilSql.getValue(result, "em_atecco_iniciar_btn");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoValidar = UtilSql.getValue(result, "em_atecco_validar");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAprmAddpayment = UtilSql.getValue(result, "em_aprm_addpayment");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoProcesar = UtilSql.getValue(result, "em_atecco_procesar");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoProcesarBtn = UtilSql.getValue(result, "em_atecco_procesar_btn");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.docaction = UtilSql.getValue(result, "docaction");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoDocaction = UtilSql.getValue(result, "em_atecco_docaction");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.copyfrom = UtilSql.getValue(result, "copyfrom");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoImprimir = UtilSql.getValue(result, "em_atecco_imprimir");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.copyfrompo = UtilSql.getValue(result, "copyfrompo");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.deliveryviarule = UtilSql.getValue(result, "deliveryviarule");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.deliveryrule = UtilSql.getValue(result, "deliveryrule");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.freightcostrule = UtilSql.getValue(result, "freightcostrule");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.freightamt = UtilSql.getValue(result, "freightamt");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isdiscountprinted = UtilSql.getValue(result, "isdiscountprinted");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.priorityrule = UtilSql.getValue(result, "priorityrule");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cCampaignId = UtilSql.getValue(result, "c_campaign_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cActivityId = UtilSql.getValue(result, "c_activity_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adOrgtrxId = UtilSql.getValue(result, "ad_orgtrx_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.quotationId = UtilSql.getValue(result, "quotation_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.quotationIdr = UtilSql.getValue(result, "quotation_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.calculatePromotions = UtilSql.getValue(result, "calculate_promotions");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.rmAddorphanline = UtilSql.getValue(result, "rm_addorphanline");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.convertquotation = UtilSql.getValue(result, "convertquotation");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cRejectReasonId = UtilSql.getValue(result, "c_reject_reason_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.validuntil = UtilSql.getDateValue(result, "validuntil", "dd-MM-yyyy");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.user1Id = UtilSql.getValue(result, "user1_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.user2Id = UtilSql.getValue(result, "user2_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.dropshipUserId = UtilSql.getValue(result, "dropship_user_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.dropshipLocationId = UtilSql.getValue(result, "dropship_location_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isselfservice = UtilSql.getValue(result, "isselfservice");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.deliverynotes = UtilSql.getValue(result, "deliverynotes");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cIncotermsId = UtilSql.getValue(result, "c_incoterms_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.incotermsdescription = UtilSql.getValue(result, "incotermsdescription");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isselected = UtilSql.getValue(result, "isselected");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.istaxincluded = UtilSql.getValue(result, "istaxincluded");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.posted = UtilSql.getValue(result, "posted");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.paymentrule = UtilSql.getValue(result, "paymentrule");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.issotrx = UtilSql.getValue(result, "issotrx");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.dateprinted = UtilSql.getDateValue(result, "dateprinted", "dd-MM-yyyy");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.processed = UtilSql.getValue(result, "processed");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.processing = UtilSql.getValue(result, "processing");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.emAteccoDocstatus = UtilSql.getValue(result, "em_atecco_docstatus");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isactive = UtilSql.getValue(result, "isactive");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isprinted = UtilSql.getValue(result, "isprinted");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isinvoiced = UtilSql.getValue(result, "isinvoiced");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.isdelivered = UtilSql.getValue(result, "isdelivered");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.generatetemplate = UtilSql.getValue(result, "generatetemplate");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.dropshipBpartnerId = UtilSql.getValue(result, "dropship_bpartner_id");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.language = UtilSql.getValue(result, "language");
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adUserClient = "";
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.adOrgClient = "";
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.createdby = "";
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.trBgcolor = "";
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.totalCount = "";
        objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    CabeceraFB9EADFCA7364909B248605C11FEAD82Data objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[] = new CabeceraFB9EADFCA7364909B248605C11FEAD82Data[vector.size()];
    vector.copyInto(objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data);
    return(objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data);
  }

/**
Create a registry
 */
  public static CabeceraFB9EADFCA7364909B248605C11FEAD82Data[] set(String convertquotation, String validuntil, String aAssetId, String emAteccoTransporte, String emAteccoDocstatus, String cOrderId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String documentno, String docstatus, String docaction, String cDoctypeId, String cDoctypetargetId, String description, String isdelivered, String isinvoiced, String isprinted, String dateordered, String datepromised, String dateacct, String salesrepId, String cPaymenttermId, String billtoId, String cCurrencyId, String invoicerule, String freightamt, String deliveryviarule, String mShipperId, String priorityrule, String totallines, String grandtotal, String mWarehouseId, String mPricelistId, String processing, String cCampaignId, String cBpartnerId, String cBpartnerIdr, String adUserId, String poreference, String cChargeId, String chargeamt, String emAteccoTipoTarjeta, String processed, String cBpartnerLocationId, String cProjectId, String cProjectIdr, String cActivityId, String quotationId, String quotationIdr, String issotrx, String dateprinted, String deliveryrule, String freightcostrule, String emAteccoIniciar, String emAteccoIniciarBtn, String paymentrule, String isdiscountprinted, String posted, String istaxincluded, String isselected, String cCostcenterId, String deliverynotes, String cIncotermsId, String incotermsdescription, String generatetemplate, String deliveryLocationId, String copyfrompo, String finPaymentmethodId, String dropshipUserId, String dropshipBpartnerId, String copyfrom, String dropshipLocationId, String isselfservice, String emAteccoAnticipostatus, String emAteccoValidar, String adOrgtrxId, String user2Id, String user1Id, String calculatePromotions, String emAteccoDocaction, String emAteccoBanco, String rmPickfromshipment, String rmReceivematerials, String rmCreateinvoice, String emAteccoImprimir, String cReturnReasonId, String rmAddorphanline, String soResStatus, String emAteccoAnticipo, String cRejectReasonId, String emAteccoProcesar, String emAteccoProcesarBtn, String emAprmAddpayment)    throws ServletException {
    CabeceraFB9EADFCA7364909B248605C11FEAD82Data objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[] = new CabeceraFB9EADFCA7364909B248605C11FEAD82Data[1];
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0] = new CabeceraFB9EADFCA7364909B248605C11FEAD82Data();
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].created = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].createdbyr = createdbyr;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].updated = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].updatedTimeStamp = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].updatedby = updatedby;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].updatedbyr = updatedbyr;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].adOrgId = adOrgId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].adOrgIdr = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cDoctypetargetId = cDoctypetargetId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cDoctypetargetIdr = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cReturnReasonId = cReturnReasonId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].documentno = documentno;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].dateordered = dateordered;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cBpartnerId = cBpartnerId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cBpartnerIdr = cBpartnerIdr;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cBpartnerLocationIdr = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].rmPickfromshipment = rmPickfromshipment;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].rmReceivematerials = rmReceivematerials;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].datepromised = datepromised;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].finPaymentmethodId = finPaymentmethodId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].finPaymentmethodIdr = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].rmCreateinvoice = rmCreateinvoice;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].mPricelistId = mPricelistId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoBanco = emAteccoBanco;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoTipoTarjeta = emAteccoTipoTarjeta;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cPaymenttermId = cPaymenttermId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].mWarehouseId = mWarehouseId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].invoicerule = invoicerule;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].docstatus = docstatus;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoAnticipostatus = emAteccoAnticipostatus;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].totallines = totallines;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].grandtotal = grandtotal;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cCurrencyId = cCurrencyId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].adUserId = adUserId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cDoctypeId = cDoctypeId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].soResStatus = soResStatus;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].poreference = poreference;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoTransporte = emAteccoTransporte;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].salesrepId = salesrepId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].salesrepIdr = "";
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].description = description;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].billtoId = billtoId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoAnticipo = emAteccoAnticipo;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].deliveryLocationId = deliveryLocationId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoIniciar = emAteccoIniciar;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoIniciarBtn = emAteccoIniciarBtn;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoValidar = emAteccoValidar;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAprmAddpayment = emAprmAddpayment;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoProcesar = emAteccoProcesar;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoProcesarBtn = emAteccoProcesarBtn;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].docaction = docaction;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoDocaction = emAteccoDocaction;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].copyfrom = copyfrom;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoImprimir = emAteccoImprimir;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].copyfrompo = copyfrompo;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].deliveryviarule = deliveryviarule;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].mShipperId = mShipperId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].deliveryrule = deliveryrule;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].freightcostrule = freightcostrule;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].freightamt = freightamt;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isdiscountprinted = isdiscountprinted;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].priorityrule = priorityrule;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cCampaignId = cCampaignId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].chargeamt = chargeamt;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cChargeId = cChargeId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cActivityId = cActivityId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].adOrgtrxId = adOrgtrxId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].quotationId = quotationId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].quotationIdr = quotationIdr;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].calculatePromotions = calculatePromotions;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].rmAddorphanline = rmAddorphanline;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].convertquotation = convertquotation;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cRejectReasonId = cRejectReasonId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].validuntil = validuntil;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cProjectId = cProjectId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cProjectIdr = cProjectIdr;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cCostcenterId = cCostcenterId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].aAssetId = aAssetId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].user1Id = user1Id;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].user2Id = user2Id;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].dropshipUserId = dropshipUserId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].dropshipLocationId = dropshipLocationId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isselfservice = isselfservice;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].deliverynotes = deliverynotes;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cIncotermsId = cIncotermsId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].incotermsdescription = incotermsdescription;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isselected = isselected;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].istaxincluded = istaxincluded;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].posted = posted;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].paymentrule = paymentrule;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].issotrx = issotrx;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].dateprinted = dateprinted;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].processed = processed;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].processing = processing;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].emAteccoDocstatus = emAteccoDocstatus;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isactive = isactive;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].adClientId = adClientId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].cOrderId = cOrderId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].dateacct = dateacct;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isprinted = isprinted;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isinvoiced = isinvoiced;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].isdelivered = isdelivered;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].generatetemplate = generatetemplate;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].dropshipBpartnerId = dropshipBpartnerId;
    objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data[0].language = "";
    return objectCabeceraFB9EADFCA7364909B248605C11FEAD82Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2166_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2168_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2762_2(ConnectionProvider connectionProvider, String C_BPartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_BPartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_BPartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3402_3(ConnectionProvider connectionProvider, String C_Project_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Project_ID FROM C_Project left join (select C_Project_ID, Value, Name from C_Project) table2 on (C_Project.C_Project_ID = table2.C_Project_ID) WHERE C_Project.isActive='Y' AND C_Project.C_Project_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Project_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_project_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef367DCAA9CF4442ADB9A76F6539102217_4(ConnectionProvider connectionProvider, String Quotation_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.DocumentNo), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table2.DateOrdered, 'DD-MM-YYYY')), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.GrandTotal), ''))), '') ) as Quotation_ID FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table2 on (C_Order.C_Order_ID = table2.C_Order_ID) WHERE C_Order.isActive='Y' AND C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Quotation_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "quotation_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef828EE0AE802C5FA1E040007F010067C7(ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT fin_paymentmethod_id as defaultValue  FROM fin_paymentmethod WHERE upper(name) LIKE 'EFECTIVO' ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for action search
 */
  public static String selectActDefM_AttributeSetInstance_ID(ConnectionProvider connectionProvider, String M_AttributeSetInstance_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT Description FROM M_AttributeSetInstance WHERE isActive='Y' AND M_AttributeSetInstance_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_AttributeSetInstance_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "description");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_Order" +
      "        SET AD_Org_ID = (?) , C_DocTypeTarget_ID = (?) , C_Return_Reason_ID = (?) , DocumentNo = (?) , DateOrdered = TO_DATE(?) , C_BPartner_ID = (?) , C_BPartner_Location_ID = (?) , RM_PickFromShipment = (?) , RM_ReceiveMaterials = (?) , DatePromised = TO_DATE(?) , FIN_Paymentmethod_ID = (?) , RM_CreateInvoice = (?) , M_PriceList_ID = (?) , EM_Atecco_Banco = (?) , EM_Atecco_Tipo_Tarjeta = (?) , C_PaymentTerm_ID = (?) , M_Warehouse_ID = (?) , InvoiceRule = (?) , DocStatus = (?) , EM_Atecco_Anticipostatus = (?) , TotalLines = TO_NUMBER(?) , GrandTotal = TO_NUMBER(?) , C_Currency_ID = (?) , AD_User_ID = (?) , C_DocType_ID = (?) , SO_Res_Status = (?) , POReference = (?) , EM_Atecco_Transporte = (?) , SalesRep_ID = (?) , Description = (?) , BillTo_ID = (?) , EM_Atecco_Anticipo = (?) , Delivery_Location_ID = (?) , EM_Atecco_Iniciar = (?) , EM_Atecco_Validar = (?) , EM_APRM_AddPayment = (?) , EM_Atecco_Procesar = (?) , DocAction = (?) , EM_Atecco_Docaction = (?) , CopyFrom = (?) , EM_Atecco_Imprimir = (?) , CopyFromPO = (?) , DeliveryViaRule = (?) , M_Shipper_ID = (?) , DeliveryRule = (?) , FreightCostRule = (?) , FreightAmt = TO_NUMBER(?) , IsDiscountPrinted = (?) , PriorityRule = (?) , C_Campaign_ID = (?) , ChargeAmt = TO_NUMBER(?) , C_Charge_ID = (?) , C_Activity_ID = (?) , AD_OrgTrx_ID = (?) , Quotation_ID = (?) , Calculate_Promotions = (?) , RM_AddOrphanLine = (?) , Convertquotation = (?) , C_Reject_Reason_ID = (?) , validuntil = TO_DATE(?) , C_Project_ID = (?) , C_Costcenter_ID = (?) , A_Asset_ID = (?) , User1_ID = (?) , User2_ID = (?) , DropShip_User_ID = (?) , DropShip_Location_ID = (?) , IsSelfService = (?) , Deliverynotes = (?) , C_Incoterms_ID = (?) , Incotermsdescription = (?) , IsSelected = (?) , IsTaxIncluded = (?) , Posted = (?) , PaymentRule = (?) , IsSOTrx = (?) , DatePrinted = TO_DATE(?) , Processed = (?) , Processing = (?) , EM_Atecco_Docstatus = (?) , IsActive = (?) , AD_Client_ID = (?) , C_Order_ID = (?) , DateAcct = TO_DATE(?) , IsPrinted = (?) , IsInvoiced = (?) , IsDelivered = (?) , Generatetemplate = (?) , DropShip_BPartner_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBanco);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipoTarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipostatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTransporte);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoIniciar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoImprimir);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_Order " +
      "        (AD_Org_ID, C_DocTypeTarget_ID, C_Return_Reason_ID, DocumentNo, DateOrdered, C_BPartner_ID, C_BPartner_Location_ID, RM_PickFromShipment, RM_ReceiveMaterials, DatePromised, FIN_Paymentmethod_ID, RM_CreateInvoice, M_PriceList_ID, EM_Atecco_Banco, EM_Atecco_Tipo_Tarjeta, C_PaymentTerm_ID, M_Warehouse_ID, InvoiceRule, DocStatus, EM_Atecco_Anticipostatus, TotalLines, GrandTotal, C_Currency_ID, AD_User_ID, C_DocType_ID, SO_Res_Status, POReference, EM_Atecco_Transporte, SalesRep_ID, Description, BillTo_ID, EM_Atecco_Anticipo, Delivery_Location_ID, EM_Atecco_Iniciar, EM_Atecco_Validar, EM_APRM_AddPayment, EM_Atecco_Procesar, DocAction, EM_Atecco_Docaction, CopyFrom, EM_Atecco_Imprimir, CopyFromPO, DeliveryViaRule, M_Shipper_ID, DeliveryRule, FreightCostRule, FreightAmt, IsDiscountPrinted, PriorityRule, C_Campaign_ID, ChargeAmt, C_Charge_ID, C_Activity_ID, AD_OrgTrx_ID, Quotation_ID, Calculate_Promotions, RM_AddOrphanLine, Convertquotation, C_Reject_Reason_ID, validuntil, C_Project_ID, C_Costcenter_ID, A_Asset_ID, User1_ID, User2_ID, DropShip_User_ID, DropShip_Location_ID, IsSelfService, Deliverynotes, C_Incoterms_ID, Incotermsdescription, IsSelected, IsTaxIncluded, Posted, PaymentRule, IsSOTrx, DatePrinted, Processed, Processing, EM_Atecco_Docstatus, IsActive, AD_Client_ID, C_Order_ID, DateAcct, IsPrinted, IsInvoiced, IsDelivered, Generatetemplate, DropShip_BPartner_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmPickfromshipment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmReceivematerials);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, finPaymentmethodId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmCreateinvoice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mPricelistId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoBanco);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTipoTarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cPaymenttermId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, invoicerule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipostatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totallines);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grandtotal);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, poreference);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTransporte);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, salesrepId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, billtoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoAnticipo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoIniciar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoValidar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAprmAddpayment);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoProcesar);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrom);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoImprimir);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copyfrompo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryviarule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliveryrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightcostrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdiscountprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priorityrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCampaignId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cActivityId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgtrxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, calculatePromotions);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rmAddorphanline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, convertquotation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cRejectReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, validuntil);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipUserId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselfservice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, deliverynotes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cIncotermsId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, incotermsdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isselected);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, istaxincluded);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paymentrule);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issotrx);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprinted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, generatetemplate);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dropshipBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_Order" +
      "        WHERE C_Order.C_Order_ID = ? " +
      "        AND C_Order.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_Order.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_Order" +
      "         WHERE C_Order.C_Order_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
