//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.nomina.TipodeIngresosoEgresos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData implements FieldProvider {
static Logger log4j = Logger.getLogger(TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String adOrgIdr;
  public String name;
  public String tipoRubro;
  public String noCalculaRubroId;
  public String noCalculaRubroIdr;
  public String isavance;
  public String isservicio;
  public String isingreso;
  public String maxHExtra;
  public String sumaIngreso;
  public String isprovision;
  public String isprovmes;
  public String mesCalculo;
  public String mesCalculor;
  public String mesPago;
  public String mesPagor;
  public String isactive;
  public String isdecimotercero;
  public String isdecimocuarto;
  public String isvariable;
  public String isfondoreserva;
  public String diaPago;
  public String issueldo;
  public String copiarubro;
  public String noTipoIngresoEgresoId;
  public String adClientId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("name"))
      return name;
    else if (fieldName.equalsIgnoreCase("tipo_rubro") || fieldName.equals("tipoRubro"))
      return tipoRubro;
    else if (fieldName.equalsIgnoreCase("no_calcula_rubro_id") || fieldName.equals("noCalculaRubroId"))
      return noCalculaRubroId;
    else if (fieldName.equalsIgnoreCase("no_calcula_rubro_idr") || fieldName.equals("noCalculaRubroIdr"))
      return noCalculaRubroIdr;
    else if (fieldName.equalsIgnoreCase("isavance"))
      return isavance;
    else if (fieldName.equalsIgnoreCase("isservicio"))
      return isservicio;
    else if (fieldName.equalsIgnoreCase("isingreso"))
      return isingreso;
    else if (fieldName.equalsIgnoreCase("max_h_extra") || fieldName.equals("maxHExtra"))
      return maxHExtra;
    else if (fieldName.equalsIgnoreCase("suma_ingreso") || fieldName.equals("sumaIngreso"))
      return sumaIngreso;
    else if (fieldName.equalsIgnoreCase("isprovision"))
      return isprovision;
    else if (fieldName.equalsIgnoreCase("isprovmes"))
      return isprovmes;
    else if (fieldName.equalsIgnoreCase("mes_calculo") || fieldName.equals("mesCalculo"))
      return mesCalculo;
    else if (fieldName.equalsIgnoreCase("mes_calculor") || fieldName.equals("mesCalculor"))
      return mesCalculor;
    else if (fieldName.equalsIgnoreCase("mes_pago") || fieldName.equals("mesPago"))
      return mesPago;
    else if (fieldName.equalsIgnoreCase("mes_pagor") || fieldName.equals("mesPagor"))
      return mesPagor;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("isdecimotercero"))
      return isdecimotercero;
    else if (fieldName.equalsIgnoreCase("isdecimocuarto"))
      return isdecimocuarto;
    else if (fieldName.equalsIgnoreCase("isvariable"))
      return isvariable;
    else if (fieldName.equalsIgnoreCase("isfondoreserva"))
      return isfondoreserva;
    else if (fieldName.equalsIgnoreCase("dia_pago") || fieldName.equals("diaPago"))
      return diaPago;
    else if (fieldName.equalsIgnoreCase("issueldo"))
      return issueldo;
    else if (fieldName.equalsIgnoreCase("copiarubro"))
      return copiarubro;
    else if (fieldName.equalsIgnoreCase("no_tipo_ingreso_egreso_id") || fieldName.equals("noTipoIngresoEgresoId"))
      return noTipoIngresoEgresoId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(no_tipo_ingreso_egreso.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_tipo_ingreso_egreso.CreatedBy) as CreatedByR, " +
      "        to_char(no_tipo_ingreso_egreso.Updated, ?) as updated, " +
      "        to_char(no_tipo_ingreso_egreso.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        no_tipo_ingreso_egreso.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = no_tipo_ingreso_egreso.UpdatedBy) as UpdatedByR," +
      "        no_tipo_ingreso_egreso.AD_Org_ID, " +
      "(CASE WHEN no_tipo_ingreso_egreso.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table1.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "no_tipo_ingreso_egreso.Name, " +
      "no_tipo_ingreso_egreso.Tipo_Rubro, " +
      "no_tipo_ingreso_egreso.NO_Calcula_Rubro_ID, " +
      "(CASE WHEN no_tipo_ingreso_egreso.NO_Calcula_Rubro_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Nombre), ''))),'') ) END) AS NO_Calcula_Rubro_IDR, " +
      "COALESCE(no_tipo_ingreso_egreso.Isavance, 'N') AS Isavance, " +
      "COALESCE(no_tipo_ingreso_egreso.Isservicio, 'N') AS Isservicio, " +
      "COALESCE(no_tipo_ingreso_egreso.Isingreso, 'N') AS Isingreso, " +
      "no_tipo_ingreso_egreso.MAX_H_Extra, " +
      "COALESCE(no_tipo_ingreso_egreso.Suma_Ingreso, 'N') AS Suma_Ingreso, " +
      "COALESCE(no_tipo_ingreso_egreso.Isprovision, 'N') AS Isprovision, " +
      "COALESCE(no_tipo_ingreso_egreso.Isprovmes, 'N') AS Isprovmes, " +
      "no_tipo_ingreso_egreso.MES_Calculo, " +
      "(CASE WHEN no_tipo_ingreso_egreso.MES_Calculo IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS MES_CalculoR, " +
      "no_tipo_ingreso_egreso.MES_Pago, " +
      "(CASE WHEN no_tipo_ingreso_egreso.MES_Pago IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list2.name),'') ) END) AS MES_PagoR, " +
      "COALESCE(no_tipo_ingreso_egreso.Isactive, 'N') AS Isactive, " +
      "COALESCE(no_tipo_ingreso_egreso.Isdecimotercero, 'N') AS Isdecimotercero, " +
      "COALESCE(no_tipo_ingreso_egreso.Isdecimocuarto, 'N') AS Isdecimocuarto, " +
      "COALESCE(no_tipo_ingreso_egreso.Isvariable, 'N') AS Isvariable, " +
      "COALESCE(no_tipo_ingreso_egreso.Isfondoreserva, 'N') AS Isfondoreserva, " +
      "no_tipo_ingreso_egreso.DIA_Pago, " +
      "COALESCE(no_tipo_ingreso_egreso.Issueldo, 'N') AS Issueldo, " +
      "no_tipo_ingreso_egreso.Copiarubro, " +
      "no_tipo_ingreso_egreso.NO_Tipo_Ingreso_Egreso_ID, " +
      "no_tipo_ingreso_egreso.AD_Client_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM no_tipo_ingreso_egreso left join (select AD_Org_ID, Name from AD_Org) table1 on (no_tipo_ingreso_egreso.AD_Org_ID = table1.AD_Org_ID) left join (select NO_Calcula_Rubro_ID, Nombre from no_calcula_rubro) table2 on (no_tipo_ingreso_egreso.NO_Calcula_Rubro_ID =  table2.NO_Calcula_Rubro_ID) left join ad_ref_list_v list1 on (no_tipo_ingreso_egreso.MES_Calculo = list1.value and list1.ad_reference_id = '46C76320911F4085B2CE2E3504166517' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (no_tipo_ingreso_egreso.MES_Pago = list2.value and list2.ad_reference_id = '46C76320911F4085B2CE2E3504166517' and list2.ad_language = ?) " +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND no_tipo_ingreso_egreso.NO_Tipo_Ingreso_Egreso_ID = ? " +
      "        AND no_tipo_ingreso_egreso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND no_tipo_ingreso_egreso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData = new TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData();
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.created = UtilSql.getValue(result, "created");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.updated = UtilSql.getValue(result, "updated");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.updatedby = UtilSql.getValue(result, "updatedby");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.name = UtilSql.getValue(result, "name");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.tipoRubro = UtilSql.getValue(result, "tipo_rubro");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.noCalculaRubroId = UtilSql.getValue(result, "no_calcula_rubro_id");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.noCalculaRubroIdr = UtilSql.getValue(result, "no_calcula_rubro_idr");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isavance = UtilSql.getValue(result, "isavance");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isservicio = UtilSql.getValue(result, "isservicio");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isingreso = UtilSql.getValue(result, "isingreso");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.maxHExtra = UtilSql.getValue(result, "max_h_extra");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.sumaIngreso = UtilSql.getValue(result, "suma_ingreso");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isprovision = UtilSql.getValue(result, "isprovision");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isprovmes = UtilSql.getValue(result, "isprovmes");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.mesCalculo = UtilSql.getValue(result, "mes_calculo");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.mesCalculor = UtilSql.getValue(result, "mes_calculor");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.mesPago = UtilSql.getValue(result, "mes_pago");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.mesPagor = UtilSql.getValue(result, "mes_pagor");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isactive = UtilSql.getValue(result, "isactive");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isdecimotercero = UtilSql.getValue(result, "isdecimotercero");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isdecimocuarto = UtilSql.getValue(result, "isdecimocuarto");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isvariable = UtilSql.getValue(result, "isvariable");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.isfondoreserva = UtilSql.getValue(result, "isfondoreserva");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.diaPago = UtilSql.getValue(result, "dia_pago");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.issueldo = UtilSql.getValue(result, "issueldo");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.copiarubro = UtilSql.getValue(result, "copiarubro");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.noTipoIngresoEgresoId = UtilSql.getValue(result, "no_tipo_ingreso_egreso_id");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.language = UtilSql.getValue(result, "language");
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.adUserClient = "";
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.adOrgClient = "";
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.createdby = "";
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.trBgcolor = "";
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.totalCount = "";
        objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[] = new TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[vector.size()];
    vector.copyInto(objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData);
    return(objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData);
  }

/**
Create a registry
 */
  public static TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[] set(String isvariable, String isactive, String mesPago, String noTipoIngresoEgresoId, String isprovision, String diaPago, String issueldo, String isingreso, String tipoRubro, String sumaIngreso, String isavance, String maxHExtra, String name, String isdecimocuarto, String mesCalculo, String isservicio, String noCalculaRubroId, String copiarubro, String adClientId, String adOrgId, String updatedby, String updatedbyr, String createdby, String createdbyr, String isdecimotercero, String isfondoreserva, String isprovmes)    throws ServletException {
    TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[] = new TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[1];
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0] = new TipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData();
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].created = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].createdbyr = createdbyr;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].updated = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].updatedTimeStamp = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].updatedby = updatedby;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].updatedbyr = updatedbyr;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].adOrgId = adOrgId;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].adOrgIdr = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].name = name;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].tipoRubro = tipoRubro;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].noCalculaRubroId = noCalculaRubroId;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].noCalculaRubroIdr = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isavance = isavance;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isservicio = isservicio;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isingreso = isingreso;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].maxHExtra = maxHExtra;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].sumaIngreso = sumaIngreso;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isprovision = isprovision;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isprovmes = isprovmes;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].mesCalculo = mesCalculo;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].mesCalculor = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].mesPago = mesPago;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].mesPagor = "";
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isactive = isactive;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isdecimotercero = isdecimotercero;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isdecimocuarto = isdecimocuarto;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isvariable = isvariable;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].isfondoreserva = isfondoreserva;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].diaPago = diaPago;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].issueldo = issueldo;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].copiarubro = copiarubro;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].noTipoIngresoEgresoId = noTipoIngresoEgresoId;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].adClientId = adClientId;
    objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData[0].language = "";
    return objectTipodeIngresosoEgresosC44BEC2E9C3A42D095E937A9E24A587DData;
  }

/**
Select for auxiliar field
 */
  public static String selectDefE3657183E1AF4D9D92BE841B9D635F43_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEF90FAC65C0C43F6BBB92A37640A08B7_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE no_tipo_ingreso_egreso" +
      "        SET AD_Org_ID = (?) , Name = (?) , Tipo_Rubro = (?) , NO_Calcula_Rubro_ID = (?) , Isavance = (?) , Isservicio = (?) , Isingreso = (?) , MAX_H_Extra = TO_NUMBER(?) , Suma_Ingreso = (?) , Isprovision = (?) , Isprovmes = (?) , MES_Calculo = (?) , MES_Pago = (?) , Isactive = (?) , Isdecimotercero = (?) , Isdecimocuarto = (?) , Isvariable = (?) , Isfondoreserva = (?) , DIA_Pago = TO_NUMBER(?) , Issueldo = (?) , Copiarubro = (?) , NO_Tipo_Ingreso_Egreso_ID = (?) , AD_Client_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE no_tipo_ingreso_egreso.NO_Tipo_Ingreso_Egreso_ID = ? " +
      "        AND no_tipo_ingreso_egreso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_tipo_ingreso_egreso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoRubro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noCalculaRubroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isavance);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isservicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isingreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maxHExtra);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sumaIngreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprovision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprovmes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesCalculo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdecimotercero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdecimocuarto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isfondoreserva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, diaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issueldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copiarubro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO no_tipo_ingreso_egreso " +
      "        (AD_Org_ID, Name, Tipo_Rubro, NO_Calcula_Rubro_ID, Isavance, Isservicio, Isingreso, MAX_H_Extra, Suma_Ingreso, Isprovision, Isprovmes, MES_Calculo, MES_Pago, Isactive, Isdecimotercero, Isdecimocuarto, Isvariable, Isfondoreserva, DIA_Pago, Issueldo, Copiarubro, NO_Tipo_Ingreso_Egreso_ID, AD_Client_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, name);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoRubro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noCalculaRubroId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isavance);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isservicio);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isingreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, maxHExtra);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sumaIngreso);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprovision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isprovmes);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesCalculo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mesPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdecimotercero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdecimocuarto);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isvariable);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isfondoreserva);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, diaPago);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, issueldo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, copiarubro);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noTipoIngresoEgresoId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM no_tipo_ingreso_egreso" +
      "        WHERE no_tipo_ingreso_egreso.NO_Tipo_Ingreso_Egreso_ID = ? " +
      "        AND no_tipo_ingreso_egreso.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND no_tipo_ingreso_egreso.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM no_tipo_ingreso_egreso" +
      "         WHERE no_tipo_ingreso_egreso.NO_Tipo_Ingreso_Egreso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM no_tipo_ingreso_egreso" +
      "         WHERE no_tipo_ingreso_egreso.NO_Tipo_Ingreso_Egreso_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
