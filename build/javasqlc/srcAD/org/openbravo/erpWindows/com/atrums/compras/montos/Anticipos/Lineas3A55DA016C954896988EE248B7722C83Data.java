//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.Anticipos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class Lineas3A55DA016C954896988EE248B7722C83Data implements FieldProvider {
static Logger log4j = Logger.getLogger(Lineas3A55DA016C954896988EE248B7722C83Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String line;
  public String mProductId;
  public String mProductIdr;
  public String emAteccoStocktienda;
  public String emAteccoStocktablero;
  public String qtyordered;
  public String mInoutlineId;
  public String mAttributesetinstanceId;
  public String cUomId;
  public String cUomIdr;
  public String cReturnReasonId;
  public String emAteccoPrecioefectivo;
  public String emAteccoTotalefectivo;
  public String emAteccoPreciotarjeta;
  public String emAteccoTotaltarjeta;
  public String priceactual;
  public String grossUnitPrice;
  public String linenetamt;
  public String lineGrossAmount;
  public String cTaxId;
  public String cTaxIdr;
  public String pricelist;
  public String grosspricelist;
  public String discount;
  public String mWarehouseRuleId;
  public String mWarehouseRuleIdr;
  public String description;
  public String createReservation;
  public String createReservationr;
  public String taxbaseamt;
  public String qtyinvoiced;
  public String qtydelivered;
  public String dateordered;
  public String datepromised;
  public String mWarehouseId;
  public String qtyreserved;
  public String mShipperId;
  public String cBpartnerId;
  public String directship;
  public String freightamt;
  public String cBpartnerLocationId;
  public String cancelpricead;
  public String mProductUomId;
  public String quantityorder;
  public String pricestd;
  public String grosspricestd;
  public String iseditlinenetamt;
  public String quotationlineId;
  public String quotationlineIdr;
  public String soResStatus;
  public String manageReservation;
  public String printDescription;
  public String overdueReturnDays;
  public String adOrgId;
  public String adOrgIdr;
  public String cProjectId;
  public String cProjectIdr;
  public String cCostcenterId;
  public String aAssetId;
  public String user1Id;
  public String user2Id;
  public String explode;
  public String bomParentId;
  public String relateOrderline;
  public String mOfferId;
  public String cCurrencyId;
  public String cOrderId;
  public String isactive;
  public String adClientId;
  public String dateinvoiced;
  public String refOrderlineId;
  public String managePrereservation;
  public String datedelivered;
  public String pricelimit;
  public String isdescription;
  public String sResourceassignmentId;
  public String cOrderlineId;
  public String chargeamt;
  public String cOrderDiscountId;
  public String cChargeId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("line"))
      return line;
    else if (fieldName.equalsIgnoreCase("m_product_id") || fieldName.equals("mProductId"))
      return mProductId;
    else if (fieldName.equalsIgnoreCase("m_product_idr") || fieldName.equals("mProductIdr"))
      return mProductIdr;
    else if (fieldName.equalsIgnoreCase("em_atecco_stocktienda") || fieldName.equals("emAteccoStocktienda"))
      return emAteccoStocktienda;
    else if (fieldName.equalsIgnoreCase("em_atecco_stocktablero") || fieldName.equals("emAteccoStocktablero"))
      return emAteccoStocktablero;
    else if (fieldName.equalsIgnoreCase("qtyordered"))
      return qtyordered;
    else if (fieldName.equalsIgnoreCase("m_inoutline_id") || fieldName.equals("mInoutlineId"))
      return mInoutlineId;
    else if (fieldName.equalsIgnoreCase("m_attributesetinstance_id") || fieldName.equals("mAttributesetinstanceId"))
      return mAttributesetinstanceId;
    else if (fieldName.equalsIgnoreCase("c_uom_id") || fieldName.equals("cUomId"))
      return cUomId;
    else if (fieldName.equalsIgnoreCase("c_uom_idr") || fieldName.equals("cUomIdr"))
      return cUomIdr;
    else if (fieldName.equalsIgnoreCase("c_return_reason_id") || fieldName.equals("cReturnReasonId"))
      return cReturnReasonId;
    else if (fieldName.equalsIgnoreCase("em_atecco_precioefectivo") || fieldName.equals("emAteccoPrecioefectivo"))
      return emAteccoPrecioefectivo;
    else if (fieldName.equalsIgnoreCase("em_atecco_totalefectivo") || fieldName.equals("emAteccoTotalefectivo"))
      return emAteccoTotalefectivo;
    else if (fieldName.equalsIgnoreCase("em_atecco_preciotarjeta") || fieldName.equals("emAteccoPreciotarjeta"))
      return emAteccoPreciotarjeta;
    else if (fieldName.equalsIgnoreCase("em_atecco_totaltarjeta") || fieldName.equals("emAteccoTotaltarjeta"))
      return emAteccoTotaltarjeta;
    else if (fieldName.equalsIgnoreCase("priceactual"))
      return priceactual;
    else if (fieldName.equalsIgnoreCase("gross_unit_price") || fieldName.equals("grossUnitPrice"))
      return grossUnitPrice;
    else if (fieldName.equalsIgnoreCase("linenetamt"))
      return linenetamt;
    else if (fieldName.equalsIgnoreCase("line_gross_amount") || fieldName.equals("lineGrossAmount"))
      return lineGrossAmount;
    else if (fieldName.equalsIgnoreCase("c_tax_id") || fieldName.equals("cTaxId"))
      return cTaxId;
    else if (fieldName.equalsIgnoreCase("c_tax_idr") || fieldName.equals("cTaxIdr"))
      return cTaxIdr;
    else if (fieldName.equalsIgnoreCase("pricelist"))
      return pricelist;
    else if (fieldName.equalsIgnoreCase("grosspricelist"))
      return grosspricelist;
    else if (fieldName.equalsIgnoreCase("discount"))
      return discount;
    else if (fieldName.equalsIgnoreCase("m_warehouse_rule_id") || fieldName.equals("mWarehouseRuleId"))
      return mWarehouseRuleId;
    else if (fieldName.equalsIgnoreCase("m_warehouse_rule_idr") || fieldName.equals("mWarehouseRuleIdr"))
      return mWarehouseRuleIdr;
    else if (fieldName.equalsIgnoreCase("description"))
      return description;
    else if (fieldName.equalsIgnoreCase("create_reservation") || fieldName.equals("createReservation"))
      return createReservation;
    else if (fieldName.equalsIgnoreCase("create_reservationr") || fieldName.equals("createReservationr"))
      return createReservationr;
    else if (fieldName.equalsIgnoreCase("taxbaseamt"))
      return taxbaseamt;
    else if (fieldName.equalsIgnoreCase("qtyinvoiced"))
      return qtyinvoiced;
    else if (fieldName.equalsIgnoreCase("qtydelivered"))
      return qtydelivered;
    else if (fieldName.equalsIgnoreCase("dateordered"))
      return dateordered;
    else if (fieldName.equalsIgnoreCase("datepromised"))
      return datepromised;
    else if (fieldName.equalsIgnoreCase("m_warehouse_id") || fieldName.equals("mWarehouseId"))
      return mWarehouseId;
    else if (fieldName.equalsIgnoreCase("qtyreserved"))
      return qtyreserved;
    else if (fieldName.equalsIgnoreCase("m_shipper_id") || fieldName.equals("mShipperId"))
      return mShipperId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("directship"))
      return directship;
    else if (fieldName.equalsIgnoreCase("freightamt"))
      return freightamt;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("cancelpricead"))
      return cancelpricead;
    else if (fieldName.equalsIgnoreCase("m_product_uom_id") || fieldName.equals("mProductUomId"))
      return mProductUomId;
    else if (fieldName.equalsIgnoreCase("quantityorder"))
      return quantityorder;
    else if (fieldName.equalsIgnoreCase("pricestd"))
      return pricestd;
    else if (fieldName.equalsIgnoreCase("grosspricestd"))
      return grosspricestd;
    else if (fieldName.equalsIgnoreCase("iseditlinenetamt"))
      return iseditlinenetamt;
    else if (fieldName.equalsIgnoreCase("quotationline_id") || fieldName.equals("quotationlineId"))
      return quotationlineId;
    else if (fieldName.equalsIgnoreCase("quotationline_idr") || fieldName.equals("quotationlineIdr"))
      return quotationlineIdr;
    else if (fieldName.equalsIgnoreCase("so_res_status") || fieldName.equals("soResStatus"))
      return soResStatus;
    else if (fieldName.equalsIgnoreCase("manage_reservation") || fieldName.equals("manageReservation"))
      return manageReservation;
    else if (fieldName.equalsIgnoreCase("print_description") || fieldName.equals("printDescription"))
      return printDescription;
    else if (fieldName.equalsIgnoreCase("overdue_return_days") || fieldName.equals("overdueReturnDays"))
      return overdueReturnDays;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("ad_org_idr") || fieldName.equals("adOrgIdr"))
      return adOrgIdr;
    else if (fieldName.equalsIgnoreCase("c_project_id") || fieldName.equals("cProjectId"))
      return cProjectId;
    else if (fieldName.equalsIgnoreCase("c_project_idr") || fieldName.equals("cProjectIdr"))
      return cProjectIdr;
    else if (fieldName.equalsIgnoreCase("c_costcenter_id") || fieldName.equals("cCostcenterId"))
      return cCostcenterId;
    else if (fieldName.equalsIgnoreCase("a_asset_id") || fieldName.equals("aAssetId"))
      return aAssetId;
    else if (fieldName.equalsIgnoreCase("user1_id") || fieldName.equals("user1Id"))
      return user1Id;
    else if (fieldName.equalsIgnoreCase("user2_id") || fieldName.equals("user2Id"))
      return user2Id;
    else if (fieldName.equalsIgnoreCase("explode"))
      return explode;
    else if (fieldName.equalsIgnoreCase("bom_parent_id") || fieldName.equals("bomParentId"))
      return bomParentId;
    else if (fieldName.equalsIgnoreCase("relate_orderline") || fieldName.equals("relateOrderline"))
      return relateOrderline;
    else if (fieldName.equalsIgnoreCase("m_offer_id") || fieldName.equals("mOfferId"))
      return mOfferId;
    else if (fieldName.equalsIgnoreCase("c_currency_id") || fieldName.equals("cCurrencyId"))
      return cCurrencyId;
    else if (fieldName.equalsIgnoreCase("c_order_id") || fieldName.equals("cOrderId"))
      return cOrderId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("dateinvoiced"))
      return dateinvoiced;
    else if (fieldName.equalsIgnoreCase("ref_orderline_id") || fieldName.equals("refOrderlineId"))
      return refOrderlineId;
    else if (fieldName.equalsIgnoreCase("manage_prereservation") || fieldName.equals("managePrereservation"))
      return managePrereservation;
    else if (fieldName.equalsIgnoreCase("datedelivered"))
      return datedelivered;
    else if (fieldName.equalsIgnoreCase("pricelimit"))
      return pricelimit;
    else if (fieldName.equalsIgnoreCase("isdescription"))
      return isdescription;
    else if (fieldName.equalsIgnoreCase("s_resourceassignment_id") || fieldName.equals("sResourceassignmentId"))
      return sResourceassignmentId;
    else if (fieldName.equalsIgnoreCase("c_orderline_id") || fieldName.equals("cOrderlineId"))
      return cOrderlineId;
    else if (fieldName.equalsIgnoreCase("chargeamt"))
      return chargeamt;
    else if (fieldName.equalsIgnoreCase("c_order_discount_id") || fieldName.equals("cOrderDiscountId"))
      return cOrderDiscountId;
    else if (fieldName.equalsIgnoreCase("c_charge_id") || fieldName.equals("cChargeId"))
      return cChargeId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static Lineas3A55DA016C954896988EE248B7722C83Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cOrderId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, cOrderId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static Lineas3A55DA016C954896988EE248B7722C83Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cOrderId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(C_OrderLine.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_OrderLine.CreatedBy) as CreatedByR, " +
      "        to_char(C_OrderLine.Updated, ?) as updated, " +
      "        to_char(C_OrderLine.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        C_OrderLine.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = C_OrderLine.UpdatedBy) as UpdatedByR," +
      "        C_OrderLine.Line, " +
      "C_OrderLine.M_Product_ID, " +
      "(CASE WHEN C_OrderLine.M_Product_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS M_Product_IDR, " +
      "C_OrderLine.EM_Atecco_Stocktienda, " +
      "C_OrderLine.EM_Atecco_Stocktablero, " +
      "C_OrderLine.QtyOrdered, " +
      "C_OrderLine.M_Inoutline_ID, " +
      "C_OrderLine.M_AttributeSetInstance_ID, " +
      "C_OrderLine.C_UOM_ID, " +
      "(CASE WHEN C_OrderLine.C_UOM_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL3.Name IS NULL THEN TO_CHAR(table3.Name) ELSE TO_CHAR(tableTRL3.Name) END)), ''))),'') ) END) AS C_UOM_IDR, " +
      "C_OrderLine.C_Return_Reason_ID, " +
      "C_OrderLine.EM_Atecco_Precioefectivo, " +
      "C_OrderLine.EM_Atecco_Totalefectivo, " +
      "C_OrderLine.EM_Atecco_Preciotarjeta, " +
      "C_OrderLine.EM_Atecco_Totaltarjeta, " +
      "C_OrderLine.PriceActual, " +
      "C_OrderLine.Gross_Unit_Price, " +
      "C_OrderLine.LineNetAmt, " +
      "C_OrderLine.Line_Gross_Amount, " +
      "C_OrderLine.C_Tax_ID, " +
      "(CASE WHEN C_OrderLine.C_Tax_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL5.Name IS NULL THEN TO_CHAR(table5.Name) ELSE TO_CHAR(tableTRL5.Name) END)), ''))),'') ) END) AS C_Tax_IDR, " +
      "C_OrderLine.PriceList, " +
      "C_OrderLine.GrossPriceList, " +
      "C_OrderLine.Discount, " +
      "C_OrderLine.M_Warehouse_Rule_ID, " +
      "(CASE WHEN C_OrderLine.M_Warehouse_Rule_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table7.Name), ''))),'') ) END) AS M_Warehouse_Rule_IDR, " +
      "C_OrderLine.Description, " +
      "C_OrderLine.Create_Reservation, " +
      "(CASE WHEN C_OrderLine.Create_Reservation IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Create_ReservationR, " +
      "C_OrderLine.Taxbaseamt, " +
      "C_OrderLine.QtyInvoiced, " +
      "C_OrderLine.QtyDelivered, " +
      "C_OrderLine.DateOrdered, " +
      "C_OrderLine.DatePromised, " +
      "C_OrderLine.M_Warehouse_ID, " +
      "C_OrderLine.QtyReserved, " +
      "C_OrderLine.M_Shipper_ID, " +
      "C_OrderLine.C_BPartner_ID, " +
      "COALESCE(C_OrderLine.DirectShip, 'N') AS DirectShip, " +
      "C_OrderLine.FreightAmt, " +
      "C_OrderLine.C_BPartner_Location_ID, " +
      "COALESCE(C_OrderLine.CANCELPRICEAD, 'N') AS CANCELPRICEAD, " +
      "C_OrderLine.M_Product_Uom_Id, " +
      "C_OrderLine.QuantityOrder, " +
      "C_OrderLine.PriceStd, " +
      "C_OrderLine.grosspricestd, " +
      "COALESCE(C_OrderLine.Iseditlinenetamt, 'N') AS Iseditlinenetamt, " +
      "C_OrderLine.Quotationline_ID, " +
      "(CASE WHEN C_OrderLine.Quotationline_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.DocumentNo), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table9.DateOrdered, 'DD-MM-YYYY')),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table9.GrandTotal), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.Line), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table8.LineNetAmt), ''))),'') ) END) AS Quotationline_IDR, " +
      "C_OrderLine.SO_Res_Status, " +
      "C_OrderLine.Manage_Reservation, " +
      "COALESCE(C_OrderLine.Print_Description, 'N') AS Print_Description, " +
      "C_OrderLine.Overdue_Return_Days, " +
      "C_OrderLine.AD_Org_ID, " +
      "(CASE WHEN C_OrderLine.AD_Org_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table10.Name), ''))),'') ) END) AS AD_Org_IDR, " +
      "C_OrderLine.C_Project_ID, " +
      "(CASE WHEN C_OrderLine.C_Project_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.Value), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table11.Name), ''))),'') ) END) AS C_Project_IDR, " +
      "C_OrderLine.C_Costcenter_ID, " +
      "C_OrderLine.A_Asset_ID, " +
      "C_OrderLine.User1_ID, " +
      "C_OrderLine.User2_ID, " +
      "C_OrderLine.Explode, " +
      "C_OrderLine.BOM_Parent_ID, " +
      "C_OrderLine.Relate_Orderline, " +
      "C_OrderLine.M_Offer_ID, " +
      "C_OrderLine.C_Currency_ID, " +
      "C_OrderLine.C_Order_ID, " +
      "COALESCE(C_OrderLine.IsActive, 'N') AS IsActive, " +
      "C_OrderLine.AD_Client_ID, " +
      "C_OrderLine.DateInvoiced, " +
      "C_OrderLine.Ref_OrderLine_ID, " +
      "C_OrderLine.Manage_Prereservation, " +
      "C_OrderLine.DateDelivered, " +
      "C_OrderLine.PriceLimit, " +
      "COALESCE(C_OrderLine.IsDescription, 'N') AS IsDescription, " +
      "C_OrderLine.S_ResourceAssignment_ID, " +
      "C_OrderLine.C_OrderLine_ID, " +
      "C_OrderLine.ChargeAmt, " +
      "C_OrderLine.C_Order_Discount_ID, " +
      "C_OrderLine.C_Charge_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM C_OrderLine left join (select M_Product_ID, Name from M_Product) table1 on (C_OrderLine.M_Product_ID = table1.M_Product_ID) left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL1 on (table1.M_Product_ID = tableTRL1.M_Product_ID and tableTRL1.AD_Language = ?)  left join (select C_UOM_ID, Name from C_UOM) table3 on (C_OrderLine.C_UOM_ID = table3.C_UOM_ID) left join (select C_UOM_ID,AD_Language, Name from C_UOM_TRL) tableTRL3 on (table3.C_UOM_ID = tableTRL3.C_UOM_ID and tableTRL3.AD_Language = ?)  left join (select C_Tax_ID, Name from C_Tax) table5 on (C_OrderLine.C_Tax_ID =  table5.C_Tax_ID) left join (select C_Tax_ID,AD_Language, Name from C_Tax_TRL) tableTRL5 on (table5.C_Tax_ID = tableTRL5.C_Tax_ID and tableTRL5.AD_Language = ?)  left join (select M_Warehouse_Rule_ID, Name from M_Warehouse_Rule) table7 on (C_OrderLine.M_Warehouse_Rule_ID = table7.M_Warehouse_Rule_ID) left join ad_ref_list_v list1 on (C_OrderLine.Create_Reservation = list1.value and list1.ad_reference_id = '1852D69AB3FD453F8F031813501B26F0' and list1.ad_language = ?)  left join (select C_OrderLine_ID, C_Order_ID, Line, LineNetAmt from C_OrderLine) table8 on (C_OrderLine.Quotationline_ID = table8.C_OrderLine_ID) left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table9 on (table8.C_Order_ID = table9.C_Order_ID) left join (select AD_Org_ID, Name from AD_Org) table10 on (C_OrderLine.AD_Org_ID = table10.AD_Org_ID) left join (select C_Project_ID, Value, Name from C_Project) table11 on (C_OrderLine.C_Project_ID = table11.C_Project_ID)" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((cOrderId==null || cOrderId.equals(""))?"":"  AND C_OrderLine.C_Order_ID = ?  ");
    strSql = strSql + 
      "        AND C_OrderLine.C_OrderLine_ID = ? " +
      "        AND C_OrderLine.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND C_OrderLine.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (cOrderId != null && !(cOrderId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        Lineas3A55DA016C954896988EE248B7722C83Data objectLineas3A55DA016C954896988EE248B7722C83Data = new Lineas3A55DA016C954896988EE248B7722C83Data();
        objectLineas3A55DA016C954896988EE248B7722C83Data.created = UtilSql.getValue(result, "created");
        objectLineas3A55DA016C954896988EE248B7722C83Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.updated = UtilSql.getValue(result, "updated");
        objectLineas3A55DA016C954896988EE248B7722C83Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectLineas3A55DA016C954896988EE248B7722C83Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectLineas3A55DA016C954896988EE248B7722C83Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.line = UtilSql.getValue(result, "line");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mProductId = UtilSql.getValue(result, "m_product_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mProductIdr = UtilSql.getValue(result, "m_product_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.emAteccoStocktienda = UtilSql.getValue(result, "em_atecco_stocktienda");
        objectLineas3A55DA016C954896988EE248B7722C83Data.emAteccoStocktablero = UtilSql.getValue(result, "em_atecco_stocktablero");
        objectLineas3A55DA016C954896988EE248B7722C83Data.qtyordered = UtilSql.getValue(result, "qtyordered");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mInoutlineId = UtilSql.getValue(result, "m_inoutline_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mAttributesetinstanceId = UtilSql.getValue(result, "m_attributesetinstance_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cUomId = UtilSql.getValue(result, "c_uom_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cUomIdr = UtilSql.getValue(result, "c_uom_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cReturnReasonId = UtilSql.getValue(result, "c_return_reason_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.emAteccoPrecioefectivo = UtilSql.getValue(result, "em_atecco_precioefectivo");
        objectLineas3A55DA016C954896988EE248B7722C83Data.emAteccoTotalefectivo = UtilSql.getValue(result, "em_atecco_totalefectivo");
        objectLineas3A55DA016C954896988EE248B7722C83Data.emAteccoPreciotarjeta = UtilSql.getValue(result, "em_atecco_preciotarjeta");
        objectLineas3A55DA016C954896988EE248B7722C83Data.emAteccoTotaltarjeta = UtilSql.getValue(result, "em_atecco_totaltarjeta");
        objectLineas3A55DA016C954896988EE248B7722C83Data.priceactual = UtilSql.getValue(result, "priceactual");
        objectLineas3A55DA016C954896988EE248B7722C83Data.grossUnitPrice = UtilSql.getValue(result, "gross_unit_price");
        objectLineas3A55DA016C954896988EE248B7722C83Data.linenetamt = UtilSql.getValue(result, "linenetamt");
        objectLineas3A55DA016C954896988EE248B7722C83Data.lineGrossAmount = UtilSql.getValue(result, "line_gross_amount");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cTaxId = UtilSql.getValue(result, "c_tax_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cTaxIdr = UtilSql.getValue(result, "c_tax_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.pricelist = UtilSql.getValue(result, "pricelist");
        objectLineas3A55DA016C954896988EE248B7722C83Data.grosspricelist = UtilSql.getValue(result, "grosspricelist");
        objectLineas3A55DA016C954896988EE248B7722C83Data.discount = UtilSql.getValue(result, "discount");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mWarehouseRuleId = UtilSql.getValue(result, "m_warehouse_rule_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mWarehouseRuleIdr = UtilSql.getValue(result, "m_warehouse_rule_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.description = UtilSql.getValue(result, "description");
        objectLineas3A55DA016C954896988EE248B7722C83Data.createReservation = UtilSql.getValue(result, "create_reservation");
        objectLineas3A55DA016C954896988EE248B7722C83Data.createReservationr = UtilSql.getValue(result, "create_reservationr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.taxbaseamt = UtilSql.getValue(result, "taxbaseamt");
        objectLineas3A55DA016C954896988EE248B7722C83Data.qtyinvoiced = UtilSql.getValue(result, "qtyinvoiced");
        objectLineas3A55DA016C954896988EE248B7722C83Data.qtydelivered = UtilSql.getValue(result, "qtydelivered");
        objectLineas3A55DA016C954896988EE248B7722C83Data.dateordered = UtilSql.getDateValue(result, "dateordered", "dd-MM-yyyy");
        objectLineas3A55DA016C954896988EE248B7722C83Data.datepromised = UtilSql.getDateValue(result, "datepromised", "dd-MM-yyyy");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mWarehouseId = UtilSql.getValue(result, "m_warehouse_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.qtyreserved = UtilSql.getValue(result, "qtyreserved");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mShipperId = UtilSql.getValue(result, "m_shipper_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.directship = UtilSql.getValue(result, "directship");
        objectLineas3A55DA016C954896988EE248B7722C83Data.freightamt = UtilSql.getValue(result, "freightamt");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cancelpricead = UtilSql.getValue(result, "cancelpricead");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mProductUomId = UtilSql.getValue(result, "m_product_uom_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.quantityorder = UtilSql.getValue(result, "quantityorder");
        objectLineas3A55DA016C954896988EE248B7722C83Data.pricestd = UtilSql.getValue(result, "pricestd");
        objectLineas3A55DA016C954896988EE248B7722C83Data.grosspricestd = UtilSql.getValue(result, "grosspricestd");
        objectLineas3A55DA016C954896988EE248B7722C83Data.iseditlinenetamt = UtilSql.getValue(result, "iseditlinenetamt");
        objectLineas3A55DA016C954896988EE248B7722C83Data.quotationlineId = UtilSql.getValue(result, "quotationline_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.quotationlineIdr = UtilSql.getValue(result, "quotationline_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.soResStatus = UtilSql.getValue(result, "so_res_status");
        objectLineas3A55DA016C954896988EE248B7722C83Data.manageReservation = UtilSql.getValue(result, "manage_reservation");
        objectLineas3A55DA016C954896988EE248B7722C83Data.printDescription = UtilSql.getValue(result, "print_description");
        objectLineas3A55DA016C954896988EE248B7722C83Data.overdueReturnDays = UtilSql.getValue(result, "overdue_return_days");
        objectLineas3A55DA016C954896988EE248B7722C83Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.adOrgIdr = UtilSql.getValue(result, "ad_org_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cProjectId = UtilSql.getValue(result, "c_project_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cProjectIdr = UtilSql.getValue(result, "c_project_idr");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cCostcenterId = UtilSql.getValue(result, "c_costcenter_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.aAssetId = UtilSql.getValue(result, "a_asset_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.user1Id = UtilSql.getValue(result, "user1_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.user2Id = UtilSql.getValue(result, "user2_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.explode = UtilSql.getValue(result, "explode");
        objectLineas3A55DA016C954896988EE248B7722C83Data.bomParentId = UtilSql.getValue(result, "bom_parent_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.relateOrderline = UtilSql.getValue(result, "relate_orderline");
        objectLineas3A55DA016C954896988EE248B7722C83Data.mOfferId = UtilSql.getValue(result, "m_offer_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cCurrencyId = UtilSql.getValue(result, "c_currency_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cOrderId = UtilSql.getValue(result, "c_order_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.isactive = UtilSql.getValue(result, "isactive");
        objectLineas3A55DA016C954896988EE248B7722C83Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.dateinvoiced = UtilSql.getDateValue(result, "dateinvoiced", "dd-MM-yyyy");
        objectLineas3A55DA016C954896988EE248B7722C83Data.refOrderlineId = UtilSql.getValue(result, "ref_orderline_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.managePrereservation = UtilSql.getValue(result, "manage_prereservation");
        objectLineas3A55DA016C954896988EE248B7722C83Data.datedelivered = UtilSql.getDateValue(result, "datedelivered", "dd-MM-yyyy");
        objectLineas3A55DA016C954896988EE248B7722C83Data.pricelimit = UtilSql.getValue(result, "pricelimit");
        objectLineas3A55DA016C954896988EE248B7722C83Data.isdescription = UtilSql.getValue(result, "isdescription");
        objectLineas3A55DA016C954896988EE248B7722C83Data.sResourceassignmentId = UtilSql.getValue(result, "s_resourceassignment_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cOrderlineId = UtilSql.getValue(result, "c_orderline_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.chargeamt = UtilSql.getValue(result, "chargeamt");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cOrderDiscountId = UtilSql.getValue(result, "c_order_discount_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.cChargeId = UtilSql.getValue(result, "c_charge_id");
        objectLineas3A55DA016C954896988EE248B7722C83Data.language = UtilSql.getValue(result, "language");
        objectLineas3A55DA016C954896988EE248B7722C83Data.adUserClient = "";
        objectLineas3A55DA016C954896988EE248B7722C83Data.adOrgClient = "";
        objectLineas3A55DA016C954896988EE248B7722C83Data.createdby = "";
        objectLineas3A55DA016C954896988EE248B7722C83Data.trBgcolor = "";
        objectLineas3A55DA016C954896988EE248B7722C83Data.totalCount = "";
        objectLineas3A55DA016C954896988EE248B7722C83Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectLineas3A55DA016C954896988EE248B7722C83Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    Lineas3A55DA016C954896988EE248B7722C83Data objectLineas3A55DA016C954896988EE248B7722C83Data[] = new Lineas3A55DA016C954896988EE248B7722C83Data[vector.size()];
    vector.copyInto(objectLineas3A55DA016C954896988EE248B7722C83Data);
    return(objectLineas3A55DA016C954896988EE248B7722C83Data);
  }

/**
Create a registry
 */
  public static Lineas3A55DA016C954896988EE248B7722C83Data[] set(String cOrderId, String lineGrossAmount, String cancelpricead, String cOrderlineId, String adClientId, String adOrgId, String isactive, String createdby, String createdbyr, String updatedby, String updatedbyr, String line, String dateordered, String datepromised, String datedelivered, String dateinvoiced, String description, String mProductId, String mProductIdr, String cUomId, String mWarehouseId, String qtyordered, String qtyreserved, String qtydelivered, String qtyinvoiced, String mShipperId, String cCurrencyId, String pricelist, String priceactual, String cTaxId, String cBpartnerId, String directship, String freightamt, String cChargeId, String chargeamt, String emAteccoTotaltarjeta, String relateOrderline, String cBpartnerLocationId, String linenetamt, String pricelimit, String discount, String cCostcenterId, String emAteccoPrecioefectivo, String printDescription, String emAteccoTotalefectivo, String cReturnReasonId, String sResourceassignmentId, String cOrderDiscountId, String user2Id, String aAssetId, String overdueReturnDays, String refOrderlineId, String iseditlinenetamt, String taxbaseamt, String mProductUomId, String quantityorder, String mOfferId, String pricestd, String mAttributesetinstanceId, String user1Id, String isdescription, String bomParentId, String grosspricestd, String explode, String mInoutlineId, String cProjectId, String cProjectIdr, String grosspricelist, String mWarehouseRuleId, String createReservation, String soResStatus, String manageReservation, String managePrereservation, String emAteccoPreciotarjeta, String quotationlineId, String quotationlineIdr, String grossUnitPrice, String emAteccoStocktablero, String emAteccoStocktienda)    throws ServletException {
    Lineas3A55DA016C954896988EE248B7722C83Data objectLineas3A55DA016C954896988EE248B7722C83Data[] = new Lineas3A55DA016C954896988EE248B7722C83Data[1];
    objectLineas3A55DA016C954896988EE248B7722C83Data[0] = new Lineas3A55DA016C954896988EE248B7722C83Data();
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].created = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].createdbyr = createdbyr;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].updated = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].updatedTimeStamp = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].updatedby = updatedby;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].updatedbyr = updatedbyr;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].line = line;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mProductId = mProductId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mProductIdr = mProductIdr;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].emAteccoStocktienda = emAteccoStocktienda;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].emAteccoStocktablero = emAteccoStocktablero;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].qtyordered = qtyordered;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mInoutlineId = mInoutlineId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mAttributesetinstanceId = mAttributesetinstanceId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cUomId = cUomId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cUomIdr = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cReturnReasonId = cReturnReasonId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].emAteccoPrecioefectivo = emAteccoPrecioefectivo;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].emAteccoTotalefectivo = emAteccoTotalefectivo;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].emAteccoPreciotarjeta = emAteccoPreciotarjeta;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].emAteccoTotaltarjeta = emAteccoTotaltarjeta;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].priceactual = priceactual;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].grossUnitPrice = grossUnitPrice;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].linenetamt = linenetamt;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].lineGrossAmount = lineGrossAmount;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cTaxId = cTaxId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cTaxIdr = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].pricelist = pricelist;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].grosspricelist = grosspricelist;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].discount = discount;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mWarehouseRuleId = mWarehouseRuleId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mWarehouseRuleIdr = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].description = description;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].createReservation = createReservation;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].createReservationr = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].taxbaseamt = taxbaseamt;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].qtyinvoiced = qtyinvoiced;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].qtydelivered = qtydelivered;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].dateordered = dateordered;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].datepromised = datepromised;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mWarehouseId = mWarehouseId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].qtyreserved = qtyreserved;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mShipperId = mShipperId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cBpartnerId = cBpartnerId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].directship = directship;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].freightamt = freightamt;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cancelpricead = cancelpricead;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mProductUomId = mProductUomId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].quantityorder = quantityorder;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].pricestd = pricestd;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].grosspricestd = grosspricestd;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].iseditlinenetamt = iseditlinenetamt;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].quotationlineId = quotationlineId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].quotationlineIdr = quotationlineIdr;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].soResStatus = soResStatus;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].manageReservation = manageReservation;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].printDescription = printDescription;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].overdueReturnDays = overdueReturnDays;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].adOrgId = adOrgId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].adOrgIdr = "";
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cProjectId = cProjectId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cProjectIdr = cProjectIdr;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cCostcenterId = cCostcenterId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].aAssetId = aAssetId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].user1Id = user1Id;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].user2Id = user2Id;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].explode = explode;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].bomParentId = bomParentId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].relateOrderline = relateOrderline;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].mOfferId = mOfferId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cCurrencyId = cCurrencyId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cOrderId = cOrderId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].isactive = isactive;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].adClientId = adClientId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].dateinvoiced = dateinvoiced;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].refOrderlineId = refOrderlineId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].managePrereservation = managePrereservation;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].datedelivered = datedelivered;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].pricelimit = pricelimit;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].isdescription = isdescription;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].sResourceassignmentId = sResourceassignmentId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cOrderlineId = cOrderlineId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].chargeamt = chargeamt;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cOrderDiscountId = cOrderDiscountId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].cChargeId = cChargeId;
    objectLineas3A55DA016C954896988EE248B7722C83Data[0].language = "";
    return objectLineas3A55DA016C954896988EE248B7722C83Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2210_0(ConnectionProvider connectionProvider, String CreatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as CreatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2212_1(ConnectionProvider connectionProvider, String UpdatedByR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as UpdatedBy FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedByR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2214(ConnectionProvider connectionProvider, String C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT COALESCE(MAX(Line),0)+10 AS DefaultValue FROM C_OrderLine WHERE C_Order_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2221_2(ConnectionProvider connectionProvider, String paramLanguage, String M_Product_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL2.Name IS NULL THEN TO_CHAR(table2.Name) ELSE TO_CHAR(tableTRL2.Name) END)), ''))), '') ) as M_Product_ID FROM M_Product left join (select M_Product_ID, Name from M_Product) table2 on (M_Product.M_Product_ID = table2.M_Product_ID)left join (select M_Product_ID,AD_Language, Name from M_Product_TRL) tableTRL2 on (table2.M_Product_ID = tableTRL2.M_Product_ID and tableTRL2.AD_Language = ?)  WHERE M_Product.isActive='Y' AND M_Product.M_Product_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, M_Product_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "m_product_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef2764(ConnectionProvider connectionProvider, String C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT C_BPartner_ID AS DefaultValue FROM C_Order WHERE C_Order_ID=? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefB2023461A08649D58E5D144D4FC2C507_3(ConnectionProvider connectionProvider, String C_Project_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Value), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as C_Project_ID FROM C_Project left join (select C_Project_ID, Value, Name from C_Project) table2 on (C_Project.C_Project_ID = table2.C_Project_ID) WHERE C_Project.isActive='Y' AND C_Project.C_Project_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Project_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_project_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefE9612846B6F5472A8F64CF729E0B25AE_4(ConnectionProvider connectionProvider, String Quotationline_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.DocumentNo), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(table3.DateOrdered, 'DD-MM-YYYY')), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.GrandTotal), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Line), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.LineNetAmt), ''))), '') ) as Quotationline_ID FROM C_OrderLine left join (select C_OrderLine_ID, C_Order_ID, Line, LineNetAmt from C_OrderLine) table2 on (C_OrderLine.C_OrderLine_ID = table2.C_OrderLine_ID)left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table3 on (table2.C_Order_ID = table3.C_Order_ID) WHERE C_OrderLine.isActive='Y' AND C_OrderLine.C_OrderLine_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, Quotationline_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "quotationline_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT C_OrderLine.C_Order_ID AS NAME" +
      "        FROM C_OrderLine" +
      "        WHERE C_OrderLine.C_OrderLine_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateOrdered, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table1 on (C_Order.C_Order_ID = table1.C_Order_ID) WHERE C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String cOrderId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateOrdered, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Order left join (select C_Order_ID, DocumentNo, DateOrdered, GrandTotal from C_Order) table1 on (C_Order.C_Order_ID = table1.C_Order_ID) WHERE C_Order.C_Order_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE C_OrderLine" +
      "        SET Line = TO_NUMBER(?) , M_Product_ID = (?) , EM_Atecco_Stocktienda = TO_NUMBER(?) , EM_Atecco_Stocktablero = TO_NUMBER(?) , QtyOrdered = TO_NUMBER(?) , M_Inoutline_ID = (?) , M_AttributeSetInstance_ID = (?) , C_UOM_ID = (?) , C_Return_Reason_ID = (?) , EM_Atecco_Precioefectivo = TO_NUMBER(?) , EM_Atecco_Totalefectivo = TO_NUMBER(?) , EM_Atecco_Preciotarjeta = TO_NUMBER(?) , EM_Atecco_Totaltarjeta = TO_NUMBER(?) , PriceActual = TO_NUMBER(?) , Gross_Unit_Price = TO_NUMBER(?) , LineNetAmt = TO_NUMBER(?) , Line_Gross_Amount = TO_NUMBER(?) , C_Tax_ID = (?) , PriceList = TO_NUMBER(?) , GrossPriceList = TO_NUMBER(?) , Discount = TO_NUMBER(?) , M_Warehouse_Rule_ID = (?) , Description = (?) , Create_Reservation = (?) , Taxbaseamt = TO_NUMBER(?) , QtyInvoiced = TO_NUMBER(?) , QtyDelivered = TO_NUMBER(?) , DateOrdered = TO_DATE(?) , DatePromised = TO_DATE(?) , M_Warehouse_ID = (?) , QtyReserved = TO_NUMBER(?) , M_Shipper_ID = (?) , C_BPartner_ID = (?) , DirectShip = (?) , FreightAmt = TO_NUMBER(?) , C_BPartner_Location_ID = (?) , CANCELPRICEAD = (?) , M_Product_Uom_Id = (?) , QuantityOrder = TO_NUMBER(?) , PriceStd = TO_NUMBER(?) , grosspricestd = TO_NUMBER(?) , Iseditlinenetamt = (?) , Quotationline_ID = (?) , SO_Res_Status = (?) , Manage_Reservation = (?) , Print_Description = (?) , Overdue_Return_Days = TO_NUMBER(?) , AD_Org_ID = (?) , C_Project_ID = (?) , C_Costcenter_ID = (?) , A_Asset_ID = (?) , User1_ID = (?) , User2_ID = (?) , Explode = (?) , BOM_Parent_ID = (?) , Relate_Orderline = (?) , M_Offer_ID = (?) , C_Currency_ID = (?) , C_Order_ID = (?) , IsActive = (?) , AD_Client_ID = (?) , DateInvoiced = TO_DATE(?) , Ref_OrderLine_ID = (?) , Manage_Prereservation = (?) , DateDelivered = TO_DATE(?) , PriceLimit = TO_NUMBER(?) , IsDescription = (?) , S_ResourceAssignment_ID = (?) , C_OrderLine_ID = (?) , ChargeAmt = TO_NUMBER(?) , C_Order_Discount_ID = (?) , C_Charge_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE C_OrderLine.C_OrderLine_ID = ? " +
      "                 AND C_OrderLine.C_Order_ID = ? " +
      "        AND C_OrderLine.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_OrderLine.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktienda);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktablero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPrecioefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotalefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPreciotarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotaltarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priceactual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grossUnitPrice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, linenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lineGrossAmount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseRuleId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxbaseamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtydelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyreserved);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, directship);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cancelpricead);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityorder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iseditlinenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, manageReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, explode);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bomParentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateOrderline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mOfferId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, refOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managePrereservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datedelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelimit);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceassignmentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderDiscountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO C_OrderLine " +
      "        (Line, M_Product_ID, EM_Atecco_Stocktienda, EM_Atecco_Stocktablero, QtyOrdered, M_Inoutline_ID, M_AttributeSetInstance_ID, C_UOM_ID, C_Return_Reason_ID, EM_Atecco_Precioefectivo, EM_Atecco_Totalefectivo, EM_Atecco_Preciotarjeta, EM_Atecco_Totaltarjeta, PriceActual, Gross_Unit_Price, LineNetAmt, Line_Gross_Amount, C_Tax_ID, PriceList, GrossPriceList, Discount, M_Warehouse_Rule_ID, Description, Create_Reservation, Taxbaseamt, QtyInvoiced, QtyDelivered, DateOrdered, DatePromised, M_Warehouse_ID, QtyReserved, M_Shipper_ID, C_BPartner_ID, DirectShip, FreightAmt, C_BPartner_Location_ID, CANCELPRICEAD, M_Product_Uom_Id, QuantityOrder, PriceStd, grosspricestd, Iseditlinenetamt, Quotationline_ID, SO_Res_Status, Manage_Reservation, Print_Description, Overdue_Return_Days, AD_Org_ID, C_Project_ID, C_Costcenter_ID, A_Asset_ID, User1_ID, User2_ID, Explode, BOM_Parent_ID, Relate_Orderline, M_Offer_ID, C_Currency_ID, C_Order_ID, IsActive, AD_Client_ID, DateInvoiced, Ref_OrderLine_ID, Manage_Prereservation, DateDelivered, PriceLimit, IsDescription, S_ResourceAssignment_ID, C_OrderLine_ID, ChargeAmt, C_Order_Discount_ID, C_Charge_ID, created, createdby, updated, updatedBy)" +
      "        VALUES (TO_NUMBER(?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), TO_DATE(?), TO_DATE(?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), TO_NUMBER(?), TO_NUMBER(?), (?), (?), (?), (?), (?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), TO_NUMBER(?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, line);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktienda);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoStocktablero);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mInoutlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mAttributesetinstanceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cReturnReasonId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPrecioefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotalefectivo);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoPreciotarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoTotaltarjeta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, priceactual);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grossUnitPrice);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, linenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, lineGrossAmount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cTaxId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricelist);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, discount);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseRuleId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, description);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, taxbaseamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtydelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateordered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datepromised);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mWarehouseId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, qtyreserved);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mShipperId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, directship);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, freightamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cancelpricead);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mProductUomId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quantityorder);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, grosspricestd);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, iseditlinenetamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, quotationlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, soResStatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, manageReservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, printDescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, overdueReturnDays);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cProjectId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCostcenterId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, aAssetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user1Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, user2Id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, explode);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, bomParentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, relateOrderline);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, mOfferId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cCurrencyId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateinvoiced);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, refOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, managePrereservation);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, datedelivered);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, pricelimit);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isdescription);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, sResourceassignmentId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderlineId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, chargeamt);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderDiscountId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cChargeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String cOrderId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM C_OrderLine" +
      "        WHERE C_OrderLine.C_OrderLine_ID = ? " +
      "                 AND C_OrderLine.C_Order_ID = ? " +
      "        AND C_OrderLine.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND C_OrderLine.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cOrderId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM C_OrderLine" +
      "         WHERE C_OrderLine.C_OrderLine_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM C_OrderLine" +
      "         WHERE C_OrderLine.C_OrderLine_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
