//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.com.atrums.compras.montos.Anticipos;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data implements FieldProvider {
static Logger log4j = Logger.getLogger(RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String coNoEstablecimiento;
  public String coPtoEmision;
  public String noAutorizacion;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String fechaEmision;
  public String tipoComprobanteVenta;
  public String tipoComprobanteVentar;
  public String noComprobanteVenta;
  public String dateacct;
  public String totalRetencion;
  public String processed;
  public String isactive;
  public String posted;
  public String postedBtn;
  public String docactionre;
  public String docactionreBtn;
  public String processing;
  public String docstatus;
  public String cDoctypetargetId;
  public String adClientId;
  public String emAteccoCOrderId;
  public String coRetencionVentaId;
  public String cInvoiceId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("co_no_establecimiento") || fieldName.equals("coNoEstablecimiento"))
      return coNoEstablecimiento;
    else if (fieldName.equalsIgnoreCase("co_pto_emision") || fieldName.equals("coPtoEmision"))
      return coPtoEmision;
    else if (fieldName.equalsIgnoreCase("no_autorizacion") || fieldName.equals("noAutorizacion"))
      return noAutorizacion;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("fecha_emision") || fieldName.equals("fechaEmision"))
      return fechaEmision;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_venta") || fieldName.equals("tipoComprobanteVenta"))
      return tipoComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_ventar") || fieldName.equals("tipoComprobanteVentar"))
      return tipoComprobanteVentar;
    else if (fieldName.equalsIgnoreCase("no_comprobante_venta") || fieldName.equals("noComprobanteVenta"))
      return noComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("total_retencion") || fieldName.equals("totalRetencion"))
      return totalRetencion;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("docactionre"))
      return docactionre;
    else if (fieldName.equalsIgnoreCase("docactionre_btn") || fieldName.equals("docactionreBtn"))
      return docactionreBtn;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("em_atecco_c_order_id") || fieldName.equals("emAteccoCOrderId"))
      return emAteccoCOrderId;
    else if (fieldName.equalsIgnoreCase("co_retencion_venta_id") || fieldName.equals("coRetencionVentaId"))
      return coRetencionVentaId;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(CO_RETENCION_VENTA.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = CO_RETENCION_VENTA.CreatedBy) as CreatedByR, " +
      "        to_char(CO_RETENCION_VENTA.Updated, ?) as updated, " +
      "        to_char(CO_RETENCION_VENTA.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        CO_RETENCION_VENTA.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = CO_RETENCION_VENTA.UpdatedBy) as UpdatedByR," +
      "        CO_RETENCION_VENTA.AD_Org_ID, " +
      "CO_RETENCION_VENTA.CO_No_Establecimiento, " +
      "CO_RETENCION_VENTA.CO_Pto_Emision, " +
      "CO_RETENCION_VENTA.NO_Autorizacion, " +
      "CO_RETENCION_VENTA.C_Doctype_ID, " +
      "(CASE WHEN CO_RETENCION_VENTA.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "CO_RETENCION_VENTA.DocumentNo, " +
      "CO_RETENCION_VENTA.Fecha_Emision, " +
      "CO_RETENCION_VENTA.Tipo_Comprobante_Venta, " +
      "(CASE WHEN CO_RETENCION_VENTA.Tipo_Comprobante_Venta IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_Comprobante_VentaR, " +
      "CO_RETENCION_VENTA.NO_Comprobante_Venta, " +
      "CO_RETENCION_VENTA.Dateacct, " +
      "CO_RETENCION_VENTA.Total_Retencion, " +
      "COALESCE(CO_RETENCION_VENTA.Processed, 'N') AS Processed, " +
      "COALESCE(CO_RETENCION_VENTA.Isactive, 'N') AS Isactive, " +
      "CO_RETENCION_VENTA.Posted, " +
      "list2.name as Posted_BTN, " +
      "CO_RETENCION_VENTA.Docactionre, " +
      "list3.name as Docactionre_BTN, " +
      "CO_RETENCION_VENTA.Processing, " +
      "CO_RETENCION_VENTA.Docstatus, " +
      "CO_RETENCION_VENTA.C_Doctypetarget_ID, " +
      "CO_RETENCION_VENTA.AD_Client_ID, " +
      "CO_RETENCION_VENTA.EM_Atecco_C_Order_ID, " +
      "CO_RETENCION_VENTA.CO_Retencion_Venta_ID, " +
      "CO_RETENCION_VENTA.C_Invoice_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM CO_RETENCION_VENTA left join (select C_DocType_ID, Name from C_DocType) table1 on (CO_RETENCION_VENTA.C_Doctype_ID =  table1.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL1 on (table1.C_DocType_ID = tableTRL1.C_DocType_ID and tableTRL1.AD_Language = ?)  left join ad_ref_list_v list1 on (CO_RETENCION_VENTA.Tipo_Comprobante_Venta = list1.value and list1.ad_reference_id = '94DD3D9C266148BEAE4E201BD84F8F76' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = '234' and list2.ad_language = ?  AND CO_RETENCION_VENTA.Posted = TO_CHAR(list2.value)) left join ad_ref_list_v list3 on (list3.ad_reference_id = 'CB29EF103ACC49108693B711ACEF6261' and list3.ad_language = ?  AND CO_RETENCION_VENTA.Docactionre = TO_CHAR(list3.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 " +
      "        AND CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data = new RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data();
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.created = UtilSql.getValue(result, "created");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.updated = UtilSql.getValue(result, "updated");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.coNoEstablecimiento = UtilSql.getValue(result, "co_no_establecimiento");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.coPtoEmision = UtilSql.getValue(result, "co_pto_emision");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.noAutorizacion = UtilSql.getValue(result, "no_autorizacion");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.documentno = UtilSql.getValue(result, "documentno");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.fechaEmision = UtilSql.getDateValue(result, "fecha_emision", "dd-MM-yyyy");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.tipoComprobanteVenta = UtilSql.getValue(result, "tipo_comprobante_venta");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.tipoComprobanteVentar = UtilSql.getValue(result, "tipo_comprobante_ventar");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.noComprobanteVenta = UtilSql.getValue(result, "no_comprobante_venta");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.totalRetencion = UtilSql.getValue(result, "total_retencion");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.processed = UtilSql.getValue(result, "processed");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.isactive = UtilSql.getValue(result, "isactive");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.posted = UtilSql.getValue(result, "posted");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.docactionre = UtilSql.getValue(result, "docactionre");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.docactionreBtn = UtilSql.getValue(result, "docactionre_btn");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.processing = UtilSql.getValue(result, "processing");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.emAteccoCOrderId = UtilSql.getValue(result, "em_atecco_c_order_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.coRetencionVentaId = UtilSql.getValue(result, "co_retencion_venta_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.language = UtilSql.getValue(result, "language");
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.adUserClient = "";
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.adOrgClient = "";
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.createdby = "";
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.trBgcolor = "";
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.totalCount = "";
        objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[] = new RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[vector.size()];
    vector.copyInto(objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data);
    return(objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data);
  }

/**
Create a registry
 */
  public static RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[] set(String cDoctypetargetId, String processed, String docstatus, String coNoEstablecimiento, String createdby, String createdbyr, String fechaEmision, String posted, String postedBtn, String dateacct, String noComprobanteVenta, String updatedby, String updatedbyr, String cInvoiceId, String isactive, String emAteccoCOrderId, String documentno, String docactionre, String docactionreBtn, String adOrgId, String processing, String adClientId, String coRetencionVentaId, String tipoComprobanteVenta, String coPtoEmision, String totalRetencion, String noAutorizacion, String cDoctypeId)    throws ServletException {
    RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[] = new RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[1];
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0] = new RetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data();
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].created = "";
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].createdbyr = createdbyr;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].updated = "";
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].updatedTimeStamp = "";
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].updatedby = updatedby;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].updatedbyr = updatedbyr;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].adOrgId = adOrgId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].coNoEstablecimiento = coNoEstablecimiento;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].coPtoEmision = coPtoEmision;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].noAutorizacion = noAutorizacion;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].cDoctypeId = cDoctypeId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].cDoctypeIdr = "";
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].documentno = documentno;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].fechaEmision = fechaEmision;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].tipoComprobanteVenta = tipoComprobanteVenta;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].tipoComprobanteVentar = "";
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].noComprobanteVenta = noComprobanteVenta;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].dateacct = dateacct;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].totalRetencion = totalRetencion;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].processed = processed;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].isactive = isactive;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].posted = posted;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].postedBtn = postedBtn;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].docactionre = docactionre;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].docactionreBtn = docactionreBtn;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].processing = processing;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].docstatus = docstatus;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].cDoctypetargetId = cDoctypetargetId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].adClientId = adClientId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].emAteccoCOrderId = emAteccoCOrderId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].coRetencionVentaId = coRetencionVentaId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].cInvoiceId = cInvoiceId;
    objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data[0].language = "";
    return objectRetenciondeVentasEC27F765E8AD4350BDA35B43E093AAF4Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef180DACC7A8C64503904FDC79FAEC513E(ConnectionProvider connectionProvider, String c_invoice_id, String EM_Atecco_C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_estab,'001') AS DefaultValue from c_bpartner where c_bpartner_id in (select c_bpartner_id from c_invoice where (c_invoice_id = ? OR c_invoice_id = (select c_invoice_id from c_invoice where c_order_id = ?))) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, EM_Atecco_C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef28707085D03D4CA593A06A750114DEEE_0(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef399EF229625B47D890FE7831F76DB253(ConnectionProvider connectionProvider, String AD_CLIENT_ID, String c_invoice_id, String EM_Atecco_C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select coalesce(em_co_nro_estab,'') || '-' || coalesce(em_co_punto_emision,'') || '-' || documentno as defaultValue from c_invoice where ad_client_id = ? and (c_invoice_id = ? OR c_invoice_id = (select c_invoice_id from c_invoice where c_order_id = ?)) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, EM_Atecco_C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef4D49B91918F34C47854FE88F1A8C4FAD_1(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef672856CB1CE8445AAD110D27B811B470(ConnectionProvider connectionProvider, String c_invoice_id, String EM_Atecco_C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select c_invoice_id AS DefaultValue from c_invoice where (c_invoice_id = ? OR c_invoice_id = (select c_invoice_id from c_invoice where c_order_id = ?)) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, EM_Atecco_C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefEAB8C210B6D541E68296959AC3104EE1(ConnectionProvider connectionProvider, String c_invoice_id, String EM_Atecco_C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_punto_emision,'001') AS DefaultValue from c_bpartner where c_bpartner_id in (select c_bpartner_id from c_invoice where (c_invoice_id = ? OR c_invoice_id = (select c_invoice_id from c_invoice where c_order_id = ?))) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, EM_Atecco_C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefFA8A7F8EBC774ABFBCF7B50F8719DFA0(ConnectionProvider connectionProvider, String c_invoice_id, String EM_Atecco_C_Order_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select COALESCE(em_co_bp_nro_aut_rt_sri,'') AS DefaultValue from c_bpartner  where c_bpartner_id in (select c_bpartner_id from c_invoice where (c_invoice_id = ? OR c_invoice_id = (select c_invoice_id from c_invoice where c_order_id = ?))) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, EM_Atecco_C_Order_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE CO_RETENCION_VENTA" +
      "        SET AD_Org_ID = (?) , CO_No_Establecimiento = (?) , CO_Pto_Emision = (?) , NO_Autorizacion = (?) , C_Doctype_ID = (?) , DocumentNo = (?) , Fecha_Emision = TO_DATE(?) , Tipo_Comprobante_Venta = (?) , NO_Comprobante_Venta = (?) , Dateacct = TO_DATE(?) , Total_Retencion = TO_NUMBER(?) , Processed = (?) , Isactive = (?) , Posted = (?) , Docactionre = (?) , Processing = (?) , Docstatus = (?) , C_Doctypetarget_ID = (?) , AD_Client_ID = (?) , EM_Atecco_C_Order_ID = (?) , CO_Retencion_Venta_ID = (?) , C_Invoice_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coNoEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPtoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO CO_RETENCION_VENTA " +
      "        (AD_Org_ID, CO_No_Establecimiento, CO_Pto_Emision, NO_Autorizacion, C_Doctype_ID, DocumentNo, Fecha_Emision, Tipo_Comprobante_Venta, NO_Comprobante_Venta, Dateacct, Total_Retencion, Processed, Isactive, Posted, Docactionre, Processing, Docstatus, C_Doctypetarget_ID, AD_Client_ID, EM_Atecco_C_Order_ID, CO_Retencion_Venta_ID, C_Invoice_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coNoEstablecimiento);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPtoEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAteccoCOrderId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionVentaId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM CO_RETENCION_VENTA" +
      "        WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? " +
      "        AND CO_RETENCION_VENTA.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND CO_RETENCION_VENTA.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM CO_RETENCION_VENTA" +
      "         WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM CO_RETENCION_VENTA" +
      "         WHERE CO_RETENCION_VENTA.CO_Retencion_Venta_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
