//Sqlc generated V1.O00-1
package org.openbravo.erpWindows.PurchaseInvoice;

import java.sql.*;

import org.apache.log4j.Logger;

import javax.servlet.ServletException;

import org.openbravo.data.FieldProvider;
import org.openbravo.database.ConnectionProvider;
import org.openbravo.data.UtilSql;
import org.openbravo.service.db.QueryTimeOutUtil;
import org.openbravo.database.SessionInfo;
import java.util.*;

/**
WAD Generated class
 */
class RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data implements FieldProvider {
static Logger log4j = Logger.getLogger(RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.class);
  private String InitRecordNumber="0";
  public String created;
  public String createdbyr;
  public String updated;
  public String updatedTimeStamp;
  public String updatedby;
  public String updatedbyr;
  public String adOrgId;
  public String isactive;
  public String cInvoiceId;
  public String cDoctypeId;
  public String cDoctypeIdr;
  public String documentno;
  public String noAutorizacion;
  public String cBpartnerId;
  public String cBpartnerIdr;
  public String rucBp;
  public String cBpartnerLocationId;
  public String cBpartnerLocationIdr;
  public String fechaEmision;
  public String tipoComprobanteVenta;
  public String tipoComprobanteVentar;
  public String noComprobanteVenta;
  public String dateacct;
  public String totalRetencion;
  public String docstatus;
  public String processed;
  public String processing;
  public String posted;
  public String postedBtn;
  public String docactionre;
  public String docactionreBtn;
  public String cDoctypetargetId;
  public String coPosted;
  public String emAtecfeDocstatus;
  public String emAtecoffDocaction;
  public String emAtecoffDocactionBtn;
  public String emAtecfeDocaction;
  public String emAtecfeMenobserrorSri;
  public String emAtecfeCodigoAcc;
  public String emAtecoffEnviardoc;
  public String adClientId;
  public String emAtecoffDocstatus;
  public String coRetencionCompraId;
  public String language;
  public String adUserClient;
  public String adOrgClient;
  public String createdby;
  public String trBgcolor;
  public String totalCount;
  public String dateTimeFormat;

  public String getInitRecordNumber() {
    return InitRecordNumber;
  }

  public String getField(String fieldName) {
    if (fieldName.equalsIgnoreCase("created"))
      return created;
    else if (fieldName.equalsIgnoreCase("createdbyr"))
      return createdbyr;
    else if (fieldName.equalsIgnoreCase("updated"))
      return updated;
    else if (fieldName.equalsIgnoreCase("updated_time_stamp") || fieldName.equals("updatedTimeStamp"))
      return updatedTimeStamp;
    else if (fieldName.equalsIgnoreCase("updatedby"))
      return updatedby;
    else if (fieldName.equalsIgnoreCase("updatedbyr"))
      return updatedbyr;
    else if (fieldName.equalsIgnoreCase("ad_org_id") || fieldName.equals("adOrgId"))
      return adOrgId;
    else if (fieldName.equalsIgnoreCase("isactive"))
      return isactive;
    else if (fieldName.equalsIgnoreCase("c_invoice_id") || fieldName.equals("cInvoiceId"))
      return cInvoiceId;
    else if (fieldName.equalsIgnoreCase("c_doctype_id") || fieldName.equals("cDoctypeId"))
      return cDoctypeId;
    else if (fieldName.equalsIgnoreCase("c_doctype_idr") || fieldName.equals("cDoctypeIdr"))
      return cDoctypeIdr;
    else if (fieldName.equalsIgnoreCase("documentno"))
      return documentno;
    else if (fieldName.equalsIgnoreCase("no_autorizacion") || fieldName.equals("noAutorizacion"))
      return noAutorizacion;
    else if (fieldName.equalsIgnoreCase("c_bpartner_id") || fieldName.equals("cBpartnerId"))
      return cBpartnerId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_idr") || fieldName.equals("cBpartnerIdr"))
      return cBpartnerIdr;
    else if (fieldName.equalsIgnoreCase("ruc_bp") || fieldName.equals("rucBp"))
      return rucBp;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_id") || fieldName.equals("cBpartnerLocationId"))
      return cBpartnerLocationId;
    else if (fieldName.equalsIgnoreCase("c_bpartner_location_idr") || fieldName.equals("cBpartnerLocationIdr"))
      return cBpartnerLocationIdr;
    else if (fieldName.equalsIgnoreCase("fecha_emision") || fieldName.equals("fechaEmision"))
      return fechaEmision;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_venta") || fieldName.equals("tipoComprobanteVenta"))
      return tipoComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("tipo_comprobante_ventar") || fieldName.equals("tipoComprobanteVentar"))
      return tipoComprobanteVentar;
    else if (fieldName.equalsIgnoreCase("no_comprobante_venta") || fieldName.equals("noComprobanteVenta"))
      return noComprobanteVenta;
    else if (fieldName.equalsIgnoreCase("dateacct"))
      return dateacct;
    else if (fieldName.equalsIgnoreCase("total_retencion") || fieldName.equals("totalRetencion"))
      return totalRetencion;
    else if (fieldName.equalsIgnoreCase("docstatus"))
      return docstatus;
    else if (fieldName.equalsIgnoreCase("processed"))
      return processed;
    else if (fieldName.equalsIgnoreCase("processing"))
      return processing;
    else if (fieldName.equalsIgnoreCase("posted"))
      return posted;
    else if (fieldName.equalsIgnoreCase("posted_btn") || fieldName.equals("postedBtn"))
      return postedBtn;
    else if (fieldName.equalsIgnoreCase("docactionre"))
      return docactionre;
    else if (fieldName.equalsIgnoreCase("docactionre_btn") || fieldName.equals("docactionreBtn"))
      return docactionreBtn;
    else if (fieldName.equalsIgnoreCase("c_doctypetarget_id") || fieldName.equals("cDoctypetargetId"))
      return cDoctypetargetId;
    else if (fieldName.equalsIgnoreCase("co_posted") || fieldName.equals("coPosted"))
      return coPosted;
    else if (fieldName.equalsIgnoreCase("em_atecfe_docstatus") || fieldName.equals("emAtecfeDocstatus"))
      return emAtecfeDocstatus;
    else if (fieldName.equalsIgnoreCase("em_atecoff_docaction") || fieldName.equals("emAtecoffDocaction"))
      return emAtecoffDocaction;
    else if (fieldName.equalsIgnoreCase("em_atecoff_docaction_btn") || fieldName.equals("emAtecoffDocactionBtn"))
      return emAtecoffDocactionBtn;
    else if (fieldName.equalsIgnoreCase("em_atecfe_docaction") || fieldName.equals("emAtecfeDocaction"))
      return emAtecfeDocaction;
    else if (fieldName.equalsIgnoreCase("em_atecfe_menobserror_sri") || fieldName.equals("emAtecfeMenobserrorSri"))
      return emAtecfeMenobserrorSri;
    else if (fieldName.equalsIgnoreCase("em_atecfe_codigo_acc") || fieldName.equals("emAtecfeCodigoAcc"))
      return emAtecfeCodigoAcc;
    else if (fieldName.equalsIgnoreCase("em_atecoff_enviardoc") || fieldName.equals("emAtecoffEnviardoc"))
      return emAtecoffEnviardoc;
    else if (fieldName.equalsIgnoreCase("ad_client_id") || fieldName.equals("adClientId"))
      return adClientId;
    else if (fieldName.equalsIgnoreCase("em_atecoff_docstatus") || fieldName.equals("emAtecoffDocstatus"))
      return emAtecoffDocstatus;
    else if (fieldName.equalsIgnoreCase("co_retencion_compra_id") || fieldName.equals("coRetencionCompraId"))
      return coRetencionCompraId;
    else if (fieldName.equalsIgnoreCase("language"))
      return language;
    else if (fieldName.equals("adUserClient"))
      return adUserClient;
    else if (fieldName.equals("adOrgClient"))
      return adOrgClient;
    else if (fieldName.equals("createdby"))
      return createdby;
    else if (fieldName.equals("trBgcolor"))
      return trBgcolor;
    else if (fieldName.equals("totalCount"))
      return totalCount;
    else if (fieldName.equals("dateTimeFormat"))
      return dateTimeFormat;
   else {
     log4j.debug("Field does not exist: " + fieldName);
     return null;
   }
 }

/**
Select for edit
 */
  public static RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cInvoiceId, String key, String adUserClient, String adOrgClient)    throws ServletException {
    return selectEdit(connectionProvider, dateTimeFormat, paramLanguage, cInvoiceId, key, adUserClient, adOrgClient, 0, 0);
  }

/**
Select for edit
 */
  public static RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[] selectEdit(ConnectionProvider connectionProvider, String dateTimeFormat, String paramLanguage, String cInvoiceId, String key, String adUserClient, String adOrgClient, int firstRegister, int numberRegisters)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(co_retencion_compra.Created, ?) as created, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = co_retencion_compra.CreatedBy) as CreatedByR, " +
      "        to_char(co_retencion_compra.Updated, ?) as updated, " +
      "        to_char(co_retencion_compra.Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp,  " +
      "        co_retencion_compra.UpdatedBy, " +
      "        (SELECT NAME FROM AD_USER u WHERE AD_USER_ID = co_retencion_compra.UpdatedBy) as UpdatedByR," +
      "        co_retencion_compra.AD_Org_ID, " +
      "COALESCE(co_retencion_compra.Isactive, 'N') AS Isactive, " +
      "co_retencion_compra.C_Invoice_ID, " +
      "co_retencion_compra.C_Doctype_ID, " +
      "(CASE WHEN co_retencion_compra.C_Doctype_ID IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR((CASE WHEN tableTRL1.Name IS NULL THEN TO_CHAR(table1.Name) ELSE TO_CHAR(tableTRL1.Name) END)), ''))),'') ) END) AS C_Doctype_IDR, " +
      "co_retencion_compra.Documentno, " +
      "co_retencion_compra.NO_Autorizacion, " +
      "co_retencion_compra.C_Bpartner_ID, " +
      "(CASE WHEN co_retencion_compra.C_Bpartner_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name), ''))),'')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table3.Name2), ''))),'') ) END) AS C_Bpartner_IDR, " +
      "co_retencion_compra.RUC_Bp, " +
      "co_retencion_compra.C_Bpartner_Location_ID, " +
      "(CASE WHEN co_retencion_compra.C_Bpartner_Location_ID IS NULL THEN '' ELSE  (COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table4.Name), ''))),'') ) END) AS C_Bpartner_Location_IDR, " +
      "co_retencion_compra.Fecha_Emision, " +
      "co_retencion_compra.Tipo_Comprobante_Venta, " +
      "(CASE WHEN co_retencion_compra.Tipo_Comprobante_Venta IS NULL THEN '' ELSE  ( COALESCE(TO_CHAR(list1.name),'') ) END) AS Tipo_Comprobante_VentaR, " +
      "co_retencion_compra.NO_Comprobante_Venta, " +
      "co_retencion_compra.Dateacct, " +
      "co_retencion_compra.Total_Retencion, " +
      "co_retencion_compra.Docstatus, " +
      "COALESCE(co_retencion_compra.Processed, 'N') AS Processed, " +
      "co_retencion_compra.Processing, " +
      "co_retencion_compra.Posted, " +
      "list2.name as Posted_BTN, " +
      "co_retencion_compra.Docactionre, " +
      "list3.name as Docactionre_BTN, " +
      "co_retencion_compra.C_Doctypetarget_ID, " +
      "co_retencion_compra.CO_Posted, " +
      "co_retencion_compra.EM_Atecfe_Docstatus, " +
      "co_retencion_compra.EM_Atecoff_Docaction, " +
      "list4.name as EM_Atecoff_Docaction_BTN, " +
      "co_retencion_compra.EM_Atecfe_Docaction, " +
      "co_retencion_compra.EM_Atecfe_Menobserror_Sri, " +
      "co_retencion_compra.EM_Atecfe_Codigo_Acc, " +
      "co_retencion_compra.EM_Atecoff_Enviardoc, " +
      "co_retencion_compra.AD_Client_ID, " +
      "co_retencion_compra.EM_Atecoff_Docstatus, " +
      "co_retencion_compra.CO_Retencion_Compra_ID, " +
      "        ? AS LANGUAGE " +
      "        FROM co_retencion_compra left join (select C_DocType_ID, Name from C_DocType) table1 on (co_retencion_compra.C_Doctype_ID =  table1.C_DocType_ID) left join (select C_DocType_ID,AD_Language, Name from C_DocType_TRL) tableTRL1 on (table1.C_DocType_ID = tableTRL1.C_DocType_ID and tableTRL1.AD_Language = ?)  left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table3 on (co_retencion_compra.C_Bpartner_ID = table3.C_BPartner_ID) left join (select C_Bpartner_Location_ID, Name from C_Bpartner_Location) table4 on (co_retencion_compra.C_Bpartner_Location_ID = table4.C_Bpartner_Location_ID) left join ad_ref_list_v list1 on (co_retencion_compra.Tipo_Comprobante_Venta = list1.value and list1.ad_reference_id = '94DD3D9C266148BEAE4E201BD84F8F76' and list1.ad_language = ?)  left join ad_ref_list_v list2 on (list2.ad_reference_id = '234' and list2.ad_language = ?  AND co_retencion_compra.Posted = TO_CHAR(list2.value)) left join ad_ref_list_v list3 on (list3.ad_reference_id = 'CB29EF103ACC49108693B711ACEF6261' and list3.ad_language = ?  AND co_retencion_compra.Docactionre = TO_CHAR(list3.value)) left join ad_ref_list_v list4 on (list4.ad_reference_id = '650F71B020D14AD4B10FA7523E116A12' and list4.ad_language = ?  AND co_retencion_compra.EM_Atecoff_Docaction = TO_CHAR(list4.value))" +
      "        WHERE 2=2 " +
      "        AND 1=1 ";
    strSql = strSql + ((cInvoiceId==null || cInvoiceId.equals(""))?"":"  AND co_retencion_compra.C_Invoice_ID = ?  ");
    strSql = strSql + 
      "        AND co_retencion_compra.CO_Retencion_Compra_ID = ? " +
      "        AND co_retencion_compra.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "           AND co_retencion_compra.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    ResultSet result;
    Vector<java.lang.Object> vector = new Vector<java.lang.Object>(0);
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateTimeFormat);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, paramLanguage);
      if (cInvoiceId != null && !(cInvoiceId.equals(""))) {
        iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      }
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      result = st.executeQuery();
      long countRecord = 0;
      long countRecordSkip = 1;
      boolean continueResult = true;
      while(countRecordSkip < firstRegister && continueResult) {
        continueResult = result.next();
        countRecordSkip++;
      }
      while(continueResult && result.next()) {
        countRecord++;
        RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data = new RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data();
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.created = UtilSql.getValue(result, "created");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.createdbyr = UtilSql.getValue(result, "createdbyr");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.updated = UtilSql.getValue(result, "updated");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.updatedTimeStamp = UtilSql.getValue(result, "updated_time_stamp");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.updatedby = UtilSql.getValue(result, "updatedby");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.updatedbyr = UtilSql.getValue(result, "updatedbyr");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.adOrgId = UtilSql.getValue(result, "ad_org_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.isactive = UtilSql.getValue(result, "isactive");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cInvoiceId = UtilSql.getValue(result, "c_invoice_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cDoctypeId = UtilSql.getValue(result, "c_doctype_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cDoctypeIdr = UtilSql.getValue(result, "c_doctype_idr");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.documentno = UtilSql.getValue(result, "documentno");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.noAutorizacion = UtilSql.getValue(result, "no_autorizacion");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cBpartnerId = UtilSql.getValue(result, "c_bpartner_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cBpartnerIdr = UtilSql.getValue(result, "c_bpartner_idr");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.rucBp = UtilSql.getValue(result, "ruc_bp");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cBpartnerLocationId = UtilSql.getValue(result, "c_bpartner_location_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cBpartnerLocationIdr = UtilSql.getValue(result, "c_bpartner_location_idr");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.fechaEmision = UtilSql.getDateValue(result, "fecha_emision", "dd-MM-yyyy");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.tipoComprobanteVenta = UtilSql.getValue(result, "tipo_comprobante_venta");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.tipoComprobanteVentar = UtilSql.getValue(result, "tipo_comprobante_ventar");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.noComprobanteVenta = UtilSql.getValue(result, "no_comprobante_venta");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.dateacct = UtilSql.getDateValue(result, "dateacct", "dd-MM-yyyy");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.totalRetencion = UtilSql.getValue(result, "total_retencion");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.docstatus = UtilSql.getValue(result, "docstatus");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.processed = UtilSql.getValue(result, "processed");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.processing = UtilSql.getValue(result, "processing");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.posted = UtilSql.getValue(result, "posted");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.postedBtn = UtilSql.getValue(result, "posted_btn");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.docactionre = UtilSql.getValue(result, "docactionre");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.docactionreBtn = UtilSql.getValue(result, "docactionre_btn");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.cDoctypetargetId = UtilSql.getValue(result, "c_doctypetarget_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.coPosted = UtilSql.getValue(result, "co_posted");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecfeDocstatus = UtilSql.getValue(result, "em_atecfe_docstatus");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecoffDocaction = UtilSql.getValue(result, "em_atecoff_docaction");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecoffDocactionBtn = UtilSql.getValue(result, "em_atecoff_docaction_btn");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecfeDocaction = UtilSql.getValue(result, "em_atecfe_docaction");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecfeMenobserrorSri = UtilSql.getValue(result, "em_atecfe_menobserror_sri");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecfeCodigoAcc = UtilSql.getValue(result, "em_atecfe_codigo_acc");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecoffEnviardoc = UtilSql.getValue(result, "em_atecoff_enviardoc");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.adClientId = UtilSql.getValue(result, "ad_client_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.emAtecoffDocstatus = UtilSql.getValue(result, "em_atecoff_docstatus");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.coRetencionCompraId = UtilSql.getValue(result, "co_retencion_compra_id");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.language = UtilSql.getValue(result, "language");
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.adUserClient = "";
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.adOrgClient = "";
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.createdby = "";
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.trBgcolor = "";
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.totalCount = "";
        objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data.InitRecordNumber = Integer.toString(firstRegister);
        vector.addElement(objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data);
        if (countRecord >= numberRegisters && numberRegisters != 0) {
          continueResult = false;
        }
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[] = new RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[vector.size()];
    vector.copyInto(objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data);
    return(objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data);
  }

/**
Create a registry
 */
  public static RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[] set(String cInvoiceId, String emAtecfeDocstatus, String rucBp, String emAtecoffDocstatus, String documentno, String updatedby, String updatedbyr, String cDoctypeId, String createdby, String createdbyr, String noAutorizacion, String totalRetencion, String adOrgId, String docactionre, String docactionreBtn, String coRetencionCompraId, String tipoComprobanteVenta, String posted, String postedBtn, String cDoctypetargetId, String noComprobanteVenta, String cBpartnerId, String cBpartnerIdr, String docstatus, String adClientId, String processed, String cBpartnerLocationId, String coPosted, String emAtecfeDocaction, String emAtecoffDocaction, String emAtecoffDocactionBtn, String dateacct, String processing, String emAtecfeMenobserrorSri, String isactive, String emAtecoffEnviardoc, String fechaEmision, String emAtecfeCodigoAcc)    throws ServletException {
    RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[] = new RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[1];
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0] = new RetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data();
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].created = "";
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].createdbyr = createdbyr;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].updated = "";
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].updatedTimeStamp = "";
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].updatedby = updatedby;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].updatedbyr = updatedbyr;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].adOrgId = adOrgId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].isactive = isactive;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cInvoiceId = cInvoiceId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cDoctypeId = cDoctypeId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cDoctypeIdr = "";
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].documentno = documentno;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].noAutorizacion = noAutorizacion;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cBpartnerId = cBpartnerId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cBpartnerIdr = cBpartnerIdr;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].rucBp = rucBp;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cBpartnerLocationId = cBpartnerLocationId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cBpartnerLocationIdr = "";
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].fechaEmision = fechaEmision;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].tipoComprobanteVenta = tipoComprobanteVenta;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].tipoComprobanteVentar = "";
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].noComprobanteVenta = noComprobanteVenta;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].dateacct = dateacct;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].totalRetencion = totalRetencion;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].docstatus = docstatus;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].processed = processed;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].processing = processing;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].posted = posted;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].postedBtn = postedBtn;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].docactionre = docactionre;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].docactionreBtn = docactionreBtn;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].cDoctypetargetId = cDoctypetargetId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].coPosted = coPosted;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecfeDocstatus = emAtecfeDocstatus;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecoffDocaction = emAtecoffDocaction;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecoffDocactionBtn = emAtecoffDocactionBtn;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecfeDocaction = emAtecfeDocaction;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecfeMenobserrorSri = emAtecfeMenobserrorSri;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecfeCodigoAcc = emAtecfeCodigoAcc;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecoffEnviardoc = emAtecoffEnviardoc;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].adClientId = adClientId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].emAtecoffDocstatus = emAtecoffDocstatus;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].coRetencionCompraId = coRetencionCompraId;
    objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data[0].language = "";
    return objectRetencionesdeCompraD94E34B6795B415FAEBE0DCA7F2E2AF8Data;
  }

/**
Select for auxiliar field
 */
  public static String selectDef2171B621BBEC47F98905797D0923C461(ConnectionProvider connectionProvider, String AD_ORG_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT p_documentno as DefaultValue FROM ad_sequence_doctype((select dt.c_doctype_id               from c_doctype dt             where dt.ad_table_id = '5F3A4CF997BA41BFB7C547777AF7CC1C' and dt.ad_org_id = ?), '', 'Y') ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef3FA9E20DEFBB40058472F91BEFB41710_0(ConnectionProvider connectionProvider, String UpdatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Updatedby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, UpdatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updatedby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef6A4EA916956646888F72D195B6AFAA7E_1(ConnectionProvider connectionProvider, String CreatedbyR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '') ) as Createdby FROM AD_User left join (select AD_User_ID, Name from AD_User) table2 on (AD_User.AD_User_ID = table2.AD_User_ID) WHERE AD_User.isActive='Y' AND AD_User.AD_User_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, CreatedbyR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "createdby");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDef6AFB295FEF6B4CB6B2C538A058CE0577(ConnectionProvider connectionProvider, String AD_CLIENT_ID, String AD_ORG_ID)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT Co_Nro_Aut_Sri AS DefaultValue FROM co_aut_sri WHERE isactive='Y' and c_doctype_id = (select c_doctype_id from c_doctype where ad_table_id = '5F3A4CF997BA41BFB7C547777AF7CC1C' and ad_client_id = ? and ad_org_id = ? ) ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_ORG_ID);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefA4ABD9DAA8484228A30932CF91999E30(ConnectionProvider connectionProvider, String AD_CLIENT_ID, String c_invoice_id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        select em_co_nro_estab || '-' || em_co_punto_emision || '-' || documentno as defaultValue from c_invoice where ad_client_id = ? and c_invoice_id = ? ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, AD_CLIENT_ID);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, c_invoice_id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "defaultvalue");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for auxiliar field
 */
  public static String selectDefAC4DD86C1F754513B6D819E073F80159_2(ConnectionProvider connectionProvider, String C_Bpartner_IDR)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT  ( COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name), ''))), '')  || ' - ' || COALESCE(TO_CHAR(TO_CHAR(COALESCE(TO_CHAR(table2.Name2), ''))), '') ) as C_Bpartner_ID FROM C_BPartner left join (select C_BPartner_ID, Name, Name2 from C_BPartner) table2 on (C_BPartner.C_BPartner_ID = table2.C_BPartner_ID) WHERE C_BPartner.isActive='Y' AND C_BPartner.C_BPartner_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, C_Bpartner_IDR);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "c_bpartner_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
return the parent ID
 */
  public static String selectParentID(ConnectionProvider connectionProvider, String key)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT co_retencion_compra.C_Invoice_ID AS NAME" +
      "        FROM co_retencion_compra" +
      "        WHERE co_retencion_compra.CO_Retencion_Compra_ID = ?";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, key);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParent(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateInvoiced, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Invoice left join (select C_Invoice_ID, DocumentNo, DateInvoiced, GrandTotal from C_Invoice) table1 on (C_Invoice.C_Invoice_ID = table1.C_Invoice_ID) WHERE C_Invoice.C_Invoice_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

/**
Select for parent field
 */
  public static String selectParentTrl(ConnectionProvider connectionProvider, String cInvoiceId)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT (TO_CHAR(COALESCE(TO_CHAR(table1.DocumentNo), '')) || ' - ' || TO_CHAR(table1.DateInvoiced, 'DD-MM-YYYY') || ' - ' || TO_CHAR(COALESCE(TO_CHAR(table1.GrandTotal), ''))) AS NAME FROM C_Invoice left join (select C_Invoice_ID, DocumentNo, DateInvoiced, GrandTotal from C_Invoice) table1 on (C_Invoice.C_Invoice_ID = table1.C_Invoice_ID) WHERE C_Invoice.C_Invoice_ID = ?  ";

    ResultSet result;
    String strReturn = "";
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "name");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public int update(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        UPDATE co_retencion_compra" +
      "        SET AD_Org_ID = (?) , Isactive = (?) , C_Invoice_ID = (?) , C_Doctype_ID = (?) , Documentno = (?) , NO_Autorizacion = (?) , C_Bpartner_ID = (?) , RUC_Bp = (?) , C_Bpartner_Location_ID = (?) , Fecha_Emision = TO_DATE(?) , Tipo_Comprobante_Venta = (?) , NO_Comprobante_Venta = (?) , Dateacct = TO_DATE(?) , Total_Retencion = TO_NUMBER(?) , Docstatus = (?) , Processed = (?) , Processing = (?) , Posted = (?) , Docactionre = (?) , C_Doctypetarget_ID = (?) , CO_Posted = (?) , EM_Atecfe_Docstatus = (?) , EM_Atecoff_Docaction = (?) , EM_Atecfe_Docaction = (?) , EM_Atecfe_Menobserror_Sri = (?) , EM_Atecfe_Codigo_Acc = (?) , EM_Atecoff_Enviardoc = (?) , AD_Client_ID = (?) , EM_Atecoff_Docstatus = (?) , CO_Retencion_Compra_ID = (?) , updated = now(), updatedby = ? " +
      "        WHERE co_retencion_compra.CO_Retencion_Compra_ID = ? " +
      "                 AND co_retencion_compra.C_Invoice_ID = ? " +
      "        AND co_retencion_compra.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND co_retencion_compra.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rucBp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPosted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserrorSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeCodigoAcc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffEnviardoc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionCompraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionCompraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public int insert(Connection conn, ConnectionProvider connectionProvider)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        INSERT INTO co_retencion_compra " +
      "        (AD_Org_ID, Isactive, C_Invoice_ID, C_Doctype_ID, Documentno, NO_Autorizacion, C_Bpartner_ID, RUC_Bp, C_Bpartner_Location_ID, Fecha_Emision, Tipo_Comprobante_Venta, NO_Comprobante_Venta, Dateacct, Total_Retencion, Docstatus, Processed, Processing, Posted, Docactionre, C_Doctypetarget_ID, CO_Posted, EM_Atecfe_Docstatus, EM_Atecoff_Docaction, EM_Atecfe_Docaction, EM_Atecfe_Menobserror_Sri, EM_Atecfe_Codigo_Acc, EM_Atecoff_Enviardoc, AD_Client_ID, EM_Atecoff_Docstatus, CO_Retencion_Compra_ID, created, createdby, updated, updatedBy)" +
      "        VALUES ((?), (?), (?), (?), (?), (?), (?), (?), (?), TO_DATE(?), (?), (?), TO_DATE(?), TO_NUMBER(?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), (?), now(), ?, now(), ?)";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(conn, strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adOrgId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, isactive);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypeId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, documentno);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noAutorizacion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, rucBp);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cBpartnerLocationId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, fechaEmision);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, tipoComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, noComprobanteVenta);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, dateacct);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, totalRetencion);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processed);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, processing);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, posted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, docactionre);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cDoctypetargetId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coPosted);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeDocaction);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeMenobserrorSri);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecfeCodigoAcc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffEnviardoc);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, adClientId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, emAtecoffDocstatus);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, coRetencionCompraId);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, createdby);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, updatedby);

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releaseTransactionalPreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

  public static int delete(ConnectionProvider connectionProvider, String param1, String cInvoiceId, String adUserClient, String adOrgClient)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        DELETE FROM co_retencion_compra" +
      "        WHERE co_retencion_compra.CO_Retencion_Compra_ID = ? " +
      "                 AND co_retencion_compra.C_Invoice_ID = ? " +
      "        AND co_retencion_compra.AD_Client_ID IN (";
    strSql = strSql + ((adUserClient==null || adUserClient.equals(""))?"":adUserClient);
    strSql = strSql + 
      ") " +
      "        AND co_retencion_compra.AD_Org_ID IN (";
    strSql = strSql + ((adOrgClient==null || adOrgClient.equals(""))?"":adOrgClient);
    strSql = strSql + 
      ") ";

    int updateCount = 0;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, param1);
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, cInvoiceId);
      if (adUserClient != null && !(adUserClient.equals(""))) {
        }
      if (adOrgClient != null && !(adOrgClient.equals(""))) {
        }

      updateCount = st.executeUpdate();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(updateCount);
  }

/**
Select for relation
 */
  public static String selectOrg(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT AD_ORG_ID" +
      "          FROM co_retencion_compra" +
      "         WHERE co_retencion_compra.CO_Retencion_Compra_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "ad_org_id");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }

  public static String getCurrentDBTimestamp(ConnectionProvider connectionProvider, String id)    throws ServletException {
    String strSql = "";
    strSql = strSql + 
      "        SELECT to_char(Updated, 'YYYYMMDDHH24MISS') as Updated_Time_Stamp" +
      "          FROM co_retencion_compra" +
      "         WHERE co_retencion_compra.CO_Retencion_Compra_ID = ? ";

    ResultSet result;
    String strReturn = null;
    PreparedStatement st = null;

    int iParameter = 0;
    try {
    st = connectionProvider.getPreparedStatement(strSql);
      QueryTimeOutUtil.getInstance().setQueryTimeOut(st, SessionInfo.getQueryProfile());
      iParameter++; UtilSql.setValue(st, iParameter, 12, null, id);

      result = st.executeQuery();
      if(result.next()) {
        strReturn = UtilSql.getValue(result, "updated_time_stamp");
      }
      result.close();
    } catch(SQLException e){
      log4j.error("SQL error in query: " + strSql + "Exception:"+ e);
      throw new ServletException("@CODE=" + Integer.toString(e.getErrorCode()) + "@" + e.getMessage());
    } catch(Exception ex){
      log4j.error("Exception in query: " + strSql + "Exception:"+ ex);
      throw new ServletException("@CODE=@" + ex.getMessage());
    } finally {
      try {
        connectionProvider.releasePreparedStatement(st);
      } catch(Exception ignore){
        ignore.printStackTrace();
      }
    }
    return(strReturn);
  }
}
