
package org.openbravo.erpWindows.com.atrums.compras.montos.CierredeCaja;


import org.openbravo.erpCommon.reference.*;


import org.openbravo.erpCommon.ad_actionButton.*;


import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class HeaderE1D8C0D641244D6482203DBC2615BCDB extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "392870BA275C4A12A95CC481340E130A";
  private static final String tabId = "E1D8C0D641244D6482203DBC2615BCDB";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "9B40480411394D6597273AA26780A030";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     
      if (command.contains("E4206530552B417E8EAAC2344077FC8B")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("E4206530552B417E8EAAC2344077FC8B");
        SessionInfo.setModuleId("9B40480411394D6597273AA26780A030");
        if (securedProcess || explicitAccess.contains("E4206530552B417E8EAAC2344077FC8B")) {
          classInfo.type = "P";
          classInfo.id = "E4206530552B417E8EAAC2344077FC8B";
        }
      }
     
      if (command.contains("E3FE0A0999954578B760CBB73925ED56")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("E3FE0A0999954578B760CBB73925ED56");
        SessionInfo.setModuleId("9B40480411394D6597273AA26780A030");
        if (securedProcess || explicitAccess.contains("E3FE0A0999954578B760CBB73925ED56")) {
          classInfo.type = "P";
          classInfo.id = "E3FE0A0999954578B760CBB73925ED56";
        }
      }
     

     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strateccoCierrecajaId = request.getParameter("inpateccoCierrecajaId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strateccoCierrecajaId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {
          if (strAtecco_Cierrecaja_ID.equals("")) strAtecco_Cierrecaja_ID = firstElement(vars, tableSQL);
          if (strAtecco_Cierrecaja_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strAtecco_Cierrecaja_ID, tableSQL);

      else printPageDataSheet(response, vars, strAtecco_Cierrecaja_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strAtecco_Cierrecaja_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strAtecco_Cierrecaja_ID.equals("")) strAtecco_Cierrecaja_ID = vars.getRequiredGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID");
      else vars.setSessionValue(windowId + "|Atecco_Cierrecaja_ID", strAtecco_Cierrecaja_ID);
      
      vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view", "EDIT");

      printPageEdit(response, request, vars, false, strAtecco_Cierrecaja_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view");
      String strAtecco_Cierrecaja_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {
          strAtecco_Cierrecaja_ID = firstElement(vars, tableSQL);
          if (strAtecco_Cierrecaja_ID.equals("")) strView = "RELATION";
        }
      }
      if (strView.equals("EDIT")) {

        if (strAtecco_Cierrecaja_ID.equals("")) strAtecco_Cierrecaja_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strAtecco_Cierrecaja_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamFecha_Apertura", tabId + "|paramFecha_Apertura");
vars.getRequestGlobalVariable("inpParamFecha_Apertura_f", tabId + "|paramFecha_Apertura_f");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|Atecco_Cierrecaja_ID");
      String strAtecco_Cierrecaja_ID="";

      String strView = vars.getSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strAtecco_Cierrecaja_ID = firstElement(vars, tableSQL);
        if (strAtecco_Cierrecaja_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strAtecco_Cierrecaja_ID, tableSQL);

      else printPageDataSheet(response, vars, strAtecco_Cierrecaja_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID", "");
      vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view", "RELATION");
      printPageDataSheet(response, vars, strAtecco_Cierrecaja_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID", "");
      vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strAtecco_Cierrecaja_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strAtecco_Cierrecaja_ID = vars.getRequiredStringParameter("inpateccoCierrecajaId");
      
      String strNext = nextElement(vars, strAtecco_Cierrecaja_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strAtecco_Cierrecaja_ID = vars.getRequiredStringParameter("inpateccoCierrecajaId");
      
      String strPrevious = previousElement(vars, strAtecco_Cierrecaja_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|Atecco_Cierrecaja_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|Atecco_Cierrecaja_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strAtecco_Cierrecaja_ID = vars.getRequiredGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strAtecco_Cierrecaja_ID, tableSQL);
          vars.setSessionValue(windowId + "|Atecco_Cierrecaja_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strAtecco_Cierrecaja_ID = vars.getRequiredStringParameter("inpateccoCierrecajaId");
      //HeaderE1D8C0D641244D6482203DBC2615BCDBData data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = HeaderE1D8C0D641244D6482203DBC2615BCDBData.delete(this, strAtecco_Cierrecaja_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|ateccoCierrecajaId");
        vars.setSessionValue(tabId + "|HeaderE1D8C0D641244D6482203DBC2615BCDB.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

     } else if (vars.commandIn("BUTTONProcesarE4206530552B417E8EAAC2344077FC8B")) {
        vars.setSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strprocesar", vars.getStringParameter("inpprocesar"));
        vars.setSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonE4206530552B417E8EAAC2344077FC8B.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "E4206530552B417E8EAAC2344077FC8B", request.getServletPath());    
     } else if (vars.commandIn("BUTTONE4206530552B417E8EAAC2344077FC8B")) {
        String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID", "");
        String strprocesar = vars.getSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strprocesar");
        String strProcessing = vars.getSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strProcessing");
        String strOrg = vars.getSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strOrg");
        String strClient = vars.getSessionValue("buttonE4206530552B417E8EAAC2344077FC8B.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcesarE4206530552B417E8EAAC2344077FC8B(response, vars, strAtecco_Cierrecaja_ID, strprocesar, strProcessing);
        }

     } else if (vars.commandIn("BUTTONProcesar_Validar_IntE3FE0A0999954578B760CBB73925ED56")) {
        vars.setSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strprocesarValidarInt", vars.getStringParameter("inpprocesarValidarInt"));
        vars.setSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("buttonE3FE0A0999954578B760CBB73925ED56.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "E3FE0A0999954578B760CBB73925ED56", request.getServletPath());    
     } else if (vars.commandIn("BUTTONE3FE0A0999954578B760CBB73925ED56")) {
        String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID", "");
        String strprocesarValidarInt = vars.getSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strprocesarValidarInt");
        String strProcessing = vars.getSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strProcessing");
        String strOrg = vars.getSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strOrg");
        String strClient = vars.getSessionValue("buttonE3FE0A0999954578B760CBB73925ED56.strClient");
        
        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcesar_Validar_IntE3FE0A0999954578B760CBB73925ED56(response, vars, strAtecco_Cierrecaja_ID, strprocesarValidarInt, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONProcesarE4206530552B417E8EAAC2344077FC8B")) {
        String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpKey", windowId + "|Atecco_Cierrecaja_ID", "");
        String strprocesar = vars.getStringParameter("inpprocesar");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "E4206530552B417E8EAAC2344077FC8B", (("Atecco_Cierrecaja_ID".equalsIgnoreCase("AD_Language"))?"0":strAtecco_Cierrecaja_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONProcesar_Validar_IntE3FE0A0999954578B760CBB73925ED56")) {
        String strAtecco_Cierrecaja_ID = vars.getGlobalVariable("inpKey", windowId + "|Atecco_Cierrecaja_ID", "");
        String strprocesarValidarInt = vars.getStringParameter("inpprocesarValidarInt");
        String strProcessing = vars.getStringParameter("inpprocessing");
        OBError myMessage = null;
        try {
          String pinstance = SequenceIdData.getUUID();
          PInstanceProcessData.insertPInstance(this, pinstance, "E3FE0A0999954578B760CBB73925ED56", (("Atecco_Cierrecaja_ID".equalsIgnoreCase("AD_Language"))?"0":strAtecco_Cierrecaja_ID), strProcessing, vars.getUser(), vars.getClient(), vars.getOrg());
          
          
          ProcessBundle bundle = ProcessBundle.pinstance(pinstance, vars, this);
          new ProcessRunner(bundle).execute(this);
          
          PInstanceProcessData[] pinstanceData = PInstanceProcessData.select(this, pinstance);
          myMessage = Utility.getProcessInstanceMessage(this, vars, pinstanceData);
        } catch (ServletException ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);






    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private HeaderE1D8C0D641244D6482203DBC2615BCDBData getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    HeaderE1D8C0D641244D6482203DBC2615BCDBData data = new HeaderE1D8C0D641244D6482203DBC2615BCDBData();
    ServletException ex = null;
    try {
    data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");     data.docstatus = vars.getRequiredGlobalVariable("inpdocstatus", windowId + "|Docstatus");     data.fechaApertura = vars.getStringParameter("inpfechaApertura");     data.usuarioAperturaId = vars.getStringParameter("inpusuarioAperturaId");     data.usuarioAperturaIdr = vars.getStringParameter("inpusuarioAperturaId_R");     data.respoAperturaId = vars.getStringParameter("inprespoAperturaId");     data.respoAperturaIdr = vars.getStringParameter("inprespoAperturaId_R");     data.observacionApertura = vars.getStringParameter("inpobservacionApertura");     data.fechaCierre = vars.getStringParameter("inpfechaCierre");     data.usuarioCierreId = vars.getStringParameter("inpusuarioCierreId");     data.usuarioCierreIdr = vars.getStringParameter("inpusuarioCierreId_R");     data.respoCierreId = vars.getStringParameter("inprespoCierreId");     data.respoCierreIdr = vars.getStringParameter("inprespoCierreId_R");     data.observacionCierre = vars.getStringParameter("inpobservacionCierre");    try {   data.valorApertura = vars.getRequiredNumericParameter("inpvalorApertura");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.totalFacturas = vars.getRequiredNumericParameter("inptotalFacturas");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.valorDeposito = vars.getRequiredNumericParameter("inpvalorDeposito");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.valorCaja = vars.getRequiredNumericParameter("inpvalorCaja");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.valorCierre = vars.getRequiredNumericParameter("inpvalorCierre");  } catch (ServletException paramEx) { ex = paramEx; }     data.procesar = vars.getRequiredStringParameter("inpprocesar");     data.procesarValidar = vars.getRequiredStringParameter("inpprocesarValidar");     data.procesarValidarInt = vars.getRequiredStringParameter("inpprocesarValidarInt");     data.procesarRetiro = vars.getRequiredStringParameter("inpprocesarRetiro");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.cDoctypeId = vars.getStringParameter("inpcDoctypeId");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.ateccoCierrecajaId = vars.getRequestGlobalVariable("inpateccoCierrecajaId", windowId + "|Atecco_Cierrecaja_ID"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

    
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|DocStatus", data[0].getField("docstatus"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));    vars.setSessionValue(windowId + "|Atecco_Cierrecaja_ID", data[0].getField("ateccoCierrecajaId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      HeaderE1D8C0D641244D6482203DBC2615BCDBData[] data = HeaderE1D8C0D641244D6482203DBC2615BCDBData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpateccoCierrecajaId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strAtecco_Cierrecaja_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamFecha_Apertura = vars.getSessionValue(tabId + "|paramFecha_Apertura");
String strParamFecha_Apertura_f = vars.getSessionValue(tabId + "|paramFecha_Apertura_f");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamFecha_Apertura) && ("").equals(strParamFecha_Apertura_f)) || !(("").equals(strParamFecha_Apertura) || ("%").equals(strParamFecha_Apertura))  || !(("").equals(strParamFecha_Apertura_f) || ("%").equals(strParamFecha_Apertura_f)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strAtecco_Cierrecaja_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strAtecco_Cierrecaja_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/compras/montos/CierredeCaja/HeaderE1D8C0D641244D6482203DBC2615BCDB_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "HeaderE1D8C0D641244D6482203DBC2615BCDB", false, "document.frmMain.inpateccoCierrecajaId", "grid", "../com.atrums.contabilidad/cierrecaja/print.html", "N".equals("Y"), "CierredeCaja", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), false, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "ateccoCierrecajaId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "HeaderE1D8C0D641244D6482203DBC2615BCDB_Relation.html", "CierredeCaja", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "HeaderE1D8C0D641244D6482203DBC2615BCDB_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strAtecco_Cierrecaja_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    HeaderE1D8C0D641244D6482203DBC2615BCDBData[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamFecha_Apertura = vars.getSessionValue(tabId + "|paramFecha_Apertura");
String strParamFecha_Apertura_f = vars.getSessionValue(tabId + "|paramFecha_Apertura_f");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamFecha_Apertura) && ("").equals(strParamFecha_Apertura_f)) || !(("").equals(strParamFecha_Apertura) || ("%").equals(strParamFecha_Apertura))  || !(("").equals(strParamFecha_Apertura_f) || ("%").equals(strParamFecha_Apertura_f)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        data = HeaderE1D8C0D641244D6482203DBC2615BCDBData.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strAtecco_Cierrecaja_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strAtecco_Cierrecaja_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new HeaderE1D8C0D641244D6482203DBC2615BCDBData[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("ateccoCierrecajaId") == null || dataField.getField("ateccoCierrecajaId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = HeaderE1D8C0D641244D6482203DBC2615BCDBData.set(Utility.getDefault(this, vars, "Valor_Deposito", "0", "392870BA275C4A12A95CC481340E130A", "0", dataField), Utility.getDefault(this, vars, "Usuario_Cierre_ID", "", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "@#AD_ORG_ID@", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Respo_Apertura_ID", "", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Procesar", "AB", "392870BA275C4A12A95CC481340E130A", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "69732EF9EAF24E18B046FA2AC155E98D", Utility.getDefault(this, vars, "Procesar", "AB", "392870BA275C4A12A95CC481340E130A", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "69732EF9EAF24E18B046FA2AC155E98D", Utility.getDefault(this, vars, "Procesar", "AB", "392870BA275C4A12A95CC481340E130A", "N", dataField))), Utility.getDefault(this, vars, "Fecha_Apertura", "@#Date@", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Observacion_Apertura", "", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Respo_Cierre_ID", "", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Updatedby", "", "392870BA275C4A12A95CC481340E130A", "", dataField), HeaderE1D8C0D641244D6482203DBC2615BCDBData.selectDef43E94606E81946C793C9628174B1825F_0(this, Utility.getDefault(this, vars, "Updatedby", "", "392870BA275C4A12A95CC481340E130A", "", dataField)), Utility.getDefault(this, vars, "Fecha_Cierre", "", "392870BA275C4A12A95CC481340E130A", "", dataField), "", Utility.getDefault(this, vars, "Createdby", "", "392870BA275C4A12A95CC481340E130A", "", dataField), HeaderE1D8C0D641244D6482203DBC2615BCDBData.selectDef560B3C922DFB49DBAB57DCB54393DE23_1(this, Utility.getDefault(this, vars, "Createdby", "", "392870BA275C4A12A95CC481340E130A", "", dataField)), Utility.getDefault(this, vars, "Docstatus", "DR", "392870BA275C4A12A95CC481340E130A", "", dataField), "Y", Utility.getDefault(this, vars, "Valor_Cierre", "0", "392870BA275C4A12A95CC481340E130A", "0", dataField), Utility.getDefault(this, vars, "Total_Facturas", "0", "392870BA275C4A12A95CC481340E130A", "0", dataField), Utility.getDefault(this, vars, "Valor_Caja", "0", "392870BA275C4A12A95CC481340E130A", "0", dataField), Utility.getDefault(this, vars, "C_Doctype_ID", "", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Procesar_Retiro", "PR", "392870BA275C4A12A95CC481340E130A", "N", dataField), Utility.getDefault(this, vars, "Procesar_Validar", "VC", "392870BA275C4A12A95CC481340E130A", "N", dataField), Utility.getDefault(this, vars, "Valor_Apertura", "0", "392870BA275C4A12A95CC481340E130A", "0", dataField), Utility.getDefault(this, vars, "Observacion_Cierre", "", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Usuario_Apertura_ID", "@#AD_USER_ID@", "392870BA275C4A12A95CC481340E130A", "", dataField), Utility.getDefault(this, vars, "Procesar_Validar_Int", "VC", "392870BA275C4A12A95CC481340E130A", "N", dataField), (vars.getLanguage().equals("en_US")?ListData.selectName(this, "69732EF9EAF24E18B046FA2AC155E98D", Utility.getDefault(this, vars, "Procesar_Validar_Int", "VC", "392870BA275C4A12A95CC481340E130A", "N", dataField)):ListData.selectNameTrl(this, vars.getLanguage(), "69732EF9EAF24E18B046FA2AC155E98D", Utility.getDefault(this, vars, "Procesar_Validar_Int", "VC", "392870BA275C4A12A95CC481340E130A", "N", dataField))));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/compras/montos/CierredeCaja/HeaderE1D8C0D641244D6482203DBC2615BCDB_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/compras/montos/CierredeCaja/HeaderE1D8C0D641244D6482203DBC2615BCDB_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "HeaderE1D8C0D641244D6482203DBC2615BCDB", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpateccoCierrecajaId", "", "../com.atrums.contabilidad/cierrecaja/print.html", "N".equals("Y"), "CierredeCaja", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strAtecco_Cierrecaja_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "STD", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strAtecco_Cierrecaja_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "HeaderE1D8C0D641244D6482203DBC2615BCDB_Relation.html", "CierredeCaja", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "HeaderE1D8C0D641244D6482203DBC2615BCDB_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Fecha_Apertura_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Fecha_Apertura_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
comboTableData = new ComboTableData(vars, this, "18", "Usuario_Apertura_ID", "110", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("usuarioAperturaId"):dataField.getField("usuarioAperturaId")));
xmlDocument.setData("reportUsuario_Apertura_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Respo_Apertura_ID", "190", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("respoAperturaId"):dataField.getField("respoAperturaId")));
xmlDocument.setData("reportRespo_Apertura_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("Fecha_Cierre_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Fecha_Cierre_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
comboTableData = new ComboTableData(vars, this, "18", "Usuario_Cierre_ID", "110", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("usuarioCierreId"):dataField.getField("usuarioCierreId")));
xmlDocument.setData("reportUsuario_Cierre_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "18", "Respo_Cierre_ID", "190", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("respoCierreId"):dataField.getField("respoCierreId")));
xmlDocument.setData("reportRespo_Cierre_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonValor_Apertura", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonTotal_Facturas", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonValor_Deposito", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonValor_Caja", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonValor_Cierre", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("Procesar_BTNname", Utility.getButtonName(this, vars, "69732EF9EAF24E18B046FA2AC155E98D", (dataField==null?data[0].getField("procesar"):dataField.getField("procesar")), "Procesar_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcesar = org.openbravo.erpCommon.utility.Utility.isModalProcess("E4206530552B417E8EAAC2344077FC8B"); 
xmlDocument.setParameter("Procesar_Modal", modalProcesar?"true":"false");
xmlDocument.setParameter("Procesar_Validar_Int_BTNname", Utility.getButtonName(this, vars, "69732EF9EAF24E18B046FA2AC155E98D", (dataField==null?data[0].getField("procesarValidarInt"):dataField.getField("procesarValidarInt")), "Procesar_Validar_Int_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcesar_Validar_Int = org.openbravo.erpCommon.utility.Utility.isModalProcess("E3FE0A0999954578B760CBB73925ED56"); 
xmlDocument.setParameter("Procesar_Validar_Int_Modal", modalProcesar_Validar_Int?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }

    private void printPageButtonProcesarE4206530552B417E8EAAC2344077FC8B(HttpServletResponse response, VariablesSecureApp vars, String strAtecco_Cierrecaja_ID, String strprocesar, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process E4206530552B417E8EAAC2344077FC8B");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/ProcesarE4206530552B417E8EAAC2344077FC8B", discard).createXmlDocument();
      xmlDocument.setParameter("key", strAtecco_Cierrecaja_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "HeaderE1D8C0D641244D6482203DBC2615BCDB_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "E4206530552B417E8EAAC2344077FC8B");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("E4206530552B417E8EAAC2344077FC8B");
        vars.removeMessage("E4206530552B417E8EAAC2344077FC8B");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    private void printPageButtonProcesar_Validar_IntE3FE0A0999954578B760CBB73925ED56(HttpServletResponse response, VariablesSecureApp vars, String strAtecco_Cierrecaja_ID, String strprocesarValidarInt, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process E3FE0A0999954578B760CBB73925ED56");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Procesar_Validar_IntE3FE0A0999954578B760CBB73925ED56", discard).createXmlDocument();
      xmlDocument.setParameter("key", strAtecco_Cierrecaja_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "HeaderE1D8C0D641244D6482203DBC2615BCDB_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "E3FE0A0999954578B760CBB73925ED56");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("E3FE0A0999954578B760CBB73925ED56");
        vars.removeMessage("E3FE0A0999954578B760CBB73925ED56");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }




    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    HeaderE1D8C0D641244D6482203DBC2615BCDBData data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.ateccoCierrecajaId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (HeaderE1D8C0D641244D6482203DBC2615BCDBData.getCurrentDBTimestamp(this, data.ateccoCierrecajaId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.ateccoCierrecajaId = "";
                }
                else {                    
                    
                        //BUTTON TEXT FILLING
                    data.procesarBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "69732EF9EAF24E18B046FA2AC155E98D", data.getField("Procesar"));
                    
                        //BUTTON TEXT FILLING
                    data.procesarValidarIntBtn = ActionButtonDefaultData.getText(this, vars.getLanguage(), "69732EF9EAF24E18B046FA2AC155E98D", data.getField("Procesar_Validar_Int"));
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|Atecco_Cierrecaja_ID", data.ateccoCierrecajaId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet HeaderE1D8C0D641244D6482203DBC2615BCDB. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
