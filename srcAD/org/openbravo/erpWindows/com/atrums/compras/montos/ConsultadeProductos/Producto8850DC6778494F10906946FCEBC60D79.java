
package org.openbravo.erpWindows.com.atrums.compras.montos.ConsultadeProductos;




import org.codehaus.jettison.json.JSONObject;
import org.openbravo.erpCommon.utility.*;
import org.openbravo.data.FieldProvider;
import org.openbravo.utils.FormatUtilities;
import org.openbravo.utils.Replace;
import org.openbravo.base.secureApp.HttpSecureAppServlet;
import org.openbravo.base.secureApp.VariablesSecureApp;
import org.openbravo.base.exception.OBException;
import org.openbravo.scheduling.ProcessBundle;
import org.openbravo.scheduling.ProcessRunner;
import org.openbravo.erpCommon.businessUtility.WindowTabs;
import org.openbravo.xmlEngine.XmlDocument;
import java.util.Vector;
import java.util.StringTokenizer;
import org.openbravo.database.SessionInfo;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import java.sql.Connection;

// Generated old code, not worth to make i.e. java imports perfect
@SuppressWarnings("unused")
public class Producto8850DC6778494F10906946FCEBC60D79 extends HttpSecureAppServlet {
  private static final long serialVersionUID = 1L;
  
  private static final String windowId = "DA99B3DF90E74B468373BF1F09F4F855";
  private static final String tabId = "8850DC6778494F10906946FCEBC60D79";
  private static final String defaultTabView = "RELATION";
  private static final int accesslevel = 3;
  private static final String moduleId = "9B40480411394D6597273AA26780A030";
  
  @Override
  public void init(ServletConfig config) {
    setClassInfo("W", tabId, moduleId);
    super.init(config);
  }
  
  
  @Override
  public void service(HttpServletRequest request, HttpServletResponse response) throws IOException,
      ServletException {
    VariablesSecureApp vars = new VariablesSecureApp(request);
    String command = vars.getCommand();
    
    boolean securedProcess = false;
    if (command.contains("BUTTON")) {
     List<String> explicitAccess = Arrays.asList( "");
    
     SessionInfo.setUserId(vars.getSessionValue("#AD_User_ID"));
     SessionInfo.setSessionId(vars.getSessionValue("#AD_Session_ID"));
     SessionInfo.setQueryProfile("manualProcess");
     
      if (command.contains("3C386BC12832466790E50F2F8C5EBD85")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("3C386BC12832466790E50F2F8C5EBD85");
        SessionInfo.setModuleId("0");
      }
     
      if (command.contains("136")) {
        SessionInfo.setProcessType("P");
        SessionInfo.setProcessId("136");
        SessionInfo.setModuleId("0");
      }
     
      try {
        securedProcess = "Y".equals(org.openbravo.erpCommon.businessUtility.Preferences
            .getPreferenceValue("SecuredProcess", true, vars.getClient(), vars.getOrg(), vars
                .getUser(), vars.getRole(), windowId));
      } catch (PropertyException e) {
      }
     

     
      if (explicitAccess.contains("3C386BC12832466790E50F2F8C5EBD85") || (securedProcess && command.contains("3C386BC12832466790E50F2F8C5EBD85"))) {
        classInfo.type = "P";
        classInfo.id = "3C386BC12832466790E50F2F8C5EBD85";
      }
     
      if (explicitAccess.contains("136") || (securedProcess && command.contains("136"))) {
        classInfo.type = "P";
        classInfo.id = "136";
      }
     
    }
    if (!securedProcess) {
      setClassInfo("W", tabId, moduleId);
    }
    super.service(request, response);
  }
  

  public void doPost (HttpServletRequest request, HttpServletResponse response) throws IOException,ServletException {
    TableSQLData tableSQL = null;
    VariablesSecureApp vars = new VariablesSecureApp(request);
    Boolean saveRequest = (Boolean) request.getAttribute("autosave");
    
    if(saveRequest != null && saveRequest){
      String currentOrg = vars.getStringParameter("inpadOrgId");
      String currentClient = vars.getStringParameter("inpadClientId");
      boolean editableTab = (!org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)
                            && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars,"#User_Org", windowId, accesslevel), currentOrg)) 
                            && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),currentClient)));
    
        OBError myError = new OBError();
        String commandType = request.getParameter("inpCommandType");
        String strmProductId = request.getParameter("inpmProductId");
        
        if (editableTab) {
          int total = 0;
          
          if(commandType.equalsIgnoreCase("EDIT") && !strmProductId.equals(""))
              total = saveRecord(vars, myError, 'U');
          else
              total = saveRecord(vars, myError, 'I');
          
          if (!myError.isEmpty() && total == 0)     
            throw new OBException(myError.getMessage());
        }
        vars.setSessionValue(request.getParameter("mappingName") +"|hash", vars.getPostDataHash());
        vars.setSessionValue(tabId + "|Header.view", "EDIT");
        
        return;
    }
    
    try {
      tableSQL = new TableSQLData(vars, this, tabId, Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    } catch (Exception ex) {
      ex.printStackTrace();
    }

    String strOrderBy = vars.getSessionValue(tabId + "|orderby");
    if (!strOrderBy.equals("")) {
      vars.setSessionValue(tabId + "|newOrder", "1");
    }

    if (vars.commandIn("DEFAULT")) {

      String strM_Product_ID = vars.getGlobalVariable("inpmProductId", windowId + "|M_Product_ID", "");
      

      String strView = vars.getSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view");
      if (strView.equals("")) {
        strView = defaultTabView;

        if (strView.equals("EDIT")) {

        }
      }
      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strM_Product_ID, tableSQL);

      else printPageDataSheet(response, vars, strM_Product_ID, tableSQL);
    } else if (vars.commandIn("DIRECT")) {
      String strM_Product_ID = vars.getStringParameter("inpDirectKey");
      
        
      if (strM_Product_ID.equals("")) strM_Product_ID = vars.getRequiredGlobalVariable("inpmProductId", windowId + "|M_Product_ID");
      else vars.setSessionValue(windowId + "|M_Product_ID", strM_Product_ID);
      
      vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view", "EDIT");

      printPageEdit(response, request, vars, false, strM_Product_ID, tableSQL);

    } else if (vars.commandIn("TAB")) {


      String strView = vars.getSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view");
      String strM_Product_ID = "";
      if (strView.equals("")) {
        strView = defaultTabView;
        if (strView.equals("EDIT")) {

        }
      }
      if (strView.equals("EDIT")) {

        if (strM_Product_ID.equals("")) strM_Product_ID = firstElement(vars, tableSQL);
        printPageEdit(response, request, vars, false, strM_Product_ID, tableSQL);

      } else printPageDataSheet(response, vars, "", tableSQL);
    } else if (vars.commandIn("SEARCH")) {
vars.getRequestGlobalVariable("inpParamValue", tabId + "|paramValue");
vars.getRequestGlobalVariable("inpParamName", tabId + "|paramName");
vars.getRequestGlobalVariable("inpParamM_Product_Category_ID", tabId + "|paramM_Product_Category_ID");
vars.getRequestGlobalVariable("inpParamProductType", tabId + "|paramProductType");
vars.getRequestGlobalVariable("inpParamC_TaxCategory_ID", tabId + "|paramC_TaxCategory_ID");

        vars.getRequestGlobalVariable("inpParamUpdated", tabId + "|paramUpdated");
        vars.getRequestGlobalVariable("inpParamUpdatedBy", tabId + "|paramUpdatedBy");
        vars.getRequestGlobalVariable("inpParamCreated", tabId + "|paramCreated");
        vars.getRequestGlobalVariable("inpparamCreatedBy", tabId + "|paramCreatedBy");
      
      
      vars.removeSessionValue(windowId + "|M_Product_ID");
      String strM_Product_ID="";

      String strView = vars.getSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view");
      if (strView.equals("")) strView=defaultTabView;

      if (strView.equals("EDIT")) {
        strM_Product_ID = firstElement(vars, tableSQL);
        if (strM_Product_ID.equals("")) {
          // filter returns empty set
          strView = "RELATION";
          // switch to grid permanently until the user changes the view again
          vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view", strView);
        }
      }

      if (strView.equals("EDIT")) 

        printPageEdit(response, request, vars, false, strM_Product_ID, tableSQL);

      else printPageDataSheet(response, vars, strM_Product_ID, tableSQL);
    } else if (vars.commandIn("RELATION")) {
      

      String strM_Product_ID = vars.getGlobalVariable("inpmProductId", windowId + "|M_Product_ID", "");
      vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view", "RELATION");
      printPageDataSheet(response, vars, strM_Product_ID, tableSQL);
    } else if (vars.commandIn("NEW")) {


      printPageEdit(response, request, vars, true, "", tableSQL);

    } else if (vars.commandIn("EDIT")) {

      String strM_Product_ID = vars.getGlobalVariable("inpmProductId", windowId + "|M_Product_ID", "");
      vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view", "EDIT");

      setHistoryCommand(request, "EDIT");
      printPageEdit(response, request, vars, false, strM_Product_ID, tableSQL);

    } else if (vars.commandIn("NEXT")) {

      String strM_Product_ID = vars.getRequiredStringParameter("inpmProductId");
      
      String strNext = nextElement(vars, strM_Product_ID, tableSQL);

      printPageEdit(response, request, vars, false, strNext, tableSQL);
    } else if (vars.commandIn("PREVIOUS")) {

      String strM_Product_ID = vars.getRequiredStringParameter("inpmProductId");
      
      String strPrevious = previousElement(vars, strM_Product_ID, tableSQL);

      printPageEdit(response, request, vars, false, strPrevious, tableSQL);
    } else if (vars.commandIn("FIRST_RELATION")) {

      vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.initRecordNumber", "0");
      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("PREVIOUS_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      if (strInitRecord.equals("") || strInitRecord.equals("0")) {
        vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.initRecordNumber", "0");
      } else {
        int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
        initRecord -= intRecordRange;
        strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
        vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.initRecordNumber", strInitRecord);
      }
      vars.removeSessionValue(windowId + "|M_Product_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("NEXT_RELATION")) {

      String strInitRecord = vars.getSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.initRecordNumber");
      String strRecordRange = Utility.getContext(this, vars, "#RecordRange", windowId);
      int intRecordRange = strRecordRange.equals("")?0:Integer.parseInt(strRecordRange);
      int initRecord = (strInitRecord.equals("")?0:Integer.parseInt(strInitRecord));
      if (initRecord==0) initRecord=1;
      initRecord += intRecordRange;
      strInitRecord = ((initRecord<0)?"0":Integer.toString(initRecord));
      vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.initRecordNumber", strInitRecord);
      vars.removeSessionValue(windowId + "|M_Product_ID");

      response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
    } else if (vars.commandIn("FIRST")) {

      
      String strFirst = firstElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strFirst, tableSQL);
    } else if (vars.commandIn("LAST_RELATION")) {

      String strLast = lastElement(vars, tableSQL);
      printPageDataSheet(response, vars, strLast, tableSQL);
    } else if (vars.commandIn("LAST")) {

      
      String strLast = lastElement(vars, tableSQL);

      printPageEdit(response, request, vars, false, strLast, tableSQL);
    } else if (vars.commandIn("SAVE_NEW_RELATION", "SAVE_NEW_NEW", "SAVE_NEW_EDIT")) {

      OBError myError = new OBError();      
      int total = saveRecord(vars, myError, 'I');      
      if (!myError.isEmpty()) {        
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
      } 
      else {
		if (myError.isEmpty()) {
		  myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsInserted");
		  myError.setMessage(total + " " + myError.getMessage());
		  vars.setMessage(tabId, myError);
		}        
        if (vars.commandIn("SAVE_NEW_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_NEW_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("SAVE_EDIT_RELATION", "SAVE_EDIT_NEW", "SAVE_EDIT_EDIT", "SAVE_EDIT_NEXT")) {

      String strM_Product_ID = vars.getRequiredGlobalVariable("inpmProductId", windowId + "|M_Product_ID");
      OBError myError = new OBError();
      int total = saveRecord(vars, myError, 'U');      
      if (!myError.isEmpty()) {
        response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
      } 
      else {
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          vars.setMessage(tabId, myError);
        }
        if (vars.commandIn("SAVE_EDIT_NEW")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=NEW");
        else if (vars.commandIn("SAVE_EDIT_EDIT")) response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        else if (vars.commandIn("SAVE_EDIT_NEXT")) {
          String strNext = nextElement(vars, strM_Product_ID, tableSQL);
          vars.setSessionValue(windowId + "|M_Product_ID", strNext);
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=EDIT");
        } else response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
      }
    } else if (vars.commandIn("DELETE")) {

      String strM_Product_ID = vars.getRequiredStringParameter("inpmProductId");
      //Producto8850DC6778494F10906946FCEBC60D79Data data = getEditVariables(vars);
      int total = 0;
      OBError myError = null;
      if (org.openbravo.erpCommon.utility.WindowAccessData.hasNotDeleteAccess(this, vars.getRole(), tabId)) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        vars.setMessage(tabId, myError);
      } else {
        try {
          total = Producto8850DC6778494F10906946FCEBC60D79Data.delete(this, strM_Product_ID, Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#User_Org", windowId, accesslevel));
        } catch(ServletException ex) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          if (!myError.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myError);
        }
        if (myError==null && total==0) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
        }
        vars.removeSessionValue(windowId + "|mProductId");
        vars.setSessionValue(tabId + "|Producto8850DC6778494F10906946FCEBC60D79.view", "RELATION");
      }
      if (myError==null) {
        myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsDeleted");
        myError.setMessage(total + " " + myError.getMessage());
        vars.setMessage(tabId, myError);
      }
      response.sendRedirect(strDireccion + request.getServletPath());

    } else if (vars.commandIn("BUTTONCreateVariants3C386BC12832466790E50F2F8C5EBD85")) {
        vars.setSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strcreatevariants", vars.getStringParameter("inpcreatevariants"));
        vars.setSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button3C386BC12832466790E50F2F8C5EBD85.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "3C386BC12832466790E50F2F8C5EBD85", request.getServletPath());
      } else if (vars.commandIn("BUTTON3C386BC12832466790E50F2F8C5EBD85")) {
        String strM_Product_ID = vars.getGlobalVariable("inpmProductId", windowId + "|M_Product_ID", "");
        String strcreatevariants = vars.getSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strcreatevariants");
        String strProcessing = vars.getSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strProcessing");
        String strOrg = vars.getSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strOrg");
        String strClient = vars.getSessionValue("button3C386BC12832466790E50F2F8C5EBD85.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonCreateVariants3C386BC12832466790E50F2F8C5EBD85(response, vars, strM_Product_ID, strcreatevariants, strProcessing);
        }
    } else if (vars.commandIn("BUTTONProcessing136")) {
        vars.setSessionValue("button136.strprocessing", vars.getStringParameter("inpprocessing"));
        vars.setSessionValue("button136.strProcessing", vars.getStringParameter("inpprocessing", "Y"));
        vars.setSessionValue("button136.strOrg", vars.getStringParameter("inpadOrgId"));
        vars.setSessionValue("button136.strClient", vars.getStringParameter("inpadClientId"));
        
        
        HashMap<String, String> p = new HashMap<String, String>();
        
        
        //Save in session needed params for combos if needed
        vars.setSessionObject("button136.originalParams", FieldProviderFactory.getFieldProvider(p));
        printPageButtonFS(response, vars, "136", request.getServletPath());
      } else if (vars.commandIn("BUTTON136")) {
        String strM_Product_ID = vars.getGlobalVariable("inpmProductId", windowId + "|M_Product_ID", "");
        String strprocessing = vars.getSessionValue("button136.strprocessing");
        String strProcessing = vars.getSessionValue("button136.strProcessing");
        String strOrg = vars.getSessionValue("button136.strOrg");
        String strClient = vars.getSessionValue("button136.strClient");

        
        if ((org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) || !(Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),strClient)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),strOrg))){
          OBError myError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
          vars.setMessage(tabId, myError);
          printPageClosePopUp(response, vars);
        }else{       
          printPageButtonProcessing136(response, vars, strM_Product_ID, strprocessing, strProcessing);
        }


    } else if (vars.commandIn("SAVE_BUTTONCreateVariants3C386BC12832466790E50F2F8C5EBD85")) {
        String strM_Product_ID = vars.getGlobalVariable("inpKey", windowId + "|M_Product_ID", "");
        
        ProcessBundle pb = new ProcessBundle("3C386BC12832466790E50F2F8C5EBD85", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("M_Product_ID", strM_Product_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        
        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);
    } else if (vars.commandIn("SAVE_BUTTONProcessing136")) {
        String strM_Product_ID = vars.getGlobalVariable("inpKey", windowId + "|M_Product_ID", "");
        
        ProcessBundle pb = new ProcessBundle("136", vars).init(this);
        HashMap<String, Object> params= new HashMap<String, Object>();
       
        params.put("M_Product_ID", strM_Product_ID);
        params.put("adOrgId", vars.getStringParameter("inpadOrgId"));
        params.put("adClientId", vars.getStringParameter("inpadClientId"));
        params.put("tabId", tabId);
        
        
        
        pb.setParams(params);
        OBError myMessage = null;
        try {
          new ProcessRunner(pb).execute(this);
          myMessage = (OBError) pb.getResult();
          myMessage.setMessage(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getMessage()));
          myMessage.setTitle(Utility.parseTranslation(this, vars, vars.getLanguage(), myMessage.getTitle()));
        } catch (Exception ex) {
          myMessage = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
          log4j.error(ex);
          if (!myMessage.isConnectionAvailable()) {
            bdErrorConnection(response);
            return;
          } else vars.setMessage(tabId, myMessage);
        }
        //close popup
        if (myMessage!=null) {
          if (log4j.isDebugEnabled()) log4j.debug(myMessage.getMessage());
          vars.setMessage(tabId, myMessage);
        }
        printPageClosePopUp(response, vars);





    } else if (vars.commandIn("SAVE_XHR")) {
      
      OBError myError = new OBError();
      JSONObject result = new JSONObject();
      String commandType = vars.getStringParameter("inpCommandType");
      char saveType = "NEW".equals(commandType) ? 'I' : 'U';
      try {
        int total = saveRecord(vars, myError, saveType);
        if (myError.isEmpty()) {
          myError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=RowsUpdated");
          myError.setMessage(total + " " + myError.getMessage());
          myError.setType("Success");
        }
        result.put("oberror", myError.toMap());
        result.put("tabid", vars.getStringParameter("tabID"));
        result.put("redirect", strDireccion + request.getServletPath() + "?Command=" + commandType);
      } catch (Exception e) {
        log4j.error("Error saving record (XHR request): " + e.getMessage(), e);
        myError.setType("Error");
        myError.setMessage(e.getMessage());
      }

      response.setContentType("application/json");
      PrintWriter out = response.getWriter();
      out.print(result.toString());
      out.flush();
      out.close();
    } else if (vars.getCommand().toUpperCase().startsWith("BUTTON") || vars.getCommand().toUpperCase().startsWith("SAVE_BUTTON")) {
      pageErrorPopUp(response);
    } else pageError(response);
  }
  private Producto8850DC6778494F10906946FCEBC60D79Data getEditVariables(Connection con, VariablesSecureApp vars) throws IOException,ServletException {
    Producto8850DC6778494F10906946FCEBC60D79Data data = new Producto8850DC6778494F10906946FCEBC60D79Data();
    ServletException ex = null;
    try {
    data.adOrgId = vars.getRequiredGlobalVariable("inpadOrgId", windowId + "|AD_Org_ID");     data.adOrgIdr = vars.getStringParameter("inpadOrgId_R");     data.emAteccoFamiliaId = vars.getStringParameter("inpemAteccoFamiliaId");     data.emAteccoGenericoId = vars.getStringParameter("inpemAteccoGenericoId");     data.mBrandId = vars.getStringParameter("inpmBrandId");     data.mBrandIdr = vars.getStringParameter("inpmBrandId_R");     data.adImageId = vars.getStringParameter("inpadImageId");     data.emAteccoCategoriaId = vars.getStringParameter("inpemAteccoCategoriaId");     data.emAteccoSubCategoriaId = vars.getStringParameter("inpemAteccoSubCategoriaId");     data.isgeneric = vars.getRequiredInputGlobalVariable("inpisgeneric", windowId + "|IsGeneric", "N");     data.emAteccoModelo = vars.getStringParameter("inpemAteccoModelo");     data.value = vars.getRequestGlobalVariable("inpvalue", windowId + "|Value");     data.emAteccoCodigoEdimca = vars.getStringParameter("inpemAteccoCodigoEdimca");     data.emAteccoCodigoCotopaxi = vars.getStringParameter("inpemAteccoCodigoCotopaxi");     data.genericProductId = vars.getStringParameter("inpgenericProductId");     data.name = vars.getStringParameter("inpname");     data.emAteccoTipo = vars.getStringParameter("inpemAteccoTipo");     data.emAteccoTextura = vars.getStringParameter("inpemAteccoTextura");     data.emAteccoCaras = vars.getStringParameter("inpemAteccoCaras");     data.emAteccoColor1 = vars.getStringParameter("inpemAteccoColor1");     data.emAteccoColor2 = vars.getStringParameter("inpemAteccoColor2");    try {   data.weight = vars.getNumericParameter("inpweight");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.emAteccoD1 = vars.getNumericParameter("inpemAteccoD1");  } catch (ServletException paramEx) { ex = paramEx; }     data.cUomWeightId = vars.getStringParameter("inpcUomWeightId");    try {   data.emAteccoD2 = vars.getNumericParameter("inpemAteccoD2");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.emAteccoEspesor = vars.getNumericParameter("inpemAteccoEspesor");  } catch (ServletException paramEx) { ex = paramEx; }     data.mAttributesetId = vars.getRequestGlobalVariable("inpmAttributesetId", windowId + "|M_AttributeSet_ID");    try {   data.emAteccoM3 = vars.getNumericParameter("inpemAteccoM3");  } catch (ServletException paramEx) { ex = paramEx; }     data.description = vars.getStringParameter("inpdescription");     data.producttype = vars.getRequiredGlobalVariable("inpproducttype", windowId + "|ProductType");     data.producttyper = vars.getStringParameter("inpproducttype_R");     data.upc = vars.getStringParameter("inpupc");     data.cUomId = vars.getRequiredStringParameter("inpcUomId");     data.cUomIdr = vars.getStringParameter("inpcUomId_R");     data.mProductCategoryId = vars.getRequiredStringParameter("inpmProductCategoryId");     data.mProductCategoryIdr = vars.getStringParameter("inpmProductCategoryId_R");     data.cTaxcategoryId = vars.getRequiredStringParameter("inpcTaxcategoryId");     data.cTaxcategoryIdr = vars.getStringParameter("inpcTaxcategoryId_R");     data.costtype = vars.getRequestGlobalVariable("inpcosttype", windowId + "|Costtype");     data.costtyper = vars.getStringParameter("inpcosttype_R");     data.isstocked = vars.getStringParameter("inpisstocked", "N");     data.salesrepId = vars.getStringParameter("inpsalesrepId");     data.issold = vars.getStringParameter("inpissold", "N");     data.ispurchased = vars.getStringParameter("inpispurchased", "N");     data.isbom = vars.getRequiredInputGlobalVariable("inpisbom", windowId + "|IsBOM", "N");     data.isactive = vars.getStringParameter("inpisactive", "N");     data.cBpartnerId = vars.getStringParameter("inpcBpartnerId");     data.imageurl = vars.getStringParameter("inpimageurl");     data.descriptionurl = vars.getStringParameter("inpdescriptionurl");     data.issummary = vars.getStringParameter("inpissummary", "N");     data.mLocatorId = vars.getStringParameter("inpmLocatorId");    try {   data.volume = vars.getNumericParameter("inpvolume");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.shelfwidth = vars.getNumericParameter("inpshelfwidth");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.shelfheight = vars.getNumericParameter("inpshelfheight");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.shelfdepth = vars.getNumericParameter("inpshelfdepth");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.unitsperpallet = vars.getNumericParameter("inpunitsperpallet");  } catch (ServletException paramEx) { ex = paramEx; }     data.discontinued = vars.getStringParameter("inpdiscontinued", "N");     data.discontinuedby = vars.getStringParameter("inpdiscontinuedby");     data.isinvoiceprintdetails = vars.getStringParameter("inpisinvoiceprintdetails", "N");     data.ispicklistprintdetails = vars.getStringParameter("inpispicklistprintdetails", "N");     data.isverified = vars.getStringParameter("inpisverified", "N");     data.isquantityvariable = vars.getStringParameter("inpisquantityvariable", "N");     data.sExpensetypeId = vars.getStringParameter("inpsExpensetypeId");     data.sResourceId = vars.getStringParameter("inpsResourceId");     data.expplantype = vars.getStringParameter("inpexpplantype");    try {   data.periodnumberExp = vars.getNumericParameter("inpperiodnumberExp");  } catch (ServletException paramEx) { ex = paramEx; }     data.defaultperiodExp = vars.getStringParameter("inpdefaultperiodExp");     data.calculated = vars.getStringParameter("inpcalculated", "N");    try {   data.capacity = vars.getNumericParameter("inpcapacity");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.delaymin = vars.getNumericParameter("inpdelaymin");  } catch (ServletException paramEx) { ex = paramEx; }     data.mrpPlannerId = vars.getStringParameter("inpmrpPlannerId");     data.mrpPlanningmethodId = vars.getStringParameter("inpmrpPlanningmethodId");    try {   data.qtymax = vars.getNumericParameter("inpqtymax");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.qtymin = vars.getNumericParameter("inpqtymin");  } catch (ServletException paramEx) { ex = paramEx; }    try {   data.qtystd = vars.getNumericParameter("inpqtystd");  } catch (ServletException paramEx) { ex = paramEx; }     data.qtytype = vars.getStringParameter("inpqtytype", "N");    try {   data.stockmin = vars.getNumericParameter("inpstockmin");  } catch (ServletException paramEx) { ex = paramEx; }     data.createvariants = vars.getStringParameter("inpcreatevariants");     data.updateinvariants = vars.getStringParameter("inpupdateinvariants");     data.islinkedtoproduct = vars.getStringParameter("inpislinkedtoproduct", "N");     data.prodCatSelection = vars.getRequestGlobalVariable("inpprodCatSelection", windowId + "|Prod_Cat_Selection");     data.productSelection = vars.getRequestGlobalVariable("inpproductSelection", windowId + "|Product_Selection");     data.returnable = vars.getStringParameter("inpreturnable", "N");    try {   data.overdueReturnDays = vars.getNumericParameter("inpoverdueReturnDays");  } catch (ServletException paramEx) { ex = paramEx; }     data.ispricerulebased = vars.getRequiredInputGlobalVariable("inpispricerulebased", windowId + "|Ispricerulebased", "N");     data.quantityRule = vars.getStringParameter("inpquantityRule");     data.uniquePerDocument = vars.getStringParameter("inpuniquePerDocument", "N");     data.printDescription = vars.getStringParameter("inpprintDescription", "N");     data.relateprodcattoservice = vars.getStringParameter("inprelateprodcattoservice");     data.relateprodtoservice = vars.getStringParameter("inprelateprodtoservice");     data.allowDeferredSell = vars.getStringParameter("inpallowDeferredSell", "N");    try {   data.deferredSellMaxDays = vars.getNumericParameter("inpdeferredSellMaxDays");  } catch (ServletException paramEx) { ex = paramEx; }     data.mAttributesetinstanceId = vars.getRequestGlobalVariable("inpmAttributesetinstanceId", windowId + "|M_AttributeSetInstance_ID");    try {   data.guaranteedays = vars.getNumericParameter("inpguaranteedays");  } catch (ServletException paramEx) { ex = paramEx; }     data.versionno = vars.getStringParameter("inpversionno");     data.maProcessplanId = vars.getStringParameter("inpmaProcessplanId");     data.enforceAttribute = vars.getStringParameter("inpenforceAttribute", "N");    try {   data.stockMin = vars.getNumericParameter("inpstockMin");  } catch (ServletException paramEx) { ex = paramEx; }     data.name2 = vars.getStringParameter("inpname2");     data.processing = vars.getRequiredStringParameter("inpprocessing");     data.help = vars.getStringParameter("inphelp");     data.documentnote = vars.getStringParameter("inpdocumentnote");     data.sku = vars.getStringParameter("inpsku");     data.adClientId = vars.getRequiredGlobalVariable("inpadClientId", windowId + "|AD_Client_ID");     data.classification = vars.getStringParameter("inpclassification");     data.bookusingpoprice = vars.getStringParameter("inpbookusingpoprice", "N");     data.mProductId = vars.getRequestGlobalVariable("inpmProductId", windowId + "|M_Product_ID");     data.isdeferredrevenue = vars.getStringParameter("inpisdeferredrevenue", "N");     data.characteristicDesc = vars.getStringParameter("inpcharacteristicDesc");    try {   data.periodnumber = vars.getNumericParameter("inpperiodnumber");  } catch (ServletException paramEx) { ex = paramEx; }     data.defaultperiod = vars.getStringParameter("inpdefaultperiod");     data.revplantype = vars.getStringParameter("inprevplantype");     data.isdeferredexpense = vars.getStringParameter("inpisdeferredexpense", "N");    try {   data.coststd = vars.getNumericParameter("inpcoststd");  } catch (ServletException paramEx) { ex = paramEx; }     data.managevariants = vars.getStringParameter("inpmanagevariants");     data.production = vars.getStringParameter("inpproduction", "N");     data.attrsetvaluetype = vars.getStringParameter("inpattrsetvaluetype");     data.ispriceprinted = vars.getStringParameter("inpispriceprinted", "N");     data.mFreightcategoryId = vars.getStringParameter("inpmFreightcategoryId");     data.downloadurl = vars.getStringParameter("inpdownloadurl"); 
      data.createdby = vars.getUser();
      data.updatedby = vars.getUser();
      data.adUserClient = Utility.getContext(this, vars, "#User_Client", windowId, accesslevel);
      data.adOrgClient = Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel);
      data.updatedTimeStamp = vars.getStringParameter("updatedTimestamp");



    
    

          if (data.value.equals("")) data.value = Utility.getDocumentNoConnection(con, this, vars.getClient(), "M_Product", true);
vars.setSessionValue(windowId + "|Value", data.value);
    }
    catch(ServletException e) {
    	vars.setEditionData(tabId, data);
    	throw e;
    }
    // Behavior with exception for numeric fields is to catch last one if we have multiple ones
    if(ex != null) {
      vars.setEditionData(tabId, data);
      throw ex;
    }
    return data;
  }




    private void refreshSessionEdit(VariablesSecureApp vars, FieldProvider[] data) {
      if (data==null || data.length==0) return;
          vars.setSessionValue(windowId + "|AD_Org_ID", data[0].getField("adOrgId"));    vars.setSessionValue(windowId + "|IsGeneric", data[0].getField("isgeneric"));    vars.setSessionValue(windowId + "|Value", data[0].getField("value"));    vars.setSessionValue(windowId + "|M_AttributeSet_ID", data[0].getField("mAttributesetId"));    vars.setSessionValue(windowId + "|ProductType", data[0].getField("producttype"));    vars.setSessionValue(windowId + "|Costtype", data[0].getField("costtype"));    vars.setSessionValue(windowId + "|IsBOM", data[0].getField("isbom"));    vars.setSessionValue(windowId + "|Prod_Cat_Selection", data[0].getField("prodCatSelection"));    vars.setSessionValue(windowId + "|Product_Selection", data[0].getField("productSelection"));    vars.setSessionValue(windowId + "|Ispricerulebased", data[0].getField("ispricerulebased"));    vars.setSessionValue(windowId + "|M_Product_ID", data[0].getField("mProductId"));    vars.setSessionValue(windowId + "|M_AttributeSetInstance_ID", data[0].getField("mAttributesetinstanceId"));    vars.setSessionValue(windowId + "|AD_Client_ID", data[0].getField("adClientId"));
    }

    private void refreshSessionNew(VariablesSecureApp vars) throws IOException,ServletException {
      Producto8850DC6778494F10906946FCEBC60D79Data[] data = Producto8850DC6778494F10906946FCEBC60D79Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), vars.getStringParameter("inpmProductId", ""), Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
      if (data==null || data.length==0) return;
      refreshSessionEdit(vars, data);
    }

  private String nextElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(), 0, 0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.NEXT, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting next element", e);
      }
      if (data!=null) {
        if (data!=null) return data;
      }
    }
    return strSelected;
  }

  private int getKeyPosition(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("getKeyPosition: " + strSelected);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.GETPOSITION, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting key position", e);
      }
      if (data!=null) {
        // split offset -> (page,relativeOffset)
        int absoluteOffset = Integer.valueOf(data);
        int page = absoluteOffset / TableSQLData.maxRowsPerGridPage;
        int relativeOffset = absoluteOffset % TableSQLData.maxRowsPerGridPage;
        log4j.debug("getKeyPosition: absOffset: " + absoluteOffset + "=> page: " + page + " relOffset: " + relativeOffset);
        String currPageKey = tabId + "|" + "currentPage";
        vars.setSessionValue(currPageKey, String.valueOf(page));
        return relativeOffset;
      }
    }
    return 0;
  }

  private String previousElement(VariablesSecureApp vars, String strSelected, TableSQLData tableSQL) throws IOException, ServletException {
    if (strSelected == null || strSelected.equals("")) return firstElement(vars, tableSQL);
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.PREVIOUS, strSelected, tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting previous element", e);
      }
      if (data!=null) {
        return data;
      }
    }
    return strSelected;
  }

  private String firstElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,1);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.FIRST, "", tableSQL.getKeyColumn());

      } catch (Exception e) { 
        log4j.debug("Error getting first element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private String lastElement(VariablesSecureApp vars, TableSQLData tableSQL) throws IOException, ServletException {
    if (tableSQL!=null) {
      String data = null;
      try{
        String strSQL = ModelSQLGeneration.generateSQLonlyId(this, vars, tableSQL, (tableSQL.getTableName() + "." + tableSQL.getKeyColumn() + " AS ID"), new Vector<String>(), new Vector<String>(),0,0);
        ExecuteQuery execquery = new ExecuteQuery(this, strSQL, tableSQL.getParameterValuesOnlyId());
        data = execquery.selectAndSearch(ExecuteQuery.SearchType.LAST, "", tableSQL.getKeyColumn());
      } catch (Exception e) { 
        log4j.error("Error getting last element", e);
      }
      if (data!=null) return data;
    }
    return "";
  }

  private void printPageDataSheet(HttpServletResponse response, VariablesSecureApp vars, String strM_Product_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: dataSheet");

    String strParamValue = vars.getSessionValue(tabId + "|paramValue");
String strParamName = vars.getSessionValue(tabId + "|paramName");
String strParamM_Product_Category_ID = vars.getSessionValue(tabId + "|paramM_Product_Category_ID");
String strParamProductType = vars.getSessionValue(tabId + "|paramProductType");
String strParamC_TaxCategory_ID = vars.getSessionValue(tabId + "|paramC_TaxCategory_ID");

    boolean hasSearchCondition=false;
    vars.removeEditionData(tabId);
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamValue) && ("").equals(strParamName) && ("").equals(strParamM_Product_Category_ID) && ("").equals(strParamProductType) && ("").equals(strParamC_TaxCategory_ID)) || !(("").equals(strParamValue) || ("%").equals(strParamValue))  || !(("").equals(strParamName) || ("%").equals(strParamName))  || !(("").equals(strParamM_Product_Category_ID) || ("%").equals(strParamM_Product_Category_ID))  || !(("").equals(strParamProductType) || ("%").equals(strParamProductType))  || !(("").equals(strParamC_TaxCategory_ID) || ("%").equals(strParamC_TaxCategory_ID)) ;
    String strOffset = vars.getSessionValue(tabId + "|offset");
    String selectedRow = "0";
    if (!strM_Product_ID.equals("")) {
      selectedRow = Integer.toString(getKeyPosition(vars, strM_Product_ID, tableSQL));
    }

    String[] discard={"isNotFiltered","isNotTest"};
    if (hasSearchCondition) discard[0] = new String("isFiltered");
    if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/compras/montos/ConsultadeProductos/Producto8850DC6778494F10906946FCEBC60D79_Relation", discard).createXmlDocument();

    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    ToolBar toolbar = new ToolBar(this, true, vars.getLanguage(), "Producto8850DC6778494F10906946FCEBC60D79", false, "document.frmMain.inpmProductId", "grid", "..", "".equals("Y"), "ConsultadeProductos", strReplaceWith, false, false, false, false, !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    
    toolbar.setDeleteable(true && !hasReadOnlyAccess);
    toolbar.prepareRelationTemplate("N".equals("Y"), hasSearchCondition, !vars.getSessionValue("#ShowTest", "N").equals("Y"), true, Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());



    StringBuffer orderByArray = new StringBuffer();
      vars.setSessionValue(tabId + "|newOrder", "1");
      String positions = vars.getSessionValue(tabId + "|orderbyPositions");
      orderByArray.append("var orderByPositions = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(positions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
      String directions = vars.getSessionValue(tabId + "|orderbyDirections");
      orderByArray.append("var orderByDirections = new Array(\n");
      if (!positions.equals("")) {
        StringTokenizer tokens=new StringTokenizer(directions, ",");
        boolean firstOrder = true;
        while(tokens.hasMoreTokens()){
          if (!firstOrder) orderByArray.append(",\n");
          orderByArray.append("\"").append(tokens.nextToken()).append("\"");
          firstOrder = false;
        }
      }
      orderByArray.append(");\n");
//    }

    xmlDocument.setParameter("selectedColumn", "\nvar selectedRow = " + selectedRow + ";\n" + orderByArray.toString());
    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("KeyName", "mProductId");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));
    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, false);
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      xmlDocument.setParameter("childTabContainer", tabs.childTabs());
      String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "Producto8850DC6778494F10906946FCEBC60D79_Relation.html", "ConsultadeProductos", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"));
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "Producto8850DC6778494F10906946FCEBC60D79_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.relationTemplate());
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
    {
      OBError myMessage = vars.getMessage(tabId);
      vars.removeMessage(tabId);
      if (myMessage!=null) {
        xmlDocument.setParameter("messageType", myMessage.getType());
        xmlDocument.setParameter("messageTitle", myMessage.getTitle());
        xmlDocument.setParameter("messageMessage", myMessage.getMessage());
      }
    }


    xmlDocument.setParameter("grid", Utility.getContext(this, vars, "#RecordRange", windowId));
xmlDocument.setParameter("grid_Offset", strOffset);
xmlDocument.setParameter("grid_SortCols", positions);
xmlDocument.setParameter("grid_SortDirs", directions);
xmlDocument.setParameter("grid_Default", selectedRow);


    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageEdit(HttpServletResponse response, HttpServletRequest request, VariablesSecureApp vars,boolean _boolNew, String strM_Product_ID, TableSQLData tableSQL)
    throws IOException, ServletException {
    if (log4j.isDebugEnabled()) log4j.debug("Output: edit");
    
    // copy param to variable as will be modified later
    boolean boolNew = _boolNew;

    HashMap<String, String> usedButtonShortCuts;
  
    HashMap<String, String> reservedButtonShortCuts;
  
    usedButtonShortCuts = new HashMap<String, String>();
    
    reservedButtonShortCuts = new HashMap<String, String>();
    
    
    
    String strOrderByFilter = vars.getSessionValue(tabId + "|orderby");
    String orderClause = " 1";
    if (strOrderByFilter==null || strOrderByFilter.equals("")) strOrderByFilter = orderClause;
    /*{
      if (!strOrderByFilter.equals("") && !orderClause.equals("")) strOrderByFilter += ", ";
      strOrderByFilter += orderClause;
    }*/
    
    
    String strCommand = null;
    Producto8850DC6778494F10906946FCEBC60D79Data[] data=null;
    XmlDocument xmlDocument=null;
    FieldProvider dataField = vars.getEditionData(tabId);
    vars.removeEditionData(tabId);
    String strParamValue = vars.getSessionValue(tabId + "|paramValue");
String strParamName = vars.getSessionValue(tabId + "|paramName");
String strParamM_Product_Category_ID = vars.getSessionValue(tabId + "|paramM_Product_Category_ID");
String strParamProductType = vars.getSessionValue(tabId + "|paramProductType");
String strParamC_TaxCategory_ID = vars.getSessionValue(tabId + "|paramC_TaxCategory_ID");

    boolean hasSearchCondition=false;
    hasSearchCondition = (tableSQL.hasInternalFilter() && ("").equals(strParamValue) && ("").equals(strParamName) && ("").equals(strParamM_Product_Category_ID) && ("").equals(strParamProductType) && ("").equals(strParamC_TaxCategory_ID)) || !(("").equals(strParamValue) || ("%").equals(strParamValue))  || !(("").equals(strParamName) || ("%").equals(strParamName))  || !(("").equals(strParamM_Product_Category_ID) || ("%").equals(strParamM_Product_Category_ID))  || !(("").equals(strParamProductType) || ("%").equals(strParamProductType))  || !(("").equals(strParamC_TaxCategory_ID) || ("%").equals(strParamC_TaxCategory_ID)) ;

       String strParamSessionDate = vars.getGlobalVariable("inpParamSessionDate", Utility.getTransactionalDate(this, vars, windowId), "");
      String buscador = "";
      String[] discard = {"", "isNotTest"};
      
      if (vars.getSessionValue("#ShowTest", "N").equals("Y")) discard[1] = new String("isTest");
    if (dataField==null) {
      if (!boolNew) {
        discard[0] = new String("newDiscard");
        
        if (("").equals(strParamValue) && ("").equals(strParamName) && ("").equals(strParamM_Product_Category_ID) && ("").equals(strParamProductType) && ("").equals(strParamC_TaxCategory_ID) && strM_Product_ID.equals("")) {
          buscador = "openSearchWindow('../businessUtility/Buscador.html', 'BUSCADOR', " + tabId + ", 'ConsultadeProductos/Producto8850DC6778494F10906946FCEBC60D79_Relation.html', " + windowId + ");";
        } else if (strM_Product_ID.equals("")) {
          Producto8850DC6778494F10906946FCEBC60D79Data[] data1 = Producto8850DC6778494F10906946FCEBC60D79Data.select(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strParamValue, strParamName, strParamM_Product_Category_ID, strParamProductType, strParamC_TaxCategory_ID, strParamSessionDate, vars.getUser(), Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel), strOrderByFilter);
          data = new Producto8850DC6778494F10906946FCEBC60D79Data[1];
          if (data1!=null && data1.length!=0) data[0] = data1[0];
        } else data = Producto8850DC6778494F10906946FCEBC60D79Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strM_Product_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
  
        if (!strM_Product_ID.equals("") && (data == null || data.length==0)) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        }
        refreshSessionEdit(vars, data);
        strCommand = "EDIT";
      }

      if (data==null || data.length==0) {
        strM_Product_ID = firstElement(vars, tableSQL);
        if (strM_Product_ID.equals("")) {
          response.sendRedirect(strDireccion + request.getServletPath() + "?Command=RELATION");
          return;
        } else {
          data = Producto8850DC6778494F10906946FCEBC60D79Data.selectEdit(this, vars.getSessionValue("#AD_SqlDateTimeFormat"), vars.getLanguage(), strM_Product_ID, Utility.getContext(this, vars, "#User_Client", windowId), Utility.getContext(this, vars, "#AccessibleOrgTree", windowId, accesslevel));
        }
      }

      if (boolNew || data==null || data.length==0) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        data = new Producto8850DC6778494F10906946FCEBC60D79Data[0];
      } else {
        discard[0] = new String ("newDiscard");
      }
    } else {
      if (dataField.getField("mProductId") == null || dataField.getField("mProductId").equals("")) {
        discard[0] = new String ("editDiscard");
        strCommand = "NEW";
        boolNew = true;
      } else {
        discard[0] = new String ("newDiscard");
        strCommand = "EDIT";
      }
    }
    
    
    
    if (dataField==null) {
      if (boolNew || data==null || data.length==0) {
        refreshSessionNew(vars);
        data = Producto8850DC6778494F10906946FCEBC60D79Data.set("", Utility.getDefault(this, vars, "AD_Client_ID", "@AD_CLIENT_ID@", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "AD_Org_ID", "0", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), "Y", Utility.getDefault(this, vars, "CreatedBy", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Producto8850DC6778494F10906946FCEBC60D79Data.selectDef1407_0(this, Utility.getDefault(this, vars, "CreatedBy", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField)), Utility.getDefault(this, vars, "UpdatedBy", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Producto8850DC6778494F10906946FCEBC60D79Data.selectDef1409_1(this, Utility.getDefault(this, vars, "UpdatedBy", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField)), Utility.getDefault(this, vars, "Name", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Description", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "IsSummary", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "C_UOM_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "IsStocked", "Y", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "IsPurchased", "Y", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "IsSold", "Y", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Volume", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Weight", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Revplantype", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Quantity_Rule", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Modelo", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Value", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Producto8850DC6778494F10906946FCEBC60D79Data.selectDef2012(this, Utility.getContext(this, vars, "AD_ORG_ID", "DA99B3DF90E74B468373BF1F09F4F855"), Utility.getContext(this, vars, "#AD_CLIENT_ID", windowId)), Utility.getDefault(this, vars, "C_TaxCategory_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "UPC", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "SKU", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "ShelfWidth", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "ShelfHeight", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "ShelfDepth", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "UnitsPerPallet", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Discontinued", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "DiscontinuedBy", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "DefaultPeriod", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Allow_Deferred_Sell", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Relateprodtoservice", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "DocumentNote", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Help", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Classification", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "SalesRep_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Bookusingpoprice", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Generico_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "IsBOM", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "IsInvoicePrintDetails", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "IsPickListPrintDetails", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "IsVerified", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Processing", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Islinkedtoproduct", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Textura", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Capacity", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Delaymin", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "MRP_Planner_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "MRP_Planningmethod_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Qtymax", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Qtymin", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Qtystd", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Qtytype", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Stockmin", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Product_Selection", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Unique_Per_Document", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Overdue_Return_Days", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Sub_Categoria_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Relateprodcattoservice", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "S_ExpenseType_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "S_Resource_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Color1", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Caras", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_D1", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Codigo_Edimca", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Ispricerulebased", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "ProductType", "I", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "ImageURL", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "DescriptionURL", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "VersionNo", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "GuaranteeDays", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Deferred_Sell_Max_Days", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Attrsetvaluetype", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "AD_Image_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "C_BPartner_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Ispriceprinted", "Y", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Name2", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Costtype", "AV", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Coststd", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Stock_Min", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Enforce_Attribute", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Calculated", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "MA_Processplan_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Production", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Isdeferredrevenue", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_D2", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "M_AttributeSet_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "M_AttributeSetInstance_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Codigo_Cotopaxi", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "DownloadURL", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "M_FreightCategory_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "M_Locator_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Tipo", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Isdeferredexpense", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Categoria_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Print_Description", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Prod_Cat_Selection", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "DefaultPeriod_Exp", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Expplantype", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Periodnumber_Exp", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_M3", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Updateinvariants", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Returnable", "Y", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Familia_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "EM_Atecco_Color2", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "M_Brand_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "IsGeneric", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Generic_Product_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "CreateVariants", "", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "Characteristic_Desc", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "ManageVariants", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "C_Uom_Weight_ID", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Isquantityvariable", "N", "DA99B3DF90E74B468373BF1F09F4F855", "N", dataField), Utility.getDefault(this, vars, "EM_Atecco_Espesor", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField), Utility.getDefault(this, vars, "Periodnumber", "", "DA99B3DF90E74B468373BF1F09F4F855", "", dataField));
        
      }
     }
      
    
    String currentOrg = (boolNew?"":(dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId")));
    if (!currentOrg.equals("") && !currentOrg.startsWith("'")) currentOrg = "'"+currentOrg+"'";
    String currentClient = (boolNew?"":(dataField!=null?dataField.getField("adClientId"):data[0].getField("adClientId")));
    if (!currentClient.equals("") && !currentClient.startsWith("'")) currentClient = "'"+currentClient+"'";
    
    boolean hasReadOnlyAccess = org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId);
    boolean editableTab = (!hasReadOnlyAccess && (currentOrg.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),currentOrg)) && (currentClient.equals("") || Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel), currentClient)));
    if (editableTab)
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/compras/montos/ConsultadeProductos/Producto8850DC6778494F10906946FCEBC60D79_Edition",discard).createXmlDocument();
    else
      xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpWindows/com/atrums/compras/montos/ConsultadeProductos/Producto8850DC6778494F10906946FCEBC60D79_NonEditable",discard).createXmlDocument();

    xmlDocument.setParameter("tabId", tabId);
    ToolBar toolbar = new ToolBar(this, editableTab, vars.getLanguage(), "Producto8850DC6778494F10906946FCEBC60D79", (strCommand.equals("NEW") || boolNew || (dataField==null && (data==null || data.length==0))), "document.frmMain.inpmProductId", "", "..", "".equals("Y"), "ConsultadeProductos", strReplaceWith, true, false, false, Utility.hasTabAttachments(this, vars, tabId, strM_Product_ID), !hasReadOnlyAccess);
    toolbar.setTabId(tabId);
    toolbar.setDeleteable(true);
    toolbar.prepareEditionTemplate("N".equals("Y"), hasSearchCondition, vars.getSessionValue("#ShowTest", "N").equals("Y"), "RO", Utility.getContext(this, vars, "ShowAudit", windowId).equals("Y"));
    xmlDocument.setParameter("toolbar", toolbar.toString());

    // set updated timestamp to manage locking mechanism
    if (!boolNew) {
      xmlDocument.setParameter("updatedTimestamp", (dataField != null ? dataField
          .getField("updatedTimeStamp") : data[0].getField("updatedTimeStamp")));
    }
    
    boolean concurrentSave = vars.getSessionValue(tabId + "|concurrentSave").equals("true");
    if (concurrentSave) {
      //after concurrent save error, force autosave
      xmlDocument.setParameter("autosave", "Y");
    } else {
      xmlDocument.setParameter("autosave", "N");
    }
    vars.removeSessionValue(tabId + "|concurrentSave");

    try {
      WindowTabs tabs = new WindowTabs(this, vars, tabId, windowId, true, (strCommand.equalsIgnoreCase("NEW")));
      xmlDocument.setParameter("parentTabContainer", tabs.parentTabs());
      xmlDocument.setParameter("mainTabContainer", tabs.mainTabs());
      // if (!strM_Product_ID.equals("")) xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  // else xmlDocument.setParameter("childTabContainer", tabs.childTabs(true));
	  xmlDocument.setParameter("childTabContainer", tabs.childTabs(false));
	  String hideBackButton = vars.getGlobalVariable("hideMenu", "#Hide_BackButton", "");
      NavigationBar nav = new NavigationBar(this, vars.getLanguage(), "Producto8850DC6778494F10906946FCEBC60D79_Relation.html", "ConsultadeProductos", "W", strReplaceWith, tabs.breadcrumb(), hideBackButton.equals("true"), !concurrentSave);
      xmlDocument.setParameter("navigationBar", nav.toString());
      LeftTabsBar lBar = new LeftTabsBar(this, vars.getLanguage(), "Producto8850DC6778494F10906946FCEBC60D79_Relation.html", strReplaceWith);
      xmlDocument.setParameter("leftTabs", lBar.editionTemplate(strCommand.equals("NEW")));
    } catch (Exception ex) {
      throw new ServletException(ex);
    }
		
    
    
    xmlDocument.setParameter("commandType", strCommand);
    xmlDocument.setParameter("buscador",buscador);
    xmlDocument.setParameter("windowId", windowId);
    xmlDocument.setParameter("changed", "");
    xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
    xmlDocument.setParameter("theme", vars.getTheme());
    final String strMappingName = Utility.getTabURL(tabId, "E", false);
    xmlDocument.setParameter("mappingName", strMappingName);
    xmlDocument.setParameter("confirmOnChanges", Utility.getJSConfirmOnChanges(vars, windowId));
    //xmlDocument.setParameter("buttonReference", Utility.messageBD(this, "Reference", vars.getLanguage()));

    xmlDocument.setParameter("paramSessionDate", strParamSessionDate);

    xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
    OBError myMessage = vars.getMessage(tabId);
    vars.removeMessage(tabId);
    if (myMessage!=null) {
      xmlDocument.setParameter("messageType", myMessage.getType());
      xmlDocument.setParameter("messageTitle", myMessage.getTitle());
      xmlDocument.setParameter("messageMessage", myMessage.getMessage());
    }
    xmlDocument.setParameter("displayLogic", getDisplayLogicContext(vars, boolNew));
    
    
     if (dataField==null) {
      xmlDocument.setData("structure1",data);
      
    } else {
      
        FieldProvider[] dataAux = new FieldProvider[1];
        dataAux[0] = dataField;
        
        xmlDocument.setData("structure1",dataAux);
      
    }
    
      
   
    try {
      ComboTableData comboTableData = null;
String userOrgList = "";
if (editableTab) 
  userOrgList=Utility.getContext(this, vars, "#User_Org", windowId, accesslevel); //editable record 
else 
  userOrgList=currentOrg;
comboTableData = new ComboTableData(vars, this, "19", "AD_Org_ID", "", "", userOrgList, Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("adOrgId"):dataField.getField("adOrgId")));
xmlDocument.setData("reportAD_Org_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "M_Brand_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("mBrandId"):dataField.getField("mBrandId")));
xmlDocument.setData("reportM_Brand_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
String strCurrentImageURLAD_Image_ID = (dataField==null?data[0].getField("adImageId"):dataField.getField("adImageId"));
if (strCurrentImageURLAD_Image_ID==null || strCurrentImageURLAD_Image_ID.equals("")){
  xmlDocument.setParameter("AD_Image_IDClass", "Image_NotAvailable_medium");
}

xmlDocument.setParameter("buttonWeight", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonEM_Atecco_D1", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonEM_Atecco_D2", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonEM_Atecco_Espesor", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonEM_Atecco_M3", Utility.messageBD(this, "Calc", vars.getLanguage()));
comboTableData = new ComboTableData(vars, this, "17", "ProductType", "270", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("producttype"):dataField.getField("producttype")));
xmlDocument.setData("reportProductType","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_UOM_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cUomId"):dataField.getField("cUomId")));
xmlDocument.setData("reportC_UOM_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "M_Product_Category_ID", "", "772B9BE4957746EC809B8FE4D8E3F924", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("mProductCategoryId"):dataField.getField("mProductCategoryId")));
xmlDocument.setData("reportM_Product_Category_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "19", "C_TaxCategory_ID", "", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("cTaxcategoryId"):dataField.getField("cTaxcategoryId")));
xmlDocument.setData("reportC_TaxCategory_ID","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
comboTableData = new ComboTableData(vars, this, "17", "Costtype", "800025", "", Utility.getReferenceableOrg(vars, (dataField!=null?dataField.getField("adOrgId"):data[0].getField("adOrgId").equals("")?vars.getOrg():data[0].getField("adOrgId"))), Utility.getContext(this, vars, "#User_Client", windowId), 0);
Utility.fillSQLParameters(this, vars, (dataField==null?data[0]:dataField), comboTableData, windowId, (dataField==null?data[0].getField("costtype"):dataField.getField("costtype")));
xmlDocument.setData("reportCosttype","liststructure", comboTableData.select(!strCommand.equals("NEW")));
comboTableData = null;
xmlDocument.setParameter("buttonVolume", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonShelfWidth", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonShelfHeight", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonShelfDepth", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("DiscontinuedBy_Format", vars.getSessionValue("#AD_SqlDateFormat"));
xmlDocument.setParameter("buttonCapacity", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonDelaymin", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonQtymax", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonQtymin", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonQtystd", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("buttonStockmin", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("CreateVariants_BTNname", Utility.getButtonName(this, vars, "A80E61B804CD4FB5990520E98C1CCCE6", "CreateVariants_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalCreateVariants = org.openbravo.erpCommon.utility.Utility.isModalProcess("3C386BC12832466790E50F2F8C5EBD85"); 
xmlDocument.setParameter("CreateVariants_Modal", modalCreateVariants?"true":"false");
xmlDocument.setParameter("Updateinvariants_BTNname", Utility.getButtonName(this, vars, "184283A732854C49B1A76A3551FB1C75", "Updateinvariants_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalUpdateinvariants = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("Updateinvariants_Modal", modalUpdateinvariants?"true":"false");
xmlDocument.setParameter("Relateprodcattoservice_BTNname", Utility.getButtonName(this, vars, "99865005E72F47FEAB2C82C24802410D", "Relateprodcattoservice_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRelateprodcattoservice = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("Relateprodcattoservice_Modal", modalRelateprodcattoservice?"true":"false");
xmlDocument.setParameter("Relateprodtoservice_BTNname", Utility.getButtonName(this, vars, "D25621E18D5A484CB537F1FC37283682", "Relateprodtoservice_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalRelateprodtoservice = org.openbravo.erpCommon.utility.Utility.isModalProcess(""); 
xmlDocument.setParameter("Relateprodtoservice_Modal", modalRelateprodtoservice?"true":"false");
xmlDocument.setParameter("Created_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Created_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("Updated_Format", vars.getSessionValue("#AD_SqlDateTimeFormat"));xmlDocument.setParameter("Updated_Maxlength", Integer.toString(vars.getSessionValue("#AD_SqlDateTimeFormat").length()));
xmlDocument.setParameter("buttonStock_Min", Utility.messageBD(this, "Calc", vars.getLanguage()));
xmlDocument.setParameter("Processing_BTNname", Utility.getButtonName(this, vars, "06298C0ADA0F4CBC84DAF6BC2F2865A5", "Processing_linkBTN", usedButtonShortCuts, reservedButtonShortCuts));boolean modalProcessing = org.openbravo.erpCommon.utility.Utility.isModalProcess("136"); 
xmlDocument.setParameter("Processing_Modal", modalProcessing?"true":"false");
xmlDocument.setParameter("buttonCoststd", Utility.messageBD(this, "Calc", vars.getLanguage()));
    } catch (Exception ex) {
      ex.printStackTrace();
      throw new ServletException(ex);
    }

    xmlDocument.setParameter("scriptOnLoad", getShortcutScript(usedButtonShortCuts, reservedButtonShortCuts));
    
    final String refererURL = vars.getSessionValue(tabId + "|requestURL");
    vars.removeSessionValue(tabId + "|requestURL");
    if(!refererURL.equals("")) {
    	final Boolean failedAutosave = (Boolean) vars.getSessionObject(tabId + "|failedAutosave");
		vars.removeSessionValue(tabId + "|failedAutosave");
    	if(failedAutosave != null && failedAutosave) {
    		final String jsFunction = "continueUserAction('"+refererURL+"');";
    		xmlDocument.setParameter("failedAutosave", jsFunction);
    	}
    }

    if (strCommand.equalsIgnoreCase("NEW")) {
      vars.removeSessionValue(tabId + "|failedAutosave");
      vars.removeSessionValue(strMappingName + "|hash");
    }

    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    out.println(xmlDocument.print());
    out.close();
  }

  private void printPageButtonFS(HttpServletResponse response, VariablesSecureApp vars, String strProcessId, String path) throws IOException, ServletException {
    log4j.debug("Output: Frames action button");
    response.setContentType("text/html; charset=UTF-8");
    PrintWriter out = response.getWriter();
    XmlDocument xmlDocument = xmlEngine.readXmlTemplate(
        "org/openbravo/erpCommon/ad_actionButton/ActionButtonDefaultFrames").createXmlDocument();
    xmlDocument.setParameter("processId", strProcessId);
    xmlDocument.setParameter("trlFormType", "PROCESS");
    xmlDocument.setParameter("language", "defaultLang = \"" + vars.getLanguage() + "\";\n");
    xmlDocument.setParameter("type", strDireccion + path);
    out.println(xmlDocument.print());
    out.close();
  }



    void printPageButtonCreateVariants3C386BC12832466790E50F2F8C5EBD85(HttpServletResponse response, VariablesSecureApp vars, String strM_Product_ID, String strcreatevariants, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 3C386BC12832466790E50F2F8C5EBD85");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/CreateVariants3C386BC12832466790E50F2F8C5EBD85", discard).createXmlDocument();
      xmlDocument.setParameter("key", strM_Product_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Producto8850DC6778494F10906946FCEBC60D79_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "3C386BC12832466790E50F2F8C5EBD85");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("3C386BC12832466790E50F2F8C5EBD85");
        vars.removeMessage("3C386BC12832466790E50F2F8C5EBD85");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }
    void printPageButtonProcessing136(HttpServletResponse response, VariablesSecureApp vars, String strM_Product_ID, String strprocessing, String strProcessing)
    throws IOException, ServletException {
      log4j.debug("Output: Button process 136");
      String[] discard = {"newDiscard"};
      response.setContentType("text/html; charset=UTF-8");
      PrintWriter out = response.getWriter();
      XmlDocument xmlDocument = xmlEngine.readXmlTemplate("org/openbravo/erpCommon/ad_actionButton/Processing136", discard).createXmlDocument();
      xmlDocument.setParameter("key", strM_Product_ID);
      xmlDocument.setParameter("processing", strProcessing);
      xmlDocument.setParameter("form", "Producto8850DC6778494F10906946FCEBC60D79_Edition.html");
      xmlDocument.setParameter("window", windowId);
      xmlDocument.setParameter("css", vars.getTheme());
      xmlDocument.setParameter("language", "defaultLang=\"" + vars.getLanguage() + "\";");
      xmlDocument.setParameter("directory", "var baseDirectory = \"" + strReplaceWith + "/\";\n");
      xmlDocument.setParameter("processId", "136");
      xmlDocument.setParameter("cancel", Utility.messageBD(this, "Cancel", vars.getLanguage()));
      xmlDocument.setParameter("ok", Utility.messageBD(this, "OK", vars.getLanguage()));
      
      {
        OBError myMessage = vars.getMessage("136");
        vars.removeMessage("136");
        if (myMessage!=null) {
          xmlDocument.setParameter("messageType", myMessage.getType());
          xmlDocument.setParameter("messageTitle", myMessage.getTitle());
          xmlDocument.setParameter("messageMessage", myMessage.getMessage());
        }
      }

          try {
    } catch (Exception ex) {
      throw new ServletException(ex);
    }

      
      out.println(xmlDocument.print());
      out.close();
    }


    private String getDisplayLogicContext(VariablesSecureApp vars, boolean isNew) throws IOException, ServletException {
      log4j.debug("Output: Display logic context fields");
      String result = "var strHideCreateVariantsButton=\"" +Utility.getContext(this, vars, "HideCreateVariantsButton", windowId) + "\";\nvar strHasInvariants=\"" +Utility.getContext(this, vars, "HasInvariants", windowId) + "\";\nvar strShowAudit=\"" +(isNew?"N":Utility.getContext(this, vars, "ShowAudit", windowId)) + "\";\n";
      return result;
    }


    private String getReadOnlyLogicContext(VariablesSecureApp vars) throws IOException, ServletException {
      log4j.debug("Output: Read Only logic context fields");
      String result = "";
      return result;
    }




 
  private String getShortcutScript( HashMap<String, String> usedButtonShortCuts, HashMap<String, String> reservedButtonShortCuts){
    StringBuffer shortcuts = new StringBuffer();
    shortcuts.append(" function buttonListShorcuts() {\n");
    Iterator<String> ik = usedButtonShortCuts.keySet().iterator();
    Iterator<String> iv = usedButtonShortCuts.values().iterator();
    while(ik.hasNext() && iv.hasNext()){
      shortcuts.append("  keyArray[keyArray.length] = new keyArrayItem(\"").append(ik.next()).append("\", \"").append(iv.next()).append("\", null, \"altKey\", false, \"onkeydown\");\n");
    }
    shortcuts.append(" return true;\n}");
    return shortcuts.toString();
  }
  
  private int saveRecord(VariablesSecureApp vars, OBError myError, char type) throws IOException, ServletException {
    Producto8850DC6778494F10906946FCEBC60D79Data data = null;
    int total = 0;
    if (org.openbravo.erpCommon.utility.WindowAccessData.hasReadOnlyAccess(this, vars.getRole(), tabId)) {
        OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
        myError.setError(newError);
        vars.setMessage(tabId, myError);
    }
    else {
        Connection con = null;
        try {
            con = this.getTransactionConnection();
            data = getEditVariables(con, vars);
            data.dateTimeFormat = vars.getSessionValue("#AD_SqlDateTimeFormat");            
            String strSequence = "";
            if(type == 'I') {                
        strSequence = SequenceIdData.getUUID();
                if(log4j.isDebugEnabled()) log4j.debug("Sequence: " + strSequence);
                data.mProductId = strSequence;  
            }
            if (Utility.isElementInList(Utility.getContext(this, vars, "#User_Client", windowId, accesslevel),data.adClientId)  && Utility.isElementInList(Utility.getContext(this, vars, "#User_Org", windowId, accesslevel),data.adOrgId)){
		     if(type == 'I') {
		       total = data.insert(con, this);
		     } else {
		       //Check the version of the record we are saving is the one in DB
		       if (Producto8850DC6778494F10906946FCEBC60D79Data.getCurrentDBTimestamp(this, data.mProductId).equals(
                vars.getStringParameter("updatedTimestamp"))) {
                total = data.update(con, this);
               } else {
                 myError.setMessage(Replace.replace(Replace.replace(Utility.messageBD(this,
                    "SavingModifiedRecord", vars.getLanguage()), "\\n", "<br/>"), "&quot;", "\""));
                 myError.setType("Error");
                 vars.setSessionValue(tabId + "|concurrentSave", "true");
               } 
		     }		            
          
            }
                else {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), Utility.messageBD(this, "NoWriteAccess", vars.getLanguage()));
            myError.setError(newError);            
          }
          releaseCommitConnection(con);
        } catch(Exception ex) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), ex.getMessage());
            myError.setError(newError);   
            try {
              releaseRollbackConnection(con);
            } catch (final Exception e) { //do nothing 
            }           
        }
            
        if (myError.isEmpty() && total == 0) {
            OBError newError = Utility.translateError(this, vars, vars.getLanguage(), "@CODE=DBExecuteError");
            myError.setError(newError);
        }
        vars.setMessage(tabId, myError);
            
        if(!myError.isEmpty()){
            if(data != null ) {
                if(type == 'I') {            			
                    data.mProductId = "";
                }
                else {                    
                    
                }
                vars.setEditionData(tabId, data);
            }            	
        }
        else {
            vars.setSessionValue(windowId + "|M_Product_ID", data.mProductId);
        }
    }
    return total;
  }

  public String getServletInfo() {
    return "Servlet Producto8850DC6778494F10906946FCEBC60D79. This Servlet was made by Wad constructor";
  } // End of getServletInfo() method
}
